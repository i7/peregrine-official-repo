#!/usr/bin/env bash
cd ./peregrine-backend/
nix-shell shell.nix --run $(stack --nix --nix-shell-file shell.nix path --local-install-root)/bin/peregrine-backend-exe
