#!/usr/bin/env bash

set -e

echo "Compiling peregrine-backend..."
cd ./peregrine-backend/
stack build --nix-shell-file shell.nix
cd ../peregrine-frontend
echo "Compiling peregrine-frontend..."
npm install
npm run build
cd ../efficientrandomsamples
echo "Compiling java simulation backend..."
ant -buildfile Makejar.xml
cd ../peregrine2.0/jhoafparser
echo "Compiling HOA2Peregrine parser..."
ant
cd ..
cd ..
echo "Done"
