{fetchurl, stdenv}:

stdenv.mkDerivation rec {
  pname = "lola";
  version = "2.0";
  src = fetchurl {
    url = "https://theo.informatik.uni-rostock.de/storages/uni-rostock/Alle_IEF/Inf_THEO/images/tools_daten/lola-${version}.tar.gz";
    sha256 = "99afa83b8e121646f8246721477714cd3067bfdbd63da1af2a474c2682f9eb71";
  };
}

