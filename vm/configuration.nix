{ lib,
  pkgs,
  ...
}:

let
  waitForBackend = pkgs.writeShellScript "script" ''
    STATUS=0
    while $STATUS -eq 0; do
      sleep 1 ;
      STATUS=$(pgrep peregrine-back | wc -l)
    done
    sleep 10
  '';

  peregrineAutoStart = ''
    [Desktop Entry]
    Encoding=UTF-8
    Version=0.9.4
    Type=Application
    Name=Peregrine
    Comment=Peregrine
    Exec=xfce4-terminal -e \"bash -c 'cd ${home}/peregrine; ./peregrine.sh'\"
    StartupNotify=false
    Terminal=false
    Hidden=false
  '';

  firefoxAutoStart = ''
    [Desktop Entry]
    Encoding=UTF-8
    Version=0.9.4
    Type=Application
    Name=Firefox
    Comment=Firefox
    Exec=sh -c \"${waitForBackend};firefox localhost:3001\"
    StartupNotify=false
    Terminal=false
    Hidden=false
  '';

  home = "/home/peregrine";

  peregrine = "${home}/peregrine/peregrine.sh";

  setupScript = pkgs.writeShellScript "setupscript" ''
    echo "Sleeping for 5 secs to wait for network"
    sleep 5
    mkdir -p ${home}/.config/autostart/
    echo "${firefoxAutoStart}" > ${home}/.config/autostart/firefox.desktop
    echo "${peregrineAutoStart}" > ${home}/.config/autostart/peregrine.desktop
    if [ ! -f ${peregrine} ]; then
      git clone https://gitlab.lrz.de/ga97cer/peregrine.git
      cd ./peregrine
      git checkout nix-vm
      xterm -e ./compile.sh
      nix-channel --update
    fi
  '';
in
with lib;
  {
    nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
        inherit pkgs;
    };
    };
    imports = [
      <nixpkgs/nixos/modules/virtualisation/virtualbox-image.nix>
    ];
    users.users.peregrine = {
      isNormalUser = true;
      home = "/home/peregrine";
      description = "Peregrine User";
      extraGroups = [ "wheel" "networkmanager" ];
      initialHashedPassword = "";
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCok8uG3Hqkcinl+pH2w6P1u3aP6FR8PKN4e7xzVgY+tIg9woBoN3UTitenFTzAChczc4Ot1UdoWA5ju8IRjmqomeA70jRbSybjYtD4HDlp/0Z8DASAmdgV4GqKXbRi9qBMbBx+O4Qlr4vAd5xZYciteEUCAbI0ssGWaTWTJqPes0cg9mUb7IEgsz57kr//AzkqY5Ls+bbEy8DJa3PMeK+pJ1QKf/vtWolDm4B8SA4ytsz4X3pmazAwrMtuGEq9Ip899+CtOISHuIAV7gzFkpSP+iRoialP5ZUbAkEzwVX3X4f0fh9f3aNDcJGivMTDhOswlcDzefUfotr+K8CTuRgx"
      ];
    };
    virtualbox.memorySize = 4096; #MB
    virtualbox.baseImageSize = 35000; #MB #diskSize
    #virtualbox.writableStoreUseTmpfs = false;
    virtualbox.vmName = "Peregrine";
    services.openssh.enable = true;
    services.openssh.permitRootLogin = "yes";
    networking.firewall.enable = false;
    services.xserver.videoDrivers = mkOverride 40 [ "virtualbox" "vmware" "cirrus" "vesa" "modesetting" ];
    services.xserver = {
      enable = true;
      displayManager = {
        defaultSession = "xfce";
        lightdm.enable = true;
        lightdm.greeter.enable = false;
        lightdm.autoLogin = {
          enable = true;
          user = "peregrine";
        };
      };
      desktopManager = {
        xterm.enable = false;
        xfce.enable = true;
      };
    };
    users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCok8uG3Hqkcinl+pH2w6P1u3aP6FR8PKN4e7xzVgY+tIg9woBoN3UTitenFTzAChczc4Ot1UdoWA5ju8IRjmqomeA70jRbSybjYtD4HDlp/0Z8DASAmdgV4GqKXbRi9qBMbBx+O4Qlr4vAd5xZYciteEUCAbI0ssGWaTWTJqPes0cg9mUb7IEgsz57kr//AzkqY5Ls+bbEy8DJa3PMeK+pJ1QKf/vtWolDm4B8SA4ytsz4X3pmazAwrMtuGEq9Ip899+CtOISHuIAV7gzFkpSP+iRoialP5ZUbAkEzwVX3X4f0fh9f3aNDcJGivMTDhOswlcDzefUfotr+K8CTuRgx"
    ];

    services.xserver.displayManager.sessionCommands = ''
      ${setupScript} 2>&1 | tee "setupscript.log"
    '';

    # Initial empty root password for easy login:
    users.users.root.initialHashedPassword = "";
    environment.systemPackages = with pkgs; [
      firefox
      git
      nodejs-12_x
      python37
      ant
      openjdk
      stack
      haskell.compiler.ghc822Binary
    ];
  }

#{
#  imports =
#    [ ../virtualisation/virtualbox-image.nix
#      ../installer/cd-dvd/channel.nix
#      ../profiles/demo.nix
#      ../profiles/clone-config.nix
#    ];
#
#  # FIXME: UUID detection is currently broken
#  boot.loader.grub.fsIdentifier = "provided";
#
#  # Allow mounting of shared folders.
#  users.users.demo.extraGroups = [ "vboxsf" ];
#
#  # Add some more video drivers to give X11 a shot at working in
#  # VMware and QEMU.
#  services.xserver.videoDrivers = mkOverride 40 [ "virtualbox" "vmware" "cirrus" "vesa" "modesetting" ];
#
#  powerManagement.enable = false;
#  system.stateVersion = mkDefault "20.03";
#
#  installer.cloneConfigExtra = ''
#  # Let demo build as a trusted user.
#  # nix.trustedUsers = [ "demo" ];
#
#  # Mount a VirtualBox shared folder.
#  # This is configurable in the VirtualBox menu at
#  # Machine / Settings / Shared Folders.
#  # fileSystems."/mnt" = {
#  #   fsType = "vboxsf";
#  #   device = "nameofdevicetomount";
#  #   options = [ "rw" ];
#  # };
#
#  # By default, the NixOS VirtualBox demo image includes SDDM and Plasma.
#  # If you prefer another desktop manager or display manager, you may want
#  # to disable the default.
#  # services.xserver.desktopManager.plasma5.enable = lib.mkForce false;
#  # services.xserver.displayManager.sddm.enable = lib.mkForce false;
#
#  # Enable GDM/GNOME by uncommenting above two lines and two lines below.
#  # services.xserver.displayManager.gdm.enable = true;
#  # services.xserver.desktopManager.gnome3.enable = true;
#
#  # Set your time zone.
#  # time.timeZone = "Europe/Amsterdam";
#
#  # List packages installed in system profile. To search, run:
#  # \$ nix search wget
#  # environment.systemPackages = with pkgs; [
#  #   wget vim
#  # ];
#
#  # Enable the OpenSSH daemon.
#  # services.openssh.enable = true;
#  '';
#}
