Peregrine 2.0: Explaining Correctness of Population Protocols through Stage Graphs(ATVA'2020 submission)
=======

About
------

On this page, you find all the information you need to review our tool *Peregrine 2.0* for our submission to ATVA'2020.

Download
-------
To make your review effortless, we provide a virtual machine image that comes with
Peregrine 2.0 pre-installed. You can download the image under the following URL:

[https://peregrine.model.in.tum.de/files/peregrine2.ova](https://peregrine.model.in.tum.de/files/peregrine2.ova)

The SHA-256 checksum of the file *peregrine2.ova* is:

```
03ac0d7a883f73c8674e212233d5eb01e73486fa1ed769a65bb6a3cc599b72dd
```

To check that your download is not corrupt, you may compute the checksum by executing (in a shell, assuming you are already in the directory where the file is located):

```
sha256sum peregrine2.ova
```

The number returned should equal the checksum given above.

Execution
-------
You need Virtualbox ( [https://www.virtualbox.org/](https://www.virtualbox.org) ) for the execution of the virtual machine. Import the file [peregrine2.ova](https://peregrine.model.in.tum.de/files/peregrine2.ova) in Virtualbox via the menu "File -> Import Appliance". When the import is done, a virtual machine with the name
*Peregrine 2.0* should be registered in Virtualbox. Once the virtual machine is booted, a browser window should appear with Peregrine's user interface as start page. There is also a second terminal window with Peregrine's backend running in the background. You can ignore the terminal window, but you should not close it - otherwise Peregrine's frontend can no longer work properly.

Since Peregrine is a web application, it can be executed in any modern web browser. It is probably most convenient for you to test Peregrine in the browser on your host system. This requires you to enable port forwarding in Virtualbox:

First, click the *Settings* button for our appliance:

![alt text](./img/SettingsButton.png "Settings button")

Then navigate to the network settings, and click the bottom button labelled *Port Forwarding*:

![alt text](./img/SettingsNetworkAdvanced.png "Settings network advanced")

Finally, add the following line:

![alt text](./img/PortForwardingRules.png "Port forwarding rules")

Now you can access Peregrine in your local browser via *http://localhost:3001*.

Documentation
-------
Peregrine contains an interactive documentation of all essential features. The documentation can be accessed by clicking the big question mark on the top right corner of Peregrine's user interface.

Trouble Shooting
-------

### Wrong keyboard layout

In the global menu bar on top, go to *Applications -> Settings -> Keyboard*, and then navigate to the tab with the name *Layout*. Here you can pick your desired keyboard layout.

### Wrong screen resolution

In the global menu bar on top, go to *Applications -> Settings -> Display*. Here you can pick your desired screen resolution.

Source Code
-------
Peregrine's source code is located in the directory */home/peregrine/peregrine* inside the virtual machine's disk. Alternatively, the source files can be browsed under the url [https://gitlab.lrz.de/ga97cer/peregrine/-/tree/atva-2020](https://gitlab.lrz.de/ga97cer/peregrine/-/tree/atva-2020).


 The source dir contains the following notable subdirectories:

* *peregrine-backend*: Implementation of a lean backend that processes all requests of the Peregrine frontend.

* *peregrine-frontend*: Implementation of Peregrine's GUI.

* *peregrine2.0*: Implementation of the new verification backend based on stage graphs.

* *population-protocols*: A Haskell library for handling population protocols

* *protocols*: specification of example protocols

Compilation of Peregrine in the VM is not necessary. However, you may want to compile it in order to verify that the binaries are reproducible from the source code. Peregrine can be compiled via executing
```
./compile.sh
```
in the directory */home/peregrine/peregrine*.
After compilation is finished, you can run
```
./peregrine.sh
```
in the same directory to start the Peregrine backend (you may have to kill running instances of Peregrine first). After starting the Peregrine backend this way, you may access Peregrine via localhost:3001 in the browser installed on the virtual machine.
