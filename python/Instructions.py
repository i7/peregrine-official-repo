import sys

# instructions with their parameter types.
# parameters can either be 'REGISTER's or 'LABEL's.
instruction_arities = {
    "NOOP": [],
    "INIT": [],
    "SET": ["REGISTER"],
    "COPY": ["REGISTER", "REGISTER"],
    "DUP": ["REGISTER", "REGISTER"],
    "CANCEL": ["REGISTER", "REGISTER"],
    "GOTO": ["REGISTER", "LABEL"],
    "GOTOTRUE": ["LABEL"],
    "FIN": [],
    "SETLEADER": ["REGISTER"]
}


def get_instruction_from_string(instruction_string):
    instruction = instruction_string.split("(")[0].strip()
    arity = len(instruction_arities[instruction])
    # true if no negation for the parameter
    parameters = [0] * arity
    if arity > 0:
        parsed_parameters = [x.strip() for x in instruction_string.split("(")[1].split(")")[0].split(",")]
        if arity != len(parsed_parameters):
            raise TypeError(
                "Too few arguments\nCommand {} requires {} parameters, but only {} parameters were given:\n{}".format(
                    instruction, arity, len(parsed_parameters), instruction_string))

        for i in range(0, arity):
            par = parsed_parameters[i]
            variable_nonnegated = True

            while par.startswith("!"):
                par = par[1:]
                variable_nonnegated = variable_nonnegated ^ True
            try:
                parameters[i] = (variable_nonnegated, int(par))
            except ValueError:
                raise TypeError(
                    "While parsing instruction {}: \n parameter {} is not an integer".format(instruction_string, i + 1))
    else:
        parameters = []
    return instruction, parameters
