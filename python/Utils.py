import copy as cp

from State import *

import Instructions as Instructions

# contrary to the description in the paper,
# when the phase clock is coupled with instructions, agents do not have to
# take on the pase of an agent in an earlier phase+instruction - they simply take the
# phase+instruction of the further of the two agents. this is the behaviour when the mode
# is set to "no-loopover".
# when the mode is set to "loopover", the default phase-clock behaviour will happen, where
# agents in earlier phases can overwrite agents with larger phases due to the loop-behaviour of
# phases
mode = "no-loopover"


# taken from https://stackoverflow.com/a/40977600 to create a range function that uses modulo
class crange:
    def __init__(self, start, stop, step=None, modulo=None):
        if step == 0:
            raise ValueError('crange() arg 3 must not be zero')

        if step is None and modulo is None:
            self.start = 0
            self.stop = start
            self.step = 1
            self.modulo = stop
        else:
            self.start = start
            self.stop = stop
            if modulo is None:
                self.step = 1
                self.modulo = step
            else:
                self.step = step
                self.modulo = modulo

    def __iter__(self):
        n = self.start
        if n > self.stop:
            while n < self.modulo:
                yield n
                n += 1
            n = 0
        while n < self.stop:
            yield n
            n += 1

    def __contains__(self, n):
        if self.start >= self.stop:
            return self.start <= n < self.modulo or 0 <= n < self.stop
        else:
            return self.start <= n < self.stop


def transition_function(state1, state2):
    new_state1, new_state2 = cp.deepcopy(state1), cp.deepcopy(state2)

    if new_state1.instruction == "FIN" or new_state2.instruction == "FIN":
        new_state1.instruction = "FIN"
        new_state1.round_number = 0
        new_state1.phase_number = 0
        new_state1.probing_state = 0

        new_state2.instruction = "FIN"
        new_state2.round_number = 0
        new_state2.phase_number = 0
        new_state2.probing_state = 0

        return new_state1, new_state2

    while new_state1.is_leader() and get_instruction_from_string(new_state1.instruction)[0] == "SETLEADER":
        register_nonnegated, register = get_instruction_from_string(new_state1.instruction)[1][0]
        # set newstate2 register to 1
        # advance statemachine for newstate1
        new_state2.registers[register] = 1 if register_nonnegated else 0
        new_state1.advance_statemachine()

    while new_state2.is_leader() and get_instruction_from_string(new_state2.instruction)[0] == "SETLEADER":
        register_nonnegated, register = get_instruction_from_string(new_state2.instruction)[1][0]
        # set newstate2 register to 1
        # advance statemachine for newstate1
        new_state1.registers[register] = 1 if register_nonnegated else 0
        new_state2.advance_statemachine()

    # also adjusts leader to next instruction if round is reached
    get_phase_numbers(new_state1, new_state2)

    get_instruction(new_state1, new_state2)

    apply_instructions(new_state1, new_state2)

    return new_state1, new_state2


def get_instruction(state1, state2):
    # state 1 is supposed to be modified
    if state1.instruction == "FIN" or state2.instruction == "FIN":
        state1.instruction = "FIN"
        state1.round_number = 0
        state1.phase_number = 0
        state1.probing_state = 0
        state2.instruction = "FIN"
        state2.round_number = 0
        state2.phase_number = 0
        state2.probing_state = 0
    elif (int(state1.round_number) + 1) % 3 == int(state2.round_number):
        state1.instruction = state2.instruction
        state1.round_number = state2.round_number
        state1.probing_state = 0
        state1.phase_number = 0
    elif (int(state2.round_number) + 1) % 3 == int(state1.round_number):
        state2.instruction = state1.instruction
        state2.round_number = state1.round_number
        state2.probing_state = 0
        state2.phase_number = 0


def get_phase_numbers(state1, state2):
    phase_count = int(state1.phase_count)
    initiator_phase = int(state1.phase_number)
    responder_phase = int(state2.phase_number)

    if state2.is_leader():
        # responder is a leader. if initiator is in same phase, leader moves to next phase
        if initiator_phase == responder_phase:
            # current instruction is over - all phase_count phases have occurred
            if (responder_phase + 1) % phase_count == 0:
                state2.advance_statemachine()
                state1.phase_number = 0
                state2.phase_number = 0
            else:
                state1.phase_number = initiator_phase
                state2.phase_number = (responder_phase + 1) % phase_count
            return
        else:
            # ignore intiators in all other phases
            return

    if mode == "loopover":
        lower_bound = (responder_phase + 1) % phase_count
        upper_bound = (((responder_phase + int(phase_count / 2)) + 1) % phase_count)
        testRange = [x for x in crange(lower_bound, upper_bound, phase_count)]

        if initiator_phase in testRange:
            state1.phase_number = initiator_phase
            state2.phase_number = initiator_phase
    elif mode == "no-loopover":
        # do not do loopover - this means agents in lower phases will never influence this agent
        # only adjust phase numbers when states are in same instruction and round number
        # otherwise, phase numbers will be adjusted by adjustment of instructions later anyways
        if state1.instruction == state2.instruction and state1.round_number == state2.round_number:
            state1.phase_number = max(state1.phase_number, state2.phase_number)
            state2.phase_number = max(state1.phase_number, state2.phase_number)
    else:
        raise TypeError(
            "In the file " + __file__
            + ", \"mode\" has to be set to a valid value. Possible values: loopover, no-loopover")


def noop(state1, state2, parameters):
    return


def set(state1, state2, parameters):
    # only one parameter
    register_number = int(parameters[0][1])
    negated = not parameters[0][0]

    # leader does not have registers
    if not state1.is_leader():
        state1.registers[register_number] = negated ^ 1
    if not state2.is_leader():
        state2.registers[register_number] = negated ^ 1


def copy(state1, state2, parameters):
    # two parameters
    register_number1 = int(parameters[0][1])
    register_number2 = int(parameters[1][1])

    negated = (parameters[0][0] ^ parameters[1][0])

    if not state1.is_leader():
        state1.registers[register_number2] = negated ^ int(state1.registers[register_number1])
    if not state2.is_leader():
        state2.registers[register_number2] = negated ^ int(state2.registers[register_number1])


def dup(state1, state2, parameters):
    if state1.is_leader() or state2.is_leader():
        # applying dup to leaders makes no sense, since they do not have registers
        return

    # two parameters
    register_number1 = int(parameters[0][1])
    register_number2 = int(parameters[1][1])

    parameter1_negated = not parameters[0][0]
    parameter2_negated = not parameters[1][0]

    # adjust registers if negation-adjusted values are 1,0, so that negation-adjusted values are 0,1
    apply_dup_to_state(state1, register_number1, parameter1_negated, register_number2, parameter2_negated)
    apply_dup_to_state(state2, register_number1, parameter1_negated, register_number2, parameter2_negated)

    state1_negation_adjusted_values = ((parameter1_negated ^ state1.registers[register_number1]), (
        parameter2_negated ^ state1.registers[register_number2]))

    state2_negation_adjusted_values = ((parameter1_negated ^ state2.registers[register_number1]), (
        parameter2_negated ^ state2.registers[register_number2]))

    if (state1_negation_adjusted_values == (1, 1) and state2_negation_adjusted_values == (0, 0)) or (
                    state1_negation_adjusted_values == (0, 0) and state2_negation_adjusted_values == (1, 1)):
        state1.registers[register_number1] = parameter1_negated ^ 0
        state1.registers[register_number2] = parameter2_negated ^ 1

        state2.registers[register_number1] = parameter1_negated ^ 0
        state2.registers[register_number2] = parameter2_negated ^ 1


def cancel(state1, state2, parameters):
    if state1.is_leader() or state2.is_leader():
        # applying cancel to leaders makes no sense, since they do not have registers
        return

    # two parameters
    register_number1 = int(parameters[0][1])
    register_number2 = int(parameters[1][1])

    parameter1_negated = not parameters[0][0]
    parameter2_negated = not parameters[1][0]

    # adjust registers if negation-adjusted values are 1,0, so that negation-adjusted values are 0,1
    apply_cancel_to_state(state1, register_number1, parameter1_negated, register_number2, parameter2_negated)
    apply_cancel_to_state(state2, register_number1, parameter1_negated, register_number2, parameter2_negated)

    state1_negation_adjusted_values = ((parameter1_negated ^ state1.registers[register_number1]), (
        parameter2_negated ^ state1.registers[register_number2]))

    state2_negation_adjusted_values = ((parameter1_negated ^ state2.registers[register_number1]), (
        parameter2_negated ^ state2.registers[register_number2]))

    if (state1_negation_adjusted_values == (1, 0) and state2_negation_adjusted_values == (0, 1)) or (
                    state2_negation_adjusted_values == (1, 0) and state1_negation_adjusted_values == (0, 1)):
        state1.registers[register_number1] = parameter1_negated ^ 0
        state1.registers[register_number2] = parameter2_negated ^ 0

        state2.registers[register_number1] = parameter1_negated ^ 0
        state2.registers[register_number2] = parameter2_negated ^ 0


def fin(state1, state2, parameters):
    return


def goto(state1, state2, parameters):
    register_number = int(parameters[0][1])
    parameter_negated = not parameters[0][0]

    x = state1.probing_state
    y = state2.probing_state

    predicate_satisfied = state2.registers[register_number] ^ parameter_negated if not state2.is_leader() else False

    state1.probing_state = x
    if not predicate_satisfied:
        state2.probing_state = max(x, y)
    else:
        if x == 0:
            state2.probing_state = y
        else:
            state2.probing_state = 2


def apply_cancel_to_state(state, register_number1, parameter1_negated, register_number2, parameter2_negated):
    normalized_variable1 = parameter1_negated ^ int(state.registers[register_number1])
    normalized_variable2 = parameter2_negated ^ int(state.registers[register_number2])

    if (normalized_variable1, normalized_variable2) == (1, 1):
        state.registers[register_number1] = parameter1_negated ^ 0
        state.registers[register_number2] = parameter2_negated ^ 0


def apply_dup_to_state(state, register_number1, parameter1_negated, register_number2, parameter2_negated):
    normalized_variable1 = parameter1_negated ^ int(state.registers[register_number1])
    normalized_variable2 = parameter2_negated ^ int(state.registers[register_number2])

    if (normalized_variable1, normalized_variable2) == (1, 0):
        state.registers[register_number1] = parameter1_negated ^ 0
        state.registers[register_number2] = parameter2_negated ^ 1


functions = {
    "NOOP": noop,
    # init behaves like noop, but simply signifies the beginning of the computation. Followers are initially in
    # this state. Should not be explicitly used in a program.
    "INIT": noop,
    "SET": set,
    "CANCEL": cancel,
    "DUP": dup,
    "COPY": copy,
    # FIN means an agent is done with its part of the computation and will not partake anymore. initiated by the leader
    # when the instruction pointer points outside the given set of instructions
    "FIN": fin,
    # GOTO is of the form GOTO REGISTER LINENUMBER
    "GOTO": goto,
    # should not be called from here, when encountered leader will apply instruction directly
    "SETLEADER": None,
    # semantical meaning is GOTO true LINENUMBER. Is of form GOTOTRUE LINENUMBER
    "GOTOTRUE": None
}


def apply_instructions(state1, state2):
    instructions = [Instructions.get_instruction_from_string(state1.instruction),
                    Instructions.get_instruction_from_string(state2.instruction)]

    if instructions[0] == instructions[1]:
        functions[instructions[0][0]](state1, state2, instructions[0][1])
