from Instructions import get_instruction_from_string


class State(object):
    def __init__(self, phaseCount):
        self.instruction = "INIT"
        self.stateMachineInstructions = []
        self.instructionPointer = 0
        self.probing_state = 0
        self.round_number = 0
        self.phase_number = 0
        self.phase_count = phaseCount

    def initialize_from_string(self, state_param):
        raise NotImplementedError("State is an abstract base class, use either FollowerState or LeaderState")

    def is_finished(self):
        return self.instruction == "FIN"

    def is_leader(self):
        raise NotImplementedError("State is an abstract base class, use either FollowerState or LeaderState")


class FollowerState(State):
    def initialize_from_string(self, state_param):
        """
        Initializes this state with a given parameter, which is in string form.
        :param state_param: The parameter as a string of the form
        non-leader: register_1|register_2|...|register_RegisterCount|probingParam||Instruction|RoundNumber|PhaseNumber.
        """

        params = state_param.split("|")
        # non-leader
        self.registers = [int(x) for x in params[0:len(self.registers)]]

        # valid probing states: 0, 1, 2
        self.probing_state = int(params[self.register_count])

        # valid instructions: NOOP, SET(A), COPY(A,B), DUP(A,B), CANCEL(A,B), PROBE(A) where A and B are numbers between 0 and register_count-1
        self.instruction = params[self.register_count + 1]

        # valid round numbers: 0, 1, 2
        self.round_number = int(params[self.register_count + 2])

        # valid phase number: 0 to phase_count-1
        self.phase_number = int(params[self.register_count + 3])

    def __init__(self, register_count, phase_count):
        """
        Initializes this state to have the given number of registers and phases.
        :param register_count: The number of registers.
        :param phase_count: The number of phases in each round.
        """
        super(FollowerState, self).__init__(phase_count)
        self.register_count = register_count
        self.registers = [0] * register_count

    def __str__(self):
        """
        Outputs this state as a string representation,
        fit for initializing a state of this class using the initialize_from_string method.
        :return: A string representation of this state.
        """
        return "|".join(
            [str(x) for x in self.registers] + [str(self.probing_state), self.instruction, str(self.round_number),
                                                str(self.phase_number)])

    def __repr__(self):
        return self.__str__()

    def __hash__(self, *args, **kwargs):
        return hash(
            (frozenset(self.registers), self.probing_state, self.round_number, self.instruction, self.phase_number))

    # todo this function is a bottleneck, and likely needs to be optimized heavily
    def __eq__(self, other):
        if isinstance(other, FollowerState):
            return self.register_count == other.register_count \
                   and self.registers == other.registers and \
                   self.probing_state == other.probing_state and self.round_number == other.round_number \
                   and self.instruction == other.instruction and self.phase_number == other.phase_number \
                   and self.phase_count == other.phase_count

    def is_leader(self):
        return False


class LeaderState(State):
    def __init__(self, phaseCount):
        super(LeaderState, self).__init__(phaseCount)
        self.stateMachineInstructions = []
        self.instructionPointer = 0

    def initialize_from_string(self, state_param):
        """
        :param state_param: A string of the form
        probingParam|Instruction|RoundNumber|PhaseNumber|instruction_list;instructionPointer
        :return:
        """
        params = state_param.split("|")
        self.probing_state = int(params[0])
        self.instruction = params[1]
        self.round_number = int(params[2])
        self.phase_number = int(params[3])

        state_machine = params[4]
        self.stateMachineInstructions = state_machine.split(";")[0].split("\n")
        self.instructionPointer = int(state_machine.split(";")[1])

    def is_leader(self):
        return True

    def advance_statemachine(self):
        if self.instruction == "FIN":
            return

        if get_instruction_from_string(self.instruction)[0] == "GOTO" and self.probing_state == 2:
            self.instructionPointer = int(self.instruction.split(",")[1].strip(")"))
        else:
            self.instructionPointer += 1

        if self.instructionPointer >= len(self.stateMachineInstructions):
            # computation done, whole instruction list was exhausted
            # TODO
            self.instruction = "FIN"
            self.round_number = 0
            self.probing_state = 0
            self.phase_number = 0
        else:
            self.instruction = self.stateMachineInstructions[self.instructionPointer]
            if get_instruction_from_string(self.instruction)[0] != "GOTOTRUE" and \
                            get_instruction_from_string(self.instruction)[0] != "SETLEADER":
                self.round_number = (self.round_number + 1) % 3
            if get_instruction_from_string(self.instruction)[0] == "GOTO":
                self.probing_state = 1

        if get_instruction_from_string(self.instruction)[0] == "GOTOTRUE":
            self.instructionPointer = int(self.instruction.split("(")[1].strip(")")) - 1
            self.advance_statemachine()
            return

    def __str__(self):
        """
        Outputs this state as a string representation,
        fit for initializing a state of this class using the initialize_from_string method.
        :return: A string representation of this state.
        """
        return "|".join([str(self.probing_state), self.instruction, str(self.round_number),
                         str(self.phase_number)] + (
                            ["\n".join(self.stateMachineInstructions) + ";{}".format(
                                str(self.instructionPointer))] if self.is_leader else []))

    def __repr__(self):
        return self.__str__()

    def __hash__(self, *args, **kwargs):
        return hash((self.probing_state, self.round_number, self.instruction, self.phase_number,
                     self.instructionPointer))

    def __eq__(self, other):
        if isinstance(other, LeaderState):
            return self.probing_state == other.probing_state and self.round_number == other.round_number \
                   and self.instruction == other.instruction and self.phase_number == other.phase_number \
                   and self.phase_count == other.phase_count and self.stateMachineInstructions \
                                                                 == other.stateMachineInstructions \
                   and self.instructionPointer == other.instructionPointer
