import Utils as Utils
from State import *

debug = True
extra_debug = False


def generate_protocol_string(register_count, phase_count, program_string):
    initial_follower = FollowerState(register_count, phase_count)
    initial_leader = LeaderState(phase_count)
    initial_leader.stateMachineInstructions = program_string.split("\n")
    initial_leader.instructionPointer = -1
    initial_leader.advance_statemachine()

    additional_followers = []

    transitions = {}
    states_collection = []
    queue = [initial_leader, initial_follower]

    for register in range(0, register_count):
        additional_follower = FollowerState(register_count, phase_count)
        additional_follower.registers[register] = 1

        queue.append(additional_follower)

    i = 0
    while queue:
        i += 1
        if extra_debug and i % 1000 == 0:
            print("------")
            print("Iteration " + str(i))
            print("Transitions:")
            for k, v in transitions.items():
                print("{}->{}".format(str(k).replace("\n", "\\n"), str(v).replace("\n", "\\n")))
            print("States")
            for state in states_collection:
                print(state)

        if debug and i % 100 == 0:
            print("State {}, # of transitions {}".format(i, len(transitions)))
        current_state = queue.pop()
        states_collection.append(current_state)

        for other_state in states_collection:
            if current_state.is_leader() and other_state.is_leader():
                continue
            if not (current_state, other_state) in transitions:
                new_state1, new_state2 = Utils.transition_function(current_state, other_state)
                transitions[current_state, other_state] = (new_state1, new_state2)
                if new_state1 not in states_collection + queue:
                    queue.append(new_state1)
                if new_state2 not in states_collection + queue:
                    queue.append(new_state2)
            if not (other_state, current_state) in transitions:
                new_state1, new_state2 = Utils.transition_function(other_state, current_state)
                transitions[other_state, current_state] = (new_state1, new_state2)
                if new_state1 not in states_collection + queue:
                    queue.append(new_state1)
                if new_state2 not in states_collection + queue:
                    queue.append(new_state2)
    return states_collection, transitions


if __name__ == "__main__":
    # cProfile.run('generate_protocol_string(4, 4, "SET(0)")')
    # exit(0)
    states, transitions = generate_protocol_string(3, 3, """COPY(1,2)\nDUP(2,0)""")
    print("------- DONE -------")
    print("------- STATES -------")
    for state in states:
        print(repr(str(state)))
    print("------- TRANSITIONS -------")
    for k, v in transitions.items():
        print("{}->{}".format(repr(str(k)), repr(str(v))))

    print("------- INFO -------")
    print("# of states: {}".format(len(states)))
    print("# of transitions: {}".format(len(transitions)))
