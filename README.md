Peregrine - A Tool for Analysis of Population Protocols
=======

About
------
Peregrine is a tool for the analysis of population protocols.
Population protocols are a model of computation very much studied by the distributed computing community,
in which mobile anonymous agents interact stochastically to achieve a common task. Peregrine allows users to design
protocols, to simulate them both manually and automatically, to gather statistics of properties such as convergence speed, and to verify correctness automatically.

Peregrine  allows the user to specify protocols either through an editor or as simple scripts, and to analyze them via a graphical interface.
The analysis features of Peregrine include manual step-by-step simulation; automatic sampling; statistics generation of average convergence speed;
detection of incorrect executions through simulation; and formal verification of correctness.

Compilation and Installation
-------

You need to install the following dependencies:

    * The SMT-solver Z3 (https://github.com/Z3Prover/z3)
    * Python in version 3.*
    * Pylint for Python 3.* (https://www.pylint.org/)
    * The Petri net tool LoLA (http://service-technology.org/lola/)
    * Haskell build tool stack (https://docs.haskellstack.org/en/stable/README/)
    * JavaScript package manager npm (https://www.npmjs.com/)
    * The module bundler webpack (https://webpack.js.org/)
    * The java build utility ant (https://ant.apache.org/)
    * lark parser for python (https://github.com/lark-parser/lark)

To use the automatic predicate to protocol generation you additionally need:

    * Z3 bindings for python

To use the new verification for a larger class of protocols (aka Peregrine v2.0) you additionally need:

    * graph_tool (https://graph-tool.skewed.de)


By default Peregrine expects the executables among these dependencies to be
callable by pylint, python and lola, respectivly.  If your names differ
(e.g. python2 instead of python), then export the following environment variables and
set them to the name of your executables:

*PEREGRINE_PYLINT*, *PEREGRINE_PYTHON* and *PEREGRINE_LOLA*, respectively.

To compile Peregrine, run the script 'compile.sh' in the top-level directory:

```
./compile.sh
```

The compile script first compiles the backend and then the frontend.
A lot of dependencies will be loaded and
compiled in the process, so this will take a while. The dependencies
take up approx. 700MB additional disk space. This is admittedly a lot,
but most of it is due to the Haskell build tool 'stack', which downloads
a suitable version of the haskell compiler ghc + dependencies.

Once the compilation is done, Peregrine can be called via the run-script
peregrine.sh (from the same folder):

```
./peregrine.sh
```

and the message "Peregrine has started..." should appear on screen.
Then Peregrine can be used in any browser of your choice under the URL
http://localhost:3001.

------


Once the backend is compiled, you may start a frontend development server
that has "hot reload" (aka "auto compile"). This feature is especially helpful,
if you do many minor changes in the frontend. First, start the backend by running:

```
./peregrine.sh
```

Then, to start the developement server on http://localhost:3000, run the following command in
a new terminal:
```
cd peregrine-frontend & npm start
```


Authors
-------


* Stefan Jaax (<jaax@in.tum.de>)
* Michael Blondin (<blondin@in.tum.de>)
* Philipp Meyer (<meyerphi@in.tum.de>)
* Martin Helfrich (<helfrich@in.tum.de>)

References
--------
* Michael Blondin, Javier Esparza, Stefan Jaax, Philipp J. Meyer: “Towards Efficient Verification of Population Protocols”, 2017; [arXiv:1703.04367](http://arxiv.org/abs/1703.04367).
* Michael Blondin, Javier Esparza, Stefan Jaax: "Peregrine: A Tool for the Analysis of Population Protocols", 2018; [Cav18 Preprint](https://www7.in.tum.de/~blondin/papers/BEJ18b.pdf).

License
-------
Peregrine is licensed under the GNU GPLv3, see [LICENSE.md](LICENSE.md) for details.
