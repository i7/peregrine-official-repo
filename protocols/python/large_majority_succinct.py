#-*- coding: utf-8 -*-
params = {
  "scheme": {
    "k": {
      "descr":  "gap parameter",
      "values": list(range(0, 11, 1)),
      "value":  3
    }
  }
}

def generateProtocol(params):
  k = params["scheme"]["k"]["value"]

  def value(q):
    return q[0]

  def output(q):
    return True if value(q) == 2**k else False if value(q) == -1 else q[1]

  def name(q):
    return value(q) if value(q) in [-1, 2**k] else f'{value(q)}{"T" if output(q) else ""}'

  # Generate states
  values = [0] + [2**x for x in range(k)]
  states = [(-1, False)] + [(v, o) for v in values for o in [True, False]] + [(2**k, True)]

  # Generate transitions
  transitions = []
  
  for i in range(len(states)):
    for j in range(i, len(states)):
      p = states[i]
      q = states[j]
      pre = (name(p), name(q))
      
      values = {value(q), value(p)}
      
      if 2**k in values and not -1 in values:
        post = (name((value(p), True)), name((value(q), True)))
        t = Utils.transition(pre, post)
        if (not Utils.silent(pre, post) and t not in transitions):
          transitions.append(t)
      
      p_new_v = value(p)
      q_new_v = value(q)
      
      if 0 < value(p) == value(q) < 2**k:
        p_new_v = 2 * value(p)
        q_new_v = 0
      elif value(p) > 1 and value(q) == 0 or value(q) > 1 and value(p) == 0:
        p_new_v = int((value(p) + value(q)) / 2)
        q_new_v = int((value(p) + value(q)) / 2)
      elif value(p) == -value(q):
        p_new_v = 0
        q_new_v = 0
        
      if {p_new_v, q_new_v} == {-1, 2**k}:
        continue
        
      new_o = False if p_new_v == -1 or q_new_v == -1 else True if p_new_v == 2**k or q_new_v == 2**k else output(p) and output(q)
      
      post = (name((p_new_v, new_o)), name((q_new_v, new_o)))
        
      t = Utils.transition(pre, post)
      if (not Utils.silent(pre, post) and t not in transitions):
        transitions.append(t)

  initial_states = [(-1,False), (1,1 >= 2**k)]
  true_states = [q for q in states if output(q)]
  predicate = f'C[{name((1,1 >= 2**k))}] - C[{name((-1, False))}] >= {2**k}'

  # Generate description
  description = """Compute if there is a large majority, i.e. if Y - N >= 2^k. Note: This protocol is succint (~2k states) but slow (2^O(n log n))."""

  # Generate style
  style = {name(q): {} for q in states}

  for q in states:
    scale = abs(value(q)) / 2**k
    style[name(q)]["size"] = 0.5 + scale * 0.7
    #style[name(q)]["shape"] = "triangle" if value(q) < 0 else "square" if value(q) > 0 else "circle"
    #style[name(q)]["color"] = f'rgb(0, 255, 0)' if q in true_states else f'rgb(255, 0, 0)' if value(q) < 0 else f'rgb(0, 0, 255)'
    style[name(q)]["color"] = f'rgb(0, 255, 0)' if q in true_states else f'rgb(255, 0, 0)' if value(q) < 0 else f'rgb(180, 180, 180)' if value(q) == 0 else f'rgb(0, 0, 255)'


  return {
    "title":         f'Large Majority (succinct): Y - N >= {2**k}',
    "states":        [name(q) for q in states],
    "transitions":   transitions,
    "initialStates": [name(q) for q in initial_states],
    "trueStates":    [name(q) for q in true_states],
    "predicate":     predicate,
    "description":   description,
    "statesStyle":   style
  }