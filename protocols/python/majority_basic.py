# EDIT_WYSIWYG_ONLY
#-*- coding: utf-8 -*-
# This file has been created by Peregrine.
# Do not edit manually!
def generateProtocol(params):
    return {
      "title":         "Majority Voting protocol",
      "states":        ["Y", "N", "y", "n"],
      "transitions":   [Utils.transition(("Y", "N"), ("y", "n")),
                        Utils.transition(("Y", "n"), ("Y", "y")),
                        Utils.transition(("N", "y"), ("N", "n")),
                        Utils.transition(("y", "n"), ("y", "y"))],
      "initialStates": ["Y", "N"],
      "trueStates":    ["Y", "y"],
      "predicate":     "C[Y] >= C[N]",
      "description":   """This protocol takes a majority vote. More precisely,
                          it computes whether there are initially more agents
                          in state Y than N.""",
      "statesStyle": {"Y": {"size": 1.0},
                      "N": {"size": 1.0},
                      "y": {"size": 0.5},
                      "n": {"size": 0.5}}
    }
