params = {
    "scheme": {
        "minCoeff": {
            "descr":  "Min. coeff. a",
            "values": list(range(-30, 31)),
            "value":  -2
        },

        "maxCoeff": {
            "descr":  "Max. coeff. b",
            "values": list(range(-30, 31)),
            "value":  2
        },

        "threshold": {
            "descr":  "Threshold c",
            "values": list(range(-30, 30)),
            "value":  0
        }
    }
}

def generateProtocol(params):
    a = params["scheme"]["minCoeff"]["value"]
    b = params["scheme"]["maxCoeff"]["value"]
    if b < a:
        i = a
        a = b
        b = i

    c = params["scheme"]["threshold"]["value"]
    s1 = min(a, c-1)
    s2 = max(b, c)

    # Helper functions
    def is_boolean(q):
        return q == "t" or q == "f"

    def is_tuple(q):
        return isinstance(q, tuple)

    def is_value(q):
        return not is_boolean(q) and not is_tuple(q)

    def value(q):
        if is_boolean(q):
            return 0
        return q[0] if is_tuple(q) else q

    def output(q):
        if is_boolean(q):
            return q == "t"
        return q[1] if is_tuple(q) else value(q) >= c

    def f(m, n):
        return max(s1, min(s2, value(m) + value(n)))

    def g(m, n):
        return value(m) + value(n) - f(m, n)

    def postFor(x, y):
        if not is_value(x) and not is_value(y):
            return None
        xn = f(x, y)
        yn = g(x, y)
        if value(yn) == 0:
            yn = "t" if xn >= c else "f"
            return xn, yn
        if output(xn) != output(yn):
            yn = (yn, output(xn))
        return xn, yn

    neutralState = "t" if 0 >= c else "f"
    states = {}
    transitions = []
    WS = {str(q): q for q in range(a, b + 1)}  # start from initial
    WS[str(neutralState)] = neutralState  # ... and neutral state

    while len(WS) != 0:
        curS, cur = WS.popitem()
        states[curS] = cur

        for sString in states:
            other = states[sString]
            pre = (cur, other)
            post = postFor(cur, other)
            if post is not None:
                new1, new2 = post
                t = Utils.transition(pre, post)
                if (not Utils.silent(pre, post)):
                    transitions.append(t)
                if str(new1) not in states and str(new1) not in WS:
                    WS[str(new1)] = new1
                if str(new2) not in states and str(new2) not in WS:
                    WS[str(new2)] = new2

    # Generate initial states
    initial_states = list(range(a, b + 1))

    # Generate true states
    true_states = [states[qS] for qS in states if output(states[qS])]

    # Generate predicate
    expr = ["{}*C[{}]".format(value(q), q) for q in initial_states]
    predicate = "{} >= {}".format(" + ".join(expr), c)

    # Generate description

    return {
        "title":         "Threshold protocol (improved)",
        "states":        states,
        "transitions":   transitions,
        "initialStates": initial_states,
        "trueStates":    true_states,
        "predicate":     predicate,
        "description":   """This protocol evaluates the linear inequality a·x_a + ... + b·x_b < c. 
                            This improved version by Martin Helfrich uses fewer states while
                            having the same fast speed as the original protocol by Angluin et al. from PODC 2004. 
                            The protocol uses only two passive states ('t' and 'f') and eliminates the output bit 
                            from most states. Further, it uses better bounds for the minimum and maximum value. The 
                            protocol also removes all states / transitions that cannot be reached / fired (This is 
                            known as the workset method)."""
    }
