# -*- coding: utf-8 -*-
params = {
  "scheme": {
    "c": {
      "descr":  "Threshold c",
      "values": list(range(1, 201)),
      "value":  4
    }
  }
}

def generateProtocol(params):
  c = params["scheme"]["c"]["value"]

  states = list(range(0, c + 1))
  transitions = []

  for i in states:
    for j in states:
      if i + j < c and (i > 0 and j > 0):
        transitions.append(Utils.transition((i, j), (i + j, 0)))
      elif i + j >= c and (i != c or j != c):
        transitions.append(Utils.transition((i, j), (c, c)))

  style = {q: {} for q in states}

  for q in states:
    style[q]["size"] = 0.75 + 0.8 * (q + 1) / (c + 1)
    if q < c:
      style[q]["color"] = "rgb({}, {}, {})".format(105 + 128 * (c - q) / c, 30, 99)

  style[0]["color"] = "rgb(175, 175, 175)"

  return {
    "title":         "Flock-of-birds protocol",
    "states":        states,
    "transitions":   transitions,
    "initialStates": [1],
    "trueStates":    [c],
    "predicate":     "C[1] >= {}".format(c),
    "description":   """This protocol computes whether a population of
                        birds contains at least c sick birds, i.e. whether
                        C[1] >= c. Described in Dana Angluin, James
                        Aspnes, Zoë Diamadi, Michael J. Fischer and
                        René Peralta. Computation in Networks of
                        Passively Mobile Finite-State Sensors.
                        PODC 2004.""",
    "statesStyle": style
  }
