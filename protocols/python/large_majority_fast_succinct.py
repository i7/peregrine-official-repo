#-*- coding: utf-8 -*-
params = {
  "scheme": {
    "k": {
      "descr":  "gap parameter",
      "values": list(range(0, 11, 1)),
      "value":  3
    }
  }
}

def states_from_transitions(transitions):
    states = set()
    for pre, post in transitions:
        for q in pre:
            states.add(q)
        for q in post:
            states.add(q)
    return states


def generateProtocol(params):
    k = params["scheme"]["k"]["value"]
    h = k+1
    threshold = 2**k
    
    # step 1: generate protocol with leader output
    NOTIFY = True
    SLEEP = False
    transitions = []
  
    def ladder_name(val, notify):
        return f'{val}{"N" if notify else ""}'

    def output_calc_name(lvl, geq_c = None):
        if geq_c is None:
            return f'L{lvl}'
        return f'O{lvl}_{"geq" if geq_c else "le"}'

    def output_reset_name(lvl):
        return f'R{lvl}'

    def output_lvl_leader_name(lvl, val, notify):
        assert -1 <= val <= 1
        return f'l{lvl}_{val}{"N" if notify else ""}'
    
    Z = ladder_name(0, SLEEP)
    ZN = ladder_name(0, NOTIFY)  # agent with value zero in ladder that needs to notify a output leader
    zeros = [Z, ZN]
    
    output_initial = output_calc_name(h)
    output_reset = output_reset_name(h)
    output_true = f'T'
    output_false = f'F'
    output_final = {output_true, output_false}
    output_all_calc = {output_initial}
    output_all_reset = {output_reset_name(lvl) for lvl in range(h+1)}


    # ladder
    for n1 in [NOTIFY, SLEEP]:
        for n2 in [NOTIFY, SLEEP]:
            for lvl in range(h + 1):
                # cancel
                transitions.append(([ladder_name(-2**lvl, n1), ladder_name(2**lvl, n2)], [Z, ZN]))
                if lvl < h:
                    # up
                    transitions.append(([ladder_name(2**lvl, n1), ladder_name(2**lvl, n2)],
                                        [ladder_name(2**(lvl+1), SLEEP), ZN]))
                    transitions.append(([ladder_name(-2**lvl, n1), ladder_name(-2**lvl, n2)],
                                        [ladder_name(-2**(lvl+1), SLEEP), ZN]))
            
            # cancel second highest
            transitions.append(([ladder_name(2**h, n1), ladder_name(-2**(h-1), n2)],
                                [ladder_name(2**(h-1), SLEEP), ZN]))
            transitions.append(([ladder_name(-2**h, n1), ladder_name(2**(h-1), n2)],
                                [ladder_name(-2**(h-1), SLEEP), ZN]))
                                
    # output calculation (checking: >= threshold)
    def suc_4_cur_value(lvl, cur_value):
        min_possible_val = cur_value - ((2 ** lvl) - 1)
        max_possible_val = cur_value + ((2 ** lvl) - 1)
        if min_possible_val >= threshold:
            return output_true, None
        elif max_possible_val < threshold:
            return output_false, None
        else:
            return output_calc_name(lvl - 1, cur_value >= threshold), cur_value >= threshold
    #   init (highest lvl)
    reachable = set()   # set to remember what output states in next level
    for val in [-1, 0, 1]:
        cur_val = val * (2**h)
        suc, r = suc_4_cur_value(h, cur_val)
        transitions.append(([output_lvl_leader_name(h, val, SLEEP), output_initial],
                            [output_lvl_leader_name(h, val, SLEEP), suc]))
        if r is not None:
            reachable.add(r)
            output_all_calc.add(suc)
    #   other lvls
    for lvl in range(h-1,-1,-1):
        last_reachable = {x for x in reachable}
        if len(last_reachable) == 0:
            break
        reachable = set()
        for geq_c in last_reachable:
            last_min_possible_val = threshold - ((2**(lvl+1)) - 1)
            last_max_possible_val = threshold + ((2**(lvl+1)) - 1) - 1
            last_value = last_max_possible_val - (last_max_possible_val % (2**(lvl+1)))
            if last_value >= threshold and not geq_c:
                last_value = last_value - (2**(lvl+1))
            for val in [-1, 0, 1]:
                cur_val = last_value + val * (2 ** lvl)
                suc, r = suc_4_cur_value(lvl, cur_val)
                transitions.append(([output_lvl_leader_name(lvl, val, SLEEP), output_calc_name(lvl, geq_c)],
                                    [output_lvl_leader_name(lvl, val, SLEEP), suc]))
                if r is not None:
                    reachable.add(r)
                    output_all_calc.add(suc)

    output_all = output_final | output_all_calc | output_all_reset
    
    # notifications
    notifying_states = [ladder_name(sign * 2**lvl, NOTIFY) for lvl in range(h+1) for sign in [-1,1]] + [ZN]
    notified = {ladder_name(sign * 2**lvl, NOTIFY): ladder_name(sign * 2**lvl, SLEEP) for lvl in range(h+1) for sign in [-1,1]}
    notified[ZN] = Z
    for i1 in range(len(notifying_states)):
        q1 = notifying_states[i1]
        for i2 in range(i1, len(notifying_states)):
            q2 = notifying_states[i2]
            # 2 notifies can cancel
            transitions.append(([q1, q2], [notified[q1], q2]))

    for o in output_all:
        for q in notifying_states:
            # ladder notify -> reset output
            transitions.append(([o, q], [output_reset, notified[q]]))
    for o in output_all_calc | {output_true, output_false}:
        for lvl in range(h + 1):
            for v1 in [-1, 0, 1]:
                # output level leader notify -> restart output calc
                transitions.append(([o, output_lvl_leader_name(lvl, v1, NOTIFY)],
                                    [output_initial, output_lvl_leader_name(lvl, v1, SLEEP)]))

    # update output lvl leaders
    for lvl in range(h+1):
        for sign in [-1, 1]:
            transitions.append(([ladder_name(sign * 2**lvl, SLEEP), output_lvl_leader_name(lvl, 0, SLEEP)],
                                [ladder_name(sign * 2**lvl, SLEEP), output_lvl_leader_name(lvl, sign, NOTIFY)]))

    # reset output lvl leaders
    for lvl in range(h + 1):
        next = output_reset_name(lvl-1) if lvl > 0 else output_initial
        for val in [-1, 0, 1]:
            for n1 in [NOTIFY, SLEEP]:
                transitions.append(([output_reset_name(lvl), output_lvl_leader_name(lvl, val, n1)],
                                    [next, output_lvl_leader_name(lvl, 0, SLEEP)]))
 
    states = list(states_from_transitions(transitions))
    leaders = {output_initial: 1}
    for lvl in range(h+1):
        leaders[output_lvl_leader_name(lvl, 0, SLEEP)] = 1 
    initialStates = [ladder_name(1,SLEEP), ladder_name(-1,SLEEP)]
    
    style = {q: {} for q in states}
    gray = f'rgb(180, 180, 180)'
    red = f'rgb(255, 0, 0)'
    blue = f'rgb(0, 0, 255)'
    green = f'rgb(0, 255, 0)'
    for n1 in [NOTIFY, SLEEP]:
        for lvl in range(h + 1):
            for sign in [-1,1]:
                style[ladder_name(sign * 2**lvl, n1)]["size"] = 0.6 + lvl / h * 0.4
                style[ladder_name(sign * 2**lvl, n1)]["color"] = red if sign == -1 else blue
                #if n1 == NOTIFY:
                #    style[ladder_name(sign * 2**lvl, n1)]["shape"] = "square"          
    for q in output_all:
        style[q]["size"] = 1.6
        style[q]["color"] = gray
        style[q]["fillcolor"] = "rgba(255,255,255,0)"
        #style[q]["shape"] = "square"
    for lvl in range(h + 1):
        for val in [-1, 0, 1]:
            for n1 in [NOTIFY, SLEEP]:
                style[output_lvl_leader_name(lvl, val, n1)]["size"] = 1.1 + lvl / h * 0.4
                #style[output_lvl_leader_name(lvl, val, n1)]["shape"] = "triangle"
                style[output_lvl_leader_name(lvl, val, n1)]["color"] = red if val == -1 else blue if val == 1 else gray
    style[Z]["size"] = 0.5
    style[Z]["color"] = gray
    style[ZN]["size"] = 0.5
    #style[ZN]["shape"] = "square"
    style[ZN]["color"] = gray
    
       
       
    # step 2: convert to normal output convention
    new_transitions = []
    
    def name(q, o):
        return f'{q}{"T" if o else ""}'

    def with_o(q, o):
        if q in output_final:
            return q
        else:
            return name(q, o)
            
    # output agents "persuade" non output agents
    for q in states:
        if q in output_all:
            continue
        new_transitions.append(([name(q, False), output_true], [name(q, True), output_true]))
        new_transitions.append(([name(q, True), output_false], [name(q, False), output_false]))
        
    # allow transitions with every output combination
    for t in transitions:
        pre, post = t
        for o1 in {True, False} if pre[0] not in output_all or post[0] not in output_all else {False}:
            for o2 in {True, False} if pre[1] not in output_all or post[1] not in output_all else {False}:
                nt = ([with_o(pre[0], o1), with_o(pre[1], o2)], [with_o(post[0], o1), with_o(post[1], o2)])
                new_transitions.append(nt)

    new_states = list(states_from_transitions(new_transitions))
    new_transitions = [Utils.transition((t[0][0], t[0][1]), (t[1][0], t[1][1])) for t in new_transitions]
    trueStates = [output_true] + list({with_o(q, True) for q in states if q not in output_all})
    new_initialStates = [with_o(q, False) for q in initialStates]
    new_style = {}
    for q in style:
        new_style[with_o(q,False)] = style[q].copy()
        new_style[with_o(q,True)] = style[q].copy()
    for q in trueStates:
        new_style[q]["color"] = green
    
    new_style[output_false]["color"] = blue
    new_leaders = {with_o(q, False): leaders[q] for q in leaders}
    description = """Compute if there is a large majority, i.e. if Y - N >= 2^k. Note: This protocol is fast (O(n^2 log n)) and succinct (O(k) states)."""

    return {
        "title":         f'Large Majority (fast & succinct): Y - N >= {2**k}',
        "states":        sorted(new_states),
        "transitions":   new_transitions,
        "initialStates": new_initialStates,
        "trueStates":    trueStates,
        "leaders":       leaders,
        "predicate":     f'C[{with_o(ladder_name(1,SLEEP), False)}] - C[{with_o(ladder_name(-1,SLEEP), False)}] >= {2**k}',
        "description":   description,
        "precondition":  "true",
        "postcondition": "true",
        "statesStyle":   new_style
    }