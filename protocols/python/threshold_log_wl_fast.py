# -*- coding: utf-8 -*-
import math

class Utils:
    @staticmethod
    def transition(pre, post):
        return {
            "name": "{}, {} -> {}, {}".format(pre[0], pre[1], post[0], post[1]),
            "pre":  [pre[0],  pre[1]],
            "post": [post[0], post[1]]
        }

params = {
    "scheme": {
        "a": {
            "descr":  "coeff. a",
            "values": list(range(0, 30)),
            "value":  5
        },

        "b": {
            "descr":  "coeff. b",
            "values": list(range(0, 30)),
            "value":  3
        },

        "c": {
            "descr":  "offset c",
            "values": list(range(-2, 30)),
            "value":  2
        }
    }
}

def generateProtocol(params):
    a = params["scheme"]["a"]["value"]
    b = params["scheme"]["b"]["value"]
    c = params["scheme"]["c"]["value"]

    n = int(math.log(max(abs(a)-1, abs(b)-1, abs(c)-1, 1), 2)) + 1

    def rep(n):
        binary = lambda n: n > 0 and [n & 1] + binary(n >> 1) or []
        binary_n = binary(n)
        return [i for i in range(len(binary_n)) if binary_n[i] == 1]

    Qp = [str(2**z) + "+" for z in range(0,n+1)]
    Qm = [str(2**z) + "-" for z in range(0,n+1)]

    x = "x"
    y = "y"
    X = [x, y]

    y0 = "0+"
    n0 = "0-"
    pn0 = "0+p"
    R = [y0, n0, pn0]

    states = X + Qp + Qm + R
    transitions = []

    for i in range(0, n):
        transitions.append(Utils.transition((Qp[i], Qp[i]), (Qp[i+1], y0)))     # up_plus
        transitions.append(Utils.transition((Qm[i], Qm[i]), (Qm[i+1], n0)))     # up_minus
        for r in R:
            transitions.append(Utils.transition((Qp[i+1], r), (Qp[i], Qp[i])))  # down_plus
            transitions.append(Utils.transition((Qm[i+1], r), (Qm[i], Qm[i])))  # down_minus

    for i in range(0, n+1):
        transitions.append(Utils.transition((Qp[i], n0), (Qp[i], y0)))  # signal_plus
        transitions.append(Utils.transition((Qp[i], pn0), (Qp[i], y0)))  # signal_plus
        transitions.append(Utils.transition((Qm[i], y0), (Qm[i], n0)))  # signal_minus

    transitions.append(Utils.transition((n0, y0), (n0, pn0)))  # signal

    for i in range(0, n + 1):
        transitions.append(Utils.transition((Qp[i], Qm[i]), (n0, n0)))  # cancel

    t_count = 0

    def addk(pre, post, states, transitions, t_count, default):
        # make sure len(pre) == len(post)
        if len(pre) > len(post):
            post = post + [default] * (len(pre) - len(post))
        if len(pre) < len(post):
            pre = pre + [default] * (len(post) - len(pre))

        k = len(pre)

        if k == 0:
            return []

        if k == 1:
            transitions.append(Utils.transition(pre + [default], post + [default]))
            return []

        if k == 2:
            transitions.append(Utils.transition(pre, post))
            return []

        q = {i: pre[i - 1] for i in range(1, k + 1)}
        r = {i: post[i - 1] for i in range(1, k + 1)}

        d = {i: "d" + str(t_count) + "_" + str(i) for i in range(1, k - 1)}
        a = {i: "a" + str(t_count) + "_" + str(i) for i in range(2, k)}
        b = {i: "b" + str(t_count) + "_" + str(i) for i in range(2, k)}
        states.extend(list(d.values()))
        states.extend(list(a.values()))
        states.extend(list(b.values()))

        transitions.append(Utils.transition((q[1], q[2]), (d[1], a[2])))  # forth_1
        transitions.append(Utils.transition((d[1], a[2]), (q[1], q[2])))  # forth_1 reversed
        for l in range(2, k - 1):
            transitions.append(Utils.transition((a[l], q[l + 1]), (d[l], a[l + 1])))  # forth
            transitions.append(Utils.transition((d[l], a[l + 1]), (a[l], q[l + 1])))  # forth reversed
        transitions.append(Utils.transition((d[1], b[2]), (r[1], r[2])))  # back_1
        for l in range(2, k - 1):
            transitions.append(Utils.transition((d[l], b[l + 1]), (b[l], r[l + 1])))  # back
        transitions.append(Utils.transition((a[k - 1], q[k]), (b[k - 1], r[k])))  # success
        return transitions

    rep_a = rep(a)
    addk([x], [Qp[i] for i in rep_a], states, transitions, t_count, R[0])
    t_count = t_count+1
    addk([x], [Qp[i] for i in rep_a], states, transitions, t_count, R[1])
    t_count = t_count+1
    addk([x], [Qp[i] for i in rep_a], states, transitions, t_count, R[2])
    t_count = t_count+1

    rep_b = rep(b)
    addk([y], [Qm[i] for i in rep_b], states, transitions, t_count, R[0])
    t_count = t_count+1
    addk([y], [Qm[i] for i in rep_b], states, transitions, t_count, R[1])
    t_count = t_count+1
    addk([y], [Qm[i] for i in rep_b], states, transitions, t_count, R[2])
    t_count = t_count+1

    leaders = {Qp[i]: 1 for i in rep(c)}
    leaders[pn0] = 4*n + 2

    return {
        "title":         "Logarithmic threshold protocol with leaders + fast",
        "states":        states,
        "transitions":   transitions,
        "initialStates": X,
        "leaders":       leaders,
        "trueStates":    Qp + [y0, x],
        "predicate":     "{}*C[x] - {}*C[y] + {} > 0".format(a, b, c),
        "description":   "Threshold protocol for {}x - {}y + {} > 0".format(a, b, c)
    }
