#-*- coding: utf-8 -*-
params = {
  "scheme": {
    "k": {
      "descr":  "gap parameter",
      "values": list(range(0, 6, 1)),
      "value":  3
    }
  }
}

def generateProtocol(params):
  k = params["scheme"]["k"]["value"]

  def is_leader(q):
    return len(q) == 1

  def value(q):
    return q[0]

  def output(q):
    return value(q) == 2**k if is_leader(q) else q[1]
    
  def name(q):
    return f'{value(q)}' if is_leader(q) else f'{value(q)}_{"T" if q[1] else "F"}'

  # Generate states
  leaders = [(v,) for v in range(-1, 2**k + 1)]
  passive = [(v,o) for v in range(-1, 2**k + 1) for o in [True, False]]
  states = leaders + passive

  # Generate transitions
  transitions = []

  for i in range(len(states)):
    for j in range(i, len(states)):
      p = states[i]
      q = states[j]
      pre  = (name(p), name(q))
      collected = max(-1, min(2**k, value(p) + value(q)))
      rest = value(p) + value(q) - collected
      p2 = (collected, output(p))
      q2 = (rest, output(q))
      if is_leader(p) or is_leader(q):
        p2 = (collected,)
        q2 = (rest, output(p2))
      elif collected == 2**k:
        q2 = (rest, True)
      elif collected == -1:
        q2 = (rest, False)
      post = (name(p2), name(q2))
      t = Utils.transition(pre, post)
      if (not Utils.silent(pre, post) and t not in transitions):
        transitions.append(t)

  initial_states = [(-1,), (1,)]
  true_states = [q for q in states if output(q)]
  predicate = f'C[1] - C[-1] >= {2**k}'

  # Generate description
  description = """Compute if there is a large majority, i.e. if Y - N >= 2^k. Note: This protocol is fast (O(n^2 log n)) but large (~2^k states)."""

  # Generate style
  style = {name(q): {} for q in states}

  for q in states:
    scale = abs(value(q)) / 2**k
    style[name(q)]["size"] = 0.5 + scale * 0.7
    if value(q) != 0:
      style[name(q)]["size"] = style[name(q)]["size"] + 0.3
    #style[name(q)]["color"] = f'rgb(0, 255, 0)' if q in true_states else f'rgb(255, 0, 0)' if value(q) < 0 else f'rgb(0, 0, 255)'
    style[name(q)]["color"] = f'rgb(0, 255, 0)' if q in true_states else f'rgb(255, 0, 0)' if value(q) < 0 else f'rgb(180, 180, 180)' if value(q) == 0 else f'rgb(0, 0, 255)'


  return {
    "title":         f'Large Majority (fast): Y - N >= {2**k}',
    "states":        [name(q) for q in states],
    "transitions":   transitions,
    "initialStates": [name(q) for q in initial_states],
    "trueStates":    [name(q) for q in true_states],
    "predicate":     predicate,
    "description":   description,
    "statesStyle":   style
  }