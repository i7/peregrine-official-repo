# EDIT_WYSIWYG_ONLY
#-*- coding: utf-8 -*-
# This file has been created by Peregrine.
# Do not edit manually!
def generateProtocol(params):
    return {
      "title":         "Inequality through majority",
      "states":        ["A", "A2", "B", "B2", "C", "c", "a", "b"],
      "transitions":   [Utils.transition(("A", "A"), ("A2", "A2")),
                        Utils.transition(("A2", "A2"), ("A", "A")),
                        Utils.transition(("B", "B"), ("B2", "B2")),
                        Utils.transition(("B2", "B2"), ("B", "B")),
                        Utils.transition(("A", "B"), ("C", "C")),
                        Utils.transition(("A", "C"), ("A", "a")),
                        Utils.transition(("A", "c"), ("A", "a")),
                        Utils.transition(("A", "b"), ("A", "c")),
                        Utils.transition(("B", "C"), ("B", "b")),
                        Utils.transition(("B", "c"), ("B", "b")),
                        Utils.transition(("B", "a"), ("B", "c")),
                        Utils.transition(("C", "a"), ("C", "c")),
                        Utils.transition(("C", "b"), ("C", "c"))],
      "initialStates": ["A", "B"],
      "trueStates":    ["A", "A2", "B", "B2", "a", "b"],
      "predicate":     "C[A] != C[B]",
      "leaders" : {},
      "description":   "This protocol computes if there are a different number of agents in A and B. It employs a majority protocol as a subprotocol. In case of inequality, at least one agent will remain in A or B, and all other agents will eventually enter a or b, respectively. In case of equality, at least one agent will remain in C and all other agents will eventually enter state c. To create a split in the stage graph for illustrative purposes, states A and B have alternative states A2 and B2."
    }
