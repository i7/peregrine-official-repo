# -*- coding: utf-8 -*-
import math

class Utils:
    @staticmethod
    def transition(pre, post):
        return {
            "name": "{}, {} -> {}, {}".format(pre[0], pre[1], post[0], post[1]),
            "pre":  [pre[0],  pre[1]],
            "post": [post[0], post[1]]
        }

params = {
    "scheme": {
        "a": {
            "descr":  "coeff. a",
            "values": list(range(0, 129)) + [256,512,1024,2048,4096],
            "value":  5
        },

        "b": {
            "descr":  "coeff. b",
            "values": list(range(0, 129)) + [256,512,1024,2048,4096],
            "value":  3
        },

        "c": {
            "descr":  "offset c",
            "values": list(range(-2, 129)) + [256,512,1024,2048,4096],
            "value":  2
        },

        "k": {
            "descr":  "mod k",
            "values": list(range(2, 129)) + [256,512,1024,2048,4096],
            "value":  7
        }
    }
}

def generateProtocol(params):
    a = params["scheme"]["a"]["value"]
    b = params["scheme"]["b"]["value"]
    k = params["scheme"]["k"]["value"]
    c = params["scheme"]["c"]["value"]
    k = max(k, 2)
    a %= k
    b %= k
    c %= k

    n = max(2, int(math.ceil(math.log(k, 2))))

    def rep(n):
        binary = lambda n: n > 0 and [n & 1] + binary(n >> 1) or []
        binary_n = binary(n)
        return [i for i in range(len(binary_n)) if binary_n[i] == 1]

    Q = [str(2**z) for z in range(0,n+1)]

    x = "x"
    y = "y"
    X = [x, y]

    p0 = "0+"
    m0 = "0-"
    R = [p0, m0]

    states = X + Q + R
    transitions = []

    for i in range(0, n):
        transitions.append(Utils.transition((Q[i], Q[i]), (Q[i+1], m0)))     # up_plus
        for r in R:
            transitions.append(Utils.transition((Q[i+1], r), (Q[i], Q[i])))  # down_plus

    for i in range(0, n+1):
        transitions.append(Utils.transition((Q[i], p0), (Q[i], m0)))  # signal_minus

    transitions.append(Utils.transition((m0, p0), (p0, p0)))  # signal

    t_count = 0

    def addk(pre, post, states, transitions, t_count, default):
        # make sure len(pre) == len(post)
        if len(pre) > len(post):
            post = post + [default] * (len(pre) - len(post))
        if len(pre) < len(post):
            pre = pre + [default] * (len(post) - len(pre))

        k = len(pre)

        if k == 0:
            return []

        if k == 1:
            transitions.append(Utils.transition(pre + [default], post + [default]))
            return []

        if k == 2:
            transitions.append(Utils.transition(pre, post))
            return []

        q = {i: pre[i - 1] for i in range(1, k + 1)}
        r = {i: post[i - 1] for i in range(1, k + 1)}

        d = {i: "d" + str(t_count) + "_" + str(i) for i in range(1, k - 1)}
        a = {i: "a" + str(t_count) + "_" + str(i) for i in range(2, k)}
        b = {i: "b" + str(t_count) + "_" + str(i) for i in range(2, k)}
        states.extend(list(d.values()))
        states.extend(list(a.values()))
        states.extend(list(b.values()))

        transitions.append(Utils.transition((q[1], q[2]), (d[1], a[2])))  # forth_1
        transitions.append(Utils.transition((d[1], a[2]), (q[1], q[2])))  # forth_1 reversed
        for l in range(2, k - 1):
            transitions.append(Utils.transition((a[l], q[l + 1]), (d[l], a[l + 1])))  # forth
            transitions.append(Utils.transition((d[l], a[l + 1]), (a[l], q[l + 1])))  # forth reversed
        transitions.append(Utils.transition((d[1], b[2]), (r[1], r[2])))  # back_1
        for l in range(2, k - 1):
            transitions.append(Utils.transition((d[l], b[l + 1]), (b[l], r[l + 1])))  # back
        transitions.append(Utils.transition((a[k - 1], q[k]), (b[k - 1], r[k])))  # success
        return transitions

    rep_k = rep(k)
    addk([Q[i] for i in rep_k], [], states, transitions, t_count, p0)
    t_count = t_count+1
    if (len(rep_k) == 1):
      addk([Q[i] for i in rep_k], [], states, transitions, t_count, m0)
      t_count = t_count + 1

    rep_a = rep(a)
    addk([x], [Q[i] for i in rep_a], states, transitions, t_count, R[0])
    t_count = t_count+1
    addk([x], [Q[i] for i in rep_a], states, transitions, t_count, R[1])
    t_count = t_count+1

    rep_b = rep(b)
    addk([y], [Q[i] for i in rep_b], states, transitions, t_count, R[0])
    t_count = t_count+1
    addk([y], [Q[i] for i in rep_b], states, transitions, t_count, R[1])
    t_count = t_count+1

    leaders = {}
    if c != 0:
        leaders = {Q[i]: 1 for i in rep(k-c)}
    leaders[p0] = 2*n + 2

    return {
        "title":         "Logarithmic remainder protocol with leaders",
        "states":        states,
        "transitions":   transitions,
        "initialStates": X,
        "leaders":       leaders,
        "trueStates":    [p0],
        "predicate":     "{}*C[x] + {}*C[y] %=_{} {}".format(a, b, k, c),
        "description":   "Remainder protocol for {}x + {}y = {} (mod {})".format(a, b, c, k)
    }

# print(generateProtocol({
#     "scheme": {
#         "a": {
#             "value": 5
#         },
#         "b": {
#             "value": 3
#         },
#         "c": {
#             "value": 2
#         },
#         "k": {
#             "value": 7
#         }
#     }
# }))
