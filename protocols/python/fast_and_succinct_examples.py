#-*- coding: utf-8 -*-
import math
import operator

params = {
  'scheme': {
    'example': {
      'descr':  "Prepared Example",
      'values': [1,2,3],
      'value':  1
    },
    'helper_scaling_factor': {
      'descr':  "Start with more helpers (factor)",
      'values': list(range(1,10)),
      'value':  1
    }
  }
}

def smallest_power_of_2_geq(x):
    return int(round(2**(int(math.ceil(math.log(x, 2))))))


def bits(x):  # lvls that are set in the binary rep. of a number, example: bits(10) = [1,3] because b0101 = 10
    assert int(x) == x
    x = abs(x)
    string = str(bin(abs(x)))[2:][::-1]
    return [i for i in range(len(string)) if string[i] == "1"]


def states_from_transitions(transitions):
    states = set()
    for pre, post in transitions:
        for q in pre:
            states.add(q)
        for q in post:
            states.add(q)
    return states


def to_2_way(protocol):
    transitions = protocol["transitions"]

    def multi_name(q,times):
        return f'{q}_x{times}'
    def commited_name(q,t,s=None):
        if s is None:
            return f'{q}_t{t}'
        return f'{q}_t{t}_{s}'

    new_transitions = []
    multi_states = {}
    for t in transitions:
        pre, post = t
        if len(pre) > 2:
            assert len(set(pre[1:])) == 1, "a transition doesn't have the form: q, p, p, ..., p -> q1, q2, ...qk"
            if pre[1] not in multi_states:
                multi_states[pre[1]] = len(pre)-1
            else:
                assert multi_states[pre[1]] == len(pre)-1, "multistates need to have same collection size!"
            assert pre[1] not in protocol["outputs"][True] and pre[1] not in protocol["outputs"][False], "output states may not stack"
    for t in transitions:
        pre, post = t
        assert len(pre) > 2 or pre[0] == pre[1] or pre[0] not in multi_states or pre[1] not in multi_states, "trans. may not use multiple multistates"

    def name(q):
        return multi_name(q, 1) if q in multi_states else q

    # collect multistates
    for q in multi_states:
        multi = multi_states[q]
        for times1 in range(1, multi):
            for times2 in range(times1, multi):
                ntimes1 = min(multi, times1 + times2)
                ntimes2 = times1 + times2 - ntimes1
                new_transitions.append(([multi_name(q, times1), multi_name(q, times2)],
                                        [multi_name(q, ntimes1), multi_name(q, ntimes2)]))

    # modify transitions
    for i in range(len(transitions)):
        t = transitions[i]
        pre, post = t
        if len(pre) == 2 and pre[0] not in multi_states and pre[1] not in multi_states:     # type 1
            new_transitions.append((pre, [name(q) for q in post]))
        elif len(pre) == 2:     # type 2
            p = pre[0]
            q = pre[1]
            if p in multi_states:
                p = pre[1]
                q = pre[0]
            multi = multi_states[q]
            pn = post[0]
            qn = post[1]
            for times in range(1, multi+1):
                new_transitions.append(([p, multi_name(q, times)],
                                        [commited_name(q,i), multi_name(q, times-1)]))
            new_transitions.append(([commited_name(q,i), multi_name(q, 0)],
                                    [name(pn), name(qn)]))
        elif pre[0] not in multi_states:     # type 3
            p = pre[0]
            q = pre[1]
            multi = multi_states[q]
            new_transitions.append(([p, multi_name(q, multi)],
                                    [name(post[0]), commited_name(q, i, 1)]))
            for i2 in range(1, len(pre) - 2):
                new_transitions.append(([commited_name(q, i, i2), multi_name(q, 0)],
                                        [commited_name(q, i, i2+1), name(post[i2])]))
            new_transitions.append(([commited_name(q, i, len(pre)-1), multi_name(q, 0)],
                                    [name(post[len(pre)-2]), name(post[len(pre)-1])]))
        else:     # type 4
            assert pre[0] == pre[1]
            q = pre[1]
            multi = multi_states[q]
            for times in range(1, multi+1):
                new_transitions.append(([multi_name(q, times), multi_name(q, multi)],
                                        [multi_name(q, times-1), commited_name(q, i, 1)]))
            for i2 in range(1, len(pre) - 1):
                new_transitions.append(([commited_name(q, i, i2), multi_name(q, 0)],
                                        [commited_name(q, i, i2 + 1), name(post[i2])]))
            new_transitions.append(([commited_name(q, i, len(pre)-1), multi_name(q, 0)],
                                    [name(post[0]), name(post[len(pre)-1])]))

    return {"inputs": {key: name(protocol["inputs"][key]) for key in protocol["inputs"]},
            "outputs": protocol["outputs"].copy(),
            "transitions": new_transitions,
            "leaders": {name(q): protocol["leaders"][q] for q in protocol["leaders"]}
            }


def to_everyone_has_output(protocol, G=None):
    transitions = protocol["transitions"]
    states = states_from_transitions(transitions)
    output_trues = protocol["outputs"][True]
    output_falses = protocol["outputs"][False]
    output_all = output_trues + output_falses

    new_transitions = []

    def name(q, o, charge=False):
        #return f'{q}_oo{"T" if o else "F"}{"C" if charge else ""}'
        return f'{q}{"C" if charge else ""}{" " if o else ""}'

    def with_o(q, o, charge=False):
        if q in output_all:
            return q
        else:
            return name(q, o, charge)

    if G is not None:
        assert G in states, "G must be a state"
        assert G not in output_all, "G mustn't be an output state"
        for t in transitions:
            pre, post = t
            assert all([q != G for q in pre]), "G mustn't have outgoing transitions"

        for q2 in output_trues:
            new_transitions.append(([name(G, False, True), q2], [name(G, True, True), q2]))
            new_transitions.append(([name(G, False, False), q2], [name(G, True, True), q2]))
            new_transitions.append(([name(G, True, False), q2], [name(G, True, True), q2]))
        for q2 in output_falses:
            new_transitions.append(([name(G, True, True), q2], [name(G, False, True), q2]))
            new_transitions.append(([name(G, True, False), q2], [name(G, False, True), q2]))
            new_transitions.append(([name(G, False, False), q2], [name(G, False, True), q2]))

        new_transitions.append(([name(G, False, True), name(G, True, True)], [name(G, False, False), name(G, True, False)]))

        for q in states:
            if q in output_all:
                continue
            new_transitions.append(([name(q, False), name(G, True, True)], [name(q, True), name(G, True, False)]))
            new_transitions.append(([name(q, True), name(G, False, True)], [name(q, False), name(G, False, False)]))

    # convert non outputs
    for q in states:
        if q in output_all:
            continue
        if G is not None and q == G:    # special
            continue
        for q2 in output_trues:
            new_transitions.append(([name(q, False), q2], [name(q, True), q2]))
        for q2 in output_falses:
            new_transitions.append(([name(q, True), q2], [name(q, False), q2]))

    # allow transitions with every output combination
    for t in transitions:
        pre, post = t
        assert len(pre) == 2, "need to convert to 2-way first!"
        for o1 in {True, False} if pre[0] not in output_all or post[0] not in output_all else {False}:
            for o2 in {True, False} if pre[1] not in output_all or post[1] not in output_all else {False}:
                nt = ([with_o(pre[0], o1), with_o(pre[1], o2)], [with_o(post[0], o1), with_o(post[1], o2)])
                new_transitions.append(nt)

    new_inputs = {key: with_o(protocol["inputs"][key], False) for key in protocol["inputs"]}
    new_outputs = {True: output_trues +
                         [with_o(q, True) for q in states if q not in output_trues and q not in output_falses],
                   False: output_falses +
                          [with_o(q, False) for q in states if q not in output_trues and q not in output_falses]}
    if G is not None:
        new_outputs[True].append(name(G, True, True))
        new_outputs[False].append(name(G, False, True))

    new_leaders = {with_o(q, False): protocol["leaders"][q] for q in protocol["leaders"]}

    return {"inputs": new_inputs,
            "outputs": new_outputs,
            "transitions": new_transitions,
            "leaders": new_leaders
            }


def alpha_threshold(sub_id, alpha, vs, threshold, H="H", G="G"):
    assert 1 > alpha > 0

    m = max(abs(threshold), max([abs(f) for f in vs.values()]))  # max abs. value in predicate
    h = math.ceil(math.log(m / (1 - alpha), 2)) + 1

    NOTIFY = True
    SLEEP = False

    def in_name(lvl, sign):
        assert sign != 0
        return f's{sub_id}_i_{"m" if sign < 0 else "p"}{lvl}'

    def ladder_name(lvl, sign, notify):
        assert sign != 0
        return f's{sub_id}_l_{"m" if sign < 0 else "p"}{lvl}{"_N" if notify else ""}'

    Z = f's{sub_id}_l_Z'    # agent with value zero in ladder
    ZN = f's{sub_id}_l_ZN'  # agent with value zero in ladder that needs to notify a output leader
    zeros = [Z, ZN]

    def output_calc_name(lvl, geq_c = None):
        if geq_c is None:
            return f's{sub_id}_o_{lvl}'
        return f's{sub_id}_o_{lvl}_{"geq" if geq_c else "le"}'

    def output_reset_name(lvl):
        return f's{sub_id}_o_{lvl}_R'

    def output_lvl_leader_name(lvl, val, notify):
        assert -1 <= val <= 1
        return f's{sub_id}_oL_{lvl}_{"m" if val < 0 else ""}{abs(val)}{"_N" if notify else ""}'

    def local_helper_name(returned):
        return f's{sub_id}_H_{returned}'

    transitions = []

    output_initial = output_calc_name(h)
    output_reset = output_reset_name(h)
    output_true = f's{sub_id}_o_T'
    output_false = f's{sub_id}_o_F'
    output_all_calc = {output_initial}
    output_all_reset = {output_reset_name(lvl) for lvl in range(h+1)}

    # inputs
    inputs = {}
    for v in vs:
        coef = vs[v]
        if coef == 0:
            inputs[0] = local_helper_name(0)   # directly "used" because agent has no value
            continue
        f = 1 if coef > 0 else -1
        for lvl in bits(coef):
            if (lvl, f) not in inputs:
                inputs[(lvl, f)] = in_name(lvl, f)

                # process value
                for z in zeros:
                    transitions.append(([in_name(lvl, f), z],
                                        [local_helper_name(0), ladder_name(lvl, f, NOTIFY)]))

    # local helper return: early return of helpers to minimize "stuck" agents
    need_2_return = int(math.ceil(1/(1-alpha))) - 1
    for returned in range(0, need_2_return):
        for returned2 in range(returned, need_2_return):
            returned_sum = returned + returned2
            if returned_sum < need_2_return:
                # return a helper
                transitions.append(([local_helper_name(returned), local_helper_name(returned2)],
                                    [local_helper_name(returned_sum+1), H]))
            else:
                # give one agents enough to be used in this subprotocol
                transitions.append(([local_helper_name(returned), local_helper_name(returned2)],
                                    [local_helper_name(need_2_return), local_helper_name(returned_sum - need_2_return)]))

    # use agents that we may keep in this subprotocol to populate bit ladder
    transitions.append(([local_helper_name(need_2_return)] * (h+1), [Z] * (h+1)))

    # ladder
    for n1 in [NOTIFY, SLEEP]:
        for n2 in [NOTIFY, SLEEP]:
            for lvl in range(h + 1):
                # cancel
                transitions.append(([ladder_name(lvl, -1, n1), ladder_name(lvl, 1, n2)], [Z, ZN]))
                if lvl < h:
                    # up
                    transitions.append(([ladder_name(lvl, 1, n1), ladder_name(lvl, 1, n2)],
                                        [ladder_name(lvl+1, 1, SLEEP), ZN]))
                    transitions.append(([ladder_name(lvl, -1, n1), ladder_name(lvl, -1, n2)],
                                        [ladder_name(lvl+1, -1, SLEEP), ZN]))
            
            # cancel second highest
            transitions.append(([ladder_name(h, 1, n1), ladder_name(h-1, -1, n2)],
                                        [ladder_name(h-1, 1, SLEEP), ZN]))
            transitions.append(([ladder_name(h, -1, n1), ladder_name(h-1, 1, n2)],
                                        [ladder_name(h-1, -1, SLEEP), ZN]))

    # output calculation (checking: >= threshold)
    def suc_4_cur_value(lvl, cur_value):
        min_possible_val = cur_value - ((2 ** lvl) - 1)
        max_possible_val = cur_value + ((2 ** lvl) - 1)
        if min_possible_val >= threshold:
            return output_true, None
        elif max_possible_val < threshold:
            return output_false, None
        else:
            return output_calc_name(lvl - 1, cur_value >= threshold), cur_value >= threshold
    #   init (highest lvl)
    reachable = set()   # set to remember what output states in next level
    for val in [-1, 0, 1]:
        cur_val = val * (2**h)
        suc, r = suc_4_cur_value(h, cur_val)
        transitions.append(([output_lvl_leader_name(h, val, SLEEP), output_initial],
                            [output_lvl_leader_name(h, val, SLEEP), suc]))
        if r is not None:
            reachable.add(r)
            output_all_calc.add(suc)
    #   other lvls
    for lvl in range(h-1,-1,-1):
        last_reachable = {x for x in reachable}
        if len(last_reachable) == 0:
            break
        reachable = set()
        for geq_c in last_reachable:
            last_min_possible_val = threshold - ((2**(lvl+1)) - 1)
            last_max_possible_val = threshold + ((2**(lvl+1)) - 1) - 1
            last_value = last_max_possible_val - (last_max_possible_val % (2**(lvl+1)))
            if last_value >= threshold and not geq_c:
                last_value = last_value - (2**(lvl+1))
            for val in [-1, 0, 1]:
                cur_val = last_value + val * (2 ** lvl)
                suc, r = suc_4_cur_value(lvl, cur_val)
                transitions.append(([output_lvl_leader_name(lvl, val, SLEEP), output_calc_name(lvl, geq_c)],
                                    [output_lvl_leader_name(lvl, val, SLEEP), suc]))
                if r is not None:
                    reachable.add(r)
                    output_all_calc.add(suc)

    output_all = {output_true, output_false} | output_all_calc | output_all_reset

    # output helper -> output leader
    output_all_list = list(output_all)
    for i1 in range(len(output_all_list)):
        o1 = output_all_list[i1]
        for i2 in range(i1, len(output_all_list)):
            o2 = output_all_list[i2]
            transitions.append(([o1, o2], [output_reset, G]))

    #  output lvl helper -> output lvl leader
    for lvl in range(h+1):
        for v1 in [-1,0,1]:
            for v2 in range(v1,2):
                for n1 in [NOTIFY, SLEEP]:
                    for n2 in [NOTIFY, SLEEP]:
                        transitions.append(([output_lvl_leader_name(lvl, v1, n1), output_lvl_leader_name(lvl, v2, n2)],
                                            [output_lvl_leader_name(lvl, 0, NOTIFY), G]))

    # notifications
    notifying_states = [ladder_name(lvl, sign, NOTIFY) for lvl in range(h+1) for sign in [-1,1]] + [ZN]
    notified = {ladder_name(lvl, sign, NOTIFY): ladder_name(lvl, sign, SLEEP) for lvl in range(h+1) for sign in [-1,1]}
    notified[ZN] = Z
    for i1 in range(len(notifying_states)):
        q1 = notifying_states[i1]
        for i2 in range(i1, len(notifying_states)):
            q2 = notifying_states[i2]
            # 2 notifies can cancel
            transitions.append(([q1, q2], [notified[q1], q2]))

    for o in output_all:
        for q in notifying_states:
            # ladder notify -> reset output
            transitions.append(([o, q], [output_reset, notified[q]]))
    for o in output_all_calc | {output_true, output_false}:
        for lvl in range(h + 1):
            for v1 in [-1, 0, 1]:
                # output level leader notify -> restart output calc
                transitions.append(([o, output_lvl_leader_name(lvl, v1, NOTIFY)],
                                    [output_initial, output_lvl_leader_name(lvl, v1, SLEEP)]))

    # update output lvl leaders
    for lvl in range(h+1):
        for sign in [-1, 1]:
            transitions.append(([ladder_name(lvl, sign, SLEEP), output_lvl_leader_name(lvl, 0, SLEEP)],
                                [ladder_name(lvl, sign, SLEEP), output_lvl_leader_name(lvl, sign, NOTIFY)]))

    # reset output lvl leaders
    for lvl in range(h + 1):
        next = output_reset_name(lvl-1) if lvl > 0 else output_initial
        for val in [-1, 0, 1]:
            for n1 in [NOTIFY, SLEEP]:
                transitions.append(([output_reset_name(lvl), output_lvl_leader_name(lvl, val, n1)],
                                    [next, output_lvl_leader_name(lvl, 0, SLEEP)]))

    # leaders
    leaders = {Z: h + 1, output_initial: 1, local_helper_name(0): 1}
    for lvl in range(h+1):
        leaders[output_lvl_leader_name(lvl, 0, SLEEP)] = 1

    return {"inputs": inputs,
            "outputs": {True: [output_true], False: [output_false]},
            "transitions": transitions,
            "leaders": leaders
            }


def full_protocol(predicates, boolean_comb):
    H = "H"
    G = "G"

    # collect variables
    vs = set()
    for pred in predicates:
        for v in pred["vs"]:
            vs.add(v)

    # calculate alpha
    distribution_size = {v: 0 for v in vs}
    for pred in predicates:
        for v in vs:
            if v not in pred["vs"]:
                pred["vs"][v] = 0
            coef = pred["vs"][v]
            #pred["vs"][scaled_var(v)] = input_scaling_factor * coef
            if coef == 0:
                distribution_size[v] = distribution_size[v] + 1
            else:
                distribution_size[v] = distribution_size[v] + len(bits(coef))

    max_distribution_size = max(2, max(distribution_size.values()))
    max_distribution_size_power2 = smallest_power_of_2_geq(max_distribution_size)
    alpha = (max_distribution_size_power2 - 1) / max_distribution_size_power2

    subprotocols = []
    for pred in predicates:
        if pred["type"] == "threshold":
            threshold = pred["threshold"]
            cur_vs = {}
            for v in pred["vs"]:
                cur_vs[v] = pred["vs"][v]
            subprotocols.append(alpha_threshold(len(subprotocols), alpha, cur_vs, threshold, H=H, G=G))
        else:
            assert False, "other types of predicates are not supported"

    transitions = []
    leaders = {H: max_distribution_size-1}

    # input transitions
    for v in vs:
        # input distribution
        post = []
        for i in range(len(predicates)):
            pred = predicates[i]
            coef = pred["vs"][v]
            if coef == 0:
                post.append(subprotocols[i]["inputs"][0])
            else:
                f = 1 if coef >= 0 else -1
                for lvl in bits(coef):
                    post.append(subprotocols[i]["inputs"][(lvl, f)])

        pre = [v] + (max_distribution_size - 1) * [H]
        post = post + (max_distribution_size - len(post)) * [H]
        transitions.append((pre, post))

    # add subprotocols
    for subp in subprotocols:
        for t in subp["transitions"]:
            transitions.append(t)
        for q in subp["leaders"]:
            assert q not in leaders
            leaders[q] = subp["leaders"][q]

    # boolean combination
    def output_helper_name(id, bl, br):
        return f'O{id}_{"T" if bl else "F"}{"T" if br else "F"}'

    def handle_bool(comb, next_helper_id):
        if comb in list(range(len(predicates))):
            return subprotocols[comb]["outputs"], next_helper_id
        op_string, comb_left, comb_right = comb

        outputs_left, next_helper_id = handle_bool(comb_left, next_helper_id)
        outputs_right, next_helper_id = handle_bool(comb_right, next_helper_id)

        if op_string == "&&":
            op = operator.and_
        elif op_string == "||":
            op = operator.or_
        else:
            assert False, "other boolean operators or not supported"

        # read outputs of left and right
        outputs = {True: [], False: []}
        for bl in [True, False]:
            for br in [True, False]:
                new_state = output_helper_name(next_helper_id, bl, br)
                outputs[op(bl, br)].append(new_state)
                for s in outputs_left[not bl]:
                    transitions.append(([new_state, s], [output_helper_name(next_helper_id, not bl, br), s]))
                for s in outputs_right[not br]:
                    transitions.append(([new_state, s], [output_helper_name(next_helper_id, bl, not br), s]))

        # helper -> leader
        new_states = outputs[True] + outputs[False]
        new_initial = output_helper_name(next_helper_id, False, False)
        for i1 in range(len(new_states)):
            q1 = new_states[i1]
            for i2 in range(i1, len(new_states)):
                q2 = new_states[i2]
                transitions.append(([q1, q2], [new_initial, G]))

        leaders[new_initial] = 1

        return outputs, next_helper_id + 1

    outputs, _ = handle_bool(boolean_comb, 0)

    inputs = {v: v for v in vs}
    #inputs[H] = H
    #inputs[G] = G

    return {"inputs": inputs,
            "outputs": outputs,
            "transitions": transitions,
            "leaders": leaders
            }



def tests():
    assert smallest_power_of_2_geq(31) == 32
    assert smallest_power_of_2_geq(31.9999) == 32
    assert smallest_power_of_2_geq(32) == 32
    assert smallest_power_of_2_geq(32) == 32
    assert smallest_power_of_2_geq(33) == 64

    assert bits(10) == [1, 3]
    assert bits(0) == []
    assert bits(4) == [2]


def generateProtocol(params):
    example = 1
    try:
        example = params["scheme"]["example"]["value"]
    except Exception as e:
        pass
    helper_scaling_factor = 1
    try:
        helper_scaling_factor = params["scheme"]["helper_scaling_factor"]["value"]
    except Exception as e:
        pass

    if example == 1:
        predicates = [
            {"type": "threshold",
             "threshold": 1,
             "vs": {"X": 2, "Y": -1}},
            {"type": "threshold",
             "threshold": 3,
             "vs": {"X": 1, "Y": 1}}
        ]
        boolean_comb = ("&&", 0, 1)
    elif example == 2:
        predicates = [
            {"type": "threshold",
             "threshold": 1,
             "vs": {"X": 2, "Y": -1}}
        ]
        boolean_comb = 0
    else:
        predicates = [
            {"type": "threshold",
             "threshold": 128,
             "vs": {"X": 1, "Y": -1}}
        ]
        boolean_comb = 0


    p = full_protocol(predicates, boolean_comb)
    p2 = to_2_way(p)
    p3 = to_everyone_has_output(p2, G="G")

    states = states_from_transitions(p3["transitions"])
    style = {q: {
        "size": 0.8 if str(q).count("ZN") == 1
        else 0.4 if str(q).count("Z") == 1 or str(q).count("H") == 1 or str(q).count("G") == 1
        else 1.2 if str(q).count("_oL_") == 1
        else 1.5 if str(q).count("_o_") == 1
        else 1.5 if str(q).count("O") == 1
        else 1,
        "shape": "circle" if str(q).count("s0") == 1 else "triangle" if str(q).count("s1") == 1 else "square"
    } for q in states}

    def get_pred(comb):
        if comb in list(range(len(predicates))):
            pred = predicates[comb]
            if pred["type"] == "threshold":
                threshold = pred["threshold"]
                first = True
                lin_comb_string = ""
                for v in pred["vs"]:
                    if first:
                        first = False
                    else:
                        lin_comb_string = lin_comb_string + f'{"+" if pred["vs"][v] >= 0 else ""} '
                    lin_comb_string = lin_comb_string + f'{pred["vs"][v]} * C[{p3["inputs"][v]}]'
                return f'{lin_comb_string} >= {threshold}'
            else:
                assert False, "other types of predicates are not supported"
        else:
            op_string, comb_left, comb_right = comb
            return f'{get_pred(comb_left)} {op_string} {get_pred(comb_right)}'

    res = {
        "title": "fast & succinct (aka backup protocol)",
        "states": list(states),
        "initialStates": [p3["inputs"][key] for key in p3["inputs"]],
        "trueStates": p3["outputs"][True],
        "leaders": {q: p3["leaders"][q] * helper_scaling_factor for q in p3["leaders"]},
        "predicate": get_pred(boolean_comb),
        "precondition": "true",
        "postcondition": "true",
        "description": f'for {get_pred(boolean_comb)}',
        "statesStyle": style
    }

    try:
        res["transitions"] = [Utils.transition((pre[0], pre[1]), (post[0], post[1])) for pre, post in p3["transitions"]]
    except NameError:
        pass

    return res


# OFFLINE TESTING
try:
    Utils
except NameError:
    tests()

    print("single alpha protocol:")
    res = alpha_threshold(0, 1 / 2, {"X": -1, "Y": 2}, 1)
    for t in res["transitions"]:
        print(t)
        asd = 1
    print(f'|S|={len(states_from_transitions(res["transitions"]))} |T|={len(res["transitions"])}')

    print("single alpha protocol, 2-way")
    res2 = to_2_way(res)
    for t in res2["transitions"]:
        #print(t)
        asd = 1
    print(f'|S|={len(states_from_transitions(res2["transitions"]))} |T|={len(res2["transitions"])}')

    print("single alpha protocol, 2-way, everyone has output")
    res3 = to_everyone_has_output(res2, G="G")
    for t in res3["transitions"]:
        asd = 1
        #print(t)
    print(f'|S|={len(states_from_transitions(res3["transitions"]))} |T|={len(res3["transitions"])}')

    print("single alpha protocol, 2-way, everyone has output, simple")
    res4 = to_everyone_has_output(res2)
    for t in res4["transitions"]:
        asd = 1
        # print(t)
    print(f'|S|={len(states_from_transitions(res4["transitions"]))} |T|={len(res4["transitions"])}')

    print()
    print()

    print(f'full protocol:')
    p = full_protocol(
        [
            {"type": "threshold",
             "threshold": 1,
             "vs": {"X": 2, "Y": -1}},
            {"type": "threshold",
             "threshold": 3,
             "vs": {"X": 1, "Y": 1}}
        ],
        ("&&", 0, 1)
    )
    print(f'|S|={len(states_from_transitions(p["transitions"]))} |T|={len(p["transitions"])}')

    print(f'full protocol, 2-way')
    p2 = to_2_way(p)
    print(f'|S|={len(states_from_transitions(p2["transitions"]))} |T|={len(p2["transitions"])}')

    print(f'full protocol, 2-way, everyone has output')
    p3 = to_everyone_has_output(p2, G="G")
    print(f'|S|={len(states_from_transitions(p3["transitions"]))} |T|={len(p3["transitions"])}')

    print(p3)

    print()
    print()

    print(f'generateProtocol:')
    x = generateProtocol(None)
    print(x["predicate"])
    print(x["leaders"])
    print(x["initialStates"])





