# EDIT_WYSIWYG_ONLY
#-*- coding: utf-8 -*-
# This file has been created by Peregrine.
# Do not edit manually!
def generateProtocol(params):
    return {
      "title":         "Fast Majority Voting Protocol",
      "states":        ["A_Y", "A_T", "A_N", "P_T", "P_Y", "P_N"],
      "transitions":   [Utils.transition(("A_Y", "A_T"), ("A_Y", "P_Y")),
                        Utils.transition(("A_Y", "A_N"), ("A_T", "P_T")),
                        Utils.transition(("A_T", "A_N"), ("A_N", "P_N")),
                        Utils.transition(("A_T", "A_T"), ("A_T", "P_T")),
                        Utils.transition(("A_Y", "P_N"), ("A_Y", "P_Y")),
                        Utils.transition(("A_Y", "P_T"), ("A_Y", "P_Y")),
                        Utils.transition(("A_N", "P_Y"), ("A_N", "P_N")),
                        Utils.transition(("A_N", "P_T"), ("A_N", "P_N")),
                        Utils.transition(("A_T", "P_N"), ("A_T", "P_T")),
                        Utils.transition(("A_T", "P_Y"), ("A_T", "P_T"))],
      "initialStates": ["A_Y", "A_N"],
      "trueStates":    ["A_T", "P_T", "P_Y", "A_Y"],
      "predicate":     "C[A_Y] >= C[A_N]",
      "postcondition": "true",
      "description":   "https://peregrine.model.in.tum.de/lics18/example4/",
      "statesStyle":   {
                         "A_Y": {"size": 1.2, "shape": "square"},
                         "A_N": {"size": 1.4, "shape": "triangle"},
                         "A_T": {"size": 1.2, "shape": "circle"},
                         "P_Y": {"size": 0.6, "shape": "square"},
                         "P_N": {"size": 0.7, "shape": "triangle"},
                         "P_T": {"size": 0.6, "shape": "circle"}
                       }
    }
