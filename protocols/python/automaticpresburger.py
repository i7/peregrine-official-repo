#-*- coding: utf-8 -*-
params = {
  "scheme": {
    "predicate": {
      "descr":  "predicate to transform",
      "values": [],
      "value":  "C[x] + C[y] %=_2 0 && 2*C[x] - C[z] %=_3 0"
    }
  }
}

from lark import Lark, Transformer, v_args
from z3 import *
import operator
from timeit import default_timer as timer


class Utils2:
    @staticmethod
    def order(pair):
        if (str(pair[0]) > str(pair[1])):
            pair = pair[::-1]
        return pair

    @staticmethod
    def transition(pre, post):
        pre = Utils2.order(pre)
        post = Utils2.order(post)
        return {
            "name": "{}, {} -> {}, {}".format(pre[0], pre[1], post[0], post[1]),
            "pre":  [pre[0],  pre[1]],
            "post": [post[0], post[1]]
        }

    @staticmethod
    def silent(t):
        pre = t["pre"]
        post = t["post"]
        return (pre[0] == post[0] and pre[1] == post[1])

    @staticmethod
    def equal(trans1, trans2):
        return ((trans1["pre"][0] == trans2["pre"][0] and trans1["pre"][1] == trans2["pre"][1])
                and
                (trans1["post"][0] == trans2["post"][0] and trans1["post"][1] == trans2["post"][1]))

    @staticmethod
    def newHash():
        return [{}, {}, {}, {}, {}, {"dupT": 0, "silT": 0, "start": timer()}]

    @staticmethod
    def addT(pre, post, hash):

        def insert(map, key, value):
            if key not in map:
                map[key] = [value]
            else:
                map[key].append(value)

        transH, preH, presH, postH, postsH, other = hash
        t = Utils2.transition(pre, post)
        pre = t["pre"]
        post = t["post"]
        name = t["name"]
        if Utils2.silent(t):
            other["silT"] = other["silT"] + 1
            return
        if name in transH:
            other["dupT"] = other["dupT"] + 1
            return
        transH[name] = t
        insert(preH, pre[0], t)
        insert(preH, pre[1], t)
        insert(presH, str(pre), t)
        insert(postH, post[0], t)
        insert(postH, post[1], t)
        insert(postsH, str(post), t)

    @staticmethod
    def removeT(t, hash):

        def remove(map, key, value):
            if key not in map:
                return
            list = map[key]
            list.remove(value)
            if len(list) == 0:
                map.pop(key)

        transH, preH, presH, postH, postsH, other = hash
        pre = t["pre"]
        post = t["post"]
        name = t["name"]
        if name not in transH:
            return
        transH.pop(name)
        remove(presH, str(pre), t)
        remove(preH, pre[0], t)
        remove(preH, pre[1], t)
        remove(postsH, str(post), t)
        remove(postH, post[0], t)
        remove(postH, post[1], t)

    @staticmethod
    def removeS(s, hash):
        transH, preH, presH, postH, postsH, other = hash
        if s in preH:
            to_rm = preH[s][:] # need to copy because preH is changed in loop
            for t in to_rm:
                Utils2.removeT(t, hash)
        if s in postH:
            to_rm = postH[s][:] # need to copy because postH is changed in loop
            for t in to_rm:
                Utils2.removeT(t, hash)

    @staticmethod
    def optimize(protocol, hash):
        tGen = timer() - hash[5]["start"]
        transH, preH, presH, postH, postsH, other = hash
        states = protocol["states"]
        initialStates = protocol["initialStates"]
        trueStates = protocol["trueStates"]
        neutralState = protocol["neutralState"]

        # optimazation1: reachability
        # -> remove all states that are not reachable
        #       all unreachable states are introduced when handling input states
        reachable = {}
        WS = {s: True for s in initialStates}  # start from initial
        WS[neutralState] = True  # ... and neutral state
        while len(WS) != 0:
            cur, _ = WS.popitem()
            reachable[cur] = True

            for s in reachable:
                preHashString = str(Utils2.order([cur, s]))
                if preHashString in presH:
                    for t in presH[preHashString]:
                        new1 = t["post"][0]
                        if new1 not in reachable and new1 not in WS:
                            WS[new1] = True
                        new2 = t["post"][1]
                        if new2 not in reachable and new2 not in WS:
                            WS[new2] = True

        for s in states:
            if s not in reachable:
                Utils2.removeS(s, hash)

        #protocol["description"] = protocol["description"] + " #sil:" + str(other["silT"]) + " #dup:" + str(other["dupT"]) + " #unreachable:" + str([len(states) - len(reachable), len(protocol["transitions"]) - len(hash[0])]) + " {" + str(tGen) + "," + str(timer() - hash[5]["start"])
        protocol["transitions"] = list(hash[0].values())
        protocol["states"] = reachable.keys()
        protocol["trueStates"] = [s for s in trueStates if s in reachable]

def TrueProtocol():
    return {
        "title": "True",
        "states": ["T"],
        "transitions": [],
        "initialStates": ["T"],
        "trueStates": ["T"],
        "neutralState": "T",
        "predicate": "0 < 1",
        "description": """true"""
    }


def FalseProtocol():
    return {
        "title": "False",
        "states": ["F"],
        "transitions": [],
        "initialStates": ["F"],
        "trueStates": [],
        "neutralState": "F",
        "predicate": "1 < 0",
        "description": """false"""
    }


def generateThresholdProtocol(coeffs, c, op, op_str):
    coeffs = {x_i: a_i for (x_i, a_i) in coeffs}
    a = [coeffs[x_i] for x_i in coeffs]
    s1 = min(min(a), c - 1)
    s2 = max(max(a), c + 1)

    # Helper functions
    def is_boolean(q):
        return q == "t" or q == "f"

    def is_tuple(q):
        return isinstance(q, tuple)

    def is_value(q):
        return not is_boolean(q) and not is_tuple(q)

    def value(q):
        if is_boolean(q):
            return 0
        return q[0] if is_tuple(q) else q

    def output(q):
        if is_boolean(q):
            return q == "t"
        return q[1] if is_tuple(q) else op(value(q), c)

    def f(m, n):
        return max(s1, min(s2, value(m) + value(n)))

    def g(m, n):
        return value(m) + value(n) - f(m, n)

    def postFor(x, y):
        if x in coeffs or y in coeffs:
            post1 = x
            post2 = y
            if x in coeffs:
                post1 = coeffs[x]
            if y in coeffs:
                post2 = coeffs[y]
            return (post1, post2)
        if not is_value(x) and not is_value(y):
            return None
        xn = f(x, y)
        yn = g(x, y)
        if value(yn) == 0:
            yn = "t" if output(xn) else "f"
            return xn, yn
        if output(xn) != output(yn):
            yn = (yn, output(xn))
        return xn, yn

    neutral_state = "t" if op(0,c) else "f"
    initial_states = {var for var in coeffs}

    states = set()
    WS = initial_states | {neutral_state}  # start from initial and neutral state
    hash = Utils2.newHash()

    while len(WS) != 0:
        cur = WS.pop()
        states.add(cur)

        for other in states:
            pre = (cur, other)
            if cur == 1 and other == "t" or cur == "t" and other == 1:
                asdasd = 1
            post = postFor(cur, other)
            if post is not None:
                new1, new2 = post
                Utils2.addT(pre, post, hash)
                if new1 not in states:
                    WS.add(new1)
                if new2 not in states:
                    WS.add(new2)

    # Generate true states
    true_states = [q for q in states - initial_states if output(q)] + [var for var in coeffs if op(coeffs[var], c)]

    # Generate predicate
    expr = ["{0}*C[{1}]".format(coeffs[var], var) for var in coeffs]
    predicate = "{} {} {}".format(" + ".join(expr), op_str, c)

    protocol = {
        "title": "Threshold protocol for " + op_str,
        "states": list(states),
        "transitions": list(hash[0].values()),
        "initialStates": list(initial_states),
        "trueStates": list(true_states),
        "neutralState": neutral_state,
        "predicate": predicate,
        "description": predicate
    }
    Utils2.optimize(protocol, hash)
    return protocol


def generateModProtocol(coeffs, c, m):
    m = m
    c %= m

    # Generate states
    trueState = "T"
    falseState = "F"
    boolean = [falseState, trueState]
    variable = [var for (var, coeff) in coeffs]
    value_for = {var: coeff for (var, coeff) in coeffs}
    neutralState = 0

    # Generate transitions
    hash = Utils2.newHash()

    def postFor(s1, s2):
        def booleanFor(b):
            return trueState if b else falseState

        if s1 in variable or s2 in variable:
            post1 = s1
            post2 = s2
            if s1 in variable:
                post1 = value_for[s1] % m
            if s2 in variable:
                post2 = value_for[s2] % m
            return (post1, post2)
        if s1 not in boolean and s2 not in boolean:
            return ((s1 + s2) % m, booleanFor(((s1 + s2) % m) == (c % m)))
        if s1 in boolean and s2 not in boolean:
            return (booleanFor(s2 == (c % m)), s2)
        if s1 not in boolean and s2 in boolean:
            return (s1, booleanFor(s1 == (c % m)))
        return None

    states = {}
    WS = {str(s): s for s in variable}  # start from initial
    WS[str(neutralState)] = neutralState  # ... and neutral state
    while len(WS) != 0:
        curS, cur = WS.popitem()
        states[curS] = cur

        for sString in states:
            other = states[sString]
            post = postFor(cur, other)
            if post is not None:
                Utils2.addT((cur, other), post, hash)
                new1, new2 = post
                if str(new1) not in states and str(new1) not in WS:
                    WS[str(new1)] = new1
                if str(new2) not in states and str(new2) not in WS:
                    WS[str(new2)] = new2

    # Generate true states
    true_states = [c % m, trueState] + [var for (var, coeff) in coeffs if coeff % m == c]

    # Generate predicate
    expr = ["{0}*C[{1}]".format(coeff, var) for (var, coeff) in coeffs]
    predicate = "({}) %=_{} {}".format(" + ".join(expr), m, c)

    protocol = {
        "title": "Remainder protocol",
        "states": list(states.values()),
        "transitions": list(hash[0].values()),
        "initialStates": list(variable),
        "trueStates": list(true_states),
        "neutralState": 0,
        "predicate": predicate,
        "description": predicate
    }
    Utils2.optimize(protocol, hash)
    return protocol


def generateNegProtocol(prot):
    prot["trueStates"] = [s for s in prot["states"] if s not in prot["trueStates"]]
    prot["predicate"] = "!(" + prot["predicate"] + ")"
    prot["description"] = "!(" + prot["description"] + ")"
    return prot


def generateBinaryProt(prot1, prot2, op, op_str):
    # helper functions
    def label(s):
        if s[0] in prot1["initialStates"] and s[1] in prot2["initialStates"] and s[0] == s[1]:
            return s[0]
        if s[0] in prot1["initialStates"] and s[0] not in prot2["initialStates"] and s[1] == prot2["neutralState"]:
            return s[0]
        if s[1] in prot2["initialStates"] and s[1] not in prot1["initialStates"] and s[0] == prot1["neutralState"]:
            return s[1]
        return "(" + str(s[0]) + "," + str(s[1]) + ")"

    hash = Utils2.newHash()

    # neutral state
    neutralState = (prot1["neutralState"], prot2["neutralState"])

    # initial states
    initial = []
    for i in prot1["initialStates"]:    # from prot1
        i2 = (i, i)
        if i not in prot2["initialStates"]:
            # not an initial state in prot2 -> use neutral state
            i2 = (i, prot2["neutralState"])
        if i2 not in initial:
            initial.append(i2)
    for i in prot2["initialStates"]:    # from prot2 (must use neutral state)
        if i not in prot1["initialStates"]:
            initial.append((prot1["neutralState"], i))

    # build lookup structures for protocols
    def build_lookup(transitions):
        lookup = {}
        for t in transitions:
            key = str(Utils2.order((t["pre"][0], t["pre"][1])))
            value = Utils2.order((t["post"][0], t["post"][1]))
            if key not in lookup:
                lookup[key] = [value]
            else:
                map[key].append(value)

        return lookup

    prot1_lookup = build_lookup(prot1["transitions"])
    prot2_lookup = build_lookup(prot2["transitions"])

    # generate reachable states and transitions using work set algorithm
    states = {}
    WS = {str(s): s for s in initial}   # start from initial
    WS[str(neutralState)] = neutralState    # ... and neutral state
    while len(WS) != 0:
        curString, cur = WS.popitem()
        states[curString] = cur

        p1, q1 = cur

        for (p2, q2) in states.values():    # combine with each already reachable state
            # get possible successors for prot1
            p_next = [(p1, p2)]
            p_key = str(Utils2.order((p1, p2)))
            if p_key in prot1_lookup:
                p_next = prot1_lookup[p_key]
            # get possible successors for prot2
            q_next = [(q1, q2)]
            q_key = str(Utils2.order((q1, q2)))
            if q_key in prot2_lookup:
                q_next = prot2_lookup[q_key]

            # for each possible successor
            for (p1n, p2n) in p_next:
                for (q1n, q2n) in q_next:
                    pre = Utils2.order(list(map(label, ((p1, q1), (p2, q2)))))
                    post = Utils2.order(list(map(label, ((p1n, q1n), (p2n, q2n)))))
                    # add transition
                    Utils2.addT(pre, post, hash)

                    # add states if new
                    new1 = (p1n, q1n)
                    new2 = (p2n, q2n)
                    if str(new1) not in states and str(new1) not in WS:
                        WS[str(new1)] = new1
                    if str(new2) not in states and str(new2) not in WS:
                        WS[str(new2)] = new2

    # true states
    trueStates = [s for s in states.values()
                  if op(s[0] in prot1["trueStates"], s[1] in prot2["trueStates"])]

    protocol = {
        "title": "Automatically generated protocol",
        "states": list(map(label, states.values())),
        "transitions": list(hash[0].values()),
        "initialStates": list(map(label, initial)),
        "trueStates": list(map(label, trueStates)),
        "neutralState": label(neutralState),
        "predicate": "(" + prot1["predicate"] + ") " + op_str + " (" + prot2["predicate"] + ")",
        "description": "(" + prot1["description"] + ") " + op_str + " (" + prot2["description"] + ")"
    }
    Utils2.optimize(protocol, hash)
    return protocol


def presburgerToProtocol(formular):
    #simplify formular (simplification only works with z3 4.8 and above)
    z3_version = get_version()
    simplified_formular = formular
    do_symplify = True
    if do_symplify and (z3_version[0] > 5 or z3_version[0] == 4 and z3_version[1] >= 8):
        simplified_formular = simplify(formular, arith_lhs=True)

    # help function
    def getCooefficient(expression):  # can be multiplication of variable with constant or just a single variable or an int value
        if expression.decl().name() == '*':
            if expression.children()[0].decl().name() != 'Int':
                raise ArgumentError("Muliplication is only allowed with a constant factor.")
            coeff = expression.children()[0].as_long()
            var = expression.children()[1].decl().name()
            return [(var, coeff)]
        if (len(expression.children()) == 0):
            if expression.decl().name() == 'Int':   # int
                return expression.as_long()
            return [(expression.decl().name(), 1)]  # only variable
        raise ArgumentError("Expected (scaled) variable..." + str(simplified_formular))

    binary_ops = {
        "and": (operator.iand, "&&"),
        "or": (operator.ior, "||"),
        "=>": (lambda x, y: operator.ior(operator.not_(x), y), "->")
    }
    comparisions = {
        "<": operator.lt,
        "<=": operator.le,
        "=": operator.eq,
        ">": operator.gt,
        ">=": operator.ge
    }

    name = simplified_formular.decl().name()

    if name == 'true':
        return TrueProtocol()
    if name == 'false':
        return FalseProtocol()
    if name == 'not':
        return generateNegProtocol(
            presburgerToProtocol(simplified_formular.children()[0])
        )
    if name in binary_ops:
        op, op_str = binary_ops[name]
        res = generateBinaryProt(
            presburgerToProtocol(simplified_formular.children()[0]),
            presburgerToProtocol(simplified_formular.children()[1]),
            op,
            op_str
        )
        next = 2
        while next < len(simplified_formular.children()):
            res = generateBinaryProt(
                res,
                presburgerToProtocol(simplified_formular.children()[next]),
                op,
                op_str
            )
            next = next + 1
        return res
    if (name == '=' and simplified_formular.children()[0].decl().name() == 'mod'):  # mod
        if (simplified_formular.children()[0].children()[1].decl().name() != 'Int'):
            raise ArgumentError("The mode of a modulo expression has to be constant.")
        if (simplified_formular.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a modulo expression has to be constant.")

        c = simplified_formular.children()[1].as_long()
        m = simplified_formular.children()[0].children()[1].as_long()
        lhs = simplified_formular.children()[0].children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):
            for exp in lhs.children():
                cur_res = getCooefficient(exp)
                if not isinstance(cur_res, list):
                    c = (c - cur_res) % m
                else:
                    coeffs = coeffs + cur_res
        else:
            cur_res = getCooefficient(lhs)
            if not isinstance(cur_res, list):
                c = (c - cur_res) % m
            else:
                coeffs = cur_res
            coeffs = getCooefficient(lhs)
        return generateModProtocol(coeffs, c, m)
    if (name in comparisions):
        if (simplified_formular.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a comparision has to be constant.")

        c = simplified_formular.children()[1].as_long()
        lhs = simplified_formular.children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):  # sum
            for exp in lhs.children():
                coeffs = coeffs + getCooefficient(exp)
        else:  # just 1 var
            coeffs = getCooefficient(lhs)
        return generateThresholdProtocol(coeffs, c, comparisions[name], name)


predicate_grammar = """
        ?start: pred

        ?pred: and_pred
             | pred "||" and_pred   -> op_or

        ?and_pred: bool_exp
             | and_pred "&&" bool_exp   -> op_and    

        ?bool_exp: comp
             | "!" bool_exp             -> op_not
             | "(" pred ")"      
             | ("True" | "true")        -> op_true
             | ("False" | "false")      -> op_false

        ?comp: exp "=" exp            -> eq
             | exp "!=" exp           -> neq
             | exp ">" exp            -> gt
             | exp "<" exp            -> lt
             | exp "<=" exp           -> le
             | exp ">=" exp           -> ge
             | exp "%" INT "=" exp    -> mod

        ?exp: product
             | exp "+" product   -> add
             | exp "-" product   -> sub

        ?product: atom
            | product "*" atom  -> mul

        ?atom: S_INT            -> number
             | "-" atom         -> neg
             | "C[" /[^\\]]+/ "]"     -> var
             | LETTER           -> var
             | "(" exp ")"

        %import common.LETTER -> LETTER
        %import common.DIGIT -> DIGIT
        %import common.INT -> INT
        %import common.SIGNED_INT -> S_INT
        %import common.WS_INLINE

        %ignore WS_INLINE
    """


@v_args(inline=True)    # Affects the signatures of the methods
class GenerateZ3Predicate(Transformer):
    def op_and(self, l, r):
        return And(l, r)

    def op_or(self, l, r):
        return Or(l, r)

    def op_not(self, e):
        return Not(e)

    def op_true(self):
        return BoolVal(True)

    def op_false(self):
        return BoolVal(False)

    def eq(self, l, r):
        return l == r

    def neq(self, l, r):
        return l != r

    def gt(self, l, r):
        return l > r

    def lt(self, l, r):
        return l < r

    def ge(self, l, r):
        return l >= r

    def le(self, l, r):
        return l <= r

    def mod(self, l, m, r):
        return l % IntVal(str(m)) == r % IntVal(str(m))

    def add(self, l, r):
        return l + r

    def sub(self, l, r):
        return l - r

    def mul(self, l, r):
        return l * r

    def neg(self, e):
        return -e

    def var(self, e):
        return Int(str(e))

    def number(self, e):
        return IntVal(str(e))

parser = Lark(predicate_grammar, parser='lalr', transformer=GenerateZ3Predicate())
parse = parser.parse

def generateProtocol(params):
  predicate = params["scheme"]["predicate"]["value"]

  string = predicate
  while string.find("%=_") != -1:
      mod_loc = string.find("%=_")
      number_loc = mod_loc
      while not string[number_loc].isdigit():
          number_loc = number_loc + 1
      number_end = number_loc
      while string[number_end].isdigit():
          number_end = number_end + 1

      string = string[0:mod_loc] + "%" + string[number_loc:number_end] + "=" + string[number_end:]

  try:
    z3 = parse(string)

    return presburgerToProtocol(z3)
  except Exception as e:
    return {
      "title": "True",
      "states": ["T"],
      "transitions": [],
      "initialStates": ["T"],
      "trueStates": ["T"],
      "neutralState": "T",
      "predicate": "0 < 1",
      "description": str(e)
    }

## OFFLINE TESTING
try:
    Utils
except NameError:
    z3 = parse("x = y")
    p = presburgerToProtocol(z3)
    print(p)
    print(len(p["transitions"]))
