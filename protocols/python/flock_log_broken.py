# -*- coding: utf-8 -*-
import math

params = {
  "scheme": {
    "threshold": {
      "descr":  "Threshold c",
      "values": list(range(1, 201)),
      "value":  13
    }
  }
}

def generateProtocol(params):
  c = params["scheme"]["threshold"]["value"]
  i = int(math.log(c, 2))
  states = [0] + [2**j for j in range(0, i + 1)]

  cur_state = 2**i

  for j in range(0, i):
    if c & 2**j:
      cur_state += 2**j
      states.append(cur_state)

  transitions = []

  for x in range(len(states)):
    for y in range(x, len(states)):
      q1 = states[x]
      q2 = states[y]
      sum = q1 + q2
      pre = (q1, q2)
      post = None

      if sum < c and q1 != 0 and q2 != 0 and sum in states:
        post = (sum, 0)
      elif sum >= c:
        post = (c, c)

      if (post is not None) and (not Utils.silent(pre, post)):
        transitions.append(Utils.transition(pre, post))

  style = {q: {} for q in states}

  for q in states:
    style[q]["size"] = 0.75 + 0.8 * (q + 1) / (c + 1)
    if q < c:
      style[q]["color"] = "rgb({}, {}, {})".format(105 + 128 * (c - q) / c, 30, 99)

  style[0]["color"] = "rgb(175, 175, 175)"

  return {
    "title": 				 "Logarithmic flock-of-birds protocol (broken)",
    "states":        states,
    "transitions":   transitions,
    "initialStates": [1],
    "trueStates":    [c],
    "predicate":     "C[1] >= {}".format(c),
    "description":   """This protocol attempts to compute whether the population size
                        is at least c. The protocol has O(log c) states. The protocol
                        is incorrect for some thresholds c.""",
    "statesStyle": style
  }
