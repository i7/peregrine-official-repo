# -*- coding: utf-8 -*-
params = {
  "scheme": {
    "threshold": {
      "descr":  "Threshold c",
      "values": list(range(1, 201)),
      "value":  7
    }
  }
}

def generateProtocol(params):
  c = params["scheme"]["threshold"]["value"]

  # Generate states
  powers = [0]
  r = c
  i = 0

  while r > 0:
    powers.append(2**i)
    r //= 2
    i += 1

  states = list(powers)

  if c not in states:
    states.append(c)

  # Generate transitions
  transitions = [Utils.transition((q, q), (2*q, 0)) for q in powers[1:-1]]

  for i in range(0, len(powers)):
    for j in range(i, len(powers)):
      p = powers[i]
      q = powers[j]

      if (p != c or q != c) and (p + q >= c):
        transitions.append(Utils.transition((p, q), (c, c)))

  if c > powers[-1]:
    for q in powers:
      if q != c:
        transitions.append(Utils.transition((c, q), (c, c)))

  style = {q: {} for q in states}

  for q in states:
    style[q]["size"] = 0.75 + 0.8 * (q + 1) / (c + 1)
    if q < c:
      style[q]["color"] = "rgb({}, {}, {})".format(105 + 128 * (c - q) / c, 30, 99)

  style[0]["color"] = "rgb(175, 175, 175)"

  return {
    "title":         "Logarithmic flock-of-birds protocol (very broken)",
    "states":        states,
    "transitions":   transitions,
    "initialStates": [1],
    "trueStates":    [c],
    "predicate":     "C[1] >= {}".format(c),
    "description":   """This protocol attempts to compute whether the population size is
                        at least c. The protocol has O(log c) states. The protocol is
                        incorrect for some thresholds c.""",
    "statesStyle": style
  }
