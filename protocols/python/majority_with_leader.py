#-*- coding: utf-8 -*-
params = {
  "scheme": {
    "predicate": {
      "descr":  "Predicate",
      "values": ["Y > N", "Y >= N", "Y < N", "Y <= N", "Y = N", "Y != N"],
      "value":  "Y >= N"
    }
  }
}

def generateProtocol(params):
  predicate = params["scheme"]["predicate"]["value"]

  states = ["Y", "N", "L", "L_Y", "L_N", "0", "0_Y", "0_N"] # List of states

  # List of transitions
  transitions = [Utils.transition(("L", "N"), ("L_N", "0_N")),
                 Utils.transition(("L", "Y"), ("L_Y", "0_Y")),
                 Utils.transition(("L_Y", "N"), ("L", "0")),
                 Utils.transition(("L_N", "Y"), ("L", "0")),
                 Utils.transition(("L", "0_N"), ("L", "0")),
                 Utils.transition(("L", "0_Y"), ("L", "0")),
                 Utils.transition(("L_Y", "0_N"), ("L_Y", "0_Y")),
                 Utils.transition(("L_Y", "0"), ("L_Y", "0_Y")),
                 Utils.transition(("L_N", "0_Y"), ("L_N", "0_N")),
                 Utils.transition(("L_N", "0"), ("L_N", "0_N"))]

  initialStates = ["Y", "N"] # List of initial states
  
  moreY = ["Y", "L_Y", "0_Y"]
  moreN = ["N", "L_N", "0_N"]
  equal = ["L", "0"]
  
  leaderStates = ["L", "L_N", "L_Y"]
  passiveStates = ["0", "0_N", "0_Y"]
  
  trueStates = []
  if predicate in ["Y > N", "Y >= N", "Y != N"]:
    trueStates = trueStates + moreY
  if predicate in ["Y = N", "Y >= N", "Y <= N"]:
    trueStates = trueStates + equal
  if predicate in ["Y < N", "Y <= N", "Y != N"]:
    trueStates = trueStates + moreN
    
   
  Y_color = "rgb(0, 0, 255)"
  Y_color_light = "rgb(180, 140, 255)"
  N_color = "rgb(255, 0, 0)"
  N_color_light = "rgb(255, 140, 140)"
  eq_color = "rgb(255, 0, 255)"
  eq_color_light = "rgb(255, 140, 255)"
  
  style = {q: 
           {
             "size": 1.3 if q in leaderStates else 0.9 if q in initialStates else 0.5, 
             "color": Y_color if q in moreY else N_color if q in moreN else eq_color, 
             "fillcolor": "rgba(255, 255, 255, 0.8)", 
             "shape": "square" if q in leaderStates else "circle"
           } for q in states
          }
  
  for q in leaderStates:
    style[q]["fillcolor"] = "rgba(255, 255, 255, 0.3)"

  return {
    "title":         "Majority (with leader)",
    "states":        states,
    "transitions":   transitions,
    "initialStates": initialStates,
    "trueStates":    trueStates,
    "predicate":     f'C[Y]{predicate[1:-1]}C[N]',
    "precondition":  "true",
    "postcondition": "true",
    "leaders":       {"L" : 1},
    "description":   """This protocol computes the majority using a leader. It can easily distinguish between all three results: A > B, A = B, A < B""",
    "statesStyle": style
  }