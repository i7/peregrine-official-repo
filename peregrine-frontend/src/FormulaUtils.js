import teal from "@material-ui/core/colors/teal";
import purple from "@material-ui/core/colors/purple";

class FormulaUtils {
  static print(predicate) {
    let newPred = predicate.trim();

    newPred = newPred.replace(/\s*>=\s*(?![^[]*\])/g, " ≥ ");
    newPred = newPred.replace(/\s*<=\s*(?![^[]*\])/g, " ≤ ");
    newPred = newPred.replace(/\s*!=\s*(?![^[]*\])/g, " ≠ ");
    newPred = newPred.replace(/\s*%=_([0-9])+\s*(?![^[]*\])/g, " ≡<sub>mod $1</sub> ");
    newPred = newPred.replace(/\s*->\s*(?![^[]*\])/g, " ⇒ ");
    newPred = newPred.replace(/\s*&&\s*(?![^[]*\])/g, " ∧ ");
    newPred = newPred.replace(/\s*\|\|\s*(?![^[]*\])/g, " ∨ ");
    newPred = newPred.replace(/\s*\+\s*(?![^[]*\])/g, " + ");
    newPred = newPred.replace(/\s*-\s+(?![^[]*\])/g, " - ");
    newPred = newPred.replace(/\s*\*\s*(?![^[]*\])/g, "·");
    newPred = newPred.replace(/!(?![^[]*\])/g, "¬");
    newPred = newPred.replace(
      /true(?![^[]*\])/g,
      "<span style='color: " + purple[700] + ";'><em>true</em></span>"
    );
    newPred = newPred.replace(
      /false(?![^[]*\])/g,
      "<span style='color: " + purple[700] + ";'><em>false</em></span>"
    );
    newPred = newPred.replace(
      /C\[(.*?)\]/g,
      "C[<span style='font-family: Courier New; font-weight: bold; color: " +
        teal[700] +
        ";'>$1</span>]"
    );

    return newPred;
  }

}

export { FormulaUtils };
