import React from "react";
import TextField from "@material-ui/core/TextField";
import { FormulaUtils } from "./FormulaUtils";
import * as Style from "./PopulationStyle";

class InfoEditForm extends React.Component {
  render() {
    return (
      <div>
        <TextField
          placeholder="Title of the protocol"
          errorText={
            this.props.title && this.props.title.trim().length !== 0
              ? ""
              : "Required"
          }
          errorStyle={{ color: "rgb(0, 188, 212)" }}
          label="Title"
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          style={{ width: "50%" }}
          value={this.props.title ? this.props.title : ""}
          onChange={(event) => this.props.onTitleChange(event.target.value)}
        />
        <div style={{ height: 20 }} />
        <TextField
          placeholder="Description of the protocol, e.g.: This protocol computes [...]"
          label="Description"
          margin="normal"
          style={{ width: "50%" }}
          InputLabelProps={{
            shrink: true,
          }}
          multiline
          minRows={4}
          maxRows={8}
          value={this.props.description ? this.props.description : ""}
          onChange={(event) =>
            this.props.onDescriptionChange(event.target.value)
          }
        />
      </div>
    );
  }
}

class FormulaEditForm extends React.Component {
  render() {
    const syntax = [
      "C[q]",
      "=, !=, >=, <=, >, <",
      "!, &&, ||",
      "true, false, 0, 1, 2, …",
      "+, -, *",
      "%=_2, %=_3, …",
    ];
    const semantics = [
      "<span>number of agents initially in state q</span>",
      "=, !=, >=, <=, >, <",
      "!, &&, ||",
      "true, false, 0, 1, 2, …",
      "+, -, constant * C[q]",
      "%=_2, %=_3, …",
    ];
    const examples = [
      ["C[q] = 1"],
      ["C[p] = C[q]", "C[p] >= C[q]", "C[p] < 3"],
      ["(C[p] > 0 && C[q] = 5) || C[p] = C[q]", "!(C[p] <= 3) || (C[q] = 5)"],
      ["C[p] = 3 || false"],
      ["C[p] + C[q] >= 2 * C[r]", "C[p] + C[q] - C[r] < 5"],
      ["C[p] + C[q] %=_2 1", "C[p] %=_3 C[q] + 2 * C[r]"],
    ];

    return (
      <div>
        <TextField
          style={{ width: "50%" }}
          placeholder="Predicate computed by the protocol"
          errorText={
            this.props.formula && this.props.formula.trim().length !== 0
              ? ""
              : "Required"
          }
          errorStyle={{ color: "rgb(0, 188, 212)" }}
          value={this.props.formula ? this.props.formula : ""}
          label="Predicate"
          required
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(event) => this.props.onFormulaChange(event.target.value)}
        />
        <br /> <br />
        <TextField
          style={{ width: "50%" }}
          placeholder="Precondition for protocol"
          errorText={
            this.props.formula && this.props.formula.trim().length !== 0
              ? ""
              : "Required"
          }
          errorStyle={{ color: "rgb(0, 188, 212)" }}
          value={this.props.precondition ? this.props.precondition : ""}
          label="Precondition"
          required
          InputLabelProps={{
            shrink: true,
          }}
          onChange={(event) =>
            this.props.onPreconditionChange(event.target.value)
          }
        />
        <div style={{ height: 20 }} />
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "auto auto auto",
            alignItems: "center",
            gridRowGap: "8px",
            gridColumnGap: "5px",
            border: "3px solid #CDCDCD",
            padding: 5,
            fontFamily: Style.MAIN_FONT,
            fontSize: "14px",
          }}
        >
          <div style={{ textAlign: "center", fontWeight: "bold" }}>Syntax</div>
          <div style={{ textAlign: "center", fontWeight: "bold" }}>
            Semantics
          </div>
          <div style={{ textAlign: "left", fontWeight: "bold" }}>Examples</div>

          {syntax.map((e, i) => [
            <div style={{ textAlign: "center", fontFamily: "Courier New" }}>
              {syntax[i]}
            </div>,
            <div
              style={{ textAlign: "center" }}
              dangerouslySetInnerHTML={{
                __html: FormulaUtils.print(semantics[i]),
              }}
            ></div>,
            <div
              style={{ textAlign: "left" }}
              dangerouslySetInnerHTML={{
                __html: examples[i]
                  .map((e) => "• " + FormulaUtils.print(e))
                  .join("<br />"),
              }}
            ></div>,
          ])}
        </div>
      </div>
    );
  }
}

export { InfoEditForm, FormulaEditForm };
