import React from "react";
import ParameterPicker from "./ParameterPicker";
import * as Style from "./PopulationStyle";

class ProtocolDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.state = { params: this.props.params };
  }

  handleParamChange(params) {
    let newParams = Object.assign({}, this.state.params);
    Object.keys(params).map(k => newParams['scheme'][k].value = params[k]);
    this.setState(
      { params: newParams },
        this.props.onParamsChange(newParams)
      );
  }

  isParametric() {
    return "scheme" in this.state.params && this.state.params["scheme"];
  }

  renderProtocolFamilyParameters() {
    if (this.isParametric()) {
      return (
        <div style={{ display: "flex", alignSelf: "flex-start" }}>
          <ParameterPicker
            parameters={this.state.params["scheme"]}
            onParamChange={this.handleParamChange.bind(this)}
          />
        </div>
      );
    } else {
      return <div>None</div>;
    }
  }

  static getDerivedStateFromProps(nextProps, state) {
    let newParams = { configuration: {} };

    if (nextProps["params"]) {
      if (nextProps.params["configuration"]) {
        for (let q of Object.keys(nextProps.params.configuration)) {
          newParams["configuration"][q] = state.params.configuration[q]
            ? state.params.configuration[q]
            : nextProps.params.configuration[q];
        }
      }

      if (nextProps.params["scheme"]) {
        newParams["scheme"] = nextProps.params["scheme"];
      }
    }
    return { params: newParams };
  }

  render() {
    const protocolStyle = {
      marginTop: "10px",
      padding: "10px 10px 0px 10px",
      backgroundColor: "#FAFAFA",
      boxShadow: Style.BOX_SHADOW,
    };

    const subtitle = {
      fontWeight: "bold",
    };

    let protocol = this.props.protocolGen(this.state.params);
    return (
      <div style={protocolStyle}>
        <div
          style={{
            fontFamily: Style.MAIN_FONT,
            fontWeight: "bold",
            paddingBottom: "15px",
          }}
        >
          {protocol.title}
        </div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "max-content auto",
            alignItems: "center",
            gridColumnGap: "10px",
            fontFamily: Style.MAIN_FONT,
            fontSize: "14px",
          }}
        >
          <div style={subtitle}>Description:</div>
          <div style={{ marginBottom: 10 }}>{protocol.description}</div>
          {this.isParametric() ? <div style={subtitle}>Parameters:</div> : ""}
          {this.isParametric() ? (
            <div>{this.renderProtocolFamilyParameters()}</div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  }
}

export default ProtocolDisplay;
