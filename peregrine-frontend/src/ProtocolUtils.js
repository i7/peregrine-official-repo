class ProtocolUtils {
  // Tests whether the pair of states pre enables transition t
  static enablesTransition(pre, t) {
    return (
      (t.pre[0] === pre[0] && t.pre[1] === pre[1]) ||
      (t.pre[0] === pre[1] && t.pre[1] === pre[0])
    );
  }

  // Tests whether transition t is silent, e.g. whether is has no effect
  static isSilent(t) {
    return (
      (t.pre[0] === t.post[0] && t.pre[1] === t.post[1]) ||
      (t.pre[0] === t.post[1] && t.pre[1] === t.post[0])
    );
  }

  // Tests whether transitions s and t are equivalent
  // e.g. AB -> CD, AB -> DC, BA -> CD, BA -> DC are all equivalent
  static equivTransitions(s, t) {
    return (
      ((s.pre[0] === t.pre[0] && s.pre[1] === t.pre[1]) ||
        (s.pre[0] === t.pre[1] && s.pre[1] === t.pre[0])) &&
      ((s.post[0] === t.post[0] && s.post[1] === t.post[1]) ||
        (s.post[0] === t.post[1] && s.post[1] === t.post[0]))
    );
  }

  static getEnabledTransitionsForCoordinates(protocol, population, x, y) {
    const m = protocol.transitions.length;
    let transitions = [];
    for (let t = 0; t < m; t++) {
      const transition = protocol.transitions[t];
      const enabled = ProtocolUtils.enablesTransition(
        [population[x], population[y]],
        transition
      );

      if (x !== y && enabled) {
        transitions.push({ x: x, y: y, transition: transition });
      }
    }
    return transitions;
  }

  static enabledPointedTransitions(protocol, population) {
    const n = population.length;

    let transitions = [];

    for (let x = 0; x < n; x++) {
      for (let y = 0; y < n; y++) {
        transitions.push.apply(
          transitions,
          ProtocolUtils.getEnabledTransitionsForCoordinates(
            protocol,
            population,
            x,
            y
          )
        );
      }
    }
    return transitions;
  }

  static findAgentsForTransition(protocol, population, transition) {
    const n = population.length;
    let q = [null, null];

    for (let i = 0; i < n; i++) {
      if (q[0] === null && transition.pre[0] === population[i]) {
        q[0] = i;
      } else if (q[1] === null && transition.pre[1] === population[i]) {
        q[1] = i;
      }

      if (q[0] !== null && q[1] !== null) {
        break;
      }
    }

    return q;
  }

  static transitionsForPredecessors(protocol, q1, q2) {
    let transitions = [];

    for (let t of protocol.transitions) {
      // If transition enabled by (q1, q2) and if t not already in transitions,
      // then add transition
      if (
        ProtocolUtils.enablesTransition([q1, q2], t) &&
        transitions.find((s) => ProtocolUtils.equivTransitions(s, t)) ===
          undefined
      ) {
        transitions.push(t);
      }
    }

    return transitions;
  }

  static applyPointedTransition(protocol, population, pointedTransition) {
    const transition = protocol.transitions[pointedTransition.transition];
    return ProtocolUtils.applyTransitionAt(
      population,
      transition,
      pointedTransition.x,
      pointedTransition.y
    );
  }

  static applyTransitionAt(population, transition, x, y) {
    const populationSucc = population.slice();

    populationSucc[x] = transition.post[0];
    populationSucc[y] = transition.post[1];

    return populationSucc;
  }

  static undoTransitionAt(population, transition, x, y) {
    const populationPred = population.slice();

    populationPred[x] = transition.pre[0];
    populationPred[y] = transition.pre[1];

    return populationPred;
  }

  static findTransitionByName(protocol, transitionName) {
    for (let t of protocol.transitions) {
      if (t.name === transitionName) {
        return t;
      }
    }

    return null;
  }

  // Construct history from given execution
  static historyFromExecution(protocol, initialPopulation, execution) {
    let history = [[initialPopulation, null]];
    for (let i = 0; i < execution.length; i++) {
      const population = history[history.length - 1][0];
      const transition = ProtocolUtils.findTransitionByName(
        protocol,
        execution[i]
      );
      const agents = ProtocolUtils.findAgentsForTransition(
        protocol,
        population,
        transition
      );
      const populationSucc = ProtocolUtils.applyTransitionAt(
        population,
        transition,
        agents[0],
        agents[1]
      );
      history.push([populationSucc, { x: agents[0], y: agents[1] }]);
    }
    return history;
  }

  static isTrueState(protocol, state) {
    return protocol.trueStates.includes(state);
  }

  static consensus(protocol, population) {
    let curr = null;

    for (let i = 0; i < population.length; i++) {
      const q = population[i];
      const out = ProtocolUtils.isTrueState(protocol, q);

      if (curr === null || out === curr) {
        curr = out;
      } else {
        return null;
      }
    }

    return curr;
  }

  static consensusConfig(protocol, configuration) {
    let curr = null;

    for (let q in configuration) {
      if (configuration[q] > 0) {
        const out = ProtocolUtils.isTrueState(protocol, q);

        if (curr === null || out === curr) {
          curr = out;
        } else {
          return null;
        }
      }
    }

    return curr;
  }

  static population2Configuration(population, protocol) {
    let dict = {};
    for (let q of protocol.states) {
      dict[q] = 0;
    }
    for (let q of population) {
      dict[q]++;
    }
    return dict;
  }

  static isTerminalConfiguration(configuration, protocol) {
    for (let t of protocol.transitions) {
      if (
        (t.pre[0] === t.pre[1] && configuration[t.pre[0]] >= 2) ||
        (t.pre[0] !== t.pre[1] &&
          configuration[t.pre[0]] >= 1 &&
          configuration[t.pre[1]] >= 1)
      ) {
        return false;
      }
    }
    return true;
  }

  static configurationSize(configuration) {
    let size = 0;

    for (let q in configuration) {
      size += configuration[q];
    }

    return size;
  }
}

export { ProtocolUtils };
