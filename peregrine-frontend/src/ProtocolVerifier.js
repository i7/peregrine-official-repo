import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import Select from "@material-ui/core/Select";
import Icon from "@material-ui/core/Icon";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import green from "@material-ui/core/colors/green";
import orange from "@material-ui/core/colors/orange";
import red from "@material-ui/core/colors/red";
import { DeterministicRunner } from "./DeterministicRunner";
import TeX from "@matejmazur/react-katex";
import "katex/dist/katex.min.css";
import axios from "axios";
import PopulationControlDisplay from "./PopulationControlDisplay";
import { StageGraphDisplay, StageGraphUtils } from "./StageGraphDisplay";
import { ProtocolUtils } from "./ProtocolUtils";
import * as Style from "./PopulationStyle";
import * as FileSaver from "file-saver";

class ProtocolVerifier extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: "none",
      property: "correctness_stage_graphs",
      showCounterExample: false,
      };
    }

    handleVerification() {
      this.setState({status: 'loading'});
      this.requestVerification();
    };

    handlePropertyChange(value) {
      this.setState({property: value});
      this.setState({stageGraph: null});  // reset stageGraph
      this.setState({status: "none"});  // reset status
    }

    handleCounterExampleTriggered() {
      this.setState({ showCounterExample: !this.state.showCounterExample });
    }

    export() {
     var json = JSON.stringify(this.state.counterExample);
     var blob = new Blob([json], { type: "application/json;charset=utf-8" });

     FileSaver.saveAs(blob, "counterexample.json");
    }

    requestVerification() {
      this.setState({stageGraph: null});  // reset stageGraph

      let requestAnchors = { correctness: 'verification'
                           , prePostCondition: 'pre-post-verification'
                           , correctness_stage_graphs: 'verification-stage-graphs'
                           };
      axios.post(requestAnchors[this.state.property], this.props.protocol)
      .then(function (response) {
        this.setState({status: "verified"});
        if(response.data["Error"]){
          this.setState({status: "error"});
        }
        else if (response.data.status === "Success") {
          switch(response.data.result) {
            case "Verified":
              this.setState({status: "verified"});
              break;
            case "Disproven":
              this.setState({status: "disproven",
                             counterExample: response.data.counterExample});
              break;
            default: // case "Unsure"
              this.setState({status: "unsure"});
              break;
          }
          this.setState({stageGraph: response.data.stageGraph});
        }
        else if (response.data.status === "Error") {
          this.setState({status: "error"});
        }
        else if (response.data.status === "Timeout"){
          this.setState({status: "timeout"});
        }
      }.bind(this))
      .catch(function (error) {
        this.setState({status: "connecterror"});
      }.bind(this));
    }

  statusIcon() {
    const colorBad = red[400];
    const colorGood = green[700];
    const colorWarning = orange[400];
    const iconStyleGood = { fontSize: "40px", color: colorGood };
    const iconStyleBad = { fontSize: "40px", color: colorBad };

    const iconStyleWarning = { fontSize: "40px", color: colorWarning };

    switch (this.state.status) {
      case "loading":
        return <CircularProgress size={30} thickness={5} />;
      case "verified":
        return (
          <Icon className="material-icons" style={iconStyleGood}>
            check_circle
          </Icon>
        );
      case "unsure":
        return (
          <Icon className="material-icons" style={iconStyleWarning}>
            help
          </Icon>
        );
      case "disproven":
        return (
          <Icon className="material-icons" style={iconStyleBad}>
            highlight_off
          </Icon>
        );
      case "error":
        return (
          <Icon className="material-icons" style={iconStyleBad}>
            report_problem
          </Icon>
        );
      case "timeout":
        return (
          <Icon className="material-icons" style={iconStyleWarning}>
            report_problem
          </Icon>
        );
      case "connecterror":
        return (
          <Icon className="material-icons" style={iconStyleBad}>
            report_problem
          </Icon>
        );
      default:
        return "";
    }
  }

  statusMessage() {
    let propertyString = { correctness_stage_graphs: 'correctness'
                         , correctness: 'correctness'
                         , prePostCondition: 'pre-condition => post-condition'
                         };

    let engineString = { correctness: 'Peregrine'
                       , prePostCondition: 'Peregrine'
                       , correctness_stage_graphs: 'Correctness with Stage Graphs'
                       };

    const messages = {
      "loading"  : engineString[this.state.property] + " is verifying " + propertyString[this.state.property] + "...",
      "verified" : "The protocol satisfies " + propertyString[this.state.property] + ".",
      "unsure"   : `Peregrine can neither prove nor disprove whether the protocol
                    satisfies ` + propertyString[this.state.property] + ".",
      "timeout" :  "Verification exceeded timeout.",
      "disproven": "The protocol does not satisfy " + propertyString[this.state.property] + ".",
      "error"    : `An error occurred. Check whether Peregrine is properly
                    installed and whether your protocol is well-formed.`,
      connecterror: `Could not connect to the verifier (peregrine-backend).
                        Make sure it is running.`,
    };

    return this.state.status in messages ? messages[this.state.status] : "";
  }

  stageGraphMessage() {
    let stageString = (stages, upperCase = false) => {
      let s;
      if (upperCase) {
        s = "Stage";
      }
      else {
        s = "stage"
      }
      if (stages.length > 1) {
        s += "s ";
        let first = stages.slice(0, stages.length - 1);
        s += first.join(", ");
        s += " and " + stages[stages.length-1];
      }
      else {
        s += " " + stages[0];
      }
      return s;
    };
    if (this.state.stageGraph) {
      if (this.state.status === "verified") {
        return (
          <div>
            The expected number of interactions until a stable consensus is reached is{" "}
            <TeX>{StageGraphUtils.speedToLatex(this.state.stageGraph.speed)}</TeX>
            . <br/>
          </div>
        );
      }
      else if (this.state.status === "disproven") {
        let counterExamples = StageGraphUtils.collectCounterExamples(this.state.stageGraph);
        let exIds = counterExamples.map((e) => e.id);
        let trueEx = counterExamples.filter((e) => e.expectedConsensus).map((e) => e.id);
        let falseEx = counterExamples.filter((e) => !e.expectedConsensus).map((e) => e.id);
        let message = "Peregrine can disprove correctness by a counterexample in ";
        message += stageString(exIds) + ".";

        let messageEx = (stages, expected) => {
          let s = stageString(stages, true);
          s += " contain";
          if (stages.length === 1) {
            s += "s";
          }
          s += " a configuration reachable from";
          s += " an initial configuration with expected consensus " + expected + ",";
          s += " but which never reaches a stable consensus of value " + expected + ".";
          return s;
        }
        if (falseEx.length > 0) {
          message += " " + messageEx(falseEx, false);
        }
        if (trueEx.length > 0) {
          message += " " + messageEx(trueEx, true);
        }
        return (
          <div>
            {message}
          </div>
        )
      }
      else if (this.state.status === "unsure") {
        let failedStages = StageGraphUtils.collectFailedStages(this.state.stageGraph);
        let message = "Peregrine could not prove correctness,"
        message += " as it could not show progress for some configuration of " + stageString(failedStages) + ",";
        message += " but the configuration is also not a counterexample to disprove correctness."
        return (
          <div>
            {message}
          </div>
        );
      }
    }
  }

  renderStatus() {
    return (
        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
          <div style={{height: '45px', width: '45px'}}>{this.statusIcon()}</div>
          <div style={{paddingLeft: '5px', fontStyle: 'italic'}}>{this.statusMessage()}</div>
          <div style={{paddingLeft: '5px', fontStyle: 'italic'}}>{this.stageGraphMessage()}</div>
        </div>
    );
  }

  renderToggle(labelText, callback){
    return(
      <div>
        <FormControlLabel
                control={<Switch onClick={callback.bind(this)} />}
                label={labelText}
                style={{padding : '0px 25px 0px 0px'}}
              />
      </div>
    );
  }

  renderStageGraph() {
    if (this.state.property !== "correctness_stage_graphs" || this.state.stageGraph == null)
        return (<div></div>);
    return <StageGraphDisplay
      protocol={this.props.protocol}
      stageGraph={this.state.stageGraph}
      initialConfigParams={this.props.initialConfigParams}
    />
  }

  renderConfigName(i) {
    return (
      <span style={{ fontStyle: "italic" }}>
        C<sub>{i}</sub>
      </span>
    );
  }

  renderConfig(c) {
    return JSON.stringify(c);
  }

  renderExecution(exec) {
    const seq = exec.map((t) => '"' + t + '"');

    return "[" + seq.join(", ") + "]";
  }

  printConsensusValue(c) {
    let consensus = ProtocolUtils.consensusConfig(this.props.protocol, c);

    if (consensus == null) {
      return "not in a consensus";
    } else {
      return [
        "of consensus ",
        <span style={{ fontStyle: "italic" }}>
          {consensus ? "true" : "false"}
        </span>,
      ];
    }
  }

  explodeConfiguration(c) {
    return [].concat.apply(
      [],
      Object.keys(c).map((k) => Array(c[k]).fill(k))
    );
  }

  renderCorrectnessCounterExample() {
    const exec = <span style={{ fontStyle: "italic" }}>π</span>;
    return (
      <div>
        <div
          style={{
            visibility:
              this.state.property === "correctness" ? "visibile" : "hidden",
          }}
        >
          Peregrine found a finite execution {exec} from initial configuration{" "}
          {this.renderConfigName(0)} to configuration {this.renderConfigName(1)}{" "}
          that violates {this.state.property}. The protocol should reach
          consensus{" "}
          <span style={{ fontStyle: "italic" }}>
            {String(this.state.counterExample.expectedConsensusValue)}{" "}
          </span>
          from {this.renderConfigName(0)}, but instead {exec} reaches{" "}
          {this.renderConfigName(1)} which is terminal and{" "}
          {this.printConsensusValue(this.state.counterExample.c1, this)}.
          Configurations {this.renderConfigName(0)} and{" "}
          {this.renderConfigName(1)} contain{" "}
          {ProtocolUtils.configurationSize(this.state.counterExample.c0)}{" "}
          agents, and execution {exec} has length{" "}
          {this.state.counterExample.t.length}.
        </div>
        <br />
        <div style={{ padding: "20px 0px 5px 10px" }}>
          <Button
            style={{ marginRight: "5px" }}
            variant="contained"
            onClick={this.handleCounterExampleTriggered.bind(this)}
            labelPosition="before"
            icon={
              <Icon className="material-icons">
                {this.state.showCounterExample ? "expand_less" : "expand_more"}
              </Icon>
            }
          >
            Show counter-example
          </Button>
          <Button
            onClick={() => this.export()}
            variant="contained"
            icon={<Icon className="material-icons">{"save"}</Icon>}
          >
            Export
          </Button>
          {!this.state.showCounterExample ? (
            ""
          ) : (
            <div>
              <div style={{ height: "10px" }} />
              <div
                style={{
                  display: "grid",
                  gridTemplateColumns: "max-content max-content auto",
                  alignItems: "center",
                  gridColumnGap: "10px",
                  gridRowGap: "10px",
                  width: "95%",
                  backgroundColor: "#F0F0F0",
                  borderLeft: "2px solid black",
                  fontFamily: "Courier New",
                  fontSize: "14px",
                  padding: "10px",
                }}
              >
                <div>{this.renderConfigName(0)}</div>
                <div>=</div>
                <div>
                  {this.renderConfig(this.state.counterExample.startConfig)}
                </div>

                <div>{this.renderConfigName(1)}</div>
                <div>=</div>
                <div>
                  {this.renderConfig(this.state.counterExample.endConfig)}
                </div>

                <div>{exec}</div>
                <div>=</div>
                <div>{this.renderExecution(this.state.counterExample.t)}</div>
              </div>
            </div>
          )}
        </div>
        <br />
        You may replay execution {exec}:<br />
        <PopulationControlDisplay
          protocol={this.props.protocol}
          population={this.explodeConfiguration(
            this.state.counterExample.startConfig
          )}
          runnerGenerator={(protocol, population, callback) => {
            return new DeterministicRunner(
              protocol,
              population,
              this.state.counterExample.t,
              callback
            );
          }}
          manualPickerDisabled={true}
        />
      </div>
    );
  }

  renderDetails() {
    if (this.state.status === "disproven") {
      if (this.state.property === "correctness_stage_graphs") {
        return ""
      }
      else {
        return this.renderCorrectnessCounterExample();
      }
    } else {
      return "";
    }
  }

  componentDidUpdate(prevProps, prevState){
    if (prevProps !== this.props){
      this.setState({
        status: "none",
        stageGraph: null,
      });
    }
  }

  render() {
    return (
      <div style={{width: '100%', display: 'flex', flexDirection: 'column', fontFamily: Style.MAIN_FONT}}>
          <div style={{display: 'flex', alignItems: 'center', flexWrap: 'wrap'}}>
            <div>
              Property to be verified:
            </div>
            <div>
              <Select
              style={{ marginTop: "-7px", marginLeft: "10px" }}
              value={this.state.property}
              onChange={e => this.handlePropertyChange(e.target.value)}
              >
                <MenuItem value={"correctness_stage_graphs"}>
                  Correctness (with stage graphs)
                </MenuItem>
                <MenuItem value={"correctness"}>Correctness (without stage graphs)</MenuItem>
              </Select>
            </div>
            <div>
              <Button
              startIcon={
                <Icon className="material-icons">{"verified_user"}</Icon>
              }
              onClick={this.handleVerification.bind(this)}
              style={{ marginLeft: 10 }}
              color="primary"
              variant="contained"
              >
                Verify
              </Button>
            </div>
          </div>
          <div style={{paddingTop: '20px'}}>
            {this.renderDetails(this)}
          </div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-start",
            height: "30px",
            padding: "20px 0px 20px 10px",
          }}
        >
          {this.renderStatus(this)}
        </div>
        <div style={{display: 'flex', justifyContent: 'flex-start'}}>
          {this.renderStageGraph(this)}
        </div>
    </div>
    );
  }
}

export default ProtocolVerifier;
