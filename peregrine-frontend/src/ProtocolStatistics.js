import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import CircularProgress from "@material-ui/core/CircularProgress";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import axios from "axios";
import Plot from "react-plotly.js";

import * as FileSaver from "file-saver";

const NEUTRAL_COLOR = "rgb(0, 188, 212)";

const CONSENSUS_COLORS = {
  0: "rgb(25, 100, 25)",
  1: "rgb(125, 225, 125)",
  2: "rgb(250, 150, 50)",
  3: "rgb(225, 125, 125)",
  4: "rgb(220, 15, 15)",
};

const CONSENSUS_NAMES = {
  0: "Correct and lasting",
  1: "Correct",
  2: "No",
  3: "Incorrect",
  4: "Incorrect and lasting",
};

const DEFAULT_PLOT_CONFIG = {
  modeBarButtonsToRemove: [
    "sendDataToCloud",
    "zoom2d",
    "select2d",
    "lasso2d",
    "resetScale2d",
    "hoverClosestCartesian",
    "hoverCompareCartesian",
    "ToggleSpikelines",
  ],
  displaylogo: false,
};

const NUM_STATS = 200;
const PLOT_AXIS_FONT_SIZE = 18;

class ProtocolStatisticsView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      minSize: 1,
      maxSize: 25,
      numSamples: 900,
      numSteps: 650,
      increaseAccuracy: false,
      schedulingMode: "UniformTransitions",
      consensusTypes: [0, 1, 3, 4],
      statsIndex: 0,
    };
  }

  avgConvergenceSteps(stats, types) {
    let numConfigurations = {};
    let sumSteps = {};

    for (let i in stats) {
      let type = stats[i]["convergence"];

      if (types.includes(type)) {
        const size = parseInt(stats[i]["size"]);
        const numSteps = parseInt(stats[i]["steps"]);

        if (size in numConfigurations) {
          numConfigurations[size]++;
          sumSteps[size] += numSteps;
        } else {
          numConfigurations[size] = 1;
          sumSteps[size] = numSteps;
        }
      }
    }

    let avgSteps = {};

    for (let size in sumSteps) {
      avgSteps[size] = sumSteps[size] / numConfigurations[size];
    }

    return avgSteps;
  }

  calculateRatios(stats) {
    let count = {};

    for (let i in stats) {
      const size = parseInt(stats[i]["size"]);
      const type = parseInt(stats[i]["convergence"]);

      if (!(size in count)) {
        count[size] = { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0 };
      }

      count[size][type]++;
    }

    let ratios = { x: [], y: [[], [], [], [], []] };

    for (let size in count) {
      let total = 0;

      for (let type = 0; type <= 4; type++) {
        total += count[size][type];
      }

      ratios["x"].push(size);

      for (let type = 0; type <= 4; type++) {
        let ratio = (100.0 * count[size][type]) / total;

        ratios["y"][type].push(ratio);
      }
    }

    return ratios;
  }

  export(stats) {
    var json = JSON.stringify(stats);
    var blob = new Blob([json], { type: "application/json;charset=utf-8" });

    FileSaver.saveAs(blob, "statistics.json");
  }

  handleToggle() {
    this.setState({ increaseAccuracy: !this.state.increaseAccuracy });
  }

  handleToggleSchedulingMode() {
    let newMode =
      this.state.schedulingMode === "UniformTransitions"
        ? "UniformAgents"
        : "UniformTransitions";
    this.setState({ schedulingMode: newMode });
  }

  updateCheck(i) {
    let types = this.state.consensusTypes.slice();
    const index = types.indexOf(i);

    if (index >= 0) {
      types.splice(index, 1);
    } else {
      types.push(i);
    }

    this.setState({ consensusTypes: types });
  }

  makeStatisticsRequest() {
    this.setState({ statsIndex: 0 });

    this.props.requestHandler({
      minConfigSize: this.state.minSize,
      maxConfigSize: this.state.maxSize,
      sampleSize: this.state.numSamples,
      numSteps: this.state.numSteps,
      schedulingMode: this.state.schedulingMode,
      increaseAccuracy: this.state.increaseAccuracy,
    });
  }

  renderStatus(icon, msg) {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "flex-start",
          paddingTop: "10px",
          paddingLeft: "15px",
        }}
      >
        <div style={{ height: "45px", width: "45px" }}>{icon}</div>
        <div style={{ paddingLeft: "5px", fontStyle: "italic" }}>{msg}</div>
      </div>
    );
  }

  renderSelectItem(property, key) {
    const labels = {
      minSize: "Min. population size",
      maxSize: "Max. population size",
      numSamples: "Number of simulations",
      numSteps: "Number of steps",
    };

    return (
      <div key={key}>
        <TextField
          label={labels[property]}
          value={this.state[property]}
          type="number"
          min="1"
          step="1"
          onChange={(event) =>
            this.setState({
              [property]: Math.max(parseInt(event.target.value), 1),
            })
          }
          style={{ width: "185px", paddingRight: "5px" }}
        ></TextField>
      </div>
    );
  }

  renderRequestForm() {
    return (
      <div>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-around",
            flexWrap: "wrap",
          }}
        >
          <div style={{ display: "flex", flexWrap: "wrap" }}>
            {["minSize", "maxSize", "numSteps", "numSamples"].map((x, i) =>
              this.renderSelectItem(x, i)
            )}
          </div>
          <div style={{ marginTop: "20px" }}>
            <div style={{ float: "left" }}>
              <FormControlLabel
                control={<Switch onClick={this.handleToggle.bind(this)} />}
                label="Increase accuracy"
                style={{ paddingLeft: 30 }}
              />
            </div>
            <div style={{ display: "inline" }}>
              <FormControlLabel
                control={
                  <Switch
                    onClick={this.handleToggleSchedulingMode.bind(this)}
                  />
                }
                label="Uniform Agent Scheduling"
                style={{ paddingLeft: 30 }}
              />
            </div>
            <div style={{ float: "right" }}>
              <Button
                onClick={this.makeStatisticsRequest.bind(this)}
                color="primary"
                variant="contained"
              >
                Generate
              </Button>
            </div>
          </div>
        </div>
        {!this.state.increaseAccuracy
          ? ""
          : this.renderStatus(
              <Icon
                className="material-icons"
                color="secondary"
                style={{ fontSize: "40px" }}
              >
                report_problem
              </Icon>,
              "Increasing accuracy may slow down statistics generation considerably."
            )}
      </div>
    );
  }

  plotHeight() {
    return window.innerWidth > 1024 ? 400 : 333;
  }

  plotWidth() {
    return window.innerWidth > 1024 ? 800 : 700;
  }

  renderAvgConvergence(stats) {
    const avgSteps = this.avgConvergenceSteps(stats, this.state.consensusTypes);
    let xPoints = [];
    let yPoints = [];

    for (let size in avgSteps) {
      xPoints.push(parseInt(size));
      yPoints.push(avgSteps[size]);
    }

    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          overflow: "auto",
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            overflow: "auto",
          }}
        >
          <Plot
            data={[
              {
                type: "bar",
                x: xPoints,
                y: yPoints,
                marker: { color: NEUTRAL_COLOR },
                text: xPoints.map(
                  (n, i) =>
                    "Population size: " +
                    xPoints[i] +
                    "<br />" +
                    "Avg. num. steps: " +
                    yPoints[i].toFixed(2)
                ),
                hoverinfo: "text",
              },
              {
                type: "scatter",
                x: xPoints,
                y: yPoints,
                hoverinfo: "none",
                marker: { size: 10 },
                line: {
                  color: "black",
                  shape: "spline",
                  smoothing: 2.0,
                  width: 4,
                },
              },
            ]}
            layout={{
              width: this.plotWidth(),
              height: this.plotHeight(),
              xaxis: {
                autosize: true,
                title: "Population size",
                tickformat: ",d", // Only keep integer ticks
                titlefont: {
                  size: PLOT_AXIS_FONT_SIZE,
                },
              },
              yaxis: {
                title: "Avg. num. steps to consensus",
                titlefont: {
                  size: PLOT_AXIS_FONT_SIZE,
                },
              },
              showlegend: false,
            }}
            config={DEFAULT_PLOT_CONFIG}
          />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "top",
            justifyContent: "space-around",
            flexWrap: "wrap",
          }}
        >
          <div style={{ paddingRight: "30px", paddingTop: "10px" }}>
            Filter by consensus type:
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "top",
              justifyContent: "center",
              flexWrap: "wrap",
            }}
          >
            {[0, 1, 3, 4].map((i) => this.renderConsensusChoice(i))}
          </div>
        </div>
      </div>
    );
  }

  renderConsensusChoice(i) {
    return (
      <div key={i}>
        <FormControlLabel
          control={
            <Checkbox
              checked={this.state.consensusTypes.includes(i)}
              onChange={() => this.updateCheck(i)}
              style ={{
                      color: CONSENSUS_COLORS[i],
                    }}
            />
          }
          label={CONSENSUS_NAMES[i]}
        />
      </div>
    );
  }

  renderStatsDetails(stats) {
    function renderConfig(c) {
      if (c.length === 0) {
        return "{}";
      } else {
        let acc = "{";

        for (const q in c) {
          if (c[q] > 0) {
            acc += q + ": " + c[q] + ", ";
          }
        }

        return acc.substring(0, acc.length - 2) + "}";
      }
    }

    function renderSimulation(i) {
      const simulation = stats[i];
      return simulation === undefined
        ? ""
        : [
            <div
              style={{
                backgroundColor: CONSENSUS_COLORS[simulation.convergence],
                width: "15px",
              }}
              key={"misc" + i}
            >
              &nbsp;
            </div>,
            <div key={"num" + i}>{i}</div>,
            <div style={{ justifySelf: "end" }} key={"steps" + i}>{simulation.steps}</div>,
            <div key={"start" + i}>{renderConfig(simulation.startConfig)}</div>,
            <div key={"end" + i}>{renderConfig(simulation.endConfig)}</div>,
          ];
    }

    return (
      <div>
        <div style={{ height: "10px" }} />
        <div
          style={{
            display: "grid",
            gridTemplateColumns:
              "max-content max-content max-content auto auto",
            alignItems: "center",
            gridColumnGap: "10px",
            gridRowGap: "10px",
            width: "95%",
            maxHeight: "500px",
            backgroundColor: "#F0F0F0",
            borderLeft: "2px solid black",
            overflowY: "scroll",
            fontFamily: "Courier New",
            fontSize: "14px",
            padding: "10px",
          }}
        >
          <div style={{ width: "15px" }}></div>
          <div>{"#"}</div>
          <div>Steps</div>
          <div>Initial configuration</div>
          <div>Reached configuration</div>
          {Array.from(
            new Array(
              Math.max(
                Math.min(NUM_STATS, stats.length - this.state.statsIndex),
                0
              )
            ),
            (x, i) => renderSimulation(this.state.statsIndex + i)
          )}
        </div>
      </div>
    );
  }

  renderRatios(stats) {
    const ratios = this.calculateRatios(stats);
    let dataPoints = [];

    for (let type = 0; type <= 4; type++) {
      dataPoints.push({
        type: "bar",
        x: ratios["x"],
        y: ratios["y"][type],
        marker: { color: CONSENSUS_COLORS[type] },
        name: CONSENSUS_NAMES[type] + " consensus",
        text: ratios["y"][type].map(
          (n, i) =>
            "" +
            n.toFixed(2) +
            "% " +
            CONSENSUS_NAMES[type] +
            " consensus" +
            " (pop. size: " +
            ratios["x"][i] +
            ")"
        ),
        hoverinfo: "text",
      });
    }

    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          overflow: "auto",
        }}
      >
        <Plot
          data={dataPoints}
          layout={{
            width: this.plotWidth(),
            height: this.plotHeight(),
            xaxis: {
              autosize: true,
              title: "Population size",
              tickformat: ",d", // Only keep integer ticks
              titlefont: {
                size: PLOT_AXIS_FONT_SIZE,
              },
            },
            yaxis: {
              title: "Percentage of configurations",
              titlefont: {
                size: PLOT_AXIS_FONT_SIZE,
              },
            },
            barmode: "stack",
          }}
          config={DEFAULT_PLOT_CONFIG}
        />
      </div>
    );
  }

  renderStatistics() {
    switch (this.props.status) {
      case "idle":
        return <div />;
      case "busy":
        return this.renderStatus(
          <CircularProgress size={30} thickness={5} />,
          "Generating statistics..."
        );
      case "timeout":
        return this.renderStatus(
          <Icon
            className="material-icons"
            color="error"
            style={{ fontSize: "40px" }}
          >
            report_problem
          </Icon>,
          "Computation exceeded timeout."
        );
      case "ready":
        return (
          <div>
            The configurations reached by the simulations were in the following
            consensuses:
            {this.renderRatios(this.props.statistics)}
            Average number of steps until reaching consensus:
            {this.renderAvgConvergence(this.props.statistics)}
            <div style={{ height: "30px" }} />
            Raw statistics:
            <br />
            <br />
            <Button
              color="primary"
              style={{ marginRight: "5px" }}
              variant="contained"
              onClick={() =>
                this.setState({
                  statsIndex: Math.max(this.state.statsIndex - NUM_STATS, 0),
                })
              }
              disabled={this.state.statsIndex - NUM_STATS < 0}
              startIcon={<Icon className="material-icons">{"arrow_back"}</Icon>}
            >
              Prev.
            </Button>
            <Button
              color="primary"
              style={{ marginRight: "5px" }}
              variant="contained"
              onClick={() =>
                this.setState({
                  statsIndex: Math.min(
                    this.state.statsIndex + NUM_STATS,
                    NUM_STATS *
                      Math.floor((this.props.statistics.length - 1) / NUM_STATS)
                  ),
                })
              }
              disabled={
                this.state.statsIndex + NUM_STATS >=
                this.props.statistics.length
              }
              endIcon={
                <Icon className="material-icons">{"arrow_forward"}</Icon>
              }
            >
              Next
            </Button>
            <Button
              color="primary"
              variant="contained"
              style={{ marginRight: "5px", paddingLeft: "10px" }}
              onClick={() => {
                this.export(this.props.statistics);
              }}
              endIcon={<Icon className="material-icons">{"save"}</Icon>}
            >
              Export
            </Button>
            {this.renderStatsDetails(this.props.statistics)}
          </div>
        );
      case "error":
        return this.renderStatus(
          <Icon
            className="material-icons"
            color="error"
            style={{ fontSize: "40px" }}
          >
            {" "}
            report_problem
          </Icon>,
          "It seems your protocol is not well-formed. Please edit protocol and try again."
        );

      case "connecterror":
        return this.renderStatus(
          <Icon
            className="material-icons"
            color="error"
            style={{ fontSize: "40px" }}
          >
            report_problem
          </Icon>,
          "Could not connect to the server (peregrine-backend). Make sure it is running."
        );
      default:
        return <div />;
    }
  }

  render() {
    return (
      <div>
        {this.renderRequestForm(this)}
        <div style={{ height: "30px" }} />
        {this.renderStatistics(this)}
      </div>
    );
  }
}

class ProtocolStatisticsContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = { status: "idle", protocol: props.protocol };
  }

  componentDidUpdate(prevProps, prevState){
    if(prevProps.protocol.title !== this.props.protocol.title){
      this.setState({ status: "idle" });
    }
  }

  makeStatisticsRequest(requestObject) {
    requestObject["protocol"] = this.props.protocol;
    this.setState({ status: "busy" });

    axios
      .post("simulation", requestObject)
      .then(
        function (response) {
          if (response.data.status) {
            // timeout
            this.setState({ status: "timeout" });
          } else if (response.data["Error"]) {
            this.setState({ status: "error" });
          } else {
            this.setState({ statistics: response.data, status: "ready" });
          }
        }.bind(this)
      )
      .catch(
        function (error) {
          this.setState({ status: "connecterror" });
        }.bind(this)
      );
  }

  render() {
    return (
      <ProtocolStatisticsView
        protocol={this.props.protocol}
        status={this.state.status}
        requestHandler={this.makeStatisticsRequest.bind(this)}
        statistics={this.state.statistics}
      />
    );
  }
}

export default ProtocolStatisticsContainer;
