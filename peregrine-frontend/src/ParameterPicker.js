import React from "react";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";

class ParameterSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentValue: this.props.parameter.value
    };
  }

  handleTextFieldChange(event) {
    if (event.key === "Enter") {
      this.props.onParamChange(event.target.value, this);
    } else {
      this.setState({ currentValue: event.target.value });
    }
  }

  renderParameterItems() {
    return this.props.parameter.values.map((val, key) => (
      <MenuItem value={val} name={this.props.parameter} key={key}>
        {val.toString()}
      </MenuItem>
    ));
  }

  render(parameter, key) {
    if (this.props.parameter.values.length === 0) {     // no options -> TextField input
      return (
        <div style={{ paddingRight: "5px", paddingTop: "5px"}} key={key}>
          <InputLabel htmlFor="parameter">
            {this.props.parameter.descr}
          </InputLabel>
          <div style={{ paddingRight: "5px", width: Math.min(640, Math.max(this.state.currentValue.length * 9, 100))}}>
            <TextField
              name={parameter}
              value={this.state.currentValue}
              fullWidth={true}
              onKeyPress={this.handleTextFieldChange.bind(this)}
              onChange={this.handleTextFieldChange.bind(this)}
            />
          </div>
        </div>
        );
    }
    // otherwise: show selection
    return (
      <div style={{ paddingRight: "5px", paddingTop: "5px" }} key={key}>
        <InputLabel htmlFor="parameter">
          {this.props.parameter.descr}
        </InputLabel>
        <Select
          name={parameter}
          value={this.props.parameter.value}
          style={{
            width: Math.max(
              this.props.parameter.descr.length * 9,
              90
            ),
          }}
          onChange={(event) =>
            this.props.onParamChange(event.target.value,
              this
            )
          }
        >
          {this.renderParameterItems()}
        </Select>
      </div>
    );
  }
}

class ParameterPicker extends React.Component {

  sendUpdatedParams(paramKey, newVal) {
    let newParams = {};
    for (let p of Object.keys(this.props.parameters)) {
      newParams[p] = this.props.parameters[p].value;
    }
    newParams[paramKey] = newVal;
    this.props.onParamChange(newParams);
  }

  render() {
    return (
      <div
        style={{ display: "flex", alignSelf: "flex-start", flexWrap: "wrap" }}
      >
        {Object.keys(this.props.parameters).map((paramKey, key) => (
          <ParameterSelect
            parameter={this.props.parameters[paramKey]}
            key={key}
            onParamChange={(newVal => this.sendUpdatedParams(paramKey, newVal))}
          />)
        )}
      </div>
    );
  }

}

export default ParameterPicker;
