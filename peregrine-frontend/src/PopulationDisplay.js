import React from "react";
import { ProtocolUtils } from "./ProtocolUtils";
import Popover from "@material-ui/core/Popover";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import * as Style from "./PopulationStyle";

class PopulationDisplay extends React.PureComponent {
  constructor(props) {
    super(props);
    this.transitionTimer = null;

    this.state = {
      population: this.props.population,
      activeAgents: [],
      transitionPickerOpen: false,
      pickerAnchor: null,
      numOfLeaders: this.computeNumOfLeaders(this.props.protocol),
      stateSize: Style.calculateStateWidth(this.props.protocol)
    };
  }

  computeNumOfLeaders(protocol) {
    let numLeaders = 0;
    if (protocol.leaders) {
      for (let x in protocol.leaders) {
        numLeaders += protocol.leaders[x];
      }
    }
    return numLeaders;
  }

  handleStateSelection(q) {
    if(this.props.manualPickerDisabled) return;

    const i = this.state.activeAgents.indexOf(q);
    const newActiveAgents = this.state.activeAgents.slice();

    if (i >= 0) {
      //same agent has been selected twice by user. Remove agent.
      newActiveAgents.splice(i, 1);
    } else {
      newActiveAgents.push(q);
    }

    if (newActiveAgents.length === 2) {
      const transitions = this.activeTransitions(newActiveAgents);


      if (transitions.length === 1) {
        this.setState({activeAgents: newActiveAgents}, () => this.props.onTransitionSelect(transitions[0], newActiveAgents));
      }
      else if (transitions.length > 1) {
        this.setState({activeAgents: newActiveAgents,
                        transitionPickerOpen: true });
        }
    }
    else {
      this.setState({activeAgents: newActiveAgents});
    }
  }

  isActiveAgent(i) {
    return this.state.activeAgents.includes(i);
  }

  isActiveState(q){
    return this.state.activeAgents.map(k =>
      this.props.population[k]).includes(q);
  }

  activeTransitions(activeAgents) {
    let q1 = this.state.population[activeAgents[0]];
    let q2 = this.state.population[activeAgents[1]];

    return ProtocolUtils.transitionsForPredecessors(
      this.props.protocol,
      q1,
      q2
    );
  }

  triggerOnTransitionCompleteCallback(){
    if(this.props.onTransitionComplete && this.props.activeAgents){
      this.transitionTimer = setTimeout(this.props.onTransitionComplete.bind(this), this.props.transitionSpeed/2);
    }
  }

  finishStep(){
    clearTimeout(this.transitionTimer);
    const speed = this.props.activeAgents ? this.props.transitionSpeed : 0;
    if(!this.props.activeAgents || this.props.activeAgents.length > 1){
      this.transitionTimer = setTimeout(() => {this.setState({
        population: this.props.population,
        activeAgents: [],
        transitionPickerOpen: false
      }); this.triggerOnTransitionCompleteCallback()}, speed);
    }
  }


  componentDidUpdate(prevProps, prevState){
    if(this.props.halt){
      clearTimeout(this.transitionTimer);
    }

    if (prevProps.population !== this.props.population ||
        prevProps.activeAgents !== this.props.activeAgents){
      this.setState(
        {
          activeAgents: this.props.activeAgents ? this.props.activeAgents : [],
          numOfLeaders: this.computeNumOfLeaders(this.props.protocol),
          stateSize: Style.calculateStateWidth(this.props.protocol),
        }, () => this.finishStep()
      );
    }
  }


  inactiveStyle(state) {
    return {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      float: "left",
      width: this.state.stateSize,
      height: this.state.stateSize,
      padding: Style.STATE_PADDING,
      margin: "1 px",
      backgroundColor: ProtocolUtils.isTrueState(this.props.protocol, state)
        ? Style.TRUE_COLOR
        : Style.FALSE_COLOR,
      color: "white",
      font: Style.STATE_FONT,
      borderRadius: "1 px",
      borderWidth: "1px",
      borderStyle: "solid",
      borderColor: "white",
      transition: "all 0.3s ease",
      boxSizing: "border-box",
      cursor: "pointer",
    };
  }

  activeStateStyle(state) {
    return {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      float: "left",
      width: this.state.stateSize,
      height: this.state.stateSize,
      padding: Style.STATE_PADDING - 2,
      margin: "1 px",
      backgroundColor: ProtocolUtils.isTrueState(this.props.protocol, state)
        ? Style.TRUE_COLOR_LIGHT
        : Style.FALSE_COLOR_LIGHT,
      color: "#37474F",
      borderRadius: "1px",
      borderColor: "yellow",
      borderWidth: "3px",
      borderStyle: "solid",
      font: Style.STATE_FONT,
      transition: "all 0.5s ease",
      boxSizing: "border-box",
    };
  }

  statesCount() {
    let count = {};

    for (let q of this.props.protocol.states) {
      count[q] = 0;
    }

    for (let q of this.state.population) {
      count[q] += 1;
    }
    return count;
  }

  support() {
    let count = this.statesCount();
    let support = {};
    for (let q of this.props.protocol.states) {
      if (count[q] > 0) {
        support[q] = count[q];
      }
    }
    return support;
  }

  renderTransitionPicker(){
      return(
      <Popover
          anchorEl={this.state.pickerAnchor}
          open={this.state.transitionPickerOpen}
          anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
          targetOrigin={{ horizontal: "left", vertical: "top" }}
        >
          {this.state.transitionPickerOpen ? (
            <List>
              {this.activeTransitions(this.state.activeAgents).map((t, i) => (
                <ListItem button
                  style={{
                    height: this.state.stateHeight,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                  onClick={(event) => {
                    this.props.onTransitionSelect(t, this.state.activeAgents);
                  }}
                  key={i}
                >
                  <span>
                    <span style={this.inactiveStyle(t.post[0])}>
                      {t.post[0]}
                    </span>
                    <span style={this.inactiveStyle(t.post[1])}>
                      {t.post[1]}
                    </span>
                  </span>
                </ListItem>
              ))}
            </List>
          ) : (
            <div></div>
          )}
        </Popover>);
    }

  renderIndividualStates() {
    const populationFrameStyle = {
      display: "flex",
      flexWrap: "wrap",
    };
    const leaderFrameStyle = {
      display: "flex",
      flexWrap: "wrap",
      paddingBottom: "30px",
    };
    let normalStates;
    let leaderStates;
    if (this.props.separateLeaders) {
      normalStates = this.state.population.slice(
        0,
        this.state.population.length - this.state.numOfLeaders
      );
      leaderStates = this.state.population.slice(
        this.state.population.length - this.state.numOfLeaders,
        this.state.population.length
      );
    }
    else {
      normalStates = this.state.population;
      leaderStates = [];
    }
    return (
      <div>
        <div style={leaderFrameStyle}>
          {leaderStates.map((x, i) => (
            <div
              style={
                this.isActiveAgent(i + normalStates.length)
                  ? this.activeStateStyle(x)
                  : this.inactiveStyle(x)
              }
              key={i + normalStates.length}
              title={x}
              onClick={(event) => {
                this.setState({ pickerAnchor: event.currentTarget }, () => {
                  this.handleStateSelection(i + normalStates.length);
                });
              }}
            >
              <span
                style={{
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
              >
                {x}
              </span>
            </div>
          ))}
        </div>
        <div style={populationFrameStyle}>
          {normalStates.map((x, i) => (
            <div
              style={
                this.isActiveAgent(i)
                  ? this.activeStateStyle(x)
                  : this.inactiveStyle(x)
              }
              key={i}
              title={x}
              onClick={(event) => {
                this.setState({ pickerAnchor: event.currentTarget }, () => {
                  this.handleStateSelection(i);
                });
              }}
            >
              <span
                style={{
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                }}
              >
                {x}
              </span>
            </div>
          ))}
        </div>
      {this.renderTransitionPicker()}
      </div>
    );
  }

  renderHorizontalBars() {
    const rowStyle = {
      height: "18px",
      lineHeight: "18px" /* Keep equal to height for vertical alignment */,
      fontFamily: Style.MAIN_FONT,
      borderRadius: "1px",
      borderColor: "white",
      borderWidth: "3px",
      borderStyle: "solid",
    };

    const horizontalBarStyle = {
      backgroundColor: "white",
      paddingLeft: "3px",
      display: "flex",
      alignSelf: "flex-start",
      transition: "width " + this.props.transitionSpeed + "ms ease-in",
    };

    const labelBaseStyle = {
      color: "black",
      fontSize: "16px",
      fontWeight: "bold",
      overflow: "hidden",
      textOverflow: "ellipsis",
      paddingLeft: "4px",
      paddingRight: "6px",
    };

    const countStyle = {
      color: "black",
      fontSize: "16px",
      textAlign: "right",
      overflow: "hidden",
      textOverflow: "ellipsis",
      paddingLeft: "10px",
    };

    const SHOW_MAX_NUM_STATES = 10;
    let count =
      this.props.protocol.states.length <= SHOW_MAX_NUM_STATES
        ? this.statesCount()
        : this.support();
    let that = this;

    function color(q) {
      return ProtocolUtils.isTrueState(that.props.protocol, q)
        ? Style.TRUE_COLOR
        : Style.FALSE_COLOR;
    }

    function size(q) {
      if (that.state.population.length > 0) {
        return "" + (98.0 * count[q]) / that.state.population.length + "%";
      } else {
        return "0%";
      }
    }

    function labelStyle(q) {
      return Object.assign(
        {},
        labelBaseStyle,
        that.isActiveState(q)
          ? {
              borderColor: "rgba(255,255,0,0.5)",
              backgroundColor: "rgba(255,255,0,0.3)",
            }
          : {}
      );
    }

    return (
      <div>
        <div style={{ width: "98%", display: "flex", flexDirection: "row" }}>
          <div style={{ maxWidth: "20%" }}>
            {Object.keys(count).map((q, key) => (
              <div title={q}
                   style={Object.assign({}, rowStyle, labelStyle(q))}
                   key={key}
              >
                {q}
              </div>
            ))}
          </div>
          <div style={{ width: "100%" }}>
            {Object.keys(count).map((q, key) => (
              <div
                title={q}
                key = {key}
                style={Object.assign({}, rowStyle, horizontalBarStyle, {
                  background: color(q),
                  width: size(q),
                })}
              ></div>
            ))}
          </div>
          <div style={{ maxWidth: "100px" }}>
            {Object.keys(count).map((q, key) => (
              <div title={q}
                   style={Object.assign({}, rowStyle, countStyle)}
                   key={key}
              >
                {count[q]}
              </div>
            ))}
          </div>
          {
            <div
              style={{
                height:
                  count.length === this.props.protocol.states.length ? 0 : 100,
              }}
            ></div>
          }
        </div>
      </div>
    );
  }

  renderPopulation() {
    switch (this.props.viewMode) {
      case "horizontalBars":
        return this.renderHorizontalBars();
      default:
        return this.renderIndividualStates();
    }
  }


  render() {
    return (
        <div style={{ padding: "10px 5px 10px 5px" }}>
          {this.renderPopulation()}
        </div>
    );
  }
}

export default PopulationDisplay;
