import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import FullApp from "./FullApp";

ReactDOM.render(
  <div>
    <FullApp />
  </div>,
  document.getElementById("root")
);
