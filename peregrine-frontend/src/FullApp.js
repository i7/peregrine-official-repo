import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import MenuIcon from "@material-ui/icons/Menu";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "axios";
import Documentation from "./Documentation";
import EditorPane from "./EditorPane";
import WYSIWYGEditor from "./WYSIWYGEditor";
import ModulesViewer from "./ModulesViewer";
import NewProtocolView from "./NewProtocolView";
import ProtocolDisplay from "./ProtocolDisplay";
import * as Style from "./PopulationStyle";
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import blue from '@material-ui/core/colors/blue';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1976d2',
      dark: '#004ba0',
      ligt: '#63a4ff',
      contrastText: '#ffffff'
    },
    secondary: blue
  }
});

const VISUAL_FLAG = "# EDIT_WYSIWYG_ONLY";

class FullApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      curView: "welcomePage",
      curFamily: 0,
      protocols: [],
      curParams: {},
      curProtocol: {
        title: "",
        states: [],
        transitions: [],
        predicate: "",
        description: "",
      },
      drawerOpen: false,
      deleteActionOpen: -1,
    };
  }

  componentDidMount() {
    axios.get("protocols").then((response) => {
      let protocols = response.data
        .slice()
        .sort((x, y) => x.title.localeCompare(y.title));

      this.setState({ protocols: protocols });
    });
  }

  handleBroken(i) {
    this.setState({
      curView: "brokenProtocolView",
      curFamily: i,
      drawerOpen: false,
    });
  }

  getParams(i, callback = () => {}) {
    axios
      .get(
        "params?fileName=" +
          encodeURIComponent(this.state.protocols[i].fileName)
      )
      .then((response) => {
        if (!response.data["Error"]) {
          this.setState({ curParams: response.data }, () =>
            this.getProtocol(this.state.curParams, i, callback)
          );
        } else {
          this.handleBroken(i);
        }
      })
      .catch((error) => {
        this.handleBroken(i);
      });
  }

  getProtocol(params, i, callback = () => {}) {
    axios
      .post(
        "protocol?fileName=" +
          encodeURIComponent(this.state.protocols[i].fileName),
        params
      )
      .then((response) => {
        if (response.data) {
          this.setState({ curProtocol: response.data }, callback);
        } else {
          this.handleBroken(i);
        }
      })
      .catch((error) => {
        this.handleBroken(i);
      });
  }

  handleEdit(i) {
    if (this.state.protocols[i].sourceCode.indexOf(VISUAL_FLAG) === 0) {
      this.getProtocol({}, i, () =>
        this.setState({
          curFamily: i,
          curView: "wysiwgView",
          drawerOpen: false,
        })
      );
    } else {
      this.setState({ curFamily: i, curView: "editView", drawerOpen: false });
    }
  }

  handleDelete(i) {
    axios
      .delete(
        "delete?fileName=" +
          encodeURIComponent(this.state.protocols[i].fileName)
      )
      .then((response) => {
        let newProtocols = this.state.protocols.slice();
        newProtocols.splice(i, 1);
        let newState = { curView : "welcomePage"
                       , curFamily: 0
                       , protocols: newProtocols
                       };
        this.setState(newState, () => this.closeDeleteDialog());
      });
  }

  handleAddProtocol() {
    this.fileInput.click();
  }

  insertIntoProtocols(p) {
    let newProtocols = this.state.protocols.slice();
    newProtocols.push(p);
    this.setState({ 'protocols': newProtocols });
  }

  updateProtocols(i, p) {
    let newProtocols = this.state.protocols.slice();
    newProtocols[i] = p;
    this.setState({ 'protocols': newProtocols });
  }

  handleFileSelect(e) {
    let files = e.target.files;

    if (files.length > 0) {
      let reader = new FileReader();

      reader.onload = () => {
        let code = reader.result;
        let request = {
          fileName: files[0].name,
          code: code,
          title: files[0].name,
        };
        axios.post("saveProtocol", request).then((response) => {
          this.insertIntoProtocols({
            fileName: response.data.fileName,
            code: code,
            title: response.data.fileName,
          });
        });
      };

      reader.readAsText(files[0]);
    }
  }

  handleCreateNew() {
    this.setState({ curView: "newProtocolView", drawerOpen: false });
  }

  handleDocumentation() {
    this.setState({ curView: "documentation", drawerOpen: false });
  }

  openDeleteDialog(i) {
    this.setState({ deleteActionOpen: i });
  }

  closeDeleteDialog() {
    this.setState({ deleteActionOpen: -1 });
  }

  renderDeleteDialog(i) {
    return (
      <Dialog
        modal={false}
        open={this.state.deleteActionOpen === i}
        onClose={this.handleClose}
      >
        <DialogContent>
          Do you really want to delete protocol{" "}
          <em>{this.state.protocols[i].title}</em>?
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={(e) => this.closeDeleteDialog()}>
            Cancel
          </Button>
          <Button onClick={(e) => this.handleDelete(i)}>
            Yes, delete protocol
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  renderMenuItem(i) {
    const that = this;

    function handleClick(e) {
      if(e.target.tagName.toLowerCase() !== 'span'){
        //User clicked menu item, not delete/edit button
        that.getParams(i, () =>
          that.setState({
            curFamily: i,
            drawerOpen: false,
            curView: "protocolView",
          })
        );
      }
    }

    function renderButton(text, icon, color, callback) {
      const ICON_SIZE = 18;

      return (
        <Tooltip title={text}>
          <IconButton
            style={{ padding: "0 0 0 0", width: ICON_SIZE, height: ICON_SIZE }}
            onClick={(e) => {
              e.stopPropagation();
              callback(i);
            }}
          >
            <Icon
              className="material-icons"
              style={{ fontSize: ICON_SIZE }}
            >
              {icon}
            </Icon>
          </IconButton>
        </Tooltip>
      );
    }
    return (
      <MenuItem
        onClick={(e) => handleClick(e)}
        style={{ minHeight: "48px", fontSize: "16px", display: "block" }}
        key={i.toString()}
      >
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <div
            style={{
              width: "90%",
              whiteSpace: "nowrap",
              overflow: "hidden",
              textOverflow: "ellipsis",
              paddingRight: 12,
              fontSize: "16px",
              fontFamily: "Roboto",
              lineHeight: "48px",
            }}
          >
            {this.state.protocols[i].title}
          </div>
          <div style={{ display: "flex" }}>
            <div style={{ display: "flex", alignSelf: "center" }}>
              {renderButton(
                "Edit",
                "edit",
                "rgb(0, 188, 212)",
                this.handleEdit.bind(this)
              )}
              <div style={{ width: 8 }} />
              {renderButton(
                "Delete",
                "delete",
                "ff1744",
                this.openDeleteDialog.bind(this)
              )}
              {this.renderDeleteDialog(i)}
            </div>
          </div>
        </div>
      </MenuItem>
    );
  }

  renderProtocolMenuItems() {
    return this.state.protocols.map((x, i) => this.renderMenuItem(i));
  }

  renderEditMenuItems() {
    const ICON_PADDING = "11px 5px 0 0";

    return (
      <div>
        <MenuItem
          onClick={() => {
            this.setState({ curView: "welcomePage", drawerOpen: false });
          }}
        >
          <div style={{ display: "flex" }}>
            <Icon className="material-icons" style={{ padding: ICON_PADDING }}>
              {"home"}
            </Icon>
            <span style={{ fontWeight: "bold", lineHeight: "45px" }}>
              Return to welcome page
            </span>
          </div>
        </MenuItem>
        <MenuItem onClick={this.handleCreateNew.bind(this)}>
          <div style={{ display: "flex" }}>
            <Icon className="material-icons" style={{ padding: ICON_PADDING }}>
              {"note_add"}
            </Icon>
            <span style={{ fontWeight: "bold", lineHeight: "45px" }}>
              Create new protocol
            </span>
          </div>
        </MenuItem>
        <MenuItem onClick={(e) => this.handleAddProtocol(this)}>
          <div style={{ display: "flex" }}>
            <Icon className="material-icons" style={{ padding: ICON_PADDING }}>
              {"file_upload"}
            </Icon>
            <span style={{ fontWeight: "bold", lineHeight: "45px" }}>
              Import protocol from file
            </span>
            <input
              type="file"
              ref={(f) => (this.fileInput = f)}
              onChange={this.handleFileSelect.bind(this)}
              hidden
            />
          </div>
        </MenuItem>
        <MenuItem onClick={(e) => this.handleDocumentation(this)}>
          <div style={{ display: "flex" }}>
            <Icon className="material-icons" style={{ padding: ICON_PADDING }}>
              {"help"}
            </Icon>
            <span style={{ fontWeight: "bold", lineHeight: "45px" }}>
              Read documentation
            </span>
          </div>
        </MenuItem>
      </div>
    );
  }

  handleNewParams(newParams) {
    this.setState({ curParams: newParams }, () =>
      this.getProtocol(newParams, this.state.curFamily)
    );
  }

  handleCodeSave(newCode) {
    let p = {
      fileName: newCode.fileName,
      title: newCode.title,
      sourceCode: newCode.sourceCode,
    };

    for (let i = 0; i < this.state.protocols.length; i++) {
      if (this.state.protocols[i].fileName === newCode.fileName) {
        this.updateProtocols(i, p);

        return p;
      }
    }

    this.insertIntoProtocols(p);

    return p;
  }

  protocolGen(params) {
    return this.state.curProtocol;
  }

  renderProtocol() {
    return (
      <div>
        <ProtocolDisplay
          params={this.state.curParams}
          protocolGen={this.protocolGen.bind(this)}
          onParamsChange={(newParams) => this.handleNewParams(newParams)}
        />
      </div>
    );
  }

  renderItem(text, icon, action) {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "left",
          justifyContent: "left",
          paddingTop: "15px",
        }}
      >
        <Button
          startIcon={
            <Icon style={{ fontSize: 34, align: "left" }}>{icon}</Icon>
          }
          style={{
            height: 40,
            textAlign: "left",
            textTransform: "none",
            fontSize: 28,
            fontWeight: "bold",
          }}
          onClick={action}
        >
          {text}
        </Button>
      </div>
    );
  }

  renderWelcomePage() {
    return (
      <div
        style={{
          width: "100%",
          height: "100%",
          padding: "10px",
          fontFamily: Style.MAIN_FONT,
        }}
      >
        <div
          style={{ display: "flex", flexDirection: "column", flexWrap: "wrap" }}
        >
          <span style={{ fontSize: 60 }}>Peregrine</span>
          <span style={{ fontSize: 40, color: "#607d8b" }}>
            <em>A tool for the analysis of population protocols</em>
          </span>
        </div>
        <div style={{ height: "90px" }} />
        <div style={{ display: "flex", flexDirection: "column", fontSize: 26 }}>
          {this.renderItem(
            "Analyze an existing protocol",
            "find_in_page",
            () => {
              this.setState({ drawerOpen: true });
            }
          )}
          {this.renderItem(
            "Create a new protocol",
            "note_add",
            this.handleCreateNew.bind(this)
          )}
          {this.renderItem(
            "Import an existing protocol",
            "file_upload",
            this.handleAddProtocol.bind(this)
          )}
        </div>
        <div style={{ height: "80px" }} />
        <div style={{ display: "flex", alignItems: "center", fontSize: 28 }}>
          <span style={{ paddingRight: 2 }}>New to Peregrine?</span>
          <Button
            endIcon={
              <Icon className="material-icons" style={{ fontSize: 34 }}>
                {"help_outline"}
              </Icon>
            }
            style={{
              height: 40,
              textAlign: "left",
              paddingLeft: 20,
              fontSize: 28,
              textTransform: "none",
              fontWeight: "bold",
            }}
            onClick={() => this.handleDocumentation()}
          >
            Read the documentation
          </Button>
        </div>
      </div>
    );
  }

  renderNewProtocolView() {
    return (
      <div>
        <NewProtocolView onCodeSave={(obj) => this.handleCodeSave(obj)} />
      </div>
    );
  }

  updateSourceCode(newCode) {
    let protocols = [...this.state.protocols];
    protocols[this.state.curFamily].sourceCode = newCode;
    this.setState({ 'protocols': protocols });
  }

  renderWysiwgView() {
    return (
      <div>
        <WYSIWYGEditor
          key={this.state.protocols[this.state.curFamily].fileName}
          protocol={this.state.curProtocol}
          title={this.state.protocols[this.state.curFamily].title}
          fileName={this.state.protocols[this.state.curFamily].fileName}
          onCodeSave={(newCode) => this.updateSourceCode(newCode.sourceCode)}
        />
      </div>
    );
  }

  renderEditorView() {
    return (
      <div>
        <EditorPane
          key={this.state.protocols[this.state.curFamily].fileName}
          code={this.state.protocols[this.state.curFamily].sourceCode}
          title={this.state.protocols[this.state.curFamily].title}
          fileName={this.state.protocols[this.state.curFamily].fileName}
          onCodeSave={(newCode) => this.updateSourceCode(newCode.sourceCode)}
        />
      </div>
    );
  }

  renderProtocolView() {
    return (
      <div>
        {this.renderProtocol()}
        <div style={{ height: "30px" }} />
        <div style={{ paddingBottom: "50px" }}>
          {
            <ModulesViewer
              protocol={this.state.curProtocol}
              params={this.state.curParams}
              key={this.state.curFamily}
              onParamChange={this.handleNewParams.bind(this)}
            />
          }
        </div>
      </div>
    );
  }

  renderBrokenProtocolView() {
    return (
      <div style={{ fontFamily: Style.MAIN_FONT, fontSize: 24 }}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Icon
            className="material-icons"
            style={{ color: "ff1744", fontSize: 50, paddingRight: 20 }}
          >
            {"warning"}
          </Icon>
          <span style={{ fontWeight: "bold" }}>
            Protocol <em>{this.state.protocols[this.state.curFamily].title}</em>{" "}
            appears to be broken.
          </span>
        </div>
        <div style={{ display: "flex", flexDirection: "column", padding: 30 }}>
          {this.renderItem("Edit protocol", "edit", () =>
            this.handleEdit(this.state.curFamily)
          )}
          {this.renderItem("Delete protocol", "delete", () =>
            this.openDeleteDialog(this.state.curFamily)
          )}
          {this.renderDeleteDialog(this.state.curFamily)}
        </div>
      </div>
    );
  }
  renderDocumentation() {
    return <Documentation />;
  }

  renderApp() {
    switch (this.state.curView) {
      case "protocolView":
        return this.renderProtocolView();
      case "newProtocolView":
        return this.renderNewProtocolView();
      case "editView":
        return this.renderEditorView();
      case "wysiwgView":
        return this.renderWysiwgView();
      case "brokenProtocolView":
        return this.renderBrokenProtocolView();
      case "help":
      case "documentation":
        return this.renderDocumentation();
      default:
        return this.renderWelcomePage();
    }
  }

  isMobile() {
    return window.innerWidth <= 1024;
  }

  render() {
    return (
      <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
        <ThemeProvider theme={theme}>
        <div>
          <AppBar position="static">
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="menu"
                onClick={() => this.setState({ drawerOpen: true })}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" style={{ flex: 1 }}>
                Peregrine
              </Typography>
              <div>
                <Tooltip title="Help">
                  <IconButton
                    onClick={() => this.setState({ curView: "help" })}
                  >
                    <Icon className="material-icons" style={{ color: "white" }}>
                      {"help"}
                    </Icon>
                  </IconButton>
                </Tooltip>
              </div>
            </Toolbar>
          </AppBar>
          <Drawer
            width={400}
            open={this.state.drawerOpen}
            onClose={() => this.setState({ drawerOpen: false })}
          >
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                fontFamily: Style.MAIN_FONT,
                padding: "8px",
              }}
            >
              Actions
            </div>
            {this.renderEditMenuItems()}
            <Divider />
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                fontFamily: Style.MAIN_FONT,
                padding: "8px",
              }}
            >
              Protocols
            </div>
            {this.renderProtocolMenuItems()}
          </Drawer>
        </div>
        <div style={{ height: "10%" }} />
        <div
          style={{
            height: "100%",
            display: "flex",
            flexDirection: "column",
            alignSelf: "center",
            width: this.isMobile() ? "100%" : "65%",
          }}
        >
          {this.renderApp()}
        </div>
        </ThemeProvider>
      </div>
    );
  }
}

export default FullApp;
