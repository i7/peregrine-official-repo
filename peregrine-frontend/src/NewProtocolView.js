import React from "react";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import Paper from "@material-ui/core/Paper";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import EditorPane from "./EditorPane";
import ParamsEditor from "./ParamsEditor";
import WYSIWYGEditor from "./WYSIWYGEditor";
import { FormulaEditForm, InfoEditForm } from "./ProtocolEditForms";

class NewProtocolView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      step: this.props.step ? this.props.step : 0,
      creationMode: undefined,
      protocolTitle: "",
      protocolDescription: "",
      protocolPredicate: "",
      protocolPrecondition: "true",
      protocolPostcondition: "true",
      params: this.props.params ? this.props.params : [],
    };
  }

  isParametric() {
    return this.state.params && this.state.params.length > 0;
  }

  generateParams() {
    let params = [];

    for (let i = 0; i < this.state.params.length; i++) {
      const p = this.state.params[i];
      let values;
      let value;

      if (p.type === "int") {
        const maxVal = parseInt(p.max) + 1;
        values = "list(range(" + p.min + ", " + maxVal + ", " + p.step + "))";
        value = p.min;
      } else if (p.type === "elem") {
        const elems = p.elements.split(",").map((e) => '"' + e + '"');

        values = "[" + elems.join(", ") + "]";
        value = elems[0];
      } else if (p.type === "free") {
        values = "[]";
        value = 1;
      }

      const entry =
        `    "` +
        p.name +
        `": {
      "descr":  "` +
        p.descr +
        `",
      "values": ` +
        values +
        `,
      "value":  ` +
        value +
        `
    }`;

      params.push(entry);
    }

    return params.join(",\n\n");
  }

  renderDescriptionEditor() {
    return (
      <div>
        <div style={{ padding: 10 }}>
          <div style={{ paddingBottom: 40 }}>
            <InfoEditForm
              title={this.state.protocolTitle}
              description={this.state.protocolDescription}
              onTitleChange={(newTitle) =>
                this.setState({ protocolTitle: newTitle })
              }
              onDescriptionChange={(newDescription) =>
                this.setState({ protocolDescription: newDescription })
              }
            />
          </div>
        </div>
      </div>
    );
  }

  renderFormulaEditor() {
    return (
      <div style={{ padding: 10 }}>
        <FormulaEditForm
          formula={this.state.protocolPredicate}
          precondition={this.state.protocolPrecondition}
          postcondition={this.state.protocolPostcondition}
          onPreconditionChange={(newPredicate) =>
            this.setState({ protocolPrecondition: newPredicate })
          }
          onPostconditionChange={(newPredicate) =>
            this.setState({ protocolPostcondition: newPredicate })
          }
          onFormulaChange={(newPredicate) =>
            this.setState({ protocolPredicate: newPredicate })
          }
        />
      </div>
    );
  }

  renderEditor() {
    const params = !this.isParametric()
      ? ""
      : `params = {
  "scheme": {
` +
        this.generateParams() +
        `
  }
}\n\n`;

    const defaultCode =
      `#-*- coding: utf-8 -*-\n` +
      params +
      `def generateProtocol(params):` +
      (params === ""
        ? ""
        : `\n  # Use params["scheme"]["name_of_param"]["value"] to make use of parameter name_of_param\n`) +
      `
  states = ["Y", "N", "y", "n"] # List of states

  # List of transitions
  transitions = [Utils.transition(("Y", "N"), ("y", "n")),
                 Utils.transition(("Y", "n"), ("Y", "y")),
                 Utils.transition(("N", "y"), ("N", "n")),
                 Utils.transition(("y", "n"), ("y", "y"))]

  initialStates = ["Y", "N"] # List of initial states
  trueStates = ["Y", "y"] # List of states of output 1
  
  style = {q: 
           {
             #"size":  1.1, 							# optional, float, default: 1
             #"color": "rgb(0, 150, 255)",	 			# optional, "rgb(_, _, _)", default: true -> blue & false -> red
             #"fillcolor": "rgba(255, 255, 255, 0.7)",	# optional, "rgba(_, _, _)", default: 70% opacity white
             #"shape": "circle" 	 					# optional, "circle" (default) / "sqaure" / "triangle"
           } for q in states
          }

  return {
    "title":         "` +
      this.state.protocolTitle +
      `",
    "states":        states,
    "transitions":   transitions,
    "initialStates": initialStates,
    "trueStates":    trueStates,
    "predicate":     "` +
      this.state.protocolPredicate +
      `",
    "precondition": "` +
      this.state.protocolPrecondition +
      `",
    "postcondition": "` +
      this.state.protocolPostcondition +
      `",
    "statesStyle":   style,
    "description":   """` +
      this.state.protocolDescription.replace(/\r?\n/g, "\n						") +
      `"""
  }`;

    return (
      <EditorPane
        code={defaultCode}
        onCodeSave={this.props.onCodeSave}
        title={this.state.protocolTitle}
      />
    );
  }

  renderWYSIWYGEditor() {
    let defaultProtocol = {
      title: this.state.protocolTitle,
      states: ["Y", "N", "y", "n"],
      transitions: [
        { name: "Y, N -> y, n", pre: ["Y", "N"], post: ["y", "n"] },
        { name: "Y, n -> Y, y", pre: ["Y", "n"], post: ["Y", "y"] },
        { name: "N, y -> N, n", pre: ["N", "y"], post: ["N", "n"] },
        { name: "y, n -> y, y", pre: ["Y", "n"], post: ["y", "y"] },
      ],
      initialStates: ["Y", "N"],
      trueStates: ["Y", "y"],
      predicate: this.state.protocolPredicate,
      precondition: this.state.protocolPrecondition,
      postcondition: this.state.protocolPostcondition,
      description: this.state.protocolDescription,
    };

    return (
      <WYSIWYGEditor
        protocol={defaultProtocol}
        title={this.state.protocolTitle}
        onCodeSave={this.props.onCodeSave}
      />
    );
  }

  renderParametersEditor() {
    return (
      <div>
        <ParamsEditor
          params={this.state.params}
          onParamsChange={(p) => this.setState({ params: p })}
        />
        {this.isParametric() ? (
          ""
        ) : (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexWrap: "wrap",
              fontSize: 22,
              paddingLeft: 20,
              paddingRight: 20,
              paddingTop: 100,
              textDecoration: "underline",
            }}
          >
            Skip this step for standard protocols. Parameters are only necessary
            for parametric protocols.
          </div>
        )}
      </div>
    );
  }

  renderCreationMode() {
    const LABEL_SIZE = 24;
    const ICON_SIZE = 40;

    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
          alignSelf: "center",
          justifySelf: "center",
          height: 350,
        }}
      >
        <Button
          color="primary"
          onClick={(e) => this.setState({ creationMode: 0 })}
          labelPosition="after"
          style={{ padding: 10, margin: 10, height: ICON_SIZE * 2 }}
          labelStyle={{ fontSize: LABEL_SIZE }}
          startIcon={
            <Icon className="material-icons" style={{ fontSize: ICON_SIZE }}>
              {"extension"}
            </Icon>
          }
          disabled={this.isParametric() || this.props.navDisabled}
        >
          Create with visual editor
        </Button>
        {this.isParametric() ? (
          <div style={{ marginTop: 10, marginBottom: 20 }}>
            Visual editor only enabled for non parametric protocols
          </div>
        ) : (
          <div style={{ height: 15 }} />
        )}
        <Button
          color="primary"
          onClick={(e) => this.setState({ creationMode: 1 })}
          labelPosition="after"
          style={{ padding: 10, margin: 10, height: ICON_SIZE * 2 }}
          labelStyle={{ fontSize: LABEL_SIZE }}
          startIcon={
            <Icon className="material-icons" style={{ fontSize: ICON_SIZE }}>
              {"code"}
            </Icon>
          }
          disabled={this.props.navDisabled}
        >
          Create with code editor
        </Button>
      </div>
    );
  }

  render() {
    const LABEL_SIZE = 20;
    const STEPS = [
      this.renderDescriptionEditor,
      this.renderFormulaEditor,
      this.renderParametersEditor,
      this.renderCreationMode,
    ];
    const CREATE = [this.renderWYSIWYGEditor, this.renderEditor];

    const that = this;

    function nextDisabled() {
      return (
        that.state.step === STEPS.length - 1 ||
        (that.state.step === 0 &&
          (!that.state.protocolTitle ||
            that.state.protocolTitle.trim().length === 0)) ||
        (that.state.step === 1 &&
          (!that.state.protocolPredicate ||
            that.state.protocolPredicate.trim().length === 0))
      );
    }

    return (
      <div>
        {this.state.creationMode !== undefined ? (
          CREATE[this.state.creationMode].bind(this)()
        ) : (
          <div>
            <Stepper
              activeStep={this.state.step}
              linear={true}
              style={{ display: "flex", flexWrap: "wrap" }}
            >
              <Step>
                <StepLabel style={{ fontSize: LABEL_SIZE }}>
                  Protocol description
                </StepLabel>
              </Step>
              <Step>
                <StepLabel style={{ fontSize: LABEL_SIZE }}>
                  Predicate
                </StepLabel>
              </Step>
              <Step>
                <StepLabel style={{ fontSize: LABEL_SIZE }}>
                  Parameters&nbsp;
                  <span style={{ fontSize: 14 }}> (optional)</span>
                </StepLabel>
              </Step>
              <Step>
                <StepLabel style={{ fontSize: LABEL_SIZE }}>Create</StepLabel>
              </Step>
            </Stepper>
            <Paper>{STEPS[this.state.step].bind(this)()}</Paper>
            {this.props.navDisabled ? (
              ""
            ) : (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  paddingTop: 10,
                }}
              >
                <Button
                  color="primary"
                  variant="contained"
                  onClick={(e) => this.setState({ step: this.state.step - 1 })}
                  disabled={this.state.step === 0}
                >
                  Previous
                </Button>
                <div style={{ width: 20 }} />
                <Button
                  color="primary"
                  variant="contained"
                  onClick={(e) => this.setState({ step: this.state.step + 1 })}
                  disabled={nextDisabled()}
                >
                  Next
                </Button>
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

export default NewProtocolView;
