import React from "react";
import EditorPane from "./EditorPane";
import PopulationControlDisplay from "./PopulationControlDisplay";
import ProtocolDetails from "./ProtocolDetails";
import ProtocolStatisticsContainer from "./ProtocolStatistics";
import ProtocolVerifier from "./ProtocolVerifier";
import SpatialSimulator from "./SpatialSimulator";
import NewProtocolView from "./NewProtocolView";
import WYSIWYGEditor from "./WYSIWYGEditor";
import {StageDetailView} from "./StageGraphDisplay";
import { FormulaUtils } from "./FormulaUtils";
import Icon from "@material-ui/core/Icon";
import * as Style from "./PopulationStyle";

const CONTENT_STYLE = { padding: "10 10 10 15", borderLeft: "2px solid gray" };
const SUBCONTENT_STYLE = {
  padding: "5 5 5 10",
  margin: "10 0 10 0",
  borderLeft: "2px solid gray",
};
const SUBTITLE_STYLE = { fontWeight: "bold", paddingRight: 8 };
const MAJORITY_PROTOCOL = {
  title: "Majority voting protocol",
  states: ["Y", "N", "y", "n"],
  transitions: [
    { name: "Y, N -> y, n", pre: ["Y", "N"], post: ["y", "n"] },
    { name: "Y, n -> Y, y", pre: ["Y", "n"], post: ["Y", "y"] },
    { name: "N, y -> N, n", pre: ["N", "y"], post: ["N", "n"] },
    { name: "y, n -> y, y", pre: ["y", "n"], post: ["y", "y"] },
  ],
  initialStates: ["Y", "N"],
  trueStates: ["Y", "y"],
  predicate: "C[Y] >= C[N]",
  description: `This protocol takes a majority vote. More precisely, it computes whether there are initially more agents in state Y than N.`,
};

const MAJORITY_PARAMS = {
  "Y": {
    "value": 2,
    "values": [1,2,3,4,5],
    "descr": "#Y"
  },
  "N": {
    "value": 2,
    "values": [1,2,3,4,5],
    "descr": "#N"
  }
};

const FLOCK_PARAMS = {
  "0": {
    "value": 2,
    "values": [1,2,3,4,5],
    "descr": "#0"
  },
  "1": {
    "value": 2,
    "values": [1,2,3,4,5],
    "descr": "#1"
  }
};

const FLOCK_PROTOCOL_THREE = {
  title: "Flock-of-birds protocol",
  states: ["0", "1", "2", "3"],
  transitions: [
    { name: "0, 3 -> 3, 3", pre: ["0", "3"], post: ["3", "3"] },
    { name: "1, 1 -> 2, 0", pre: ["1", "1"], post: ["2", "0"] },
    { name: "1, 2 -> 3, 3", pre: ["1", "2"], post: ["3", "3"] },
    { name: "1, 3 -> 3, 3", pre: ["1", "3"], post: ["3", "3"] },
    { name: "2, 2 -> 3, 3", pre: ["2", "2"], post: ["3", "3"] },
    { name: "2, 3 -> 3, 3", pre: ["2", "3"], post: ["3", "3"] },
  ],
  initialStates: ["0", "1"],
  trueStates: ["3"],
  predicate: "C[1] >= 3",
  description: `This protocol computes whether at least 3 birds are sick.`,
};

const MAJORITY_PROTOCOL_BROKEN = {
  title: "Majority voting protocol without tie-breaker",
  states: ["Y", "N", "y", "n"],
  transitions: [
    { name: "Y, N -> y, n", pre: ["Y", "N"], post: ["y", "n"] },
    { name: "Y, n -> Y, y", pre: ["Y", "n"], post: ["Y", "y"] },
    { name: "N, y -> N, n", pre: ["N", "y"], post: ["N", "n"] },
  ],
  initialStates: ["Y", "N"],
  trueStates: ["Y", "y"],
  predicate: "C[Y] >= C[N]",
  description: `This protocol takes a majority vote. More precisely, it (attempts to) compute whether there are initially more agents in state Y than N. The protocol is incorrect whenever there is a tie.`,
};

const MAJORITY_PROTOCOL_CODE = `def generateProtocol(params):
    return {
      "title":         "Majority protocol",
      "states":        ["Y", "N", "y", "n"],
      "transitions":   [Utils.transition(("Y", "N"), ("y", "n")),
                        Utils.transition(("Y", "n"), ("Y", "y")),
                        Utils.transition(("N", "y"), ("N", "n")),
                        Utils.transition(("y", "n"), ("y", "y"))],
      "initialStates": ["Y", "N"],
      "trueStates":    ["Y", "y"],
      "predicate":     "C[Y] >= C[N]",
      "description":   """This protocol takes a majority vote. More
                          precisely, it computes whether there are
                          initially more agents in state Y than N."""
    }`;

const STAGE = {
  "parent": "S0",
  "deadTransitions": [
    {
      "pre": [
        "Y",
        "N"
      ],
      "post": [
        "y",
        "n"
      ],
      "name": ""
    },
    {
      "pre": [
        "Y",
        "n"
      ],
      "post": [
        "Y",
        "y"
      ],
      "name": ""
    }
  ],
  "checkedProperty": "FALSE_CONSENSUS",
  "potReachFromConfiguration": {
    "N": 2,
    "n": 0,
    "Y": 1,
    "y": 0
  },
  "eventuallyDeadTransitions": [
    {
      "pre": [
        "N",
        "y"
      ],
      "post": [
        "N",
        "n"
      ],
      "name": ""
    },
    {
      "pre": [
        "y",
        "n"
      ],
      "post": [
        "y",
        "y"
      ],
      "name": ""
    }
  ],
  "speed": "EXPONENTIAL",
  "reachSequence": null,
  "id": "S4",
  "certificate": [
    {
      "state": "y",
      "value": 1
    }
  ],
  "constraint": [
    [
      {
        "state": "Y",
        "value": 0
      }
    ]
  ],
  "configuration": {
    "N": 1,
    "n": 1,
    "Y": 0,
    "y": 1
  },
  "failed": false,
  "configurationUnstable": null
}


const MAJORITY_PROTOCOL_CODE_LEADERS = `def generateProtocol(params):
    return {
      "title":         "Majority protocol with leaders and offset 4",
      "states":        ["Y", "N", "y", "n"],
      "transitions":   [Utils.transition(("Y", "N"), ("y", "n")),
                        Utils.transition(("Y", "n"), ("Y", "y")),
                        Utils.transition(("N", "y"), ("N", "n")),
                        Utils.transition(("y", "n"), ("y", "y"))],
      "initialStates": ["Y", "N"],
      "trueStates":    ["Y", "y"],
      "predicate":     "C[Y] + 4 >= C[N]",
      "leaders": {"Y" : 4},
      "description":   """This protocol takes a majority vote. More
                          precisely, it computes whether there are
                          initially more agents in state Y than N."""
    }`;

const FLOCK_PROTOCOL_CODE = `# -*- coding: utf-8 -*-
params = {
  "scheme": {
    "c": {
      "descr":  "Threshold c",
      "values": range(1, 201), # range of parameter c
      "value":  3              # default value of parameter c
    }
  }
}


def generateProtocol(params):
  c = params["scheme"]["c"]["value"] # current value of parameter c

  states = range(0, c + 1)
  transitions = []

  for i in states:
    for j in states:
      if i + j < c and (i > 0 and j > 0):
        transitions.append(Utils.transition((i, j), (i + j, 0)))
      elif i + j >= c and (i != c or j != c):
        transitions.append(Utils.transition((i, j), (c, c)))

  return {
    "title":         "Flock-of-birds protocol",
    "states":        states,
    "transitions":   transitions,
    "initialStates": [0, 1],
    "trueStates":    [c],
    "predicate":     "C[1] >= {}".format(c),
    "description":   """This protocol computes whether a population of
                        birds contains at least c sick birds, i.e. whether
                        C[1] >= c. Described in Dana Angluin, James
                        Aspnes, Zoë Diamadi, Michael J. Fischer and
                        René Peralta. Computation in Networks of
                        Passively Mobile Finite-State Sensors.
                        PODC 2004."""
  }`;

class Documentation extends React.Component {
  renderMenu() {
    return (
      <div
        style={{
          minWidth: 300,
          width: "50%",
          border: "solid 2px black",
          backgroundColor: "rgba(0, 188, 212, 0.25)",
        }}
      >
        <ol>
          <li>
            <a href="#started">Getting started</a>
            <ol>
              <li>
                <a href="#started-majority">
                  A simple protocol for majority voting
                </a>
              </li>
              <li>
                <a href="#started-flock">
                  A simple protocol for counting sick birds
                </a>
              </li>
            </ol>
          </li>
          <li>
            <a href="#modules">Protocol analysis</a>
            <ol>
              <li>
                <a href="#modules-details">Protocol details</a>
              </li>
              <li>
                <a href="#modules-simulation">Simulation</a>
              </li>
              <li>
                <a href="#modules-spatial-simulation">Spatial Simulation</a>
              </li>
              <li>
                <a href="#modules-statistics">Statistics</a>
              </li>
              <li>
                <a href="#modules-verification">Verification</a>
              </li>
            </ol>
          </li>
          <li>
            <a href="#create">Creating a protocol</a>
            <ol>
              <li>
                <a href="#create-description">Description</a>
              </li>
              <li>
                <a href="#create-predicate">Predicate</a>
              </li>
              <li>
                <a href="#create-parameters">Parameters</a>
              </li>
              <li>
                <a href="#create-editor">Editor</a>
              </li>
              <li>
                <a href="#create-leaders">Protocols with leaders</a>
              </li>
              <li>
                <a href="#create-shapes">Shapes</a>
              </li>
            </ol>
          </li>
          <li>
            <a href="#manage">Managing protocols</a>
            <ol>
              <li>
                <a href="#manage-edit">Editing protocols</a>
              </li>
              <li>
                <a href="#manage-remove">Removing protocols</a>
              </li>
              <li>
                <a href="#manage-add">Importing existing protocols</a>
              </li>
            </ol>
          </li>
        </ol>
      </div>
    );
  }

  renderTitle(title, id, level) {
    return (
      <div style={{ display: "flex", alignItems: "center", marginLeft: -10 }}>
        <a href="#documentation" alt="Go back">
          <Icon className="material-icons">keyboard_arrow_up</Icon>
        </a>
        &nbsp;
        {level === 1 ? (
          <div id={id}>
            <h1>{title}</h1>
          </div>
        ) : level === 2 ? (
          <div id={id}>
            <h2>{title}</h2>
          </div>
        ) : level === 3 ? (
          <div id={id}>
            <h3>{title}</h3>
          </div>
        ) : (
          { title }
        )}
      </div>
    );
  }

  embedComponent(component) {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          padding: "20 0 20 0",
        }}
      >
        <div style={{ width: "80%" }}>{component}</div>
      </div>
    );
  }

  renderFormula(formula) {
    return (
      <span dangerouslySetInnerHTML={{ __html: FormulaUtils.print(formula) }} />
    );
  }

  renderCode(code) {
    return <span style={{ fontFamily: "Courier New" }}>{code}</span>;
  }

  coloredRectangle(text, color, textColor = "white") {
    return (
      <span
        style={{
          display: "inline-block",
          padding: "0 5 0 5",
          backgroundColor: color,
          color: textColor,
        }}
      >
        {text}
      </span>
    );
  }

  renderStarted() {
    return (
      <div style={{ paddingTop: 20 }}>
        {this.renderTitle("1. Getting started", "started", 2)}
        <div style={CONTENT_STYLE}>
          The features and protocols of Peregrine can be accessed through the
          top left menu{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            menu
          </Icon>
          . To load an existing protocol, simply click on its name.
          <br />
          <br />
          In the remainder of this section, we give a brief introduction to
          population protocols by introducing two simple standard protocols. The
          other sections of the documentation illustrate the different features
          of Peregrine with these two protocols.
        </div>
        {this.renderTitle(
          "1.1 A simple protocol for majority voting",
          "started-majority",
          3
        )}
        <div style={CONTENT_STYLE}>
          Suppose anonymous and mobile agents wish to take a majority vote.
          Intuitively, <em>anonymous</em> means that agents have no identity,
          and <em>mobile</em> means that agents are "wandering around", and can
          only interact whenever they bump into each other. In order to vote,
          all agents conduct the following protocol. At any moment, each agent
          is in one of the four following states: <em>Y, N, y, n</em>. Initially
          all agents are in the states <em>Y</em> or <em>N</em>, corresponding
          to how they want to vote (states <em>y</em>, <em>n</em> are auxiliary
          states). Since agents are anonymous, only the number of agents in each
          state is relevant. Thus, a configuration of the protocol can be
          described as a multiset. For example, ⟅<em>Y, Y, N</em>⟆ is a possible
          configuration made of two agents in state <em>Y</em> and one agent in
          state <em>N</em>.
          <br />
          <br />
          Agents repeatedly interact pairwise according to the following rules
          (called <em>transitions</em>):
          <br />
          <br />
          <div
            style={{
              display: "grid",
              gridTemplateColumns:
                "max-content max-content max-content max-content",
              gridColumnGap: "10px",
              gridRowGap: "5px",
              fontStyle: "italic",
              paddingLeft: 15,
              alignItems: "center",
            }}
          >
            <div>a:</div>
            <div>Y, N</div>
            <div>→</div>
            <div>y, n</div>
            <div>b:</div>
            <div>Y, n</div>
            <div>→</div>
            <div>Y, y</div>
            <div>c:</div>
            <div>N, y</div>
            <div>→</div>
            <div>N, n</div>
            <div>d:</div>
            <div>y, n</div>
            <div>→</div>
            <div>y, y</div>
          </div>
          <br />
          For example, a possible execution from ⟅<em>Y, Y, N</em>⟆ consists in
          applying transition <em>a</em> and then <em>b</em>: ⟅<em>Y, Y, N</em>⟆
          → ⟅<em>Y, y, n</em>⟆ → ⟅<em>Y, y, y</em>⟆. Since no further
          interaction is enabled in ⟅<em>Y, y, y</em>⟆, we say that it is a{" "}
          <em>terminal</em> configuration. Moreover, ⟅<em>Y, y, y</em>⟆ is in a
          consensus since all agents are now in "yes"-states. Because this
          consensus cannot be destroyed by further interactions, we say that the
          consensus is <em>lasting</em>. Another possible execution of the
          protocol from ⟅<em>Y, Y, N, N</em>⟆ consists in executing transitions{" "}
          <em>a, c, a, d, d, d</em>:
          <div
            style={{ display: "flex", justifyContent: "center", padding: 10 }}
          >
            ⟅<em>Y, Y, N, N</em>⟆ → ⟅<em>Y, y, N, n</em>⟆ → ⟅<em>Y, n, N, y</em>
            ⟆ → ⟅<em>y, n, n, y</em>⟆ → ⟅<em>y, n, y, y</em>⟆ → ⟅
            <em>y, y, y, y</em>⟆
          </div>
          which reaches a lasting "yes"-consensus. Assuming agents interact
          uniformly and independently at random, it can be shown that the agents
          will almost surely reach a lasting consensus, i.e. with probability 1.
          Moreover, this lasting consensus will indicate whether there were
          initially at least {"50%"} agents who voted yes. More precisely, we
          say that the protocol computes the predicate{" "}
          {this.renderFormula("C[Y] >= C[N]")}, where{" "}
          {this.renderFormula("C[Y]")} and {this.renderFormula("C[Y]")} stand
          respectively for the number of agents initially in states <em>Y</em>{" "}
          and <em>N</em>. You can observe this behaviour empirically by
          simulating random executions from ⟅<em>Y, Y, Y, N, N</em>⟆ with the
          simulator of Peregrine (which will be covered in{" "}
          <a href="#modules-simulation">Section 2.2</a>):
          {this.embedComponent(
            <PopulationControlDisplay
              protocol={MAJORITY_PROTOCOL}
              population={["Y", "Y", "Y", "N", "N"]}
              viewMode="individualStates"
            />
          )}
          Such simulations give confidence in the protocol, but do not guarantee
          that it always work as intended. Peregrine offers a powerful
          verification module which can formally and automatically prove that a
          protocol always reaches a <em>correct</em> lasting consensus,
          regardless of the number of agents and of their initial states. For
          example, Peregrine can verify that the majority protocol of{" "}
          <a href="#started-majority">Section 1.1</a> indeed computes{" "}
          {this.renderFormula("C[Y] >= C[N]")}:
          {this.embedComponent(
            <ProtocolVerifier
              protocol={MAJORITY_PROTOCOL}
              initialConfigParams={MAJORITY_PARAMS}
            />
          )}
        </div>
        {this.renderTitle(
          "1.2 A simple protocol for counting sick birds",
          "started-flock",
          3
        )}
        <div style={CONTENT_STYLE}>
          Imagine we wish to determine whether at least <em>c</em> birds of a
          flock are sick. Initially, each bird is either in state <em>0</em>{" "}
          (not sick) or state <em>1</em> (sick), which is provided by a sensor.
          Whenever two birds get close to each other, one of them updates its
          state to the sum of their states, and the other bird updates its state
          to 0. If the sum of both states equals or exceeds <em>c</em>, then
          both birds instead update their state to <em>c</em>. Intuitively, the
          flock as a whole carries the number of sick birds. If at least{" "}
          <em>c</em> birds are sick, then one bird will eventually end up in
          state <em>c</em>. When this happens, this bird will start notifying
          the other birds, which will also end up in state <em>c</em>. In more
          details, the states of the protocol are <em>0, 1, 2, …, c</em> and the
          transitions are:
          <br />
          <br />
          <div
            style={{
              display: "grid",
              gridTemplateColumns:
                "max-content max-content max-content max-content",
              gridColumnGap: "10px",
              gridRowGap: "5px",
              fontStyle: "italic",
              paddingLeft: 15,
              alignItems: "center",
            }}
          >
            <div>i, j</div>
            <div>→</div>
            <div>i + j, 0</div>
            <div>{"if i + j < c and (i > 0 and j > 0)"}</div>
            <div>i, j</div>
            <div>→</div>
            <div>c, c</div>
            <div>{"if i + j ≥ c and (i ≠ c or j ≠ c)"}</div>
          </div>
          <br />
          The unique "yes"-state is <em>c</em> and all of the other states are
          "no"-states. The predicate to be computed is{" "}
          {this.renderFormula("C[1] >= c")}. It can be shown that the birds
          reach a correct lasting consensus with probability 1. You can observe
          this behaviour empirically for <em>c = 3</em> by simulating random
          executions from a population of two healthy birds and four sick birds:
          {this.embedComponent(
            <PopulationControlDisplay
              protocol={FLOCK_PROTOCOL_THREE}
              population={["0", "0", "1", "1", "1", "1"]}
              viewMode="individualStates"
            />
          )}
          Peregrine can verify that the protocol indeed computes{" "}
          {this.renderFormula("C[1] >= 3")}:
          {this.embedComponent(
            <ProtocolVerifier
              protocol={FLOCK_PROTOCOL_THREE}
              initialConfigParams={FLOCK_PARAMS}
            />
          )}
          <span style={SUBTITLE_STYLE}>Parametric protocols.</span>
          The flock-of-birds protocol is not a protocol per se, it is in fact a
          family of protocols which depends on a parameter <em>c</em>. We refer
          to such a family as a <em>parametric protocol</em>. As we will see in
          the forthcoming sections, Peregrine supports the creation and
          manipulation of parametric protocols.
        </div>
      </div>
    );
  }

  renderModules() {
    return (
      <div style={{ paddingTop: 20 }}>
        {this.renderTitle("2. Protocol analysis", "modules", 2)}
        <div style={CONTENT_STYLE}>
          When a protocol is loaded, it can be analyzed through five modules:{" "}
          <em>protocol details</em>, <em>simulation</em>, <em>spatial simulation</em>, <em>statistics</em>{" "}
          and <em>verification</em>.
        </div>
        {this.renderTitle("2.1 Protocol details", "modules-details", 3)}
        <div style={CONTENT_STYLE}>
          This module allows the user to inspect the basic properties of a
          protocol. For example, the module looks as follows for the majority
          voting protocol of <a href="#started-majority">Section 1.1</a>:
          {this.embedComponent(
            <ProtocolDetails protocol={MAJORITY_PROTOCOL} />
          )}
          The <em>expected predicate</em> is the predicate which the protocol is
          supposed to compute. The <em>true</em> and <em>false</em> states are
          the states of the protocol with output 1 and 0, respectively. The
          protocol can be inspected by toggling the <em>Show transitions</em>{" "}
          button. This displays a list of all transitions of the protocol. If
          the protocol has a large number of transitions, then only a fixed
          number of transitions are displayed. However, the list of transitions
          can be refined by entering specific states in the fields{" "}
          <em>State 1</em> and <em>State 2</em>.
        </div>
        {this.renderTitle("2.2 Simulation", "modules-simulation", 3)}
        <div style={CONTENT_STYLE}>
          This module allows the user to simulate a protocol graphically. The
          number of occurrences of each initial state must first be chosen. Once
          this is done, the initial configuration appears in the player. For
          example, for three occurrences of state <em>Y</em> and two occurrences
          of state <em>N</em>, the player looks as follows:
          {this.embedComponent(
            <PopulationControlDisplay
              protocol={MAJORITY_PROTOCOL}
              population={["Y", "Y", "Y", "N", "N"]}
              viewMode="individualStates"
            />
          )}
          <span style={SUBTITLE_STYLE}>Basics.</span>
          Each square represents an agent and its current state. The color of a
          square represents its output:{" "}
          {this.coloredRectangle("blue", Style.TRUE_COLOR)} for states of output
          1 (<em>true states</em>) and{" "}
          {this.coloredRectangle("magenta", Style.FALSE_COLOR)} for states of
          output 0 (<em>false states</em>). To start a simulation, simply click
          on the <em>Play</em> button. At each step, a transition is picked at
          random among agents that can interact. The simulation halts when a
          terminal configuration is reached. At any point, the simulation can be
          paused with the <em>Pause</em> button. The player keeps track of the
          history of the simulation. The <em>Prev</em> and <em>Next</em> buttons
          let the user skip back and forth in the history. If the user goes back
          in the history and then presses <em>Play</em>, the player's moves
          follow the history deterministically until the history is consumed and
          then new steps will again be picked at random. The history can be
          reset using the <em>Reset</em> button. The speed of the simulation can
          be adjusted with the slider at the bottom.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Manual steps.</span>
          Whenever the player is paused, it is possible to perform manual steps
          of the simulation. This is achieved by clicking on some square
          representing an agent. For example, in the above player, you may click
          on an agent in state <em>Y</em> and an agent in state <em>N</em>.
          These two agents will be converted to states <em>y</em> and <em>n</em>
          , respectively. Note that upon selecting the first agent, it will only
          be possible to click on a second agent with whom an interaction is
          possible. For example, in the above player, it is not possible to
          click on some agent in state <em>Y</em> and then on some other agent
          in state <em>Y</em>, since these two states are not part of a
          transition of the protocol. The selection of the first agent can be
          undone by clicking on the agent a second time. If multiple transitions
          exist for a given pair of states, a popover will let the user choose
          one of them.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>View mode.</span>
          The current configuration can be visualized in either one of two{" "}
          <em>view modes</em>. The view mode can be switched at the bottom left
          of the player. The first mode, depicted above, displays the current
          configuration spatially as a population of agents represented by
          colored squares. The second mode, depicted below, displays the number
          of agents in each state of the protocol. Manual steps are not possible
          in the second mode. By default, the player will automatically switch
          to the second view mode for large configurations. It is possible to
          switch back to the first mode, but it might slow down Peregrine.
          {this.embedComponent(
            <PopulationControlDisplay
              protocol={MAJORITY_PROTOCOL}
              population={["Y", "Y", "Y", "N", "N"]}
              viewMode="horizontalBars"
            />
          )}
        </div>
        {this.renderTitle("2.3 Spatial Simulation", "modules-spatial-simulation", 3)}
        <div style={CONTENT_STYLE}>
          This module offers another way of simulating population protocols.
          Here agents are represented by flying particles, and interactions occur
          whenever two particles collide. As in the previous simulation module,
          there also is a speed slider for the spatial visualization module. 
		  Further, the movement and speed of particles can be influence through
           <i>shock waves</i>, which can be triggered by tapping/clicking in the rectangular arena
          where the particles reside. The height of the simulation canvas is determined by the size of the browser window when the module is loaded. The width of the canvas is dynamic and can be changed by changing the size of the browser window. By clicking <Icon className="material-icons">fullscreen</Icon>, you can switch to a fullscreen view of the simulation. You can try out spatial simulation for the majority protocol below.
          <SpatialSimulator
              protocol={MAJORITY_PROTOCOL}
              population={["Y", "Y", "Y", "N", "N"]}
              viewMode="horizontalBars"
            />

          States of opinion yes/no are visualized by blue/red circles by default.
          The color and shape of individual states can be modified, as shown in <a href="#create-shapes">Section 3.6 </a>.
          The legend on the right hand side helps you identify particle shapes with states.
          Alternatively, state labels can be displayed using the bottom switch.
        </div>
        {this.renderTitle("2.4 Statistics", "modules-statistics", 3)}
        <div style={CONTENT_STYLE}>
          This module allows the user to execute several simulations
          automatically and to visualize/analyze statistics from these
          simulations.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Basics.</span>
          Four parameters can be supplied: the minimal population size{" "}
          <em>
            s<sub>min</sub>
          </em>
          , the maximal population size{" "}
          <em>
            s<sub>max</sub>
          </em>
          , a number of steps <em>m</em> and a number of simulations <em>n</em>.
          Once this is done, the simulations can be generated by clicking on{" "}
          <em>Generate</em>. Peregrine will perform <em>n</em> simulations. Each
          simulation is generated as follows. A population size <em>s</em> is
          picked at random from [
          <em>
            s<sub>min</sub>
          </em>
          ,{" "}
          <em>
            s<sub>max</sub>
          </em>
          ], an initial configuration of size <em>s</em> is picked at random,
          and enabled transitions are applied at random from this configuration
          until either <em>m</em> steps are performed or a terminal
          configuration is reached. The module looks as follows:
          {this.embedComponent(
            <ProtocolStatisticsContainer protocol={MAJORITY_PROTOCOL} />
          )}
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Consensus statistics.</span>
          Peregrine classifies the generated executions according to the
          consensus they end up in:
          <ul>
            <li>
              {this.coloredRectangle(
                "correct and lasting consensus",
                "rgb(25, 100, 25)"
              )}
              <div style={SUBCONTENT_STYLE}>
                The last observed configuration of the execution forms a
                consensus that is correct with respect to the expected predicate
                and it cannot be destroyed if the execution is further extended.
              </div>
            </li>
            <li>
              {this.coloredRectangle(
                "correct consensus",
                "rgb(125, 225, 125)",
                "black"
              )}
              <div style={SUBCONTENT_STYLE}>
                The last observed configuration forms a consensus that is
                correct with respect to the expected predicate, but this
                consensus may potentially be destroyed if the execution is
                extended.
              </div>
            </li>
            <li>
              {this.coloredRectangle(
                "no consensus",
                "rgb(250, 150, 50)",
                "black"
              )}
              <div style={SUBCONTENT_STYLE}>
                At least one true state and one false state is present in the
                last observed configuration.
              </div>
            </li>
            <li>
              {this.coloredRectangle(
                "incorrect consensus",
                "rgb(225, 125, 125)",
                "black"
              )}
            </li>
            <div style={SUBCONTENT_STYLE}>
              The last observed configuration forms a consensus that is
              incorrect with respect to the expected predicate, but this
              consensus may potentially be destroyed if the execution is
              extended.
            </div>
            <li>
              {this.coloredRectangle(
                "incorrect and lasting consensus",
                "rgb(220, 15, 15)"
              )}
              <div style={SUBCONTENT_STYLE}>
                The last observed configuration of the execution forms a
                consensus that is incorrect with respect to the expected
                predicate and it cannot be destroyed if the execution is further
                extended.
              </div>
            </li>
          </ul>
          The first plot displayed by Peregrine depicts this classification. For
          every population size <em>s</em>, the percentage of configurations of
          size <em>s</em> of each consensus type is plotted.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Convergence speed statistics.</span>
          Peregrine provides information on a protocol's convergence speed. For
          each simulation that reached a consensus, Peregrine identifies the
          step at which the consensus was obtained. Then, for each population
          size, Peregrine computes the average number of steps to convergence
          among all consensus-forming executions of that size. This information
          is displayed in the second generated plot. It is possible to exclude
          some types of consensuses from the plot by toggling the legend below
          the plot.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Increased accuracy.</span>
          By default, Peregrine can only conclude that the consensus of a
          configuration is lasting if the configuration is terminal. Peregrine
          is capable of verifying whether a consensus is lasting even when the
          configuration is non terminal. This can be enabled by toggling{" "}
          <em>Increase accuracy</em>. Since this verification is more costly, it
          may slow down the generation time significantly.
        </div>
        {this.renderTitle("2.5 Verification", "modules-verification", 3)}
        <div style={CONTENT_STYLE}>
          <span style={SUBTITLE_STYLE}>Basics.</span>

          This module allows the user to <em>automatically</em> verify whether a
          population protocol computes the specified predicate. Peregrine features
          two verification approaches: <em>Verification (without stage graphs)</em> and
          {" "} <em>Verification (with stage graphs)</em>. The approach can be selected
          in a drop-down menu at the top of the verification module. The first approach is
          based on the technique presented in <a href="https://arxiv.org/abs/1703.04367">{ " " }[PODC'2017]</a> and restricted
          to silent protocols.
          The second approach was described in <i>Checking Qualitative Liveness Properties of
          Replicated Systems with Stochastic Scheduling[CAV'2020]
          </i>
          . The latter approach is more advanced: in addition to being able to verify correctness,
          it provides proof certificates (so-called stage graphs) that are typically succinct and can be independently checked.
          These certificates also make it possible to estimate  average speed until stabilization to consensus.
          A stage graph constists of <em>stages</em>: inductive Presburger constraints that describe sets of configurations.
          Intuitively, these constraints describe stages of the execution of the protocol.
          <br/>
          <br/>
          <span style={SUBTITLE_STYLE}>Verifying with stage graphs.</span>

          For example, consider the
          majority voting protocol of <a href="#started-majority">Section 1.1</a>.
          This protocol is expected to compute {this.renderFormula("C[Y] >= C[N]")}{" "}
          for every initial configuration, regardless of the population size.
          Peregrine can verify this:
          {this.embedComponent(
            <ProtocolVerifier protocol={MAJORITY_PROTOCOL}
                              initialConfigParams={MAJORITY_PARAMS}
            />
          )}

          After clicking the <em>Verify</em> button, a stage graph is generated
          as witness for correctness. Moreover, an overall speed estimate is automatically
          derived from the stage graph and given on top.
          You can inspect the stage graph by clicking on one of its stages, shown in
          the bottom left half of the screen. Clicking on,
          say, Stage 4 yields the following detailed view:
          {this.embedComponent(
            <StageDetailView  protocol={MAJORITY_PROTOCOL}
                              initialConfigParams={MAJORITY_PARAMS}
                              population={["Y", "Y", "N"]}
                              stage={STAGE}
            />
          )}

          The constraint describes the set of configurations for this stage.
          <em>{ " "} PotReach(EXPR) { " "}</em> signifies that the configurations
          of this stage are potentially reachable from some initial configuration
          satisfying <em>{" "} EXPR</em>. Otherwise,
          constraints are described in the same syntax as introduced in Section <a href="#create-predicate">Section 3.2</a>.
          In particular, the above constraint says that all configurations of this stage
          have zero agents in state Y.

           Peregrine also displays
          the transitions that are permanently disabled (dead) at this stage, and
          those that will eventually become dead when the execution transitions to
          the next stage (in this case S5). Moreover, the detail view contains an upper bound
          for the speed of executions that start in any configuration of this stage.

          <br/>
          <br/>
          <span style={SUBTITLE_STYLE}>Stepping through the stage graph.</span>

          You may visualize a run through the stage graph by picking an initial configuration
          and pressing the <em>Play</em> button. You have the usual control buttons from
          the Simulation module at your disposal. Additionally, you may choose to press
          the <em>Pick Progress</em> button. If possible, this will pick a transition
          that reduces the certificate value, thereby making definitive progress towards stabilization.
          You may also press the <em>Reset</em> button to return to the initial configuration
          that was initially selected or from which the current configuration is potentially reachable.

          <br/>

          After multiple steps, Peregrine displays the trajectory of the run
          in the stage graph, with configurations displayed as  gray dots. You
          can navigate through this trajectory by clicking on any such configuration.

          <br/>

          If the run is very large, it may be beneficial to switch to full-screen mode
          by clicking the icon <Icon className="material-icons" style={{padding: "0px", margin: "0px"}}>fullscreen</Icon> below the stage-graph display.
          You may exit the full-screen view by pressing the ESC button on your keyboard.

          <br/>
          <br/>
          <span style={SUBTITLE_STYLE}>Error diagnostics.</span>

          Peregrine can also detect bugs in a protocol. Whenever Peregrine finds
          a bug, it reports a faulty execution which can be replayed
          graphically. For example, if the
          majority voting protocol of{" "}
          <a href="#started-majority">Section 1.1</a> is altered by removing its
          tie-breaker rule <em>d: y&#8201;n → y&#8201;y</em>, then the protocol
          does not correctly compute {this.renderFormula("C[Y] >= C[N]")}. This
          can be detected by Peregrine:
          {this.embedComponent(
            <ProtocolVerifier
              protocol={MAJORITY_PROTOCOL_BROKEN}
              initialConfigParams={MAJORITY_PARAMS}
             />
          )}

          In this case, Peregrine generates a short counter example that leads us to a
          failed stage (shown with striped yellow background), where no more progress towards the correct consensus
          can be made.
        </div>
      </div>
    );
  }

  renderCreate() {
    return (
      <div style={{ paddingTop: 20 }}>
        {this.renderTitle("3. Creating a protocol", "create", 2)}
        <div style={CONTENT_STYLE}>
          New protocols can be created by clicking on{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            note_add
          </Icon>
          <span style={{ fontWeight: "bold" }}>Create new protocol</span> in the
          top left menu{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            menu
          </Icon>{" "}
          or on the Welcome page. This will open a wizard guiding the user
          through the creation of the new protocol.
        </div>
        {this.renderTitle("3.1 Description", "create-description", 3)}
        <div style={CONTENT_STYLE}>
          The protocol must be given a title and optionally a description:
          {this.embedComponent(<NewProtocolView navDisabled step={0} />)}
        </div>
        {this.renderTitle("3.2 Predicate", "create-predicate", 3)}
        <div style={CONTENT_STYLE}>
          The predicate which the protocol should compute must then be entered.
          This predicate can be specified as a quantifier-free formula of
          Presburger arithmetic extended with modulo predicates:
          {this.embedComponent(<NewProtocolView navDisabled step={1} />)}
          Note that multiplication must always involve at least one constant,
          e.g. the following expression is forbidden:{" "}
          {this.renderFormula("C[Y] * C[N] > 0")}.
        </div>
        {this.renderTitle("3.3 Parameters", "create-parameters", 3)}
        <div style={CONTENT_STYLE}>
          Peregrine lets the user describe <em>parametric protocols</em>. Such
          protocols are not protocols per se, but uniform families of population
          protocols which depend on some parameters (see{" "}
          <a href="started-flock">Section 1.2</a> for more details). For
          example, the flock-of-birds protocol of{" "}
          <a href="started-flock">Section 1.2</a> computes{" "}
          {this.renderFormula("C[q] >= c")} where <em>c</em> is a fixed but
          arbitrary non negative integer. Here, <em>c</em> is a parameter of the
          family, i.e. of the parametric protocol. This parameter can be created
          by adding an <em>Integer</em> parameter which ranges from 0 to some
          positive number:
          {this.embedComponent(
            <NewProtocolView
              navDisabled
              step={2}
              params={[
                {
                  name: "c",
                  descr: "Threshold",
                  type: "int",
                  min: 0,
                  max: 100,
                  step: 1,
                },
              ]}
            />
          )}
          It is also possible to create parameters drawn from a finite set. For
          example, if the protocol depends on a Boolean parameter <em>b</em>,
          then this can be represented as follows:
          {this.embedComponent(
            <NewProtocolView
              navDisabled
              step={2}
              params={[
                {
                  name: "b",
                  descr: "Some Boolean parameter",
                  type: "elem",
                  elements: "false, true",
                },
              ]}
            />
          )}
        </div>
        {this.renderTitle("3.4 Editor", "create-editor", 3)}
        <div style={CONTENT_STYLE}>
          Once the first three steps are completed, the new protocol can be
          created either with the visual editor or the code editor. Parametric
          protocols, i.e. protocols with at least one parameter, can only be
          created with the code editor.
          {this.embedComponent(<NewProtocolView navDisabled step={3} />)}
          <span style={SUBTITLE_STYLE}>Visual editor.</span>
          The visual editor comes in handy for creating small protocols such as
          the majority voting protocol of{" "}
          <a href="#started-majority">Section 1.1</a>:
          {this.embedComponent(
            <WYSIWYGEditor protocol={MAJORITY_PROTOCOL} saveDisabled />
          )}
          The output of a state can be changed by clicking on it. A state is
          made initial by click on its gray surrounding container. Note that
          state names are only allowed to be composed of the following
          characters: alphanumeric characters, parentheses, commas, +, - and
          underscores.
          <br />
          <br />
          <span style={SUBTITLE_STYLE}>Code editor.</span>
          Protocols can be described as Python 3.* scripts. For example, the
          majority voting protocol of <a href="started-majority">
            Section 1.1
          </a>{" "}
          can be described as follows:
          {this.embedComponent(
            <EditorPane code={MAJORITY_PROTOCOL_CODE} saveDisabled />
          )}
          Some utilities functions are available through the{" "}
          {this.renderCode("Utils")} module which is imported implicitly. For
          example, given two pairs of states {this.renderCode("pre")} and{" "}
          {this.renderCode("post")},{" "}
          {this.renderCode("Utils.transition(pre, post)")} creates transition{" "}
          <em>pre → post</em>, and{" "}
          {this.renderCode("Utils.transition(pre, post)")} tests whether
          transition <em>pre → post</em> is silent (i.e. has no effect).
          Standard Python modules such as {this.renderCode("math")} can also be
          imported explicitly.
          <br />
          <br />
          The argument {this.renderCode("params")} is a dictionary containing
          the protocol parameters. A parameter <em>p</em> can be accessed
          through {this.renderCode('params["scheme"]["p"]["value"]')}. For
          example, the flock-of-birds protocol of{" "}
          <a href="started-flock">Section 1.2</a> can be described as follows:
          {this.embedComponent(
            <EditorPane code={FLOCK_PROTOCOL_CODE} saveDisabled />
          )}
          The values that parameter <em>p</em> can hold can be accessed through{" "}
          {this.renderCode('params["scheme"]["p"]["values"]')}, and the
          description of <em>p</em> through{" "}
          {this.renderCode('params["scheme"]["p"]["descr"]')}. As for the visual
          mode, states names must be composed of the following characters:
          alphanumeric characters, parentheses, commas, +, - and underscores.
        </div>
        {this.renderTitle("3.5 Protocols with leaders", "create-leaders", 3)}
        <div style={CONTENT_STYLE}>
          Leaders are a constant number of initial agents that are added on top of
          the initial population. For example, imagine instead of computing the majority
          predicate {this.renderFormula("C[Y] >= C[N]")}, we would like to compute {this.renderFormula("C[Y] + 4 >= C[N]")} instead. The offset of 4 could be realized
          by adding 4 leaders in state Y to the majority protocol from
          previous sections. Leaders can be specified in the code via an additional optional
          attribute "leaders", which is specified by a dictionary with state labels as keys, and number values
          values for the multiplicity of leaders in a given state.

          For example, the majority protocol with offset = 4 can be specified as follows:
           {this.embedComponent(
              <EditorPane code={MAJORITY_PROTOCOL_CODE_LEADERS} saveDisabled />
            )}

          The visual protocol editor also allows the user to pick a leader configuration.
          All modules (simulation, spatial simulation, statistics, verification) support leaders,
          and leaders are added to the initial configuration, if necessary.
        </div>
        {this.renderTitle("3.6 Shapes", "create-shapes", 3)}
        <div style={CONTENT_STYLE}>
          The spatial simulation module displays states as particles of various shapes, colors,
          and sizes. The shape, color, and size of a given state can be specified through the optional
          attribute <i>statesStyle</i> in the dictionary returned by <i>generateProtocol</i>.
          This attribute is specified as a dictionary with state labels as
          keys, and values are dictionaries of the form
          <span>
          {
          `
          {
            "shape": "SHAPE",
            "size": NUM,
            "color": "COLOR",
            "fillcolor": "COLOR"
          }
          `
          }
          </span>
          where
          <ul>
            <li> SHAPE is either <i>circle</i>, <i>square</i>, or <i>triangle</i>.</li>
            <li> NUM is a double (e.g. <i>2.5</i>).</li>
            <li> COLOR is a CSS color code (e.g. <i>rgba(255,255,255,0.7)</i> or <i>#FF0000</i>).</li>
          </ul>

          All four attributes <i>shape</i>, <i>size</i>, <i>color</i> and <i>fillcolor</i> are optional.
        </div>
      </div>
    );
  }

  renderAdd() {
    return (
      <div style={{ paddingTop: 20 }}>
        {this.renderTitle("4. Managing protocols", "manage", 2)}
        <div style={CONTENT_STYLE}>
          Protocols can be managed through the top left menu{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            menu
          </Icon>
          .
        </div>
        {this.renderTitle("4.1 Editing protocols", "manage-edit", 3)}
        <div style={CONTENT_STYLE}>
          A protocol can be edited by clicking on the edit icon{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            edit
          </Icon>{" "}
          to its right. Protocols can only be edited with the editor with which
          they were created.
        </div>
        {this.renderTitle("4.2 Removing protocols", "manage-remove", 3)}
        <div style={CONTENT_STYLE}>
          A protocol can be deleted by clicking on the delete icon{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            delete
          </Icon>{" "}
          to its right.
        </div>
        {this.renderTitle("4.3 Importing existing protocols", "manage-add", 3)}
        <div style={CONTENT_STYLE}>
          It is possible to create a protocol as a Python script locally with a
          text editor. Such a protocol can be imported into Peregrine by
          clicking on
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            file_upload
          </Icon>
          <span style={{ fontWeight: "bold" }}>Import protocol from file</span>{" "}
          in the top left menu{" "}
          <Icon className="material-icons" style={{ verticalAlign: "bottom" }}>
            menu
          </Icon>{" "}
          or on the welcome page.
        </div>
      </div>
    );
  }

  render() {
    return (
      <div
        id="documentation"
        style={{
          fontFamily: Style.MAIN_FONT,
          textAlign: "justify",
          padding: 10,
        }}
      >
        <h1>Documentation</h1>
        <br />
        {this.renderMenu()}
        <br />
        {this.renderStarted()}
        {this.renderModules()}
        {this.renderCreate()}
        {this.renderAdd()}
      </div>
    );
  }
}

export default Documentation;
