import React from "react";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import * as Style from "./PopulationStyle";

class CodeSaver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      code: props.code,
      fileName: props.fileName
        ? props.fileName
        : this.normalizedFileName(props.title),
      title: props.title,
      delay: props.delay,
      onCodeSave: props.onCodeSave,
    };
  }

  getTime() {
    return new Date().toLocaleTimeString();
  }

  normalizedFileName(filename) {
    let newFilename =
      !filename || filename.length === 0 ? "new_protocol" : filename;

    newFilename = newFilename.toLowerCase().trim().replace(/ /g, "_").replace(/[^\w]/g, '');

    return newFilename.slice(-3) !== ".py" ? newFilename + ".py" : newFilename;
  }

  validTitle() {
    return this.state.title && this.state.title.trim().length > 0;
  }

  save() {
    if (this.validTitle()) {
      let requestData = {
        fileName: this.state.fileName,
        sourceCode: this.state.code,
        title: this.state.title,
      };

      axios.put("edit", requestData).then(
        function (response) {
          this.state.onCodeSave(requestData);
          this.setState({ lastSave: this.getTime() });
        }.bind(this)
      );
    }
  }


  static getDerivedStateFromProps(props, state) {
    return {
      code: props.code,
      delay: props.delay,
      onCodeSave: props.onCodeSave,
    };
  }

  componentDidUpdate(prevProps, prevState){
    const codeChanged = prevState.code !== this.props.code;
    if (codeChanged) {
          window.clearTimeout(this.autoSaveTimer);
          this.autoSaveTimer = setTimeout(
            this.save.bind(this),
            this.state.delay
          );
    }
  }

  componentWillUnmount() {
    window.clearTimeout(this.autoSaveTimer);
  }

  render() {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "flex-end",
          justifyContent: "space-between",
        }}
      >
        <div>
          <TextField
            placeholder="Enter protocol short title"
            label="Short title"
            value={this.state.title}
            onChange={(e, v) => this.setState({ title: v })}
          />
        </div>
        <div style={{ display: "flex" }}>
          <div
            style={{
              fontFamily: Style.MAIN_FONT,
              padding: "0 5 5 0",
              alignSelf: "flex-end",
            }}
          >
            {this.state.lastSave ? "Last saved at " + this.state.lastSave : ""}
          </div>
          <IconButton
            style={{ padding: "0 0 0 0", width: 30, height: 30 }}
            tooltip={"Save"}
            onClick={(e) => {
              this.save();
            }}
            disabled={!this.validTitle()}
          >
            <Icon className="material-icons">
              save
            </Icon>
          </IconButton>
        </div>
      </div>
    );
  }
}

export default CodeSaver;
