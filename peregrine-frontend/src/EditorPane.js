import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import axios from "axios";
import CodeSaver from "./CodeSaver";
import { Controlled as CodeMirror } from "react-codemirror2";
require("codemirror/mode/python/python");
require("codemirror/lib/codemirror.css");
require("codemirror/theme/rubyblue.css");
require("codemirror/mode/javascript/javascript");

const FONT_SIZE = 16;

class EditorPane extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      errorMessages: [],
      code: props.code ? props.code : "",
      title: props.title,
      fileName: props.fileName,
      onCodeSave: props.onCodeSave,
    };
  }

  reduceError(error) {
    return {
      line: error.line,
      column: error.column,
      evidence: error.obj,
      reason: error.message,
    };
  }

  updateCode(newCode) {
    this.setState({ code: newCode });

    window.clearTimeout(this.errorTimer);

    this.errorTimer = setTimeout(this.checkForErrors.bind(this), 2000);

    if (this.state.onCodeSave) {
      this.state.onCodeSave(newCode);
    }
  }

  checkForErrors() {
    axios.put("lint", { sourceCode: this.state.code }).then(
      function (response) {
        this.setState({ errorMessages: response.data });
      }.bind(this)
    );
  }

  renderErrorMessage(e) {
    return (
      <TableRow key={e.reason + e.line + Date.now()} style={{ fontFamily: "Courier new" }}>
        <TableCell
          style={{ fontSize: FONT_SIZE, width: "10%", textAlign: "left" }}
        >
          {e.line}
        </TableCell>
        <TableCell
          style={{ fontSize: FONT_SIZE, width: "90%", textAlign: "left" }}
        >
          <div dangerouslySetInnerHTML={{ __html: e.reason }}></div>
        </TableCell>
      </TableRow>
    );
  }

  renderErrorMessages() {
    return (
      <Table
        style={{
          width: "100%",
          border:
            this.state.errorMessages.length > 0 ? "3px solid #112435" : "",
        }}
      >
        <TableBody>
          <TableRow>
            <TableCell
              style={{
                fontSize: FONT_SIZE,
                fontWeight: "bold",
                width: "10%",
                textAlign: "left",
              }}
            >
              {this.state.errorMessages.length > 0 ? "Line" : ""}
            </TableCell>
            <TableCell
              style={{
                fontSize: FONT_SIZE,
                fontWeight: "bold",
                width: "90%",
                textAlign: "left",
              }}
            >
              {this.state.errorMessages.length > 0 ? "Error" : ""}
            </TableCell>
          </TableRow>
          {this.state.errorMessages.map(this.renderErrorMessage)}
        </TableBody>
      </Table>
    );
  }

  render() {
    return (
      <div>
        {this.props.saveDisabled ? (
          ""
        ) : (
          <CodeSaver
            code={this.state.code}
            title={this.state.title}
            onCodeSave={this.state.onCodeSave}
            fileName={this.state.fileName}
            delay={3000}
          />
        )}
        <CodeMirror
          key={this.state.title}
          style={{ fontSize: "50pt !important" }}
          value={this.state.code}
          onBeforeChange={(edit, data, value) => this.updateCode(value)}
          options={{ lineNumbers: true, mode: "python", theme: "rubyblue" }}
        />
        <div>{this.renderErrorMessages(this)}</div>
      </div>
    );
  }
}

export default EditorPane;
