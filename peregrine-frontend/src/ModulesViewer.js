import React from "react";
import Icon from "@material-ui/core/Icon";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import AppBar from "@material-ui/core/AppBar";
import PopulationControlDisplay from "./PopulationControlDisplay";
import ProtocolVerifier from "./ProtocolVerifier";
import ProtocolDetails from "./ProtocolDetails";
import ProtocolStatisticsContainer from "./ProtocolStatistics";
import * as Style from "./PopulationStyle";
import SpatialSimulator from "./SpatialSimulator";
import ParameterPicker from "./ParameterPicker";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import Tooltip from "@material-ui/core/Tooltip";
import Box from "@material-ui/core/Box";

class ModulesViewer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curTab: 0,
      params : {configuration: {}},
      isFullScreen: false,
    };
  }
  
  static getDerivedStateFromProps(props, state) { 
	let newState = {params: {configuration: {}}};
	
	let is_microcode_protocol =
        props.params &&
        props.params["scheme"] &&
        props.params["scheme"]["RegisterNumber"] &&
        props.params["scheme"]["NumberOfPhases"];

    for (let i in props.protocol.initialStates) {
      const q = props.protocol.initialStates[i];

      if (!is_microcode_protocol) {
        newState.params.configuration[q.toString()] = {
          descr: "#" + q.toString(),
          values: Array.from({ length: 200 }, (x, i) => i),
          value: i <= 2 ? 3 - i : 0,
        };
      } else {
        newState.params.configuration[
          q.toString()
        ] = this.getInititialProperties(
          q,
          props.params["scheme"]["RegisterNumber"]["value"]
        );
      }
    }
	
	// try to keep current inputs if possible
	if (state.params.configuration) {
      for (let q in state.params.configuration) {
        if (newState.params.configuration[q]) {
          newState.params.configuration[q].value = state.params.configuration[q].value;
		}
      }
    }
	
	return newState;
  }
  
  initialConfiguration() {
    // generate initial population
	let population = [];
    for (let q in this.state.params.configuration) {
      population = population.concat(
        Array(this.state.params.configuration[q].value).fill(q)
      );
    }
	
    //add leaders to current population
    if (typeof this.props.protocol.leaders != "undefined") {
      for (const q in this.props.protocol.leaders) {
        for (let i = 0; i < this.props.protocol.leaders[q]; i++) {
          population.push(q);
        }
      }
    }
    return population;
  }


  static getInititialProperties(state, numberOfRegisters) {
    // this is a microcode protocol
    let description, values, value;
    if (state.includes(";")) {
      // q is a leader
      description = "# Leaders";
      values = [1];
      value = 1;
    } else {
      // find in which register the agent has a 1, if any
      let represented_register = ModulesViewer.findRegister(state, numberOfRegisters);
      description =
        represented_register === null
          ? "# of empty agents"
          : "Initial Value of Register " +
            String.fromCharCode(represented_register + 65);
      values = Array.from({ length: 200 }, (x, i) => i);
      value = 1;
    }
    return { descr: description, values: values, value: value };
  }

  static findRegister(state, numberOfRegisters) {
    let components = state.split("|");
    for (let r = 0; r < numberOfRegisters; r++) {
      if (components[r] === "1") {
        return r;
      }
    }

    return null;
  }

  renderDetailsModule() {
    return (
      <div>
        <ProtocolDetails protocol={this.props.protocol} />
      </div>
    );
  }

  handleParamChange(newParamSetting) {
    let newParams = Object.assign({}, this.state.params);
    for (let q of Object.keys(newParamSetting)) {
      newParams.configuration[q].value = newParamSetting[q];
    }
    this.setState({ params: newParams });
  }

  renderConfigurationParameters(){
    return(
      <div style={{ display: "flex", paddingLeft: "10px" }}>
        <ParameterPicker
            parameters={this.state.params.configuration}
            onParamChange={this.handleParamChange.bind(this)}
        />
      </div>
    );
  }
  
  toggleFullScreen(){
    this.setState({'isFullScreen' : !this.state.isFullScreen});
  }
  
  renderFullscreenButton() {
    const fullScreenButtonIcon = ["fullscreen_exit", "fullscreen"][this.state.isFullScreen ? 0 : 1];
    const fullScreenButtonLabel = ["Exit fullscreen", "Show fullscreen"][this.state.isFullScreen? 0 : 1];
	
	return (
	  <Tooltip title={fullScreenButtonLabel}>
        <IconButton
          aria-label={fullScreenButtonLabel}
          onClick={this.toggleFullScreen.bind(this)}
        >
        <Icon className="material-icons">{fullScreenButtonIcon}</Icon>
        </IconButton>
      </Tooltip>
	);
  }

  renderSimulationModule() {
    return (
      <div>
        {this.renderConfigurationParameters()}
        <div>
          <PopulationControlDisplay
            protocol={this.props.protocol}
            params={this.state.params}
            separateLeaders={true}
            population={this.initialConfiguration()}
          />
        </div>
      </div>
    );
  }

  renderSpatialModule() {
    let spatialModule = (
      <div>
	    <Box display="flex" flexWrap="wrap">
		  <Box>
		    {this.renderConfigurationParameters()} 
		  </Box>
          <Box flexGrow={1}>
		  
	      </Box>
		  <Box>
		    {this.renderFullscreenButton()}
		  </Box>
	    </Box>
        <SpatialSimulator
          protocol={this.props.protocol}
          population={this.initialConfiguration()}
        />
      </div>
    );
	if(this.state.isFullScreen){
      return(<Dialog fullScreen open={this.state.isFullScreen}
                onClose={this.toggleFullScreen.bind(this)}
              >
                {spatialModule}
             </Dialog>);
    }
    else{
      return(
	    <div>
          {spatialModule}
        </div>);
    }
  }

  renderStatisticsModule() {
    return (
      <div
        style={{ padding: "10px 0px 0px 10px", fontFamily: Style.MAIN_FONT }}
      >
        <ProtocolStatisticsContainer protocol={this.props.protocol} />
      </div>
    );
  }

  renderVerificationModule() {
    return (
      <div
        style={{ display: "flex", justifyContent: "center", padding: "10px" }}
      >
        <ProtocolVerifier
          protocol={this.props.protocol}
          initialConfigParams={this.state.params.configuration}
        />
      </div>
    );
  }

  renderCurModule(curTab) {
    let renderFunctions = [
      this.renderDetailsModule,
      this.renderSimulationModule,
      this.renderSpatialModule,
      this.renderStatisticsModule,
      this.renderVerificationModule,
    ];
    if (0 <= curTab && curTab < renderFunctions.length) {
      return renderFunctions[curTab].bind(this)();
    } else {
      return <div>Should not happen. CurTab is {this.state.curTab}. </div>;
    }
  }

  render() {
    const modulesStyle = {
      paddingBottom: "10px",
      boxShadow: Style.BOX_SHADOW,
    };

    return (
      <div style={modulesStyle}>
        <AppBar position="static" color="default">
          <Tabs
            indicatorColor="primary"
            textColor="primary"
            value={this.state.curTab}
            onChange={(event, v) => this.setState({ curTab: v })}
          >
            <Tab
              label="Details"
              icon={<Icon className="material-icons">description</Icon>}
            />
            <Tab
              label="Simulation"
              icon={<Icon className="material-icons">play_circle_outline</Icon>}
            />
            <Tab
              label="Spatial simulation"
              icon={<Icon className="material-icons">grain</Icon>}
            />
            <Tab
              label="Statistics"
              icon={<Icon className="material-icons">equalizer</Icon>}
            />
            <Tab
              label="Verification"
              icon={<Icon className="material-icons">verified_user</Icon>}
            />
          </Tabs>
        </AppBar>
        <div style={{ marginTop: "5px" }}>
          {this.renderCurModule(this.state.curTab)}
        </div>
      </div>
    );
  }
}

export default ModulesViewer;
