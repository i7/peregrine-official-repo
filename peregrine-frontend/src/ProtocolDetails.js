import React from "react";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import TransitionsInspector from "./TransitionsInspector";
import State from "./State"
import { FormulaUtils } from "./FormulaUtils";
import * as Style from "./PopulationStyle";

class ProtocolDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showTransitions: false,
      showInitialStates: true,
      showStates: true,
    };
  }

  handleExpand() {
    this.setState({ showTransitions: !this.state.showTransitions });
  }

  renderStates() {
    const populationFrameStyle = {
      display: "flex",
      flexWrap: "wrap",
    };
    return (
      <div style={populationFrameStyle}>
        {this.props.protocol.states.map((q, key) =>
          <State label={q} protocol={this.props.protocol} key={key} />)
        }
      </div>
    );
  }

  //TODO: remove code duplication
  renderInitialStates() {
    const populationFrameStyle = {
      display: "flex",
      flexWrap: "wrap"
    };
    return (
      <div style={populationFrameStyle}>
        {this.props.protocol.initialStates.map((q, key) =>
          <State label={q} protocol={this.props.protocol} key={key}/>
          )
        }
      </div>
    );
  }

  renderTransitions() {
    return (
      <div style={{ display: "flex", justifyContent: "center" }}>
        <TransitionsInspector protocol={this.props.protocol} />
      </div>
    );
  }

  render() {
    const subtitle = {
      fontWeight: "bold",
    };

    //TODO: remove Code duplication;
    let numOfLeaders = 0;
    if (this.props.protocol.leaders) {
      for (let q in this.props.protocol.leaders) {
        numOfLeaders += this.props.protocol.leaders[q];
      }
    }
    return (
      <div>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "max-content auto",
            alignItems: "center",
            gridColumnGap: "10px",
            gridRowGap: "10px",
            fontFamily: Style.MAIN_FONT,
            fontSize: "14px",
            padding: "10px",
          }}
        >
          <div style={subtitle}>Expected predicate:</div>
          <div
            dangerouslySetInnerHTML={{
              __html: FormulaUtils.print(this.props.protocol.predicate),
            }}
          ></div>

          <div style={subtitle}> Precondition:</div>
          <div
            dangerouslySetInnerHTML={{
              __html: this.props.protocol.precondition
                ? FormulaUtils.print(this.props.protocol.precondition)
                : "None",
            }}
          ></div>

          <div style={subtitle}>Number of states:</div>
          <div>{this.props.protocol.states.length}</div>

          <div style={subtitle}> Number of initial states:</div>
          <div>{this.props.protocol.initialStates.length}</div>

          <div style={subtitle}>Number of true states:</div>
          <div>{this.props.protocol.trueStates.length}</div>

          <div style={subtitle}>Number of false states:</div>
          <div>
            {this.props.protocol.states.length -
              this.props.protocol.trueStates.length}
          </div>

          <div style={subtitle}>Number of transitions:</div>
          <div>{this.props.protocol.transitions.length}</div>

          <div style={subtitle}>Number of Leaders:</div>
          <div>{numOfLeaders}</div>

          <div style={subtitle}>Leaders:</div>
          <div>
            {this.props.protocol.leaders
              ? JSON.stringify(this.props.protocol.leaders)
              : "None"}
          </div>
        </div>

        <div style={{ padding: "10px 0px 0px 10px" }}>
          <Button
            color="primary"
            onClick={() =>
              this.setState({ showStates: !this.state.showStates })
            }
            endIcon={
              <Icon className="material-icons">
                {this.state.showStates ? "expand_less" : "expand_more"}
              </Icon>
            }
          >
            States
          </Button>
          {this.state.showStates ? this.renderStates() : ""}
        </div>

        <div style={{ padding: "10px 0px 0px 10px" }}>
          <Button
            color="primary"
            onClick={() =>
              this.setState({
                showInitialStates: !this.state.showInitialStates,
              })
            }
            endIcon={
              <Icon className="material-icons">
                {this.state.showInitialStates ? "expand_less" : "expand_more"}
              </Icon>
            }
          >
            Initial states
          </Button>
          {this.state.showInitialStates ? this.renderInitialStates() : ""}
        </div>

        <div style={{ padding: "10px 0px 0px 10px" }}>
          <Button
            color="primary"
            onClick={this.handleExpand.bind(this)}
            endIcon={
              <Icon className="material-icons">
                {this.state.showTransitions ? "expand_less" : "expand_more"}
              </Icon>
            }
          >
            Transitions
          </Button>
          {this.state.showTransitions ? this.renderTransitions() : ""}
        </div>
      </div>
    );
  }
}

export default ProtocolDetails;
