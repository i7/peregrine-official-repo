import { ProtocolUtils } from "./ProtocolUtils";

class DeterministicRunner {
  constructor(protocol, population, execution, callback = undefined) {
    this.protocol = protocol;
    this.callback = callback;
    this.history = [[population.slice(), null]];
    this.curr = 0;

    this.history = ProtocolUtils.historyFromExecution(protocol, population, execution);
  }

  reset() {
    this.curr = 0;

    this.updateStatus(null);
  }

  currStep() {
    return this.history[this.curr];
  }

  next() {
    let step = null;

    if (!this.isLast()) {
      this.curr++;

      step = this.currStep();
    }

    this.updateStatus(step);
  }

  prev() {
    let step = null;

    if (this.curr > 0) {
      const population = this.history[this.curr - 1][0];
      const transition = this.history[this.curr][1];
      this.curr--;

      step = [population, transition];
    }

    this.updateStatus(step);
  }

  isFirst() {
    return this.curr === 0;
  }

  isLast() {
    return this.curr === this.history.length - 1;
  }

  isDone() {
    return this.isLast();
  }

  getCurrentPopulation() {
    return this.currStep()[0];
  }

  updateStatus(step) {
    if (this.callback) {
      this.callback({
        isDone: this.isDone(),
        isFirst: this.isFirst(),
        isLast: this.isLast(),
        step: step,
        currentPopulation: this.getCurrentPopulation()
      });
    }
  }
}

export { DeterministicRunner };
