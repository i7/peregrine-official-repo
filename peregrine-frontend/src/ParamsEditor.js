import React from "react";
import Select from "@material-ui/core/Select";
import Icon from "@material-ui/core/Icon";
import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import red from "@material-ui/core/colors/red";

class ParamsEditor extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      paramType: "int",
      params: props.params,
    };
  }

  updateParams(newParams) {
    this.setState({ params: newParams }, () =>
      this.props.onParamsChange(this.state.params)
    );
  }

  updateParam(i, param, value) {
    const newParams = this.state.params.slice();

    newParams[i][param] = value;

    this.updateParams(newParams);
  }

  addParam() {
    let newParams = this.state.params.slice();
    let newParam = {};

    switch (this.state.paramType) {
      case "int":
        newParam = {
          name: "Parameter " + (newParams.length + 1),
          descr: "Description " + (newParams.length + 1),
          type: "int",
          min: 1,
          max: 10,
          step: 1,
        };
        break;
      case "elem":
        newParam = {
          name: "Parameter " + (newParams.length + 1),
          descr: "Description " + (newParams.length + 1),
          type: "elem",
          elements: "x, y, z",
        };
        break;
      default:
        // case "renderFreeParam"
        newParam = {
          name: "Parameter " + (newParams.length + 1),
          descr: "Description " + (newParams.length + 1),
          type: "free",
        };
        break;
    }

    newParams.push(newParam);
    this.updateParams(newParams);
  }

  removeParam(i) {
    let newParams = this.state.params.slice();

    newParams.splice(i, 1);

    this.updateParams(newParams);
  }

  renderAddParameter() {
    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        Parameter type:
        <FormControl>
          <div style={{ margin: "20px" }}>
            <Select
              value={this.state.paramType}
              style={{ paddingBottom: 15, minWidth: 200 }}
              onChange={(event) =>
                this.setState({ paramType: event.target.value })
              }
            >
              <MenuItem value={"int"}> Integer </MenuItem>
              <MenuItem value={"elem"}> From finite set </MenuItem>
              <MenuItem value={"free"}> Text Input </MenuItem>
            </Select>
          </div>
        </FormControl>
        <Button
          color="primary"
          variant="contained"
          onClick={(e) => this.addParam()}
        >
          Add
        </Button>
      </div>
    );
  }

  renderParameters() {
    return <div>{this.state.params.map((p, i) => this.renderParam(i))}</div>;
  }

  renderIntegerParam(i) {
    return (
      <div
        style={{ display: "flex", alignItems: "flex-end", marginTop: "20px" }}
      >
        <TextField
          label="Parameter name"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].name}
          style={{ width: 200 }}
          onChange={(event) => this.updateParam(i, "name", event.target.value)}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Description"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].descr}
          style={{ width: 250 }}
          onChange={(event) => this.updateParam(i, "descr", event.target.value)}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Min. value"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].min}
          type="number"
          onChange={(event) => this.updateParam(i, "min", event.target.value)}
          style={{ width: 100 }}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Max. value"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].max}
          type="number"
          onChange={(event) => this.updateParam(i, "max", event.target.value)}
          style={{ width: 100 }}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Step"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].step}
          type="number"
          min="1"
          step="1"
          onChange={(event) =>
            this.updateParam(
              i,
              "step",
              Math.max(Math.ceil(event.target.value), 1)
            )
          }
          style={{ width: 100 }}
        />
        <div style={{ width: 10 }} />
        {this.renderRemoveParam(i)}
      </div>
    );
  }

  renderElemParam(i) {
    return (
      <div
        style={{ display: "flex", alignItems: "flex-end", marginTop: "20px" }}
      >
        <TextField
          label="Parameter name"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].name}
          style={{ width: 200 }}
          onChange={(event) => this.updateParam(i, "name", event.target.value)}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Description"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].descr}
          style={{ width: 250 }}
          onChange={(event) => this.updateParam(i, "descr", event.target.value)}
        />
        <div style={{ width: 10 }} />
        <TextField
          placeholder="Comma separated elements"
          label="Set elements"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].elements}
          onChange={(event) =>
            this.updateParam(i, "elements", event.target.value)
          }
          style={{ width: 320 }}
        />
        <div style={{ width: 10 }} />
        {this.renderRemoveParam(i)}
      </div>
    );
  }

  renderFreeParam(i) {
    return (
      <div
        style={{ display: "flex", alignItems: "flex-end", marginTop: "20px" }}
      >
        <TextField
          label="Parameter name"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].name}
          style={{ width: 200 }}
          onChange={(event) => this.updateParam(i, "name", event.target.value)}
        />
        <div style={{ width: 10 }} />
        <TextField
          label="Description"
          InputLabelProps={{
            shrink: true,
          }}
          value={this.state.params[i].descr}
          style={{ width: 250 }}
          onChange={(event) => this.updateParam(i, "descr", event.target.value)}
        />
        <div style={{ width: 10 }} />
        {this.renderRemoveParam(i)}
      </div>
    );
  }

  renderRemoveParam(i) {
    return (
      <IconButton
        style={{ padding: "0 0 32 0", width: 20, height: 20 }}
        iconStyle={{ fontSize: 20, fontWeight: "bold" }}
        tooltip={"Remove"}
        touch={true}
        onClick={(e) => this.removeParam(i)}
      >
        <Icon className="material-icons" hoverColor={red[400]}>
          clear
        </Icon>
      </IconButton>
    );
  }

  renderParam(i) {
    if (this.state.params[i].type === "int") return this.renderIntegerParam(i);
    if (this.state.params[i].type === "elem") return this.renderElemParam(i);
    return this.renderFreeParam(i);
  }

  render() {
    return (
      <div style={{ padding: "20px" }}>
        {this.renderAddParameter()}
        <div style={{ height: 20 }} />
        {this.renderParameters()}
      </div>
    );
  }
}
export default ParamsEditor;
