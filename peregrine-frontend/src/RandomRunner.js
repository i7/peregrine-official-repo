import { ProtocolUtils } from "./ProtocolUtils";

class RandomRunner {
  constructor(protocol, population, callback = undefined) {
    this.protocol = protocol;
    this.population = population.slice();
    this.callback = callback;
    this.history = [[this.population, null]];
    this.curr = 0;
    this.transitionDict = {}; //lookup table for faster access to transitions
    for (let t of protocol.transitions) {
      let key = t.pre[0] + t.pre[1];
      if (key in this.transitionDict) {
        this.transitionDict[key].push(t);
      } else {
        this.transitionDict[key] = [t];
      }
    }
  }

  getTransitionsForPreset(q1, q2) {
    let ts = [];
    for (let x of [q1 + q2, q2 + q1]) {
      if (this.transitionDict[x] !== undefined) {
        ts = ts.concat(this.transitionDict[x]);
      }
    }
    return ts;
  }

  reset() {
    this.history = [[this.population, null]];
    this.curr = 0;

    this.updateStatus(null);
  }

  currStep() {
    return this.history[this.curr];
  }

  next() {
    let step;

    if (this.curr === this.history.length - 1) {
      step = this.randomStep();

      if (step !== null) {
        this.history.push(step);
        this.curr++;
      }
    } else {
      this.curr++;

      step = this.currStep();
    }

    this.updateStatus(step);
  }

  /*
    manual step randomly selected from list of enabled transitions
  */
  nextFrom(enabledTransitions) {
    // If manual step occurs in the middle of the history, then delete future steps
    if (!this.isLast()) {
      this.history.splice(this.curr + 1);
    }

    let step = this.randomStep(enabledTransitions);

    if (step !== null) {
      this.history.push(step);
      this.curr++;
    }

    this.updateStatus(step, true);
  }

  manualNext(transition, agents) {
    const population = this.currStep()[0];
    const newPopulation = ProtocolUtils.applyTransitionAt(
      population,
      transition,
      agents[0],
      agents[1]
    );
    const step = [newPopulation, { x: agents[0], y: agents[1] }];

    // If manual step occurs in the middle of the history, then delete future steps
    if (!this.isLast()) {
      this.history.splice(this.curr + 1);
    }

    this.history.push(step);
    this.curr = this.history.length - 1;

    this.updateStatus(step, true);
  }

  prev() {
    let step = null;

    if (this.curr > 0) {
      const population = this.history[this.curr - 1][0];
      const transition = this.history[this.curr][1];
      this.curr--;

      step = [population, transition];
    }

    this.updateStatus(step);
  }

  isFirst() {
    return this.curr === 0;
  }

  isLast() {
    return this.curr === this.history.length - 1;
  }

  isDone() {
    const enabledTransitions = this.getTransitions();
    return enabledTransitions.length === 0;
  }

  length() {
    return this.history.length;
  }

  stepAt(position) {
    return this.history[position];
  }

  getCurrent() {
    return this.curr;
  }

  setCurrent(newCurrent) {
    this.curr = newCurrent;
    this.updateStatus(null);
  }

  getCurrentPopulation() {
    return this.currStep()[0];
  }

  /*
    getTransitions() returns list of randomly picked pointed enabled transitions for current population.
    If no transition can be found  after n samples, all pointed transitions are computed.
  */
  getTransitions() {
    const population = this.getCurrentPopulation();
    if (population.length <= 1) {
      // if there are not at least 2 agents, then no transitions are enabled
      return [];
    }

    let enabledTransitions = [];
    for (let i = 0; i < population.length; i++) {
      let x = Math.floor(Math.random() * population.length);
      let y = x;
      while (y === x) y = Math.floor(Math.random() * population.length);
      let ts = this.getTransitionsForPreset(population[x], population[y]);
      enabledTransitions = ts.map((t) => {
        return { x: x, y: y, transition: t };
      });
      if (enabledTransitions.length > 0) {
        break;
      }
    }

    if (enabledTransitions.length === 0) {
      enabledTransitions = ProtocolUtils.enabledPointedTransitions(
        this.protocol,
        population
      );
    }
    return enabledTransitions;
  }

  randomStep(enabledTransitions = null) {
    const population = this.getCurrentPopulation();
    if (!enabledTransitions) {
      enabledTransitions = this.getTransitions();
    }
    const n = enabledTransitions.length;
    let step = null;
    if (n > 0) {
      const i = Math.floor(Math.random() * n);
      const t = enabledTransitions[i];
      const populationSucc = ProtocolUtils.applyTransitionAt(
        population,
        t.transition,
        t.x,
        t.y
      );
      step = [populationSucc, t];
    }

    return step;
  }

  updateStatus(step, manualStep = false) {
    if (this.callback) {
      this.callback({
        isDone: this.isDone(),
        isFirst: this.isFirst(),
        isLast: this.isLast(),
        step: step,
        manualStep: manualStep,
        currentPopulation: this.getCurrentPopulation()
      });
    }
  }
}

export { RandomRunner };
