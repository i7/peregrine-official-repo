import React from "react";
import Button from "@material-ui/core/Button";
import Slider from "@material-ui/core/Slider";
import { RandomRunner } from "./RandomRunner";
import { ProtocolUtils } from "./ProtocolUtils";
import Icon from "@material-ui/core/Icon";
import Snackbar from "@material-ui/core/Snackbar";
import Tooltip from "@material-ui/core/Tooltip";
import PopulationDisplay from "./PopulationDisplay"
import * as Style from "./PopulationStyle";

const MAX_DELAY = 500.0;

class PopulationControlDisplay extends React.PureComponent {
  constructor(props) {
    super(props);

    this.runner = this.props.runnerGenerator(
      this.props.protocol,
      this.props.population,
      this.handleNewStatus.bind(this)
    );
    this.state = {
      speed: this.props.speed ? this.props.speed : 30,
      population: this.runner.getCurrentPopulation(),
      playing: this.props.playing,
      message: "",
      messageOpen: false,
      onTransComplete: null,
      viewMode :
        (this.props.population.length > 100
          ? "horizontalBars"
          : "individualStates")
    };
  }

  stopPlay(){
    if(this.props.onStopPlaying){
      this.props.onStopPlaying();
    }
    else{
      this.setState({ playing: false, activeAgents : []});
    }
  }

  handleNewStatus(status) {
    let newPopulation = status.currentPopulation;
    let newActiveAgents = null;
    let callback = null;
    let playing = this.props.playing ? this.props.playing : this.state.playing;
    let messageOpen = false;
    let message = "";

    if (status.step) {
      newPopulation = status.step[0];
      const transition = status.step[1];
      newActiveAgents = [transition.x, transition.y];
      callback = this.state.playing ? this.play.bind(this) : this.stopPlay.bind(this);
    }
    else if (this.state.playing) {
      this.stopPlay();
      playing = false;
    }
  if (this.runner.isDone()) {
      const consensus = ProtocolUtils.consensus(
        this.props.protocol,
        status.currentPopulation
      );
      message =
        "Reached a terminal configuration with " +
        (consensus == null
          ? "no consensus."
          : "consensus '" + consensus + "'.");
      messageOpen = true;
      this.stopPlay();
    }

    this.setState({
      population: newPopulation,
      activeAgents: newActiveAgents,
      message: message,
      messageOpen: messageOpen,
      playing: playing,
      onTransComplete: callback,
      isDone: this.runner.isDone()
    });
  }

  changeActiveStates(newActiveAgents, callback) {
    this.setState({ activeAgents: newActiveAgents }, callback);
  }

  changePopulation(newPopulation, callback) {
    this.setState({ activeAgents: [], population: newPopulation }, callback);
  }

  getSpeed() {
    if(this.state.playing){
      const speed = this.props.speed ? this.props.speed : this.state.speed;
      const val = MAX_DELAY / 2 + (MAX_DELAY / 2 - speed)*5;
      return val < 0.5 ? 0.5 : val;
    }
    else{
      return 800;
    }
  }

  play() {
    if(this.state.playing){
      this.runner.next();
    }
  }

  handlePlayButton() {
    if (!this.state.playing) {
      if(this.props.onStartPlaying){
        this.props.onStartPlaying();
      }
      this.setState({'playing': true}, () => this.play());
    } else {
      this.stopPlay();
    }
  }

  handleResetButton(event) {
    this.runner.reset();
  }

  handlePrevButton(event) {
    this.runner.prev();
  }

  handleNextButton(event) {
    this.runner.next();
  }

  handleSpeedSlider(event, value) {
    if(this.props.onSpeedChange){
      this.props.onSpeedChange(value);
    }
    else{
      this.setState({ speed: value });
    }
  }

  manualStep(transition, agents) {
    this.runner.manualNext(transition, agents);
  }

  componentDidUpdate(prevProps, prevState){
    if( prevProps.protocol !== this.props.protocol ||
        prevProps.population !== this.props.population ||
        prevProps.activeAgents !== this.props.activeAgents ||
        prevProps.runnerGenerator !== this.props.runnerGenerator
       ){
      let newPopulation = this.props.population.slice();
      let newViewMode =
        this.state.viewMode !== "columnRegisters"
          ? this.state.viewMode
          : "individualStates";
      let newRunner = this.props.runnerGenerator(
        this.props.protocol,
        newPopulation,
        this.handleNewStatus.bind(this)
      );
      const playing = this.props.playing !== undefined ? this.props.playing : this.state.playing;
      this.setState(
        {
          population: newRunner.getCurrentPopulation(),
          viewMode: newViewMode,
          playing: playing,
          stateWidth: Style.calculateStateWidth(this.props.protocol),
          isDone: newRunner.isDone()
        },
        () => {
          this.runner = newRunner;
        }
      );
    }
  }

  makeLabel(str) {
    return !this.isMobile ? str : "";
  }

  renderResetButton(buttonStyle) {
    if (!this.props.hideResetButton) {
      return (
        <Button
          onClick={this.handleResetButton.bind(this)}
          disabled={this.state.playing}
          style={buttonStyle}
          color="primary"
          variant="contained"
          startIcon={<Icon className="material-icons">{"refresh"}</Icon>}
        >
          {this.makeLabel("Reset")}
        </Button>
      );
    }
  }

  renderSimulationControls() {
    let buttonSize = function () {
      return window.innerWidth <= 800 ? "80px" : "120px";
    };

    const buttonStyle = {
      textAlign: "center",
      fontSize: "16px",
      margin: "2px",
      borderRadius: "4px",
      minWidth: buttonSize(),
      width: buttonSize(),
      minHeight: "30px",
    };

    return (
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
          flexWrap: "wrap",
          paddingTop: "15px",
        }}
      >
        <div style={{ display: "flex", justifyContent: "flex-start" }}>
          <Button
            onClick={this.handlePrevButton.bind(this)}
            disabled={this.state.playing || this.runner.isFirst()}
            style={buttonStyle}
            color="primary"
            variant="contained"
            startIcon={
              <Icon className="material-icons">{"skip_previous"}</Icon>
            }
          >
            {this.makeLabel("Prev")}
          </Button>
          <Button
            onClick={this.handlePlayButton.bind(this)}
            disabled={this.state.isDone}
            style={buttonStyle}
            color="primary"
            variant="contained"
            startIcon={
              <Icon className="material-icons">
                {this.state.playing ? "pause" : "play_arrow"}
              </Icon>
            }
          >
            {this.makeLabel(this.state.playing ? "Pause" : "Play")}
          </Button>
          <Button
            onClick={this.handleNextButton.bind(this)}
            disabled={this.state.playing || this.runner.isLast()}
            style={buttonStyle}
            color="primary"
            variant="contained"
            startIcon={<Icon className="material-icons">{"skip_next"}</Icon>}
          >
            {this.makeLabel("Next")}
          </Button>
          {this.renderResetButton(buttonStyle)}
          {this.props.renderAdditionalControlButtons()}
        </div>
      </div>
    );
  }

  handleViewMode(mode) {
    this.setState({ viewMode: mode });
  }

  renderViewModeSelector() {
    return (
      <div>
        <Tooltip title="Tiled view">
          <span>
            <Button
              style={{ width: "40px", minWidth: "40px" }}
              color="primary"
              onClick={this.handleViewMode.bind(this, "individualStates")}
              disabled={this.state.viewMode === "individualStates"}
            >
            <Icon className="material-icons">{"view_comfy"}</Icon>
            </Button>
          </span>
        </Tooltip>
        <Tooltip title="Bar View">
          <span>
            <Button
              color="primary"
              style={{ width: "40px", minWidth: "40px" }}
              onClick={this.handleViewMode.bind(this, "horizontalBars")}
              disabled={this.state.viewMode === "horizontalBars"}
            >
            <Icon className="material-icons">{"format_align_left"}</Icon>
            </Button>
          </span>
        </Tooltip>
      </div>
    );
  }


  renderBottomControls() {
    return (
      <div style={{height: '50px', marginLeft: "10px", marginRight: "10px", marginTop: "10px"}}>
        <div style={{width: "20%", float: "left"}}>
          {this.renderViewModeSelector()}
        </div>
        <div style={{width: "80%", float: "right", display: "flex", paddingTop: "5px"}}>
            <Icon className="material-icons" style={{ fontSize: "16px", paddingRight: "5px", paddingTop: "5px"}} color="primary">
              {"timer"}
            </Icon>
          <Tooltip title="Acceleration">
            <Slider
              min={0.5}
              max={MAX_DELAY}
              style={{ height: "10px", maxWidth: "580px"}}
              value={this.props.speed ? this.props.speed : this.state.speed}
              onChange={this.handleSpeedSlider.bind(this)}
            />
          </Tooltip>
        </div>
      </div>
    );
  }


  renderMessages() {
    return (
      <Snackbar
        open={this.state.messageOpen}
        message={this.state.message}
        autoHideDuration={5000}
        onClose={() => this.setState({ messageOpen: false })}
      />
    );
  }

  render() {
    return (
      <div>
        <PopulationDisplay
          population={this.state.population}
          transitionSpeed={this.getSpeed()}
          onTransitionSelect={this.manualStep.bind(this)}
          protocol={this.props.protocol}
          activeAgents={this.state.activeAgents}
          manualPickerDisabled={this.props.manualPickerDisabled}
          separateLeaders={this.props.separateLeaders}
          onTransitionComplete={this.state.onTransComplete}
          viewMode={this.state.viewMode}
        />
        {this.renderSimulationControls()}
        {this.renderBottomControls()}
        {this.renderMessages()}
      </div>
    );
  }
}

PopulationControlDisplay.defaultProps = {
  runnerGenerator: (protocol, population, callback) => {
    return new RandomRunner(protocol, population, callback);
  },
  manualPickerDisabled: false,
  hideResetButton: false,
  playing: false,
  separateLeaders: false,
  renderAdditionalControlButtons: () => {return (<></>)}
};

export default PopulationControlDisplay;
