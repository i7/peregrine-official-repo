import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import State from "./State"
import * as Style from "./PopulationStyle";

const NUM_TRANSITIONS = 50;
const PADDING = 0;

function getFilteredTransitionsFromState(props, state) {
  return props.protocol.transitions.filter(x =>
       (!state.filter1 || String(x["pre"][0]).trim() === state.filter1.trim())
    && (!state.filter2 || String(x["pre"][1]).trim() === state.filter2.trim())
  );
}

class TransitionsInspector extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filter1: "",
      filter2: "",
      filtered: this.props.protocol.transitions,
      size: Style.calculateStateWidth(this.props.protocol),
    };
  }
    
  static getDerivedStateFromProps(props, state){
    return { filtered: getFilteredTransitionsFromState(props, state) };
  }

  updateFilter(str, i) {
      this.setState((state, props) => {
          let update = {filter1: state.filter1, filter2: state.filter2};
          update["filter" + i] = str;
          update.filtered = getFilteredTransitionsFromState(props, update);
          return update;
      });
  }

  render() {
   const wrapperStyle = {
      display: "grid",
      gridTemplateColumns: "58px 58px",
    };
    return (
      <div>
        <Table style={{ width: 320, overflow: "hidden" }}>
          <TableHead>
            <TableRow>
              <TableCell
                style={{
                  width: this.state.size,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                }}
              >
                <Autocomplete
                  options={this.props.protocol.states}
                  freeSolo
                  onChange={(event, str) => this.updateFilter(str, 1, this)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ width: this.state.size }}
                      label="State1"
                    />
                  )}
                />
              </TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                }}
              >
                <Autocomplete
                  options={this.props.protocol.states}
                  freeSolo
                  onChange={(event, str) => this.updateFilter(str, 2, this)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      style={{ width: this.state.size }}
                      label="State2"
                    />
                  )}
                />
              </TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                }}
              ></TableCell>
              <TableCell
                style={{
                  width: (2 * this.state.size + PADDING),
                  paddingLeft: PADDING,
                  paddingRight: 0,
                  fontSize: 16,
                  textAlign: "center",
                }}
              >
                Successors
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.filtered.map((x, i) =>
              i >= NUM_TRANSITIONS ? (
                ""
              ) : (
                <TableRow key={i}>
                <TableCell style={{padding: PADDING}}>
                  <State
                    size={this.state.size}
                    label={x["pre"][0]}
                    protocol={this.props.protocol}
                  />
                </TableCell>
                <TableCell style={{padding: PADDING}}>
                  <State
                    size={this.state.size}
                    label={x["pre"][1]}
                    protocol={this.props.protocol}
                  />
                </TableCell>
                <TableCell>
                  <State
                    size={this.state.size}
                    label={"↦"}
                  />
                </TableCell>
                <TableCell style={{
                  width: (2 * this.state.size + 2*PADDING)}}>
                  <div style={wrapperStyle}>
                  <State
                    size={this.state.size}
                    label={x["post"][0]}
                    protocol={this.props.protocol}
                  />
                  <State
                    size={this.state.size}
                    label={x["post"][1]}
                    protocol={this.props.protocol}
                  />
                  </div>
                </TableCell>
                </TableRow>
              )
            )}
            <TableRow>
              <TableCell
                style={{
                  width: this.state.size,
                  fontSize: 22,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                  textAlign: "center",
                }}
              >
                {this.state.filtered.length > NUM_TRANSITIONS ? "⋮" : ""}
              </TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  fontSize: 22,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                  textAlign: "center",
                }}
              >
                {this.state.filtered.length > NUM_TRANSITIONS ? "⋮" : ""}
              </TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                }}
              ></TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  fontSize: 22,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                  textAlign: "center",
                }}
              >
                {this.state.filtered.length > NUM_TRANSITIONS ? "⋮" : ""}
              </TableCell>
              <TableCell
                style={{
                  width: this.state.size,
                  fontSize: 22,
                  paddingLeft: 0,
                  paddingRight: PADDING,
                  textAlign: "center",
                }}
              >
                {this.state.filtered.length > NUM_TRANSITIONS ? "⋮" : ""}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default TransitionsInspector;
