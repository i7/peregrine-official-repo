import React from "react";
import Cytoscape from 'cytoscape';
import * as util from 'cytoscape/src/util/extend';
import fcose from 'cytoscape-fcose';
import noOverlap from 'cytoscape-no-overlap';
import CytoscapeComponent from "react-cytoscapejs";
import TeX from "@matejmazur/react-katex";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import CardContent from "@material-ui/core/CardContent";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import "katex/dist/katex.min.css";
import State from "./State";
import { FormulaUtils } from "./FormulaUtils";
import { ProtocolUtils } from "./ProtocolUtils";
import PopulationControlDisplay from "./PopulationControlDisplay";
import { RandomRunner } from "./RandomRunner";
import { InitializedRandomRunner } from "./InitializedRandomRunner";
import axios from "axios";
import ParameterPicker from "./ParameterPicker";
import Typography from "@material-ui/core/Typography";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import Divider from "@material-ui/core/Divider";

Cytoscape.use(fcose);
Cytoscape.use(noOverlap);


class StageGraphUtils {
  static consensusValueToProperty(consensusValue) {
    if (consensusValue) {
      return "TRUE_CONSENSUS";
    }
    else {
      return "FALSE_CONSENSUS";
    }
  }

  static getStageById(stages, id) {
    return stages.find((s) => s["id"] === id);
  }

  static propertyToConsensusValue(checkedProperty) {
    if (checkedProperty === "TRUE_CONSENSUS") {
      return true;
    }
    else if (checkedProperty === "FALSE_CONSENSUS") {
      return false;
    }
    else {
      throw new Error("Unsupported checked property: " + checkedProperty);
    }
  }

  static configurationToPopulation(configuration) {
    let population = [];
    for (let q in configuration) {
      population = population.concat(
        Array(configuration[q]).fill(q)
        );
    }
    return population;
  }

  static configurationValue(configuration, state) {
    let value = 0;
    for (let q of Object.keys(configuration)) {
      if (q === state) {
        value += configuration[q];
      }
    }
    return value;
  }

  static populationValue(population, state) {
    return population
      .filter(populationState => populationState === state)
      .length
  }

  static populationsEqual(p1, p2) {
    if (p1 === p2) return true;
    if (p1 == null || p2 == null) return false;
    if (p1.length !== p2.length) return false;

    let p1s = p1.slice();
    let p2s = p2.slice();
    p1s.sort();
    p2s.sort();

    for (var i = 0; i < p1s.length; i++) {
      if (p1s[i] !== p2s[i]) return false;
    }
    return true;
  }

  static speedToLatex(speed) {
    switch (speed) {
      case "ZERO":
        return String.raw`\mathcal{O}(1)`;
      case "QUADRATIC":
        return String.raw`\mathcal{O}(n^2)`;
      case "QUADRATIC_LOG":
        return String.raw`\mathcal{O}(n^2 \log n)`;
      case "CUBIC":
        return String.raw`\mathcal{O}(n^3)`;
      case "POLYNOMIAL":
        return String.raw`\mathcal{O}(n^k)`;
      case "EXPONENTIAL":
        return String.raw`2^{\mathcal{O}(n \log n)}`;
      default:
        return String.raw`\bot`;
    }
  }

  static collectCounterExamples(stageGraph) {
    let ex = [];
    for (let stage of stageGraph.stages) {
      if (stage.failed && stage.reachSequence && stage.configurationUnstable) {
        let consensus = StageGraphUtils.propertyToConsensusValue(stage.checkedProperty);
        ex.push({ id: stage.id, expectedConsensus: consensus });
      }
    }
    return ex;
  }

  static collectFailedStages(stageGraph) {
    let ids = [];
    for (let stage of stageGraph.stages) {
      if (stage.failed) {
        ids.push(stage.id);
      }
    }
    return ids;
  }

  static evaluateCertificate(population, certificate) {
    if (certificate) {
      return certificate
        .map(term => term.value * StageGraphUtils.populationValue(population, term.state))
        .reduce((a, b) => a + b, 0)
        .toString();
    }
    else {
      return "⊥"
    }
  }
}

let GRAPH_STYLE = {
  stageBorderWidth: 2,
  stageBackgroundColor: "#eddeec",
  stageHighlightBackgroundColor: '#f6eef6',
  stageSelectBorderWidth: 3,

  textColor: "#ffffff",
  selectTextColor: "#ffff00",

  trueBorderColor: "#3f51b5", //"#616161"
  falseBorderColor: "#e91e63",

  terminalTrueBackgroundColor: "#6172d1", //"#3f51b5", //"#9fa8da",
  terminalFalseBackgroundColor: "#f05588", //"#e91e63", // "#f48eb1",

  terminalTrueHighlightBackgroundColor: "#b0b8e8",
  terminalFalseHighlightBackgroundColor: "#f8aac4",

  failedBackgroundColor1: "#9fa8da",
  failedBackgroundColor2: "#f48eb1",

  failedHighlightBackgroundColor1: "#cfd4ec",
  failedHighlightBackgroundColor2: "#fac6d8",

  configurationBackgroundColor: "#ffffff",
  configurationSelectBackgroundColor: "#ffff00",
  configurationHighlightBackgroundColor: "#999999",
  configurationSelectHighlightBackgroundColor: "#999900",

  configurationBorderColor: "#000000",
  configurationWidth: 10,
  configurationHeight: 10,
  configurationBorderWidth: 1,
  numberDeadTransitionsShown: 5,
}

GRAPH_STYLE.terminalFalseStripes = {
  colors: [GRAPH_STYLE.stageBackgroundColor, GRAPH_STYLE.terminalFalseBackgroundColor],
  numbers: 10
};
GRAPH_STYLE.terminalFalseHighlightStripes = {
  colors: [GRAPH_STYLE.stageHighlightBackgroundColor, GRAPH_STYLE.terminalFalseHighlightBackgroundColor],
  numbers: 10
};

GRAPH_STYLE.terminalTrueStripes = {
  colors: [GRAPH_STYLE.stageBackgroundColor, GRAPH_STYLE.terminalTrueBackgroundColor],
  numbers: 10
};
GRAPH_STYLE.terminalTrueHighlightStripes = {
  colors: [GRAPH_STYLE.stageHighlightBackgroundColor, GRAPH_STYLE.terminalTrueHighlightBackgroundColor],
  numbers: 10
};

GRAPH_STYLE.failedStripes = {
  colors: [GRAPH_STYLE.failedBackgroundColor1, GRAPH_STYLE.failedBackgroundColor2],
  numbers: 10
};

GRAPH_STYLE.failedHighlightStripes = {
  colors: [GRAPH_STYLE.failedHighlightBackgroundColor1, GRAPH_STYLE.failedHighlightBackgroundColor2],
  numbers: 10
};

GRAPH_STYLE.expandStripes = (stripes) => {
  let colors = stripes.colors;
  let n = stripes.numbers;
  let stopColors = [];
  let stopPositions = [];
  for (let i = 0; i < n; i++) {
    stopColors.push(colors[i % colors.length]);
    stopColors.push(colors[i % colors.length]);
    stopPositions.push(i*100/n + "%");
    stopPositions.push((i+1)*100/n + "%");
  }
  return {
    stopColors: stopColors,
    stopPositions: stopPositions,
  }
}

GRAPH_STYLE.stripeCSSStyle = (stripes) => {
  let s = GRAPH_STYLE.expandStripes(stripes);
  let stops = s.stopColors.map(function(c, i) {
    return `${c} ${s.stopPositions[i]}`;
  }).join(", ");
  return {
    backgroundImage: `linear-gradient(to bottom right, ${stops})`
  }
}

GRAPH_STYLE.stripeCytoscapeStyle = (stripes) => {
  let s = GRAPH_STYLE.expandStripes(stripes);
  return {
    'background-fill': 'linear-gradient',
    'background-gradient-stop-colors': s.stopColors.join(" "),
    'background-gradient-stop-positions': s.stopPositions.join(" "),
    'background-gradient-direction': 'to-bottom-right'
  }
}

/*
  TransitionView renders eventually and dead transitions.
*/
class TransitionView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showEventuallyDeadTransitions: false,
      showDeadTransitions: false,
    };
  }

  handleExpand(stateVariable) {
    let newState = {};
    newState[stateVariable] = !this.state[stateVariable];
    this.setState(newState);
  }

  renderTransition(transition, index) {
    const size = "40px";
    const wrapperStyle = {
      display: "grid",
      gridTemplateColumns: "50px 50px 45px 50px 50px",
    };
    return (
      <div key={index} style={wrapperStyle}>
        <State
          size={size}
          label={transition["pre"][0]}
          protocol={this.props.protocol}
        />
        <State
          size={size}
          label={transition["pre"][1]}
          protocol={this.props.protocol}
        />
        <State
          size={size}
          label={"↦"}
        />
        <State
          size={size}
          label={transition["post"][0]}
          protocol={this.props.protocol}
        />
        <State
          size={size}
          label={transition["post"][1]}
          protocol={this.props.protocol}
        />
      </div>
    );
  }

  renderTransitions(transitions) {
    return (
      <div style={{
        paddingLeft: "15px",
        paddingRight: "5px",
        paddingTop: "2px",
        paddingBottom: "2px",
      }}>
        {transitions.map(this.renderTransition.bind(this))}
      </div>
    );
  }

  renderExpandButton(showExpandButton, currentlyExpanded, stateVariable) {
    if (showExpandButton) {
      return (
        <Button
          color="primary"
          onClick={(e) => this.handleExpand(stateVariable)}
          endIcon={
            <Icon className="material-icons">
              {currentlyExpanded ? "expand_less" : "expand_more"}
            </Icon>
          }
        >
          {currentlyExpanded ? "Show less" : "Show more"}
        </Button>
      );
    }
  }

  renderTransitionBlock(title, transitions, stateVariable) {
    if (transitions && transitions.length > 0) {
      let count = transitions.length;
      let showAllTransitions = this.state[stateVariable];
      let showExpandButton = count > GRAPH_STYLE.numberDeadTransitionsShown;
      let renderedCount = showAllTransitions ? count : GRAPH_STYLE.numberDeadTransitionsShown;
      let renderedTransitions = transitions.slice(0, renderedCount)
      return (
      <div>
        <div>
          <span style={{
            fontWeight: "bold",
            paddingTop: "8px",
            paddingBottom: "8px",
            display: "inline-block",
          }}>
            {title} ({count}):
          </span>
          {this.renderExpandButton(showExpandButton, showAllTransitions, stateVariable)}
        </div>
        <div>
          {this.renderTransitions(renderedTransitions)}
        </div>
      </div>
      );
    }
  }

  render() {
    return (
      <div>
        {this.renderTransitionBlock(
          "Eventually dead transitions",
          this.props.eventuallyDeadTransitions,
          "showEventuallyDeadTransitions"
        )}
        {this.renderTransitionBlock(
          "Dead transitions",
          this.props.deadTransitions,
          "showDeadTransitions"
        )}
      </div>
    );
  }
}

/*
  Cytoscape layout to layout a collection of stage graphs
*/
// default layout options
const defaults = {
  ready: function(){}, // on layoutready
  stop: function(){}, // on layoutstop
  baseLayout: { name: 'null' },  // base layout to use
};

function StageGraphLayout(options) {
  this.options = util.extend({}, defaults, options);
}

StageGraphLayout.prototype.getComponent = function(group) {
  group = group.union(group.descendants());
  group = group.union(group.edgesWith(group));
  return group;
}

StageGraphLayout.prototype.run = function() {
  let options = this.options;
  let cy = options.cy;

  let s0 = cy.$(".predfalse[^parent]");
  let s1 = cy.$(".predtrue[^parent]");
  let s0group = this.getComponent(s0);
  let s1group = this.getComponent(s1);

  let l0 = s0group.layout(options.baseLayout);
  let l1 = s1group.layout(options.baseLayout);

  this.emit( 'layoutstart' );

  l0.on("layoutstop", () => l1.run());
  l1.on("layoutstop", () => {
    // trigger layoutready when each node has had its position set at least once
    this.one( 'layoutready', options.ready );
    this.emit( 'layoutready' );

    let b0 = s0.renderedBoundingBox();
    let b1 = s1.renderedBoundingBox();
    let shift_x = (b0['x2'] - b1['x1']) / cy.zoom() + 10;
    let shift_y = (b0['y1'] - b1['y1']) / cy.zoom();
    s1.shift({ x: shift_x, y: shift_y });

    if (options.fitStage) {
      cy.fit(cy.$('#' + options.fitStage));
    }
    else {
      cy.fit();
    }

    // trigger layoutstop when the layout stops (e.g. finishes)
    this.one( 'layoutstop', options.stop );
    this.emit( 'layoutstop' );
  });
  l0.run();

  return this;
}

StageGraphLayout.prototype.stop = function() {
  return this;
}

let registerStageGraphLayout = function(cytoscape) {
  cytoscape( 'layout', 'stageGraphLayout', StageGraphLayout ); // register with cytoscape.js
};

Cytoscape.use(registerStageGraphLayout);

/*

  StageGraphGlobalView renders the StageGraph using Cytoscape

*/
class StageGraphGlobalView extends React.Component {
  constructor(props) {
    super(props);
    this.cy = null;
    this.layout = {
      name: "stageGraphLayout",
      animate: false,
      animationDuration: 1500,
      baseLayout: {
        name: "fcose",
        quality: "proof",
        nodeDimensionsIncludeLabels: true,
        packComponents: true,
        fit: false,
        randomize: false,
        animate: false,
        packComponentns: true,
        nodeSeparation: 50,
        nodeRepulsion: 3000,
        idealEdgeLength: 20,
        nestingFactor: 0.8,
        tile: true,
        tilingPaddingVertical: 50,
        tilingPaddingHorizontal: 25,
        gravity: 2.5,
        gravityRange: 3.8,
        gravityCompound: 0.1,
        gravityRangeCompound: 0.15,
      }
    }
  }

  stagesToElements(stageGraph) {
    let terminal = {};
    for (let i = 0; i < stageGraph.length; i++) {
      terminal[stageGraph[i].id] = true;
    }
    for (let i = 0; i < stageGraph.length; i++) {
      let stage = stageGraph[i];
      if (stage.parent) {
        terminal[stage.parent] = false;
      }
    }

    let elements = [];
    for (let i = 0; i < stageGraph.length; i++) {
      let stage = stageGraph[i];
      let stageElement = {
        group: "nodes",
        data: {
          id: stage.id,
          label: stage.id,
        },
        classes: "stage"
      };
      if (StageGraphUtils.propertyToConsensusValue(stage.checkedProperty)) {
        stageElement.classes += " predtrue"
      }
      else {
        stageElement.classes += " predfalse";
      }
      if (stage.failed) {
        stageElement.classes += " failed"
      }
      if (this.props.curStageId === stage.id) {
        stageElement.classes += " selected";
      }
      if (terminal[stage.id]) {
        stageElement.classes += " terminal";
      }
      if (stage.parent) {
        stageElement.data.parent = stage.parent;
      }
      elements.push(stageElement);
    }
    return elements;
  }

  historyToElements(historyGraph) {
    let elements = [];
    for (let i = 0; i < historyGraph.nodes.length; i++) {
      let node = historyGraph.nodes[i];
      let classes = "configuration";
      if (node.selected) {
        classes += " selected"
      }
      elements.push({
        group: "nodes",
        data: {
          id: "C" + i,
          label: node.population,
          parent: node.stage
        },
        classes: classes
      });
    }
    for (let i = 0; i < historyGraph.edges.length; i++) {
      let edge = historyGraph.edges[i];
      elements.push({
        group: "edges",
        data: {
          id: "E" + edge.source + "-" + edge.target,
          source: "C" + edge.source,
          target: "C" + edge.target,
        }
      });
    }
    return elements;
  }

  invisibleElements(stageGraph, historyGraph) {
    // add invisible configurations to empty child stages for nicer layout
    let elements = [];
    let hasChild = {};
    for (let i = 0; i < stageGraph.length; i++) {
      if (stageGraph[i].parent) {
        hasChild[stageGraph[i].parent] = true;
      }
    }
    for (let i = 0; i < historyGraph.nodes.length; i++) {
      hasChild[historyGraph.nodes[i].stage] = true;
    }
    for (let i = 0; i < stageGraph.length; i++) {
      let id = stageGraph[i].id;
      if (!hasChild[id]) {
        elements.push({
          group: "nodes",
          data: {
            id: "D" + id,
            parent: id
          },
          classes: "configuration invisible",
        });
      }
    }
    return elements;
  }

  stageGraphToElements() {
    let elements = []
    elements.push(...this.stagesToElements(this.props.stages));
    elements.push(...this.historyToElements(this.props.historyGraph));
    elements.push(...this.invisibleElements(this.props.stages, this.props.historyGraph));
    return elements;
  }

  computeNestingFactor(stages, historyGraph) {
    let visitedStages = {};
    let count = 0;
    let visitStage = (id) => {
      if (!visitedStages[id]) {
        visitedStages[id] = true;
        count += 1;
      }
      let stage = StageGraphUtils.getStageById(stages, id);
      if (stage.parent) {
        visitStage(stage.parent);
      }
    };
    for (let i = 0; i < historyGraph.nodes.length; i++) {
      visitStage(historyGraph.nodes[i].stage);
    }
    return 0.36 * count;
  }

  runLayout() {
    let curLayout = Object.assign({}, this.layout);
    curLayout.baseLayout = Object.assign({}, this.layout.baseLayout);
    let nest = this.computeNestingFactor(this.props.stages, this.props.historyGraph);
    curLayout.baseLayout.nestingFactor = nest;
    if (this.props.zoomToStage) {
      curLayout.fitStage = this.props.curStageId;
    }
    this.cy.layout(curLayout).run();
    this.cy.nodes().noOverlap({ padding: 0 });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    let n0 = prevProps.historyGraph.nodes.length;
    let n1 = this.props.historyGraph.nodes.length;
    let e0 = prevProps.historyGraph.edges.length;
    let e1 = this.props.historyGraph.edges.length;
    // check if graph layout needs to be updated
    if (
      this.props.zoomToStage !== prevProps.zoomToStage ||
      (this.props.zoomToStage && (this.props.curStageId !== prevProps.curStageId)) ||
      n0 !== n1 || e0 !== e1 ||
      (
        n0 > 0 && n1 > 0 &&
        (
          this.props.historyGraph.nodes[0].stage !== prevProps.historyGraph.nodes[0].stage ||
          this.props.historyGraph.nodes[n1-1].stage !== prevProps.historyGraph.nodes[n0-1].stage
        )
      ) ||
      (
        e0 > 0 && e1 > 0 &&
        (
          this.props.historyGraph.edges[e1-1].source !== prevProps.historyGraph.edges[e0-1].source ||
          this.props.historyGraph.edges[e1-1].target !== prevProps.historyGraph.edges[e0-1].target
        )
      )
    ) {
      if (this.cy) {
        this.runLayout();
      }
    }
  }

  handleCy(cy) {
    if (this.cy !== cy) {
      this.cy = cy;
      this.runLayout();
    }
  }

  resetHighlight() {
    if (this.cy) {
      this.cy.$("node").removeClass('highlight');
    }
  }

  componentDidMount() {
    // only set up listeners once and not during every render
    this.setUpListeners()
  }

  setUpListeners() {
    this.cy.on("mousemove", "node", (e) => {
      let sel = e.target;
      sel.addClass('highlight')
    });
    this.cy.on("mouseout", "node", (e) => {
      let sel = e.target;
      sel.removeClass('highlight')
    });
    if (this.props.onStageSelect) {
      //listen to node selection event and trigger callback
      this.cy.on("tap", "node.stage", (e) => {
        this.props.onStageSelect(e.target.id());
      });
    }
    if (this.props.onConfigurationSelect) {
      //listen to node selection event and trigger callback
      this.cy.on("tap", "node.configuration", (e) => {
        this.props.onConfigurationSelect(e.target.data('label'))
      });
    }
    //rerun layout on resize
    this.cy.on("resize", (e) => this.runLayout());
  }

  render() {
    const stylesheet = [
      {
        selector: '.stage',
        style: {
          'label': 'data(label)',
          'text-valign': 'top',
          'text-halign': 'left',
          'text-margin-x': 22 - (GRAPH_STYLE.stageBorderWidth/2),
          'text-margin-y': 18 - (GRAPH_STYLE.stageBorderWidth/2),
          'text-background-shape': 'rectangle',
          'text-background-padding': 2,
          'text-background-opacity': 1.0,
          'color': GRAPH_STYLE.textColor,
          'background-color': GRAPH_STYLE.stageBackgroundColor,
          'border-width': GRAPH_STYLE.stageBorderWidth,
          'shape': 'roundrectangle',
          'width': 25,
          'height': 25,
          'min-width': 25,
          'min-height': 25,
          'padding': 25,
          'compound-sizing-wrt-labels': 'include',
          'cursor': "pointer",
        }
      },
      {
        selector: '.stage.selected',
        style: {
          'font-weight': 'bold',
          'border-style': 'solid',
          'text-margin-x': 22 - (GRAPH_STYLE.stageSelectBorderWidth/2),
          'text-margin-y': 18 - (GRAPH_STYLE.stageSelectBorderWidth/2),
          'color': GRAPH_STYLE.selectTextColor,
          'border-width': GRAPH_STYLE.stageSelectBorderWidth,
        }
      },
      {
        selector: '.stage.highlight',
        style: {
          'background-color': GRAPH_STYLE.stageHighlightBackgroundColor,
        }
      },
      {
        selector: '.predfalse',
        style: {
          'border-color': GRAPH_STYLE.falseBorderColor,
          'text-background-color': GRAPH_STYLE.falseBorderColor,
        }
      },
      {
        selector: '.predtrue',
        style: {
          'border-color': GRAPH_STYLE.trueBorderColor,
          'text-background-color': GRAPH_STYLE.trueBorderColor,
        }
      },
      {
        selector: '.predfalse.terminal',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.terminalFalseStripes),
      },
      {
        selector: '.predfalse.terminal.highlight',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.terminalFalseHighlightStripes),
      },
      {
        selector: '.predtrue.terminal',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.terminalTrueStripes),
      },
      {
        selector: '.predtrue.terminal.highlight',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.terminalTrueHighlightStripes),
      },
      {
        selector: '.failed',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.failedStripes),
      },
      {
        selector: '.failed.highlight',
        style: GRAPH_STYLE.stripeCytoscapeStyle(GRAPH_STYLE.failedHighlightStripes),
      },
      {
        selector: '.configuration',
        style: {
          'background-color': GRAPH_STYLE.configurationBackgroundColor,
          'border-width': GRAPH_STYLE.configurationBorderWidth,
          'border-color': GRAPH_STYLE.configurationBorderColor,
          'width': GRAPH_STYLE.configurationWidth,
          'height': GRAPH_STYLE.configurationHeight,
          'shape': 'ellipse',
        }
      },
      {
        selector: '.configuration.highlight',
        style: {
          'background-color': GRAPH_STYLE.configurationHighlightBackgroundColor,
        }
      },
      {
        selector: '.configuration.selected',
        style: {
          'background-color': GRAPH_STYLE.configurationSelectBackgroundColor,
        }
      },
      {
        selector: '.configuration.selected.highlight',
        style: {
          'background-color': GRAPH_STYLE.configurationSelectHighlightBackgroundColor,
        }
      },
      {
        selector: '.invisible',
        style: {
          'visibility': 'hidden',
        }
      },
      {
        selector: 'edge',
        style: {
          'width': 1,
          'line-color': '#000000',
          'curve-style': 'bezier',
          'target-arrow-color': '#000000',
          'target-arrow-shape': 'triangle',
          'control-point-step-size': 15,
          'edge-distances': 'node-position',
          'loop-direction': '-45deg',
          'loop-sweep': '-45deg',
        }
      },
    ];
    return (
      <div onMouseOut={this.resetHighlight.bind(this)}>
        <CytoscapeComponent
          elements={this.stageGraphToElements()}
          userZoomingEnabled={false}
          // handle select in parent react component
          autounselectify={true}
          style={{
            width: "100%",
            height: this.props.height,
          }}
          stylesheet={stylesheet}
          layout={this.layout}
          cy={(cy) => this.handleCy(cy)}
        />
      </div>
    );
  }
}

class StageGraphLegend extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showLegend: false,
    };
  }

  handleExpand() {
    this.setState({ showLegend: !this.state.showLegend });
  }

  renderLegend() {
    let stageStyle = {
      width: "20px",
      height: "20px",
      borderStyle: "solid",
      borderRadius: "5px",
      backgroundColor: GRAPH_STYLE.stageBackgroundColor,
      borderWidth: GRAPH_STYLE.stageBorderWidth,
      display: "inline-block",
    }
    let entryStyle = {
      width: "24px",
      height: "24px",
      textAlign: "center",
      verticalAlign: "center",
    }

    let falseStyle = Object.assign({}, stageStyle, {
      borderColor: GRAPH_STYLE.falseBorderColor,
    });
    let trueStyle = Object.assign({}, stageStyle, {
      borderColor: GRAPH_STYLE.trueBorderColor,
    });
    let terminalFalseStyle = Object.assign({}, stageStyle,
      GRAPH_STYLE.stripeCSSStyle(GRAPH_STYLE.terminalFalseStripes));
    let terminalTrueStyle = Object.assign({}, stageStyle,
      GRAPH_STYLE.stripeCSSStyle(GRAPH_STYLE.terminalTrueStripes));
    let failedStyle = Object.assign({}, stageStyle,
      GRAPH_STYLE.stripeCSSStyle(GRAPH_STYLE.failedStripes));

    let configurationStyle = {
      width: GRAPH_STYLE.configurationWidth,
      height: GRAPH_STYLE.configurationHeight,
      backgroundColor: GRAPH_STYLE.configurationBackgroundColor,
      borderRadius: "50%",
      borderColor: GRAPH_STYLE.configurationBorderColor,
      borderWidth: GRAPH_STYLE.configurationBorderWidth,
      borderStyle: "solid",
      display: "inline-block",
    };

    let configurationSelectStyle = Object.assign({}, configurationStyle, {
      backgroundColor: GRAPH_STYLE.configurationSelectBackgroundColor,
    });

    return (
      <div style={{
        display: "grid",
        gridColumnGap: "5px",
        gridRowGap: "5px",
        gridTemplateColumns: "48px auto",
      }}>
          <div style={entryStyle}><div style={falseStyle}></div></div>
          <div>Stage with expected consensus false</div>

          <div style={entryStyle}><div style={trueStyle}></div></div>
          <div>Stage with expected consensus true</div>

          <div style={entryStyle}><div style={terminalFalseStyle}></div></div>
          <div>Terminal stage with stable consensus false</div>

          <div style={entryStyle}><div style={terminalTrueStyle}></div></div>
          <div>Terminal stage with stable consensus true</div>

          <div style={entryStyle}><div style={failedStyle}></div></div>
          <div>Failed terminal stage</div>

          <div style={entryStyle}><div style={configurationStyle}></div>
          <div style={configurationSelectStyle}></div></div><div>Configuration</div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <div style={{
          fontWeight: "bold",
          paddingBottom: "5px",
        }}>
          <Button
            color="primary"
            onClick={this.handleExpand.bind(this)}
            endIcon={
              <Icon className="material-icons">
                {this.state.showLegend ? "expand_less" : "expand_more"}
              </Icon>
            }
          >
            Legend
          </Button>
        </div>
        <div>
          {this.state.showLegend ? this.renderLegend() : ""}
        </div>
      </div>
    );
  }
}

/*
  StageDetailView renders the details of the currently selected stage.
*/
class StageDetailView extends React.Component {

  certificateToFormula(certificate) {
    if (!certificate) {
      return "⊥";
    }
    if (certificate.length === 0) {
      return "0";
    }
    let mapTerm = (state, value) => {
      if (value === 1) {
        return state;
      }
      else {
        return value + "·" + state;
      }
    }
    return certificate
      .map((c) => mapTerm("C[" + c["state"] + "]", c["value"]))
      .join(" + ");
  }

  boundToFormula(bound) {
    let mapTerm = (state, value) => {
      if (value === 0) {
        return state + "=" + value;
      }
      else {
        return state + "≤" + value;
      }
    }
    return mapTerm("C[" + bound["state"] + "]", bound["value"]);
  }

  clauseToFormula(clause) {
    let clauseString = clause.map(this.boundToFormula.bind(this)).join(" ∨ ");
    if (clause.length === 1) {
      return clauseString;
    }
    else {
      return ("(" + clauseString + ")");
    }
  }

  potReachPredicate(protocol, checkedProperty) {
    let predicate = protocol.predicate;
    if (checkedProperty === "FALSE_CONSENSUS") {
      predicate = "¬(" + predicate + ")";
    }
    if (protocol.precondition) {
      predicate = predicate + " ∧ " + protocol.precondition;
    }
    return "PotReach(" + predicate + ")";
  }

  constraintToFormula(protocol, checkedProperty, constraint) {
    let p = this.potReachPredicate(protocol, checkedProperty);
    if (!constraint || constraint.length === 0) {
      return p;
    }
    if (constraint.some((c) => c.length === 0)) {
      return "⊥";
    }
    return (
      p + " ∧ " +
      constraint.map(this.clauseToFormula.bind(this)).join(" ∧ ")
    );
  }

  render() {
    const protocol = this.props.protocol;
    const checkedProperty = this.props.stage.checkedProperty;
    const constraint = this.props.stage.constraint;
    const certificate = this.props.stage.certificate;
    const certificateValue = this.props.certificateValue;
    const eventuallyDeadTransitions = this.props.stage.eventuallyDeadTransitions;
    const deadTransitions = this.props.stage.deadTransitions;
    const speed = this.props.stage.speed;

    const divStyle = {
      paddingTop: "2px",
      paddingBottom: "2px",
    };
    const spanStyle = {
      paddingRight: "10px",
      fontWeight: "bold"
    };

    return (
      <div style={{
        paddingLeft: "10px",
        paddingRight: "10px",
      }}>
        <div style={divStyle}>
          <span style={spanStyle}>Speed:</span>
          <TeX>{StageGraphUtils.speedToLatex(speed)}</TeX>
        </div>
        <div style={divStyle}>
          <span style={spanStyle}>Certificate:</span>
          <span
          dangerouslySetInnerHTML={{
           __html: FormulaUtils.print(this.certificateToFormula(certificate)),
         }}
          ></span>
        </div>
        <div style={divStyle}>
          <span style={spanStyle}>Certificate Value:</span>
          <span >{certificateValue}</span>
        </div>
        <div style={divStyle}>
          <span style={spanStyle}>Constraint:</span>
          <span
          dangerouslySetInnerHTML={{
           __html: FormulaUtils.print(this.constraintToFormula(protocol, checkedProperty, constraint)),
         }}
          ></span>
        </div>
        <div style={divStyle}>
          <TransitionView
            protocol={this.props.protocol}
            eventuallyDeadTransitions={eventuallyDeadTransitions}
            deadTransitions={deadTransitions}
          />
        </div>
      </div>
    );
  }
}

/*
  StageGraphDisplay is the main component that wraps all preceding components
*/
class StageGraphDisplay extends React.Component {
  constructor(props) {
    super(props);
    this.protocol = props.protocol;
    let initialStage = this.selectInitialStage();
    this.state = this.stateForStage(initialStage, null, true);
    this.state.displayConfigurations = true;
    this.state.zoomToStage = false;
    this.state.isFullScreen = false;
    this.speed = 30;
  }

  selectInitialStage() {
    // select stage with counterexample, if one exists
    let counterExamples = StageGraphUtils.collectCounterExamples(this.props.stageGraph);
    if (counterExamples.length > 0) {
      return counterExamples[0].id;
    }
    // select failed stage, if one exists
    let failedStages = StageGraphUtils.collectFailedStages(this.props.stageGraph);
    if (failedStages.length > 0) {
      return failedStages[0];
    }
    // select root stage with consensus true
    return this.selectRootStage(true);
  }

  selectRootStage(consensusValue) {
    // select root stage with consensus consensusValue
    const checkedProperty = StageGraphUtils.consensusValueToProperty(consensusValue);
    for (let stage of this.props.stageGraph.stages) {
      if (!stage.parent && stage.checkedProperty === checkedProperty) {
        return stage.id;
      }
    }
    return null;
  }

  setStageForInitialConfiguration(configuration, consensusValue) {
    let initialStageIndex = this.selectRootStage(consensusValue);
    let population = StageGraphUtils.configurationToPopulation(configuration);
    let stageIndex = this.findSuccessorStage(initialStageIndex, population)
    let newState = this.stateForStage(stageIndex, configuration);
    this.setState(newState);
  }

  handleParamsChange(newParams) {
    let newConfiguration = {};
    for (let q of this.props.protocol.initialStates) {
      newConfiguration[q] = newParams[q];
    }
    if (this.props.protocol.leaders) {
      for (let q of Object.keys(this.props.protocol.leaders)) {
        if (!newConfiguration[q]) {
          newConfiguration[q] = 0;
        }
        newConfiguration[q] += this.props.protocol.leaders[q];
      }
    }

    // Query backend to obtain consensus value of predicate for initial configuration,
    // and update current stage accordingly.
    axios.post('evaluate-predicate', {
        'configuration' : newConfiguration,
        'predicate' : this.props.protocol.predicate
      }).then(function(response) {
          this.setStageForInitialConfiguration(newConfiguration, response.data)
      }.bind(this));
  }

  getCurrentParams() {
    let params = {};
    let configuration = this.state.initialConfiguration;
    for (let q of this.props.protocol.initialStates) {
      params[q] = { 'values': this.props.initialConfigParams[q].values
                  , 'value': StageGraphUtils.configurationValue(configuration, q)
                  , 'descr': this.props.initialConfigParams[q].descr
                  };
      if (params[q]['values'].indexOf(params[q]['value']) === -1) {
        params[q]['values'].push(params[q]['value']);
      }
    };
    return params;
  }


  /*
    Returns list of transitions that are enabled and reduce
    the given certificate. At most one transition is returned if single is true.
  */
  getReducingTransitions(population, certificate, single = false) {
    let enabledTransitions = ProtocolUtils.enabledPointedTransitions(
      this.props.protocol,
      population
    );
    let reducingTransitions = [];
    for (let t of enabledTransitions) {
      let value =
        StageGraphUtils.evaluateCertificate(t.transition.post, certificate) -
        StageGraphUtils.evaluateCertificate(t.transition.pre, certificate);
      if (value < 0) {
        reducingTransitions.push(t);
        if (single) {
          return reducingTransitions;
        }
      }
    }
    return reducingTransitions;
  }

  disableCertificateButton(population, certificate) {
    return this.getReducingTransitions(population, certificate, true).length === 0;
  }

  handleCertificateButton() {
    let population = this.getCurrentPopulation();
    const certificate = this.getStageById(this.state.curStageId).certificate;
    let reducingTransitions = this.getReducingTransitions(population, certificate);
    this.runner.nextFrom(reducingTransitions);
  }

  handleResetButton() {
    let configuration = this.state.initialConfiguration;
    let population = StageGraphUtils.configurationToPopulation(configuration);
    let initialPopulation = this.state.history.populations[0].population;
    if (StageGraphUtils.populationsEqual(population, initialPopulation)) {
      // if runner starts at this population, simply reset current index
      this.setCurrentPopulation(0);
    }
    else {
      // otherwise reset runner
      let stage = this.getStageById(this.state.curStageId);
      let curConsensusValue = StageGraphUtils.propertyToConsensusValue(stage.checkedProperty);
      this.setStageForInitialConfiguration(configuration, curConsensusValue);
    }
  }

  stateForStage(initialStage, initialConfiguration = null, resetSequence = false) {
    let stage = this.getStageById(initialStage);
    let configuration = undefined;
    let sequence = undefined;
    if (initialConfiguration) {
      configuration = initialConfiguration;
    }
    else {
      initialConfiguration = this.getStageById(initialStage).potReachFromConfiguration;
      configuration = this.getStageById(initialStage).configuration;
      if (stage.reachSequence) {
        sequence = stage.reachSequence;
      }
    }
    let population = StageGraphUtils.configurationToPopulation(configuration);
    this.displayCallback = undefined;
    if (sequence) {
      // find initial population and stage
      let initialPopulation = StageGraphUtils.configurationToPopulation(initialConfiguration);
      let position = resetSequence ? 0 : sequence.length;
      this.runner = new InitializedRandomRunner(
        this.props.protocol,
        initialPopulation,
        sequence,
        position,
        this.runnerCallback.bind(this)
      );
      initialStage = this.rootStage(initialStage)
      initialStage = this.findSuccessorStage(initialStage, initialPopulation);
    }
    else {
      this.runner = new RandomRunner(
        this.props.protocol,
        population,
        this.runnerCallback.bind(this)
      );
    }
    let newState = {
      initialStage: initialStage,
      initialConfiguration: initialConfiguration,
    };
    let runnerState = this.stateFromRunner(initialStage);
    return Object.assign(newState, runnerState);
  }

  evaluateConstraint(population, constraint) {
    return constraint.every(clause =>
      clause.some(term =>
        StageGraphUtils.populationValue(population, term.state) <= term.value
      )
    )
  }

  setCurStageId(id) {
    this.setState(this.stateForStage(id));
  }

  getStageById(id) {
    return StageGraphUtils.getStageById(this.props.stageGraph.stages, id);
  }

  // returns if stage1 is an ancestor of stage2
  isAncestorOf(stage1, stage2) {
    if (stage1.id === stage2.id) {
      return true;
    }
    else if (stage2.parent) {
      return this.isAncestorOf(stage1, this.getStageById(stage2.parent));
    }
    else {
      return false;
    }
  }

  configurationGraphFromHistory() {
    if (!this.state.displayConfigurations) {
      return {
        nodes: [],
        edges: [],
      }
    }
    let pops = this.state.history.populations;
    let nodes = [];
    let edges = [];
    let node_cache = {};
    let edge_cache = {};
    let source = undefined;
    for (let i = 0; i < pops.length; i++) {
      let currentRepresentant = pops[i].population.slice();
      currentRepresentant.sort();
      let target = node_cache[currentRepresentant];
      if (target === undefined) {
        target = nodes.length;
        nodes.push({ population: i, stage: pops[i].stage });
        node_cache[currentRepresentant] = target;
      }
      else {
        // only save last population visited at this node
        nodes[target].population = i;
      }
      if (i === this.state.history.current) {
        nodes[target].selected = true;
      }
      if (i > 0) {
        let key = source * pops.length + target;
        if (!edge_cache[key]) {
          edge_cache[key] = true;
          edges.push({ source: source, target: target });
        }
      }
      source = target;
    }
    return {
      nodes: nodes,
      edges: edges,
    }
  }

  // find root of curStage
  rootStage(curStageId) {
    let curStage = this.getStageById(curStageId);
    if (!curStage.parent) {
      return curStage.id;
    }
    else {
      return this.rootStage(curStage.parent);
    }
  }

  // find smallest stage descending from curStage this population is in
  findSuccessorStage(curStageId, population) {
    let curStage = this.getStageById(curStageId);
    for (let stage of this.props.stageGraph.stages) {
      if (
        this.evaluateConstraint(population, stage.constraint) &&
        this.isAncestorOf(curStage, stage)
      ) {
        curStage = stage;
      }
    }
    return curStage.id;
  }

  stateFromRunner(initialStage) {
    let populations = [];
    for (let i = 0; i < this.runner.length(); i++) {
      let population = this.runner.stepAt(i)[0];
      let stage = undefined;
      if (i === 0) {
        stage = initialStage;
      }
      else {
        let lastStage = populations[i-1].stage;
        stage = this.findSuccessorStage(lastStage, population);
      }
      populations.push({
        population: population,
        stage: stage
      });
    }
    let current = this.runner.getCurrent();
    let currentStage = populations[current].stage;
    return {
      history: {
        populations: populations,
        current: current
      },
      curStageId: currentStage,
    };
  }

  getCurrentPopulation() {
    let current = this.state.history.current;
    return this.state.history.populations[current].population;
  }

  setCurrentPopulation(populationIndex) {
    this.runner.setCurrent(populationIndex);
  }

  runnerCallback(state) {
    if (this.displayCallback) {
      this.displayCallback(state);
    }
    this.setState(this.stateFromRunner(this.state.initialStage));
  }

  toggleFullScreen(){
    this.setState({'isFullScreen' : !this.state.isFullScreen});
  }

  renderStageGraphArea(){
    const fullScreenButtonIcon = ["fullscreen_exit", "fullscreen"][this.state.isFullScreen ? 0 : 1];
    const fullScreenButtonLabel = ["Exit fullscreen", "Show fullscreen"][this.state.isFullScreen? 0 : 1];
    const protocol = this.props.protocol;
    const curStageId = this.state.curStageId;
    const stage = this.getStageById(curStageId);
    const population = this.getCurrentPopulation();
    const certificate = stage.certificate;
    const certificateValue = StageGraphUtils.evaluateCertificate(population, certificate);
    const historyGraph = this.configurationGraphFromHistory();

    return(
      <div>
        <div style={{ width: "60%", float: "left" }}>
          <Card>
            <CardContent>
              <StageGraphGlobalView
                height={this.state.isFullScreen ? (window.innerHeight - 100) : 300}
                stages={this.props.stageGraph.stages}
                onStageSelect={this.setCurStageId.bind(this)}
                onConfigurationSelect={this.setCurrentPopulation.bind(this)}
                zoomToStage={this.state.zoomToStage}
                curStageId={curStageId}
                historyGraph={historyGraph}
              />
            </CardContent>
          </Card>
          <div>
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.displayConfigurations}
                  onChange={() =>
                    this.setState({
                      displayConfigurations: !this.state.displayConfigurations
                    })
                  }
                />
              }
              label="Display configurations"
              style={{ paddingLeft: 30 }}
            />
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.zoomToStage}
                  onChange={() =>
                    this.setState({
                      zoomToStage: !this.state.zoomToStage
                    })
                  }
                />
              }
              label="Zoom to stage"
              style={{ paddingLeft: 30 }}
            />

            <Tooltip title={fullScreenButtonLabel}>
              <IconButton
                aria-label={fullScreenButtonLabel}
                onClick={this.toggleFullScreen.bind(this)}
              >
              <Icon className="material-icons">{fullScreenButtonIcon}</Icon>
              </IconButton>
            </Tooltip>

          </div>
          <StageGraphLegend />
        </div>
        <div style={{"width": "40%", "float": "right"}}>
          {this.renderPopulationViewInFullscreenMode()}
          <Card>
            <CardContent>
              <h3>Stage {this.state.curStageId}</h3>
              <StageDetailView
                protocol={protocol}
                certificateValue={certificateValue}
                stage={stage}
              />
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }

  renderPopulationDescr(){
    const dotStyle = {
      width: GRAPH_STYLE.configurationWidth,
      height: GRAPH_STYLE.configurationHeight,
      backgroundColor: GRAPH_STYLE.configurationSelectBackgroundColor,
      borderRadius: "50%",
      borderColor: GRAPH_STYLE.configurationBorderColor,
      borderWidth: GRAPH_STYLE.configurationBorderWidth,
      borderStyle: "solid",
      display: "inline-block",
    }

    const spanStyle = {
      fontWeight: "bold",
      paddingRight: "10px",
    }

    return( <span style={spanStyle}>
            Current configuration (<span style={dotStyle}></span>):
            </span>);
  }

  startPlay(){
    this.setState({playing: true});
  }

  stopPlay(){
    this.setState({playing: false});
  }

  changeSpeed(value){
    this.setState({speed: value});
  }

  renderPopulationControlDisplay(){
    const protocol = this.props.protocol;
    const population = this.getCurrentPopulation();
    return(
      <PopulationControlDisplay
        protocol={protocol}
        population={population}
        hideResetButton={true}
        playing={this.state.playing}
        speed={this.state.speed}
        onSpeedChange={this.changeSpeed.bind(this)}
        onStopPlaying={this.stopPlay.bind(this)}
        onStartPlaying={this.startPlay.bind(this)}
        runnerGenerator={(protocol, population, callback) => {
          this.displayCallback = callback;
          return this.runner;
        }}
        renderAdditionalControlButtons={this.renderCertificateButton.bind(this)}
      />
    );
  }

  renderPopulationViewInFullscreenMode(){
    if(this.state.isFullScreen){
      return(
        <Card style={{marginBottom: "5px"}}>
          <CardContent>
            {this.renderPopulationDescr()}
            {this.renderPopulationControlDisplay()}
          </CardContent>
        </Card>
      );
    }
    else{
      return (<></>);
    }
  }

  renderStageGraphInfo(){
    if(this.state.isFullScreen){
      return(<Dialog fullScreen open={this.state.isFullScreen}
                      onClose={this.toggleFullScreen.bind(this)}
              >
                      {this.renderStageGraphArea()}
             </Dialog>);
    }
    else{
      return(<div>
                      {this.renderStageGraphArea()}
             </div>);
    }
  }

  renderCertificateButton(){
    const population = this.getCurrentPopulation();
    const curStageId = this.state.curStageId;
    const stage = this.getStageById(curStageId);
    const certificate = stage.certificate;
    const certificateValue = StageGraphUtils.evaluateCertificate(population, certificate);
    let label = `Progress (${certificateValue})`;

    return(
    <Button
      onClick={this.handleCertificateButton.bind(this)}
      disabled={this.disableCertificateButton(population, certificate)}
      color="secondary"
      variant="contained"
      style={{height:'39px', marginLeft: '5px', marginTop: '2px'}}
      startIcon={<Icon className="material-icons">{"terrain"}</Icon>}
    >
      {label}
    </Button>
    );
  }


  render() {
    let buttonSize = function () {
      return window.innerWidth <= 800 ? "80px" : "120px";
    };
    const buttonStyle = {
      textAlign: "center",
      fontSize: "16px",
      margin: "2px",
      borderRadius: "4px",
      minWidth: buttonSize(),
      width: buttonSize(),
      minHeight: "30px",
    };

    return (
      <div style={{ width: "100%" }}>
        <div style={{ width: "100%", minWidth: "400px", overflow: "hidden" }}>
              <div style={{'width': "100%"}}>
                  <Typography>
                  Pick custom initial configuration:
                  </Typography>
                  <div style={{display: 'flex', alignItems: 'center', flexWrap: 'wrap'}}>
                    <ParameterPicker
                      parameters={this.getCurrentParams()}
                      onParamChange={this.handleParamsChange.bind(this)}
                    />
                    <Button
                      onClick={this.handleResetButton.bind(this)}
                      style={buttonStyle}
                      color="primary"
                      variant="contained"
                      startIcon={<Icon className="material-icons">{"refresh"}</Icon>}
                    >
                      Reset
                    </Button>
                </div>
              </div>
              <Divider style={{marginTop: "5px"}}/>
              <div style={{"width": "100%", "paddingTop" : "10px"}}>
                {this.renderPopulationDescr()}
                {this.renderPopulationControlDisplay()}
                <Divider style={{marginTop: "2px"}}/>
                {this.renderStageGraphInfo()}
              </div>
        </div>
      </div>
    );
  }
}

export { StageGraphDisplay, StageGraphUtils, StageDetailView };
