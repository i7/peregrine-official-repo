import React from "react";
import * as Style from "./PopulationStyle";
import { ProtocolUtils } from "./ProtocolUtils";

class State extends React.Component{

  stateStyle(){
    let foregroundColor = "black";
    let backgroundColor = "white";
    if (this.props.protocol) {
      foregroundColor = "white";
      if (ProtocolUtils.isTrueState(this.props.protocol, this.props.label)) {
        backgroundColor = Style.TRUE_COLOR;
      }
      else {
        backgroundColor = Style.FALSE_COLOR;
      }
    }
    return {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      float: "left",
      height: this.props.size ? this.props.size : Style.STATE_SIZE,
      width: this.props.size ? this.props.size : Style.STATE_SIZE,
      padding: Style.STATE_PADDING,
      margin: "1px",
      font: Style.STATE_FONT,
      backgroundColor: backgroundColor,
      color: foregroundColor,
      borderRadius: "10px",
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",
    };
  }

  render(){
    return (
      <div title={this.props.label} style={this.stateStyle()}>
        {this.props.label}
      </div>
    );
  }
}

export default State;
