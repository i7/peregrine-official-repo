import React from "react";

// Colors
const TRUE_COLOR = "#3F51B5";
const FALSE_COLOR = "#E91E63";
const TRUE_COLOR_LIGHT = "#9FA8DA";
const FALSE_COLOR_LIGHT = "#F48FB1";
const COLOR_GRAY = "#616161";
const COLOR_DARK_GRAY = "#2e2c2c";

// Fonts
const MAIN_FONT = "Roboto, sans-serif";
const SECOND_FONT = "Roboto";

// States
const STATE_PADDING = 3;
const STATE_SIZE = window.innerWidth <= 800 ? 40 : 50;
const STATE_FONT = "bold 14px " + MAIN_FONT;

// Source: https://stackoverflow.com/questions/118241/calculate-text-width-with-javascript/21015393#21015393
function textWidth(text, font) {
  const canvas =
    textWidth.canvas || (textWidth.canvas = document.createElement("canvas"));
  const context = canvas.getContext("2d");
  context.font = font;

  const metrics = context.measureText(text);

  return metrics.width;
}

function rewriteLabel(label, isReact = true) {
  let subscripts = label.split("_");
  if (subscripts.length === 2) {
    return isReact ? (
      <span>
        {subscripts[0]}
        <sub>{subscripts[1]}</sub>
      </span>
    ) : (
      subscripts[0] + "<sub>" + subscripts[1] + "</sub>"
    );
  } else {
    return label;
  }
}

function calculateStateWidth(protocol) {
  let temp = STATE_SIZE;

  for (let i = 0; i < protocol.states.length; i++) {
    temp = Math.max(
      temp,
      textWidth(protocol.states[i], STATE_FONT) + 2 * (STATE_PADDING + 1)
    );
  }

  return Math.min(temp, 3 * STATE_SIZE);
}

// Boxes
const BOX_SHADOW = "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)";

export {
  BOX_SHADOW,
  TRUE_COLOR,
  FALSE_COLOR,
  TRUE_COLOR_LIGHT,
  FALSE_COLOR_LIGHT,
  COLOR_GRAY,
  COLOR_DARK_GRAY,
  MAIN_FONT,
  SECOND_FONT,
  STATE_PADDING,
  STATE_SIZE,
  STATE_FONT,
  calculateStateWidth,
  rewriteLabel,
};
