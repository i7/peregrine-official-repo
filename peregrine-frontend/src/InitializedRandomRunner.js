import { ProtocolUtils } from "./ProtocolUtils";
import { RandomRunner } from "./RandomRunner";

/*
 * Random runner that can be initialized with a deterministic prefix
 */
class InitializedRandomRunner extends RandomRunner {
  constructor(protocol, population, execution = [], current = 0, callback = undefined) {
    super(protocol, population, callback);
    this.history = ProtocolUtils.historyFromExecution(protocol, population, execution);
    this.curr = current;
    this.initialHistory = this.history.slice();
    this.initialCurrent = current;
  }

  reset() {
    this.history = this.initialHistory.slice();
    this.curr = this.initialCurrent;

    this.updateStatus(null);
  }

}

export { InitializedRandomRunner };
