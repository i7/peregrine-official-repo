import React from "react";
import Icon from "@material-ui/core/Icon";
import Button from "@material-ui/core/Button";
import Switch from "@material-ui/core/Switch";
import Slider from "@material-ui/core/Slider";
import Tooltip from "@material-ui/core/Tooltip";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { ProtocolUtils } from "./ProtocolUtils";
import * as Style from "./PopulationStyle";
import Box from "@material-ui/core/Box";
import { throttle } from 'lodash'

const RADIUS_UNIT = 12;
const BOOM_RADIUS = 200;
const DRAG_RADIUS = 25;
const MAX_SPEED = 5;
const BOUNCE = true;	// true: change direction after interaction, false: passthrough
const COOLDOWN = 10;	// minimum "time" between two interactions with same agent (this stops many fast interaction with the same pair)

class SpatialSimulator extends React.Component {
  constructor(props) {
    super(props);
    this.protocol = props.protocol;
    this.state = {
      stateSize: 0.4,
      showSignal: false,
      showName: false,
      playing: false,
      speedLevel: 0,
      hasTerminated: false,
      messageOpen: false,
      message: "",
    };
    this.state["colorShapes"] = this.getColorShapes(props.protocol);
    this.isDesktop =
      !(typeof window.orientation !== "undefined") &&
      !(navigator.userAgent.indexOf("IEMobile") !== -1);
    this.width = 600; // this is automatically resized using flexbox
    this.height = window.innerHeight - 200;
    this.transitions = {};
    this.legendRefs = {};
    this.initalize(props.population); // Initialize this.population
	this.resizeFunc = throttle(this.handleCanvasResize.bind(this), 25);
	this.dragged = null;
  }

  getColorShapes(protocol) {
    let colorShapes = {};
    for (let q of protocol.states) {
      let dict = {
        color: this.color(q),
        shape: this.shape(q),
        size: this.radius(q),
      };
      let key = JSON.stringify(dict);
      if (key in colorShapes) {
        colorShapes[key].push(q);
      } else {
        colorShapes[key] = [q];
      }
    }
    return colorShapes;
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeFunc);
	  
    const effects = this.refs.effects;

    effects.addEventListener(
      "click",
      (e) => {
        if (this.state.playing) {
          this.boom(e.offsetX, e.offsetY);
        }
      },
      false
    );
	effects.addEventListener(
      "mousedown",
      (e) => {
		const center = { x: e.offsetX, y: e.offsetY};
		var best = null;
		var bestDist = 1000000;
		for (let i = 0; i < this.population.length; i++) {
	      const agent = this.population[i];
          const dist = this.distance(agent, center);
		  if (dist < this.radius(agent.state) && dist < bestDist) {
			best = agent;
			bestDist = dist;
		  }
		  agent.cooldown = 0;
        }
		this.dragged = best;
      },
      false
    );
	effects.addEventListener(
      "mouseup",
      (e) => {
		this.dragged = null;
      },
      false
    );
	effects.addEventListener(
      "mousemove",
      throttle((e) => {
        if (this.dragged !== null) {
		  this.dragged.x = e.layerX;
		  this.dragged.y = e.layerY;
		  if (!this.state.playing) {
			this.applyInteractions();
			this.sanitizePositions();
			this.dragged.cooldown = 0;
			this.redraw();
		  }
		}
      }),
      false
    );
	effects.addEventListener(
      "mouseout",
      (e) => {
		this.dragged = null;
      },
      false
    );
    Object.keys(this.state.colorShapes).map((k) =>
      this.drawLegendItem(this.state.colorShapes[k][0])
    );
	this.handleCanvasResize();
    this.redraw();
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeFunc);
	cancelAnimationFrame(this.animation);
  }

  componentDidUpdate(prevProps, prevState){
    if(prevProps !== this.props){
      this.protocol = this.props.protocol;
      this.transitions = {};
      this.setState(
        {
          hasTerminated: false,
          messageOpen: false,
          colorShapes: this.getColorShapes(this.protocol),
        },
        () => {
          Object.keys(this.state.colorShapes).map((k) =>
            this.drawLegendItem(this.state.colorShapes[k][0])
          );
        }
      );
      this.initalize(this.props.population);
      }
    this.handleCanvasResize();
    this.redraw();
  }

  getMaxRadius() {
    return Math.max.apply(
      null,
      this.props.protocol.states.map((q) => this.radius(q))
    );
  }
  
  getRandomSpeed() {
    return Math.floor(Math.random() * MAX_SPEED) + 1;
  }

  initalize(population) {
    let pop = [];

    for (let i = 0; i < population.length; i++) {
      const state = population[i];
      const radius = this.radius(state);
      const posX =
        Math.floor(Math.random() * (this.width - 2 * radius)) + radius;
      const posY =
        Math.floor(Math.random() * (this.height - 2 * radius)) + radius;
      const velX = this.getRandomSpeed();
      const velY = this.getRandomSpeed();

      pop.push({ x: posX, y: posY, vx: velX, vy: velY, state: state, cooldown: 0});
    }

    this.population = pop;
  }

  transitionsFrom(p, q) {
    if (!(p in this.transitions)) {
      this.transitions[p] = {};
    }

    if (!(q in this.transitions[p])) {
      this.transitions[p][q] = ProtocolUtils.transitionsForPredecessors(
        this.protocol,
        p,
        q
      );
    }

    return this.transitions[p][q];
  }

  defaultRadius() {
    let radius = RADIUS_UNIT;

    if (this.state.stateSize > 0) {
      radius *= this.state.stateSize + 1;
    } else if (this.state.stateSize < 0) {
      radius /= 1 - this.state.stateSize;
    }

    return radius;
  }

  radius(state) {
    let radius = this.defaultRadius();

    if (
      this.protocol &&
      this.protocol.statesStyle &&
      this.protocol.statesStyle[state] &&
      this.protocol.statesStyle[state].size
    ) {
      radius *= this.protocol.statesStyle[state].size;
    }

    return Math.max(radius, 1);
  }

  shape(state) {
    if (
      this.protocol &&
      this.protocol.statesStyle &&
      this.protocol.statesStyle[state] &&
      this.protocol.statesStyle[state].shape
    ) {
      return this.protocol.statesStyle[state].shape;
    } else {
      return "circle";
    }
  }

  color(state) {
    if (
      this.protocol &&
      this.protocol.statesStyle &&
      this.protocol.statesStyle[state] &&
      this.protocol.statesStyle[state].color
    ) {
      return this.protocol.statesStyle[state].color;
    } else {
      return ProtocolUtils.isTrueState(this.protocol, state)
        ? Style.TRUE_COLOR
        : Style.FALSE_COLOR;
    }
  }
  
  fillcolor(state) {
    if (
      this.protocol &&
      this.protocol.statesStyle &&
      this.protocol.statesStyle[state] &&
      this.protocol.statesStyle[state].fillcolor 
    ) {
      return this.protocol.statesStyle[state].fillcolor;
    } else {
      return "rgba(255, 255, 255, 0.7)";
    }
  }

  distance(a, b) {
    return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
  }

  interact(i, j) {
    const p = this.population[i].state;
    const q = this.population[j].state;
    const transitions = this.transitionsFrom(p, q);

    if (transitions.length > 0) {
      const k = Math.floor(Math.random() * transitions.length);
	  const t = transitions[k];
	  if (this.population[i].state === t.pre[0]) {
		this.population[i].state = t.post[0];
		this.population[j].state = t.post[1];
	  } else {
		this.population[j].state = t.post[0];
		this.population[i].state = t.post[1];
	  }
	  if (BOUNCE) {
		var velX = this.getRandomSpeed();
		var velY = this.getRandomSpeed();
		if (this.population[i].x < this.population[j].x) velX = -velX;
		if (this.population[i].y < this.population[j].y) velY = -velY;
        this.population[i].vx = velX;
	    this.population[i].vy = velY;
	    this.population[i].cooldown = COOLDOWN;
	    this.population[j].vx = -velX;
	    this.population[j].vy = -velY;
	    this.population[j].cooldown = COOLDOWN;
	  }
	  
	  /*if (this.population[i] === this.dragged || this.population[j] === this.dragged) {
		this.dragged = null;
	  }*/
    }
  }

  applyInteractions() {
    let configuration = ProtocolUtils.population2Configuration(
      this.population.map((x) => x.state),
      this.props.protocol
    );
    if (
      ProtocolUtils.isTerminalConfiguration(
        configuration,
        this.props.protocol
      ) &&
      !this.state.hasTerminated
    ) {
      const consensus = ProtocolUtils.consensus(
        this.props.protocol,
        this.population.map((x) => x.state)
      );
      const message =
        "Reached a terminal configuration with " +
        (consensus == null
          ? "no consensus."
          : "consensus '" + consensus + "'.");

      this.setState({
        message: message,
        messageOpen: true,
        hasTerminated: true,
      });
    }
    this.population.sort((a, b) => (a.x !== b.x ? a.x - b.x : a.y - b.y));
	const maxRadius = this.getMaxRadius();
    for (let i = 0; i < this.population.length; i++) {
      const agent = this.population[i];
	  if (agent.cooldown > 0) {
		continue;
	  }
      let j = i + 1;
	  const radius = this.radius(agent.state);
      while (
        j < this.population.length &&
        this.population[j].x <= agent.x + radius + maxRadius
      ) {
        if (
          this.distance(agent, this.population[j]) <= radius + this.radius(this.population[j].state) 
		  && this.population[j].cooldown <= 0
		  && (this.state.playing || agent === this.dragged || this.population[j] === this.dragged)
        ) {
          this.interact(i, j);
        }

        j++;
      }
    }
  }

  redraw() {
    const canvas = this.refs.canvas;

    if (canvas) {
      const context = canvas.getContext("2d");

      // Erase current drawings
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.beginPath();
      context.lineWidth = 3;
      context.strokeStyle = Style.COLOR_GRAY;
      context.rect(0, 0, canvas.width, canvas.height);
      context.stroke();

      // Move and redraw agents
      for (let i = 0; i < this.population.length; i++) {
        const agent = this.population[i];
		
        //draw agent
        this.drawState(agent, context);

        // Draw state name as text
        if (this.state.showName) {
          context.fillStyle = "rgba(0, 0, 0, 1)";
          context.font = parseInt(this.radius(agent.state)) + "px Arial";
          context.textAlign = "center";
          context.fillText(
            agent.state,
            agent.x,
            agent.y - 2 + this.radius(agent.state) / 2
          );
        }
      }
    }
  }

  drawTerminal() {
    const canvas = this.refs.terminal;
    if (canvas) {
      const context = canvas.getContext("2d");
      if (this.state.messageOpen) {
        context.globalAlpha = 0.5;
        context.rect(
          this.width / 2 - 300,
          this.height / 2 - 25,
          600,
          50
        );
        context.fillStyle = "white";
        context.fill();
        context.globalAlpha = 1.0;
        context.save();
        context.translate(canvas.width / 2, canvas.height / 2);
        //context.rotate(- Math.PI / 4);
        context.font = "25px Roboto";
        context.fillStyle = Style.COLOR_DARK_GRAY;
        context.textAlign = "center";
        context.textBaseline = "middle";
        context.fillText(this.state.message, 0, 0);
        context.restore();
      } else {
        context.clearRect(0, 0, canvas.width, canvas.height);
      }
    }
  }

  drawLegend() {
    if (this.isDesktop) {
      let shapes = this.state.colorShapes;
      return (
        <div
          style={{
            visibility: this.isDesktop ? "visible" : "hidden",
			border: "3px outset rgba(0,0,0,0.5)", 
			borderRadius: "5px"
          }}
        >
          <h5 style={{ fontSize: "1.2em", color: Style.COLOR_GRAY, margin: "3px"}}>Legend</h5>
		  <div
            style={{
              maxWidth: "250px",
              maxHeight: this.height-50,
              overflow: "auto",
              resize: "vertical",
			  height: "fit-content"
            }}
          >
          <table>
            <tbody>
            {Object.keys(shapes).map((k, key) => (
              <tr key={key}>
                <td>
                  <canvas
                    width={this.getMaxRadius() * 2.2}
                    height={this.radius(shapes[k][0]) * 2.2}
                    ref={(ref) => (this.legendRefs[shapes[k][0]] = ref)}
                  />
                </td>
                <td>
                  {shapes[k].map((q, key) => (
                    <span
                      style={{ marginRight: "8px", color: Style.COLOR_GRAY }}
                      key={key}
                    >
                      {Style.rewriteLabel(q)}
                    </span>
                  ))}
                </td>
              </tr>
            ))}
            </tbody>
          </table>
		  </div>
        </div>
      );
    } else {
      return <div></div>;
    }
  }

  drawLegendItem(q) {
    if (this.isDesktop) {
      this.drawState(
        { x: this.getMaxRadius(), y: this.radius(q), state: q, cooldown: 0 },
        this.legendRefs[q].getContext("2d")
      );
    }
  }

  drawState(agent, context) {
    // Draw agent
    const radius = this.radius(agent.state);
    const innerRadius = Math.max(radius - 3, 1);
    const color = this.color(agent.state);
    const innerFillColor =  agent.cooldown === 0 ? this.fillcolor(agent.state) : "rgba(255,255,255,1)";

    switch (this.shape(agent.state)) {
      case "circle":
        context.fillStyle = color;
        context.beginPath();
        context.arc(agent.x, agent.y, radius, 0, 2 * Math.PI);
        context.fill();
        //only border
        context.fillStyle = innerFillColor;
        context.beginPath();
        context.arc(agent.x, agent.y, innerRadius, 0, 2 * Math.PI);
        context.fill();
        break;
      case "square":
        context.fillStyle = color;
        context.beginPath();
        context.rect(
          agent.x - radius,
          agent.y - radius,
          radius * 2.0,
          radius * 2.0
        );
        context.fill();
        //only border
        context.fillStyle = innerFillColor;
        context.beginPath();
        context.rect(
          agent.x - innerRadius,
          agent.y - innerRadius,
          innerRadius * 2.0,
          innerRadius * 2.0
        );
        context.fill();
        break;
      case "triangle":
        //draw equilateral triangle inscribed in shape "circle"
        let points = [];
        let innerPoints = [];
        for (let angle of [
          0,
          (1.0 / 3) * (2 * Math.PI),
          (2.0 / 3) * (2 * Math.PI),
        ]) {
          points.push({
            x: radius * Math.cos(angle) + agent.x,
            y: radius * Math.sin(angle) + agent.y,
          });
          innerPoints.push({
            x: innerRadius * Math.cos(angle) + agent.x,
            y: innerRadius * Math.sin(angle) + agent.y,
          });
        }
        context.fillStyle = color;
        context.beginPath();
        context.moveTo(points[0].x, points[0].y);
        context.lineTo(points[1].x, points[1].y);
        context.lineTo(points[2].x, points[2].y);
        context.closePath();
        context.fill();
        // only border
        context.fillStyle = innerFillColor;
        context.beginPath();
        context.fillStyle = "rgba(255, 255, 255, 0.7)";
        context.moveTo(innerPoints[0].x, innerPoints[0].y);
        context.lineTo(innerPoints[1].x, innerPoints[1].y);
        context.lineTo(innerPoints[2].x, innerPoints[2].y);
        context.closePath();
        context.fill();
        break;
      default:
        break;
    }
  }
  
  sanitizePositions() {
	for (let i = 0; i < this.population.length; i++) {
      const agent = this.population[i];
      const radius = this.radius(agent.state);

      // Move back inside if outside (this can happen during resizing)
      if (agent.x + radius > this.width) {
        agent.x = this.width - radius;
      } else if (agent.x - radius < 0) {
        agent.x = radius;
      }

      if (agent.y + radius > this.height) {
        agent.y = this.height - radius;
      } else if (agent.y - radius < 0) {
        agent.y = radius;
      }
	}
  }

  update() {
    this.sanitizePositions();
    this.applyInteractions();

    for (let i = 0; i < this.population.length; i++) {
      const agent = this.population[i];
      const radius = this.radius(agent.state);
	  const speedsliderfactor = 1.4 ** this.state.speedLevel;
	  
	  // update cooldown
	  if (agent.cooldown > 0) {
		agent.cooldown = agent.cooldown - speedsliderfactor;
	    if (agent.cooldown < 0) agent.cooldown = 0;
	  }
	  

      // Bump into right or left wall
      if (
        agent.x + radius + agent.vx * speedsliderfactor >= this.width ||
        agent.x - radius + agent.vx * speedsliderfactor <= 0
      ) {
        agent.vx *= -1;
      }

      // Bump into bottom or top wall
      if (
        agent.y + radius + agent.vy * speedsliderfactor >= this.height ||
        agent.y - radius + agent.vy * speedsliderfactor <= 0
      ) {
        agent.vy *= -1;
      }
	  
	  if (this.dragged === agent) continue;

      // Move agent
      agent.x += agent.vx * speedsliderfactor;
      agent.y += agent.vy * speedsliderfactor;
    }

    this.redraw();
    this.animation = requestAnimationFrame(() => this.update());
  }

  renderBoom(boom, animation) {
    const effects = this.refs.effects;

    if (effects) {
      const context = effects.getContext("2d");

      context.clearRect(0, 0, effects.width, effects.height);

      if (boom.radius >= 10) {
        const radius = BOOM_RADIUS - boom.radius;
        const transparency = (-1.0 / BOOM_RADIUS) * radius + 1.0;

        context.fillStyle = "rgba(150, 150, 255, " + transparency + ")";
        context.beginPath();
        context.arc(boom.x, boom.y, radius, 0, 2 * Math.PI);
        context.fill();

        boom.radius /= 1.25;
        requestAnimationFrame((id) => this.renderBoom(boom, id));
      } else {
        cancelAnimationFrame(animation);
      }
    }
  }

  boom(x, y) {
    const center = { x: x, y: y, radius: BOOM_RADIUS };

    for (let i = 0; i < this.population.length; i++) {
      const agent = this.population[i];
      const dist = this.distance(agent, center);
	  
	  if (dist === 0) {
		agent.vx = this.getRandomSpeed();
		agent.vy = this.getRandomSpeed();
	  }
	  else if (dist <= center.radius) {
        const MAX = 10;
        const MIN = 1;
        const speed = ((MIN - MAX) / center.radius) * dist + MAX;

        agent.vx = (speed * (agent.x - center.x)) / dist;
        agent.vy = (speed * (agent.y - center.y)) / dist;
      }
    }

    requestAnimationFrame((id) => this.renderBoom(center, id));
  }

  handlePlay() {
    if (this.state.playing) {
      this.setState({ playing: false }, () =>
        cancelAnimationFrame(this.animation)
      );
    } else {
	  this.dragged = null;
      this.setState(
        { playing: true },
        () => (this.animation = requestAnimationFrame(() => this.update()))
      );
    }
  }

  handleReset() {
    this.setState({ hasTerminated: false, messageOpen: false });
    this.initalize(this.props.population);
	this.dragged = null;

    if (!this.state.playing) {
      this.redraw();
    }
  }
  
  handleSpeedSlider(event, value) {
    this.setState({ speedLevel: value });
  }
  
  handleCanvasResize() {
    const canvas = this.refs.canvas;
    const effects = this.refs.effects;
    const terminal = this.refs.terminal;
    const canvasbox = this.refs.canvasbox;
	
	this.width = canvasbox.clientWidth-15;
	this.height = window.innerHeight - 200;
	
    canvas.width = this.width;
    canvas.height = this.height;
	
    effects.width = this.width;
    effects.height = this.height;
	
    terminal.width = this.width;
    terminal.height = this.height;
	
	this.sanitizePositions();
	this.redraw();
	this.drawTerminal();
  }

  render() {
    const buttonStyle = {
      textAlign: "center",
      fontSize: "16px",
      margin: "2px",
      borderRadius: "4px",
      width: 125,
    };

    return (
	  <Box display="flex" flexWrap="wrap" p={1}> 
	    <Box 
		  ref="canvasbox"
		  flexGrow={1} 
		  flexBasis="70%"
          style={{
            height: "fit-content",
			overflow: "hidden",
          }}>
		  <div
            style={{
              position: "relative",
              height: "fit-content",
              float: "left"
          }}
        >
          <canvas
            ref="effects"
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              zIndex: 2,
            }}
          />
          <canvas
            ref="terminal"
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              zIndex: 1,
            }}
          />
          <canvas ref="canvas"/>
		  </div>
        </Box>
		<Box flexBasis="150px" p={1}>
          {this.drawLegend()}
          {this.drawTerminal()}
		</Box>
		<Box flexGrow={1} flexBasis="30%">
		  <Box display="flex" flexWrap="wrap" alignItems="flex-start" justifyContent="center">
		    <Box p={1}>
		      <Button
                onClick={() => this.handlePlay()}
                disabled={false}
                style={buttonStyle}
                color="primary"
                variant="contained"
                startIcon={
                  <Icon className="material-icons">
                    {this.state.playing ? "pause" : "play_arrow"}
                  </Icon>
                }
              >
                {this.state.playing ? "Pause" : "Play"}
              </Button>
		    </Box>
		    <Box p={1}>
		      <Button
                onClick={() => this.handleReset()}
                disabled={false}
                style={buttonStyle}
                color="primary"
                variant="contained"
                startIcon={<Icon className="material-icons">{"refresh"}</Icon>}
              >
                Reset
              </Button>
		    </Box>
		    <Box p={1}>
		      <FormControlLabel
                control={
                  <Switch
                    onClick={() =>
                      this.setState({ showName: !this.state.showName }, () =>
                        this.redraw()
                      )
                    }
                  />
                }
                label="Display state labels"
              />
		    </Box>
		    <Box flexGrow={1} flexBasis="50%" p={1}>
		      <Box display="flex" flexWrap="wrap">
			    <Box p={1}>
		          <Icon className="material-icons" style={{ fontSize: "16px"}} color="primary">
                    {"timer"}
                  </Icon>
			    </Box>
                <Box flexGrow={1} p={1}>
			      <Tooltip title="Speed">
                    <Slider
                      min={-9}
                      max={9}
	                  step={0.1}
                      scale={(x) => "x" + (x < 0 ? (1.4 ** x).toFixed(2) : (1.4 ** x).toPrecision(2))}
                      valueLabelDisplay="auto"
                      style={{minWidth: "50px"}}
                      value={this.state.speedLevel}
                      onChange={throttle(this.handleSpeedSlider.bind(this), 100)}
                    />
                  </Tooltip>
			    </Box>
			  </Box>
			</Box>
		  </Box>
		</Box>
	  </Box> 
    );
  }
}

export default SpatialSimulator;
