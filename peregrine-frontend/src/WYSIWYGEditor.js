import React from "react";
import Chip from "@material-ui/core/Chip";
import Divider from "@material-ui/core/Divider";
import Icon from "@material-ui/core/Icon";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import TextField from "@material-ui/core/TextField";
import { FormulaEditForm, InfoEditForm } from "./ProtocolEditForms";
import CodeSaver from "./CodeSaver";
import * as Style from "./PopulationStyle";
import State from "./State";


const INITIAL_COLOR = "#ade0a6";
const NON_INITIAL_COLOR = "#d4d9d8";

class WYSIWYGEditor extends React.Component {
  constructor(props) {
    super(props);

    let protocol = props.protocol;
    if (!protocol.leaders) {
      protocol.leaders = {};
    }
    this.state = {
      protocol: protocol,
      title: props.title,
      fileName: props.fileName,
      addStateVal: "",
      onCodeSave: props.onCodeSave,
      curTab: 1,
    };
  }

  generateCode() {
    function toState(q) {
      return '"' + q + '"';
    }

    function toTransition(t) {
      return (
        "Utils.transition((" +
        t.pre.map(toState).join(", ") +
        "), (" +
        t.post.map(toState).join(", ") +
        "))"
      );
    }

    return (
      `# EDIT_WYSIWYG_ONLY
#-*- coding: utf-8 -*-
# This file has been created by Peregrine.
# Do not edit manually!
def generateProtocol(params):
    return {
      "title":         ` +
      JSON.stringify(this.state.protocol.title) +
      `,
      "states":        [` +
      this.state.protocol.states.map(toState).join(", ") +
      `],
      "transitions":   [` +
      this.state.protocol.transitions
        .map(toTransition)
        .join(",\n                        ") +
      `],
      "initialStates": [` +
      this.state.protocol.initialStates.map(toState).join(", ") +
      `],
      "trueStates":    [` +
      this.state.protocol.trueStates.map(toState).join(", ") +
      `],
      "predicate":     "` +
      this.state.protocol.predicate +
      `",
      "leaders" : ` +
      JSON.stringify(this.state.protocol.leaders) +
      `,
      "precondition":     "` +
      (this.state.protocol.precondition
        ? this.state.protocol.precondition
        : "true") +
      `",
      "description":   ` +
      JSON.stringify(this.state.protocol.description) +
      `
    }`
    );
  }

  handleRequestDeleteState(state) {
    this.protocol = this.state.protocol;

    // Remove state from states
    const stateToDelete = this.protocol.states.indexOf(state);
    this.protocol.states.splice(stateToDelete, 1);

    // Remove state from initial states if it is initial
    const initStateToDelete = this.protocol.initialStates.indexOf(state);

    if (initStateToDelete >= 0) {
      this.protocol.initialStates.splice(stateToDelete, 1);
    }

    // Remove state from initial states if it is initial
    const trueStateToDelete = this.protocol.trueStates.indexOf(state);

    if (trueStateToDelete >= 0) {
      this.protocol.trueStates.splice(trueStateToDelete, 1);
    }

    // Remove state from all transitions which contain it
    let transitionsToDelete = [];

    for (let c = 0; c < this.protocol.transitions.length; c++) {
      let t = this.protocol.transitions[c];

      if (
        t.pre[0] === state ||
        t.pre[1] === state ||
        t.post[0] === state ||
        t.post[1] === state
      ) {
        transitionsToDelete.push(c);
      }
    }

    while (transitionsToDelete.length) {
      this.protocol.transitions.splice(transitionsToDelete.pop(), 1);
    }

    this.protocol.leaders[state] = 0;
    this.setState({ protocol: this.protocol });
  }

  handleRequestTransitionDelete(transition) {
    this.protocol = this.state.protocol;
    const transitionToDelete = this.protocol.transitions.indexOf(transition);
    this.protocol.transitions.splice(transitionToDelete, 1);

    this.setState({ protocol: this.protocol });
  }

  handleTouchTap(state) {
    this.protocol = this.state.protocol;
    const index = this.protocol.trueStates.indexOf(state);

    if (index === -1) {
      this.protocol.trueStates.push(state);
    } else {
      this.protocol.trueStates.splice(index, 1);
    }

    this.setState({ protocol: this.protocol });
  }

  handleInitStateToggle(state) {
    this.protocol = this.state.protocol;
    const index = this.protocol.initialStates.indexOf(state);

    if (index === -1) {
      this.protocol.initialStates.push(state);
    } else {
      this.protocol.initialStates.splice(index, 1);
    }

    this.setState({ protocol: this.protocol });
  }

  doesStateExist(state) {
    return this.state.protocol.states.indexOf(state) !== -1;
  }

  isInitialState(state) {
    return this.state.protocol.initialStates.indexOf(state) !== -1;
  }

  handleAddState() {
    this.protocol = this.state.protocol;
    let state = this.state.addStateVal;
    const stateExistent = this.doesStateExist(state);

    if (!stateExistent) {
      this.protocol = this.state.protocol;
      this.protocol.states.push(state);

      this.setState({ protocol: this.protocol, addStateVal: "" });
    }
  }

  transState(i) {
    return this.state["addTransState" + i.toString()];
  }

  allStatesExist() {
    let allStatesExist = true;

    for (let i = 1; i <= 4; i++) {
      allStatesExist &= this.doesStateExist(this.transState(i));
    }

    return allStatesExist;
  }

  addTransition() {
    if (this.allStatesExist()) {
      let protocol = this.state.protocol;

      const p = this.transState(1);
      const q = this.transState(2);
      const p_ = this.transState(3);
      const q_ = this.transState(4);

      const t = {
        name: "" + p + ", " + q + " -> " + p_ + ", " + q_,
        pre: [p, q],
        post: [p_, q_],
      };

      protocol.transitions.push(t);

      this.setState({
        protocol: protocol,
      });
    }
  }

  handleTextFieldChange(event) {
    if (event.key === "Enter") {
      this.handleAddState(this);
    } else {
      this.setState({ addStateVal: event.target.value });
    }
  }

  handleTransitionFieldChange(event, num) {
    if (event.key === "Enter") {
      this.addTransition(this);
    } else {
      let newState = {};
      newState["addTransState" + num] = event.target.value;

      this.setState(newState);
    }
  }

  renderStateSelect(label, num) {
    return (
      <div
        style={{
          display: "flex",
          alignContent: "center",
          justifyContent: "center",
          alignSelf: "center",
          justifySelf: "center",
        }}
      >
        <FormControl>
          <InputLabel id={"state-label-id" + num}>{label}</InputLabel>
          <Select
            labelId={"state-label-id" + num}
            value={this.state["addTransState" + num]}
            onChange={(event) => this.handleTransitionFieldChange(event, num)}
            style={{ width: "150px", paddingLeft: 10, marginRight: 10 }}
          >
            {this.state.protocol.states.map((x, i) => (
              <MenuItem value={x}>{x}</MenuItem>
            ))}
          </Select>
        </FormControl>
      </div>
    );
  }

  renderState(state) {
    return (
      <State key={state} label={state} protocol={this.state.protocol}/>
    );
  }

  renderStateHolder(q) {
    return (
      <div style={{ paddingRight: 20, paddingTop: 20 }}>
        <Chip
          style={{
            width: "100px",
            height: "80px",
            paddingBottom: 10,
            backgroundColor: (this.isInitialState(q)
              ? INITIAL_COLOR
              : NON_INITIAL_COLOR),
          }}
          key={q}
          label={
            <div
              onClick={(key) => {
                key.stopPropagation();
                this.handleTouchTap(q);
              }}
              style={{ decoration: "none" }}
            >
              {this.renderState(q)}
            </div>
          }
          onDelete={(key) => this.handleRequestDeleteState(q)}
          onClick={(key) => this.handleInitStateToggle(q)}
        />
      </div>
    );
  }

  renderStateEditor() {
    function sampleState(output) {
      return (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: 25,
            width: output === 0 || output === 1 ? 25 : 80,
            backgroundColor:
              output === 0
                ? Style.FALSE_COLOR
                : output === 1
                ? Style.TRUE_COLOR
                : output === "init"
                ? INITIAL_COLOR
                : NON_INITIAL_COLOR,
            color: output === 0 || output === 1 ? "white" : "black",
            fontWeight: "bold",
            borderRadius: "5px",
          }}
        >
          {output === 0 || output === 1
            ? output
            : output === "init"
            ? "init."
            : "non init."}
        </div>
      );
    }

    return (
      <div style={{ padding: 20 }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
            justifyContent: "space-between",
          }}
        >
          <div>
            <TextField
              placeholder="Enter state name"
              label="Add state"
              value={this.state.addStateVal}
              style={{ marginTop: -10 }}
              onKeyPress={this.handleTextFieldChange.bind(this)}
              onChange={this.handleTextFieldChange.bind(this)}
            />
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              fontFamily: Style.MAIN_FONT,
              fontSize: 16,
              border: "3px solid #CDCDCD",
              padding: 5,
            }}
          >
            <div
              style={{
                display: "grid",
                gridTemplateColumns: "auto auto auto",
                justifyContent: "center",
                alignItems: "center",
                gridRowGap: "2px",
                gridColumnGap: "5px",
              }}
            >
              <div>Toggle state to change its output:</div>
              <div style={{ justifySelf: "center" }}>{sampleState(1)}</div>
              <div style={{ justifySelf: "center" }}>{sampleState(0)}</div>
              <div>Toggle state container to make it initial:</div>
              <div style={{ justifySelf: "center" }}>{sampleState("init")}</div>
              <div style={{ justifySelf: "center" }}>
                {sampleState("noninit")}
              </div>
            </div>
          </div>
        </div>
        <div style={{ display: "flex", flexWrap: "wrap", paddingTop: 20 }}>
          {this.state.protocol.states.map((q, key) =>
            this.renderStateHolder(q)
          )}
        </div>
        <div style={{ paddingTop: 30, paddingBottom: "20px" }}>
          <Divider />
          <h3 style={{ color: "#322e2e" }}>Leaders </h3>
          {this.state.protocol.states.map((q) =>
            this.renderLeaderEditorElement(q, this)
          )}
        </div>
      </div>
    );
  }

  handleLeaderChange(q, value) {
    let protocol = this.state.protocol;
    protocol.leaders[q] = value;
    this.setState({ protocol: protocol });
  }

  renderLeaderEditorElement(q) {
    let currentVal = this.state.protocol.leaders[q]
      ? this.state.protocol.leaders[q]
      : 0;
    let range = [
      0,
      1,
      2,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
    ];
    return (
      <div
        style={{
          paddingRight: "5px",
          float: "left",
          height: "200px",
          marginBottom: "10px",
        }}
      >
        <InputLabel id={"label" + q}>{q}</InputLabel>
        <Select
          labelId={"label" + q}
          name={q}
          label={q}
          value={currentVal}
          onChange={(event) =>
            this.handleLeaderChange(q, event.target.value, this)
          }
        >
          {range.map((val, k) => (
            <MenuItem value={val} name={q} key={k}>
              {val.toString()}
            </MenuItem>
          ))}
        </Select>
      </div>
    );
  }

  changePredicate(newPredicate) {
    this.protocol = this.state.protocol;
    this.protocol.predicate = newPredicate;

    this.setState({ protocol: this.protocol });
  }

  changePrecondition(newPredicate) {
    this.protocol = this.state.protocol;
    this.protocol.precondition = newPredicate;
    this.setState({ protocol: this.protocol });
  }

  changeDescription(newDescription) {
    this.protocol = this.state.protocol;
    this.protocol.description = newDescription;

    this.setState({ protocol: this.protocol });
  }

  changeTitle(newTitle) {
    this.protocol = this.state.protocol;
    this.protocol.title = newTitle;

    this.setState({ protocol: this.protocol });
  }

  renderProtocolInfoEditor() {
    return (
      <div style={{ paddingLeft: 20, paddingRight: 20 }}>
        <div>
          <InfoEditForm
            description={this.state.protocol.description}
            states={this.state.protocol.states}
            leaders={this.state.protocol.leaders}
            onDescriptionChange={this.changeDescription.bind(this)}
            title={this.state.protocol.title}
            onTitleChange={this.changeTitle.bind(this)}
          />
        </div>
        <div style={{ paddingTop: 10 }}>
          <FormulaEditForm
            formula={this.state.protocol.predicate}
            precondition={this.state.protocol.precondition}
            onPreconditionChange={this.changePrecondition.bind(this)}
            onFormulaChange={this.changePredicate.bind(this)}
          />
        </div>
      </div>
    );
  }

  renderTransition(t) {
    return (
      <div style={{ paddingRight: 20, paddingTop: 20 }}>
        <Chip
          style={{ paddingBottom: 8, width: "450px", height: "80px" }}
          key={t}
          onDelete={(key) => this.handleRequestTransitionDelete(t)}
          label={
            <div
              style={{
                display: "flex",
                alignContent: "center",
                justifyContent: "center",
              }}
            >
              {this.renderState(t.pre[0])}
              {this.renderState(t.pre[1])}
              <span
                style={{
                  display: "flex",
                  alignSelf: "center",
                  justifySelf: "center",
                  fontSize: 26,
                  padding: "5 10 0 10",
                }}
              >
                ↦
              </span>
              {this.renderState(t.post[0])}
              {this.renderState(t.post[1])}
            </div>
          }
        />
      </div>
    );
  }

  renderTransitionEditor() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingLeft: 10,
        }}
      >
        <div
          style={{
            display: "flex",
            alignContent: "center",
            flexWrap: "wrap",
            paddingTop: 10,
          }}
        >
          {this.renderStateSelect("First state", 1)}
          {this.renderStateSelect("Second state", 2)}
          <div
            style={{
              display: "flex",
              alignSelf: "center",
              justifySelf: "center",
              fontSize: 26,
              padding: "0 10 0 30",
            }}
          >
            ↦
          </div>
          {this.renderStateSelect("First state", 3)}
          {this.renderStateSelect("Second state", 4)}
          <div
            style={{
              display: "flex",
              alignSelf: "center",
              justifySelf: "center",
              paddingLeft: 20,
              paddingBottom: 5,
            }}
          >
            <Button
              color="primary"
              variant="contained"
              style={{ width: 40 }}
              disabled={!this.allStatesExist()}
              onClick={(event) => this.addTransition(this)}
            >
              Add
            </Button>
          </div>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexWrap: "wrap",
            paddingTop: 50,
          }}
        >
          {this.state.protocol.transitions.map((t, key) =>
            this.renderTransition(t)
          )}
        </div>
      </div>
    );
  }

  renderCurTab(curTab) {
    let render = [
      this.renderProtocolInfoEditor,
      this.renderStateEditor,
      this.renderTransitionEditor,
    ];
    return render[curTab].bind(this)();
  }

  render() {
    return (
      <div>
        {this.props.saveDisabled ? (
          ""
        ) : (
          <CodeSaver
            code={this.generateCode()}
            onCodeSave={this.state.onCodeSave}
            fileName={this.state.fileName}
            title={this.state.title}
            delay={3000}
          />
        )}
        <div style={{ paddingBottom: "10px", boxShadow: Style.BOX_SHADOW }}>
          <Tabs
            indicatorColor="primary"
            textColor="primary"
            value={this.state.curTab}
            onChange={(event, v) => this.setState({ curTab: v })}
          >
            <Tab
              label="Protocol details"
              icon={<Icon className="material-icons">description</Icon>}
            />
            <Tab
              label="States"
              icon={<Icon className="material-icons">crop_square</Icon>}
            />
            <Tab
              label="Transitions"
              icon={<Icon className="material-icons">trending_flat</Icon>}
            />
          </Tabs>
          {this.renderCurTab(this.state.curTab)}
        </div>
      </div>
    );
  }
}

export default WYSIWYGEditor;
