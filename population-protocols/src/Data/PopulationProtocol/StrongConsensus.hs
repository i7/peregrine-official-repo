{-# LANGUAGE FlexibleContexts #-}

module Data.PopulationProtocol.StrongConsensus
    (checkStrongConsensusSat,
     checkReachabilityCompleteSat,
     StrongConsensusCounterExample,
     RefinementObjects,
     findTrapConstraintsSat,
     findSiphonConstraintsSat,
     findUTrapConstraintsSat,
     findUSiphonConstraintsSat,
     ReachabilityProperty(..)
    )
where

import Data.SBV
import Data.Maybe (fromMaybe, fromJust)
import qualified Data.Map as M
import Data.List ((\\), genericLength, nub)

import Data.PopulationProtocol.Util
import Data.PopulationProtocol
import Data.PopulationProtocol.Solver
import Data.PopulationProtocol.Presburger

import Debug.Trace (trace)

data ReachabilityProperty = Correctness | StrongConsensus | PrePostCondition

type StrongConsensusCounterExample = (Configuration, Configuration, Configuration, FlowVector, FlowVector)
type StrongConsensusCompleteCounterExample = (Configuration, Configuration, Configuration, FlowVector, FlowVector, Configuration, Configuration, Configuration, Configuration)
type RefinementObjects = ([Trap], [Siphon], [(Configuration, Configuration)])

stateEquationConstraints :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap Transition -> SBool
stateEquationConstraints pp m0 m x =
            bAnd $ map checkStateEquation $ states pp
        where checkStateEquation q =
                let incoming = map addTransition $ statePre pp q
                    outgoing = map addTransition $ statePost pp q
                in  val m0 q + sum incoming - sum outgoing .== val m q
              addTransition (t,w) = literal (fromIntegral w) * val x t

nonNegativityConstraints :: (Ord a, Show a) => SIMap a -> SBool
nonNegativityConstraints m =
            bAnd $ map checkVal $ vals m
        where checkVal x = x .>= 0

terminalConstraints :: PopulationProtocol -> SIMap State -> SBool
terminalConstraints pp m =
            bAnd $ map checkTransition $ transitions pp
        where checkTransition t = bOr $ map checkState $ transitionPre pp t
              checkState (q,w) = val m q .<= literal (fromIntegral (w - 1))

addLeaders :: PopulationProtocol -> SIMap State -> SIMap State
addLeaders pp m = trace ("Add leaders with: " ++ show [(q, i) | q <- states pp, let i = numOfLeadersForState pp q]) $  M.mapWithKey (\q num -> num + fromIntegral (numOfLeadersForState pp q)) m

initialConfiguration :: PopulationProtocol -> SIMap State -> SBool
initialConfiguration pp m0 =
  let preCond = (evaluateFormula . innerFormula)  (fromMaybe (ExQuantFormula [] FTrue) (precondition pp)) in
        (sum (mval m0 (getInitialStates pp)) .>= 2) &&&
        (sum (mval m0 (getNonInitialStates pp)) .== 0) &&&
        preCond (stateToLabelMap pp m0)


differentConsensusConstraints :: ReachabilityProperty -> PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap VarLabel -> SIMap VarLabel -> SBool
differentConsensusConstraints reachabilityProperty pp m0 m1 m2 qe qa = case reachabilityProperty of
    StrongConsensus  -> (oT &&& oF)
    Correctness      -> correctnessConstraints
    PrePostCondition -> prePostConstraints
    where
            oT = sum (mval m1 (getTrueStates pp)) .> 0
            oF = sum (mval m2 (getFalseStates pp)) .> 0
            prePostConstraints = evaluateFormula (innerFormula (fromJust (precondition pp))) m0' &&&
                                 evaluateFormula (Neg (innerFormula (fromJust (postcondition pp)))) m1'
            correctnessConstraints =
                if null (quantifiedVariables (predicate pp)) then
                    let oP = evaluateFormula (innerFormula (predicate pp)) m0'
                    in (oP &&& oF) ||| (bnot oP &&& oT)
                else
                    let oPT = evaluateFormula (innerFormula (predicate pp)) (M.union m0' qe)
                        oPF = evaluateFormula (Neg (innerFormula (predicate pp))) (M.union m0' qa)
                    in (oPT &&& oF) ||| (oPF &&& oT)
            m0' = stateToLabelMap pp m0
            m1' = stateToLabelMap pp m1


unmarkedByConfiguration :: [State] -> SIMap State -> SBool
unmarkedByConfiguration r m = sum (mval m r) .== 0

markedByConfiguration :: [State] -> SIMap State -> SBool
markedByConfiguration r m = sum (mval m r) .> 0

sequenceNotIn :: [Transition] -> SIMap Transition -> SBool
sequenceNotIn u x = sum (mval x u) .== 0

sequenceIn :: [Transition] -> SIMap Transition -> SBool
sequenceIn u x = sum (mval x u) .> 0

checkUTrap :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition -> Trap -> SBool
checkUTrap pp m0 m1 m2 x1 x2 utrap =
            (((sequenceIn upre x1) &&& (sequenceNotIn uunmark x1)) ==> (markedByConfiguration utrap m1)) &&&
            (((sequenceIn upre x2) &&& (sequenceNotIn uunmark x2)) ==> (markedByConfiguration utrap m2))
        where upost = nub $ concatMap (statePostList pp) utrap
              upre = nub $ concatMap (statePreList pp) utrap
              uunmark = upost \\ upre

checkUTrapConstraints :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition -> [Trap] -> SBool
checkUTrapConstraints pp m0 m1 m2 x1 x2 traps =
        bAnd $ map (checkUTrap pp m0 m1 m2 x1 x2) traps

checkUSiphon :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition -> Siphon -> SBool
checkUSiphon pp m0 m1 m2 x1 x2 usiphon =
            (((sequenceIn upost x1) &&& (sequenceNotIn umark x1)) ==> (markedByConfiguration usiphon m0)) &&&
            (((sequenceIn upost x2) &&& (sequenceNotIn umark x2)) ==> (markedByConfiguration usiphon m0))
        where upost = nub $ concatMap (statePostList pp) usiphon
              upre = nub $ concatMap (statePreList pp) usiphon
              umark = upre \\ upost

checkUSiphonConstraints :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition -> [Siphon] -> SBool
checkUSiphonConstraints pp m0 m1 m2 x1 x2 siphons =
        bAnd $ map (checkUSiphon pp m0 m1 m2 x1 x2) siphons

checkStrongConsensus :: ReachabilityProperty -> PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition ->
        SIMap VarLabel -> SIMap VarLabel -> RefinementObjects -> SBool
checkStrongConsensus reachabilityProperty pp m0 m1 m2 x1 x2 qe qa (utraps, usiphons, nonreach) =
        stateEquationConstraints pp m0 m1 x1 &&&
        stateEquationConstraints pp m0 m2 x2 &&&
        initialConfiguration pp m0 &&&
        nonNegativityConstraints m0 &&&
        nonNegativityConstraints m1 &&&
        nonNegativityConstraints m2 &&&
        nonNegativityConstraints x1 &&&
        nonNegativityConstraints x2 &&&
        terminalConstraints pp m1 &&&
        terminalConstraints pp m2 &&&
        differentConsensusConstraints reachabilityProperty pp m0' m1 m2 qe qa &&&
        checkUTrapConstraints pp m0' m1 m2 x1 x2 utraps &&&
        checkUSiphonConstraints pp m0' m1 m2 x1 x2 usiphons &&&
        nonReachConstraints pp m0' m1 m2 nonreach where
          m0' = addLeaders pp m0

nonReachConstraints :: PopulationProtocol -> SIMap State -> SIMap State -> SIMap State -> [(Configuration, Configuration)] -> SBool
nonReachConstraints pp m0 m1 m2 nonreach = bAnd [nonEq m0 c ||| (nonEq m1 c' &&& nonEq m2 c') | (c, c') <- nonreach] where
      nonEq :: SIMap State -> Configuration -> SBool
      nonEq m c = bOr [val m q ./= (fromIntegral (val c q)) | q <- states pp]

checkStrongConsensusSat :: ReachabilityProperty -> PopulationProtocol -> RefinementObjects -> ConstraintProblem Integer StrongConsensusCounterExample
checkStrongConsensusSat reachabilityProperty pp refinements =
        let m0 = makeVarMapWith ("m0'"++) $ states pp
            m1 = makeVarMapWith ("m1'"++) $ states pp
            m2 = makeVarMapWith ("m2'"++) $ states pp
            x1 = makeVarMapWith ("x1'"++) $ transitions pp
            x2 = makeVarMapWith ("x2'"++) $ transitions pp
            qe = makeVarMapWith ("e'"++) $ quantifiedVariables (predicate pp)
            qa = makeVarMapWith ("a'"++) $ quantifiedVariables (predicate pp)
        in  ("strong consensus", "(c0, c1, c2, x1, x2)",
             getNames m0 ++ getNames m1 ++ getNames m2 ++ getNames x1 ++ getNames x2, getNames qe, getNames qa,
             \fm -> checkStrongConsensus reachabilityProperty pp (fmap fm m0) (fmap fm m1) (fmap fm m2) (fmap fm x1) (fmap fm x2) (fmap fm qe) (fmap fm qa) refinements,
             \fm -> counterExampleFromAssignment (fmap fm m0) (fmap fm m1) (fmap fm m2) (fmap fm x1) (fmap fm x2))

counterExampleFromAssignment :: IMap State -> IMap State -> IMap State -> IMap Transition -> IMap Transition -> StrongConsensusCounterExample
counterExampleFromAssignment m0 m1 m2 x1 x2 =
        (configurationFromMap m0, configurationFromMap m1, configurationFromMap m2, configurationFromMap x1, configurationFromMap x2)

-- trap and siphon refinement constraints

trapConstraint :: PopulationProtocol -> SIMap State -> Transition -> SBool
trapConstraint pp b t =
        sum (mval b $ transitionPreStates pp t) .> 0 ==> sum (mval b $ transitionPostStates pp t) .> 0

siphonConstraint :: PopulationProtocol -> SIMap State -> Transition -> SBool
siphonConstraint pp b t =
        sum (mval b $ transitionPostStates pp t) .> 0 ==> sum (mval b $ transitionPreStates pp t) .> 0

trapConstraints :: PopulationProtocol -> SIMap State -> SBool
trapConstraints pp b =
        bAnd $ map (trapConstraint pp b) $ transitions pp

siphonConstraints :: PopulationProtocol -> SIMap State -> SBool
siphonConstraints pp b =
        bAnd $ map (siphonConstraint pp b) $ transitions pp

uTrapConstraints :: PopulationProtocol -> FlowVector -> SIMap State -> SBool
uTrapConstraints pp x b =
        bAnd $ map (trapConstraint pp b) $ nonZeroElems x

uSiphonConstraints :: PopulationProtocol -> FlowVector -> SIMap State -> SBool
uSiphonConstraints pp x b =
        bAnd $ map (siphonConstraint pp b) $ nonZeroElems x

statesMarkedByConfiguration :: PopulationProtocol -> Configuration -> SIMap State -> SBool
statesMarkedByConfiguration pp m b = sum (mval b $ nonZeroElems m) .> 0

statesUnmarkedByConfiguration :: PopulationProtocol -> Configuration -> SIMap State -> SBool
statesUnmarkedByConfiguration pp m b = sum (mval b $ nonZeroElems m) .== 0

statesPostsetOfSequence :: PopulationProtocol -> FlowVector -> SIMap State -> SBool
statesPostsetOfSequence pp x b = sum (mval b $ concatMap (transitionPostStates pp) $ nonZeroElems x) .> 0

statesPresetOfSequence :: PopulationProtocol -> FlowVector -> SIMap State -> SBool
statesPresetOfSequence pp x b = sum (mval b $ concatMap (transitionPreStates pp) $ nonZeroElems x) .> 0

noOutputTransitionEnabled :: PopulationProtocol -> Configuration -> SIMap State -> SBool
noOutputTransitionEnabled pp m b =
            bAnd $ map outputTransitionNotEnabled $ transitions pp
        where
            outputTransitionNotEnabled t = outputTransitionOfB t ==> transitionNotEnabledInB t
            outputTransitionOfB t = sum [val b q | (q, w) <- transitionPre pp t, val m q >= w] .> 0
            transitionNotEnabledInB t = sum [val b q | (q, w) <- transitionPre pp t, val m q < w] .> 0

nonemptySet :: (Ord a, Show a) => SIMap a -> SBool
nonemptySet b = sum (vals b) .> 0

checkBinary :: SIMap State -> SBool
checkBinary = bAnd . map (\x -> x .== 0 ||| x .== 1) . vals

checkSizeLimit :: SIMap State -> Maybe (Int, Integer) -> SBool
checkSizeLimit _ Nothing = true
checkSizeLimit b (Just (1, curSize)) = (.< literal curSize) $ sumVal b
checkSizeLimit b (Just (2, curSize)) = (.> literal curSize) $ sumVal b
checkSizeLimit _ (Just (_, _)) = error "minimization method not supported"

minimizeMethod :: Int -> Integer -> String
minimizeMethod 1 curSize = "size smaller than " ++ show curSize
minimizeMethod 2 curSize = "size larger than " ++ show curSize
minimizeMethod _ _ = error "minimization method not supported"

findTrapConstraints :: PopulationProtocol -> StrongConsensusCounterExample -> SIMap State -> Maybe (Int, Integer) -> SBool
findTrapConstraints pp (m0, m1, m2, x1, x2) b sizeLimit =
        checkSizeLimit b sizeLimit &&&
        checkBinary b &&&
        trapConstraints pp b &&&
        (
            (statesPostsetOfSequence pp x1 b &&& statesUnmarkedByConfiguration pp m1 b) |||
            (statesPostsetOfSequence pp x2 b &&& statesUnmarkedByConfiguration pp m2 b)
        )

findTrapConstraintsSat :: PopulationProtocol -> StrongConsensusCounterExample -> MinConstraintProblem Integer Trap Integer
findTrapConstraintsSat pp c =
        let b = makeVarMap $ states pp
        in  (minimizeMethod, \sizeLimit ->
            ("trap marked by x1 or x2 and not marked in m1 or m2", "trap",
             getNames b, [],  [],
             \fm -> findTrapConstraints pp c (fmap fm b) sizeLimit,
             \fm -> statesFromAssignment (fmap fm b)))

findUTrapConstraints :: PopulationProtocol -> StrongConsensusCounterExample -> SIMap State -> Maybe (Int, Integer) -> SBool
findUTrapConstraints pp (m0, m1, m2, x1, x2) b sizeLimit =
        checkSizeLimit b sizeLimit &&&
        checkBinary b &&&
        (
            (statesPostsetOfSequence pp x1 b &&& uTrapConstraints pp x1 b &&& statesUnmarkedByConfiguration pp m1 b) |||
            (statesPostsetOfSequence pp x2 b &&& uTrapConstraints pp x2 b &&& statesUnmarkedByConfiguration pp m2 b)
        )

findUTrapConstraintsSat :: PopulationProtocol -> StrongConsensusCounterExample -> MinConstraintProblem Integer Trap Integer
findUTrapConstraintsSat pp c =
        let b = makeVarMap $ states pp
        in  (minimizeMethod, \sizeLimit ->
            ("u-trap (w.r.t. x1 or x2) marked by x1 or x2 and not marked in m1 or m2", "u-trap",
             getNames b, [],  [],
             \fm -> findUTrapConstraints pp c (fmap fm b) sizeLimit,
             \fm -> statesFromAssignment (fmap fm b)))

findSiphonConstraints :: PopulationProtocol -> StrongConsensusCounterExample -> SIMap State -> Maybe (Int, Integer) -> SBool
findSiphonConstraints pp (m0, m1, m2, x1, x2) b sizeLimit =
        checkSizeLimit b sizeLimit &&&
        checkBinary b &&&
        siphonConstraints pp b &&&
        statesUnmarkedByConfiguration pp m0 b &&&
        (statesPresetOfSequence pp x1 b ||| statesPresetOfSequence pp x2 b)

findSiphonConstraintsSat :: PopulationProtocol -> StrongConsensusCounterExample -> MinConstraintProblem Integer Siphon Integer
findSiphonConstraintsSat pp c =
        let b = makeVarMap $ states pp
        in  (minimizeMethod, \sizeLimit ->
            ("siphon used by x1 or x2 and unmarked in m0", "siphon",
             getNames b, [],  [],
             \fm -> findSiphonConstraints pp c (fmap fm b) sizeLimit,
             \fm -> statesFromAssignment (fmap fm b)))


findUSiphonConstraints :: PopulationProtocol -> StrongConsensusCounterExample -> SIMap State -> Maybe (Int, Integer) -> SBool
findUSiphonConstraints pp (m0, m1, m2, x1, x2) b sizeLimit =
        checkSizeLimit b sizeLimit &&&
        checkBinary b &&&
        statesUnmarkedByConfiguration pp m0 b &&&
        (
            (statesPresetOfSequence pp x1 b &&& uSiphonConstraints pp x1 b) |||
            (statesPresetOfSequence pp x2 b &&& uSiphonConstraints pp x2 b)
        )

findUSiphonConstraintsSat :: PopulationProtocol -> StrongConsensusCounterExample -> MinConstraintProblem Integer Siphon Integer
findUSiphonConstraintsSat pp c =
        let b = makeVarMap $ states pp
        in  (minimizeMethod, \sizeLimit ->
            ("u-siphon (w.r.t. x1 or x2) used by x1 or x2 and unmarked in m0", "u-siphon",
             getNames b, [],  [],
             \fm -> findUSiphonConstraints pp c (fmap fm b) sizeLimit,
             \fm -> statesFromAssignment (fmap fm b)))

statesFromAssignment :: IMap State -> ([State], Integer)
statesFromAssignment b = (M.keys (M.filter (> 0) b), sum (M.elems b))

-- method with all refinements directly encoded into the SMT theoryüjw

findMaximalUnmarkedTrap :: PopulationProtocol -> Integer -> SIMap Transition -> SIMap State -> SIMap State -> SBool
findMaximalUnmarkedTrap pp max x m rs =
        let stateConstraints q = unmarkedConstraints q &&& trapConstraints q
            unmarkedConstraints q = (val m q .> 0) .== (val rs q .== 0)
            trapConstraints q = (val rs q .< literal max) .== ((val rs q .== 0) ||| (successorConstraints q))
            successorConstraints q = bOr [ transitionConstraints q t | t <- statePostList pp q ]
            transitionConstraints q t = (val x t .> 0) &&& bAnd [ val rs q' .< val rs q | q' <- transitionPostStates pp t ]
        in  bAnd [ stateConstraints q | q <- states pp ]

findMaximalUnmarkedSiphon :: PopulationProtocol -> Integer -> SIMap Transition -> SIMap State -> SIMap State -> SBool
findMaximalUnmarkedSiphon pp max x m s =
        findMaximalUnmarkedTrap (invertPopulationProtocol pp) max x m s

unmarkedBySequence :: PopulationProtocol -> Integer -> SIMap State -> SIMap Transition -> SBool
unmarkedBySequence pp max trap x =
            bAnd [ stateUnmarkedBySequence q | q <- states pp ]
        where stateUnmarkedBySequence q = (val trap q .== literal max) ==> sum (mval x $ statePreList pp q) .== 0

unusedBySequence :: PopulationProtocol -> Integer -> SIMap State -> SIMap Transition -> SBool
unusedBySequence pp max siphon x =
            bAnd [ stateUnusedBySequence q | q <- states pp ]
        where stateUnusedBySequence q = (val siphon q .== literal max) ==> sum (mval x $ statePostList pp q) .== 0

checkBounds :: Integer -> SIMap State -> SBool
checkBounds max = bAnd . map (\x -> x .>= 0 &&& x .<= literal max) . vals

checkReachabilityComplete :: ReachabilityProperty -> PopulationProtocol -> Integer -> SIMap State -> SIMap State -> SIMap State -> SIMap Transition -> SIMap Transition ->
        SIMap State -> SIMap State -> SIMap State -> SIMap State -> SIMap VarLabel -> SIMap VarLabel -> SBool
checkReachabilityComplete checkCorrectness pp max m0 m1 m2 x1 x2 r1 r2 s1 s2 qe qa =
        stateEquationConstraints pp m0 m1 x1 &&&
        stateEquationConstraints pp m0 m2 x2 &&&
        initialConfiguration pp m0 &&&
        nonNegativityConstraints m0 &&&
        nonNegativityConstraints m1 &&&
        nonNegativityConstraints m2 &&&
        nonNegativityConstraints x1 &&&
        nonNegativityConstraints x2 &&&
        terminalConstraints pp m1 &&&
        terminalConstraints pp m2 &&&
        differentConsensusConstraints checkCorrectness pp m0 m1 m2 qe qa &&&
        checkBounds max r1 &&&
        checkBounds max r2 &&&
        checkBounds max s1 &&&
        checkBounds max s2 &&&
        findMaximalUnmarkedTrap pp max x1 m1 r1 &&&
        findMaximalUnmarkedTrap pp max x2 m2 r2 &&&
        findMaximalUnmarkedSiphon pp max x1 m0 s1 &&&
        findMaximalUnmarkedSiphon pp max x2 m0 s2 &&&
        unmarkedBySequence pp max r1 x1 &&&
        unmarkedBySequence pp max r2 x2 &&&
        unusedBySequence pp max s1 x1 &&&
        unusedBySequence pp max s2 x2

checkReachabilityCompleteSat :: ReachabilityProperty -> PopulationProtocol -> ConstraintProblem Integer StrongConsensusCompleteCounterExample
checkReachabilityCompleteSat reachabilityProperty pp =
        let max = genericLength (states pp) + 1
            m0 = makeVarMapWith ("m0'"++) $ states pp
            m1 = makeVarMapWith ("m1'"++) $ states pp
            m2 = makeVarMapWith ("m2'"++) $ states pp
            x1 = makeVarMapWith ("x1'"++) $ transitions pp
            x2 = makeVarMapWith ("x2'"++) $ transitions pp
            r1 = makeVarMapWith ("r1'"++) $ states pp
            r2 = makeVarMapWith ("r2'"++) $ states pp
            s1 = makeVarMapWith ("s1'"++) $ states pp
            s2 = makeVarMapWith ("s2'"++) $ states pp
            qe = makeVarMapWith ("e'"++) $ quantifiedVariables (predicate pp)
            qa = makeVarMapWith ("a'"++) $ quantifiedVariables (predicate pp)
        in  ("strong consensus", "(m0, m1, m2, x1, x2, r1, r2, s1, s2)",
             concatMap getNames [m0, m1, m2, r1, r2, s1, s2] ++ concatMap getNames [x1, x2], getNames qe, getNames qa,
             \fm -> checkReachabilityComplete reachabilityProperty pp max (fmap fm m0) (fmap fm m1) (fmap fm m2) (fmap fm x1) (fmap fm x2)
                        (fmap fm r1) (fmap fm r2) (fmap fm s1) (fmap fm s2) (fmap fm qe) (fmap fm qa),
             \fm -> completeCounterExampleFromAssignment max (fmap fm m0) (fmap fm m1) (fmap fm m2) (fmap fm x1) (fmap fm x2) (fmap fm r1) (fmap fm r2) (fmap fm s1) (fmap fm s2))

completeCounterExampleFromAssignment :: Integer -> IMap State -> IMap State -> IMap State -> IMap Transition -> IMap Transition ->
        IMap State -> IMap State -> IMap State -> IMap State -> StrongConsensusCompleteCounterExample
completeCounterExampleFromAssignment max m0 m1 m2 x1 x2 r1 r2 s1 s2 =
            (configurationFromMap m0, configurationFromMap m1, configurationFromMap m2, configurationFromMap x1, configurationFromMap x2, configurationFromMap r1, configurationFromMap r2, configurationFromMap s1, configurationFromMap s2)
        where maximalSet q = M.keys $ M.filter (== max) q
