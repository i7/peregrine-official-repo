module Data.PopulationProtocol.Statistics
(gatherStatistics, PopulationProtocolStatistics(..), SimulationMode(..), ConvergenceValue(..), randomConfigurationOfSize, randomConfigurationInRange)
where
import qualified Data.PopulationProtocol as P
import qualified Data.Vector as V
import qualified Data.Set as S
import Data.PopulationProtocol.IO (lolaAnyStateCoverability)
import Data.PopulationProtocol.Util
import Data.PopulationProtocol.Verification (doesSatisfy)
import Data.PopulationProtocol.Presburger (evaluateFormula, QuantFormula(..))
import Data.Maybe (fromJust, isNothing, isJust)
import Data.List
import System.Random
import Debug.Trace

data SimulationMode = UniformSimulation
data ConvergenceValue = CorrectConsensus
                      | PossiblyCorrectConsensus
                      | NoConsensus
                      | PossiblyWrongConsensus
                      | WrongConsensus deriving (Eq, Enum, Show)


data PopulationProtocolStatistics = PopulationProtocolStatistics {
  c0 :: P.Configuration,
  c1 :: P.Configuration,
  convergenceTime :: Maybe Integer,
  convergenceValue :: ConvergenceValue
}


consensusValue :: P.PopulationProtocol -> P.Configuration -> Maybe Bool
consensusValue pp c = foldl1 foldMaybeBool opinions where
  opinions = [Just (S.member q (P.trueStates pp)) | q <- P.states pp, val c q > 0]
  foldMaybeBool (Just a) (Just b) = if a == b then Just a else Nothing
  foldMaybeBool _ _ = Nothing


hasStabilized :: P.PopulationProtocol -> P.Configuration -> Bool -> IO Bool
hasStabilized pp c increaseAccuracy = case consensusValue pp c of
  Nothing -> return False
  Just b ->
    if increaseAccuracy then
      not <$> lolaAnyStateCoverability pp c (P.statesOfOpinion pp (not b))
    else
      return $ null $ P.enabledTransitionsAtConfiguration pp c


makeStep :: P.PopulationProtocol -> P.Configuration -> SimulationMode -> IO (Maybe (P.Transition, P.Configuration))
makeStep pp c UniformSimulation = do
  let ts = P.enabledTransitionsAtConfiguration pp c
  case ts of
    [] -> return Nothing
    _ -> do
      let weightedTs = [(t, i) | t <- ts, let i = P.numInteractionsPerTransition pp c t]
      let numInteractions = sum $ map snd weightedTs
      index <- randomRIO (1, numInteractions)
      let t = fst $ fromJust $ find (\x -> snd x >= index) $ scanl (\(t1, i1) (t2, i2) -> (t2, i1+i2)) (undefined, 0) weightedTs
      return $ Just (t, P.successorConfiguration pp c t)


simulateMultipleSteps :: P.PopulationProtocol -> P.Configuration -> SimulationMode -> Integer -> IO [(P.Transition, P.Configuration)]
simulateMultipleSteps pp c mode n
  | n == 0 = return []
  | otherwise = do
      nextStep <- makeStep pp c mode
      case nextStep of
        Nothing -> return []
        (Just (t, c')) -> do
          xs <- simulateMultipleSteps pp c' mode (n-1)
          return $ (t, c'):xs


gatherStatistics :: P.PopulationProtocol -> P.Configuration -> SimulationMode -> Bool -> Integer -> IO PopulationProtocolStatistics
gatherStatistics pp c mode increaseAccuracy t = do
  let predicateValue = doesSatisfy pp c
  steps <- simulateMultipleSteps pp c mode t
  let (_, c') = if null steps then (undefined, c) else last steps
  s <- hasStabilized pp c' increaseAccuracy
  let val = consensusValue pp c'
  let numSteps = if isNothing val || not s then
                    Nothing
                  else
                    Just $ findEarliestLastingConsensus pp steps
  let convergenceValue = case val of
                          Nothing ->  NoConsensus
                          (Just x) -> (if s then definitelyBool else possiblyBool) (predicateValue == x)
  return PopulationProtocolStatistics { c0 = c
                                      , c1 = c'
                                      , convergenceTime = numSteps
                                      , convergenceValue = convergenceValue
                                      }


definitelyBool :: Bool -> ConvergenceValue
definitelyBool True =  CorrectConsensus
definitelyBool False = WrongConsensus


possiblyBool :: Bool -> ConvergenceValue
possiblyBool True = PossiblyCorrectConsensus
possiblyBool False = PossiblyWrongConsensus


findEarliestLastingConsensus :: P.PopulationProtocol -> [(P.Transition, P.Configuration)] -> Integer
findEarliestLastingConsensus pp steps = iter pp (zip (map snd steps) [1..]) 0

iter pp ((c, j):xs) i = case consensusValue pp c of Nothing -> iter pp xs j
                                                    (Just b) -> iter pp xs i
iter _ [] i =  i


randomDraw :: Int -> [a] -> IO [(a, Int)]
randomDraw _ [] = return []
randomDraw n [x] = return [(x, n)]
randomDraw n (x:xs) = do
  i <- randomRIO (0, n)
  ys <- randomDraw (n-i) xs
  return  $ (x, i):ys


randomConfigurationOfSize :: P.PopulationProtocol -> Int -> IO P.Configuration
randomConfigurationOfSize pp n = do
  conf <- P.configurationFromList (P.numOfStates pp) <$> randomDraw n (S.toList (P.initialStates pp))
  let leaderConf =  P.configurationFromList (P.numOfStates pp) (P.getLeaders pp)
  return (leaderConf |+| conf)


randomConfigurationInRange :: P.PopulationProtocol -> (Int, Int) -> IO P.Configuration
randomConfigurationInRange pp (a, b) = do
  x <- randomRIO (a, b)
  randomConfigurationOfSize pp x
