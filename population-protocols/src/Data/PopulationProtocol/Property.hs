{-# OPTIONS_GHC -fno-warn-name-shadowing #-}

module Data.PopulationProtocol.Property
    (Property(..),
     PropResult(..),
     resultAnd,
     resultOr,
     resultNot,
     resultsAnd,
     resultsOr)
where

data Property = LayeredTermination
              | StrongConsensus
              | StrongConsensusWithCorrectness

instance Show Property where
        show LayeredTermination = "layered termination"
        show StrongConsensus = "strong consensus"
        show StrongConsensusWithCorrectness = "strong consensus with correctness"

data PropResult = Satisfied | Unsatisfied | Unknown deriving (Eq)

instance Show PropResult where
        show Satisfied = "satisfied"
        show Unsatisfied = "not satisfied"
        show Unknown = "may not be satisfied"

resultAnd :: PropResult -> PropResult -> PropResult
resultAnd Satisfied x = x
resultAnd Unsatisfied _ = Unsatisfied
resultAnd _ Unsatisfied = Unsatisfied
resultAnd Unknown _ = Unknown

resultOr :: PropResult -> PropResult -> PropResult
resultOr Satisfied _ = Satisfied
resultOr _ Satisfied = Satisfied
resultOr Unsatisfied x = x
resultOr Unknown _ = Unknown

resultNot :: PropResult -> PropResult
resultNot Satisfied = Unsatisfied
resultNot Unsatisfied = Unsatisfied
resultNot Unknown = Unknown

resultsAnd :: [PropResult] -> PropResult
resultsAnd = foldr resultAnd Satisfied

resultsOr :: [PropResult] -> PropResult
resultsOr = foldr resultOr Unsatisfied
