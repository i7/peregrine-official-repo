{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}

module Data.PopulationProtocol.Util
    (IVector, Model, VarMap, SIMap, SRMap, SBMap, RMap, BMap, IMap,
    (|+|), (|*|), (|-|), makeVarMap, makeVarMapWith,
    MapLike(..), getNames, configurationFromMap, nonZeroElems)
where

import qualified Data.Vector.Unboxed as V
import qualified Data.Vector as BV
import qualified Data.Map.Strict as M
import Data.SBV

type IVector = V.Vector Int

type Model a = M.Map String a
type VarMap a = M.Map a String
type SIMap a = M.Map a SInteger
type SRMap a = M.Map a SReal
type SBMap a = M.Map a SBool
type IMap a = M.Map a Integer
type RMap a = M.Map a AlgReal
type BMap a = M.Map a Bool

(|+|) :: IVector -> IVector -> IVector
(|+|) = V.zipWith (+)

(|*|) :: Int -> IVector -> IVector
k |*| c = V.map (* k) c

(|-|) :: IVector  -> IVector -> IVector
(|-|) = V.zipWith (-)

items :: (V.Unbox a) => V.Vector a -> [(Int, a)]
items v = zip [0..V.length v - 1] (V.toList v)

class MapLike a key value  | a -> key, a -> value where
  val :: a -> key -> value
  vals :: a -> [value]
  mval :: a -> [key] -> [value]
  mval = map . val
  sumVal :: (Num value) => a -> value
  sumVal = sum . vals

instance (Num a, V.Unbox a) => MapLike (V.Vector a) Int a where
  val m q = if q >= V.length m then 0 else V.unsafeIndex m q
  vals = V.toList

instance (Num a) =>  MapLike (BV.Vector a) Int a where
  val m q = if q >= BV.length m then 0 else BV.unsafeIndex m q
  vals = BV.toList

instance (Show key, Show value, Ord key) => MapLike  (M.Map key value) key value where
  val m x  = M.findWithDefault (error ("key " ++ show x ++ " not found in " ++ show m)) x m
  vals = M.elems

makeVarMap :: (Show a, Ord a) => [a] -> VarMap a
makeVarMap = makeVarMapWith id

makeVarMapWith :: (Show a, Ord a) => (String -> String) -> [a] -> VarMap a
makeVarMapWith f xs = M.fromList $ xs `zip` map (f . show) xs

configurationFromMap :: IMap Int -> IVector
configurationFromMap = V.fromList . map fromInteger . M.elems

nonZeroElems :: IVector -> [Int]
nonZeroElems v = [q | q <- [0..(V.length v - 1)], v V.! q /= 0]

getNames :: VarMap a -> [String]
getNames = M.elems
