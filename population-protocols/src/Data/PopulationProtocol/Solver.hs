{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

module Data.PopulationProtocol.Solver
    (checkSat, checkSatMin, ConstraintProblem, MinConstraintProblem, VarLabel, Invariant(..))
where

import Data.SBV
import qualified Data.Map as M
import qualified Data.Text as T
import Data.PopulationProtocol.Util
import Data.PopulationProtocol.Options
import Data.PopulationProtocol.Presburger
import Control.Monad.IO.Class
import Control.Applicative

type VarLabel = T.Text

type ConstraintProblem a b =
        (String, String, [String], [String], [String], (String -> SBV a) -> SBool, (String -> a) -> b)

type MinConstraintProblem a b c =
        (Int -> c -> String, Maybe (Int, c) -> ConstraintProblem a (b, c))

class Invariant a where
        invariantSize :: a -> Int


rebuildModel :: SymWord a => [String] -> Either String (Bool, [a]) ->
        Maybe (Model a)
rebuildModel _ (Left _) = Nothing
rebuildModel _ (Right (True, _)) = error "Prover returned unknown"
rebuildModel vars (Right (False, m)) = Just $ M.fromList $ vars `zip` m

symConstraints :: SymWord a => [String] -> [String] -> [String] -> ((String -> SBV a) -> SBool) ->
        Symbolic SBool
symConstraints vars exVars allVars constraint = do
        syms <- mapM exists vars
        exSyms <- mapM exists exVars
        allSyms <- mapM forall allVars
        return $ constraint $ (M.!) $ M.fromList $ (vars `zip` syms) ++ (exVars `zip` exSyms) ++ (allVars `zip` allSyms)

getSolverConfig :: Bool -> Bool -> SMTConfig
getSolverConfig verbose auto =
        let tweaks = if auto then [] else ["(set-option :auto_config false)"]
        in  z3{ verbose=verbose, solverTweaks=tweaks }

checkSat :: (SatModel a, SymWord a, Show a, Show b) =>
        ConstraintProblem a b -> OptIO (Maybe b)
checkSat (problemName, resultName, vars, exVars, allVars, constraint, interpretation) = do
        verbosePut 2 $ "Checking SAT of " ++ problemName
        verbosity <- opt optVerbosity
        autoConf <- opt optSMTAuto
        result <- liftIO (satWith (getSolverConfig (verbosity >= 4) autoConf)
                    (symConstraints vars exVars allVars constraint))
        case rebuildModel vars (getModel result) of
            Nothing -> do
                verbosePut 2 "- unsat"
                return Nothing
            Just rawModel -> do
                verbosePut 2 "- sat"
                let model = interpretation $ val rawModel
                verbosePut 3 $ "- " ++ resultName ++ ": " ++ show model
                verbosePut 4 $ "- raw model: " ++ show rawModel
                return $ Just model

checkSatMin :: (SatModel a, SymWord a, Show a, Show b, Show c) =>
        MinConstraintProblem a b c -> OptIO (Maybe b)
checkSatMin (minMethod, minProblem) = do
        optMin <- opt optMinimizeRefinement
        r0 <- checkSat $ minProblem Nothing
        case r0 of
            Nothing -> return Nothing
            Just (result, curSize) ->
                if optMin > 0 then
                    Just <$> findSmaller optMin result curSize
                else
                    return $ Just result
    where findSmaller optMin result curSize = do
            verbosePut 2 $ "Checking for " ++ minMethod optMin curSize
            r1 <- checkSat $ minProblem (Just (optMin, curSize))
            case r1 of
                Nothing -> return result
                Just (result', curSize') -> findSmaller optMin result' curSize'
