{-# LANGUAGE DuplicateRecordFields #-}

module Data.PopulationProtocol.Verification where

import Data.PopulationProtocol.Property
import Data.PopulationProtocol
import Data.PopulationProtocol.Presburger
import Data.PopulationProtocol.Solver
import Data.PopulationProtocol.LayeredTermination
import Data.PopulationProtocol.StrongConsensus
import qualified Data.PopulationProtocol.IO as PIO
import System.IO
import Data.PopulationProtocol.Options
import Control.Monad.Reader
import Data.List (genericLength)
import Data.Aeson
import qualified Data.Text as T
import qualified Data.Text.Lazy.IO as T
import qualified Data.Text.Lazy.Encoding as T

type StrongConsensusRealCounterExample = (Configuration, Configuration, Configuration, [Transition], [Transition])

doesSatisfy :: PopulationProtocol -> Configuration -> Bool
doesSatisfy pp c = let (ExQuantFormula xs f) = predicate pp in
  evaluateFormula f $ stateToLabelMap pp c


printInvariant :: (Show a, Invariant a) => [a] -> OptIO ()
printInvariant inv = do
        verbosePut 0 "Invariant found"
        let invSize = map invariantSize inv
        verbosePut 2 $ "Number of atoms in invariants: " ++ show invSize ++
                " (total of " ++ show (sum invSize) ++ ")"
        mapM_ (liftIO . putStrLn . show) inv



checkReachability :: ReachabilityProperty -> PopulationProtocol -> OptIO (Maybe StrongConsensusRealCounterExample, RefinementObjects)
checkReachability reachabilityProperty pp = checkReachability' reachabilityProperty pp ([], [], [])


checkReachability' :: ReachabilityProperty -> PopulationProtocol -> RefinementObjects ->
        OptIO (Maybe StrongConsensusRealCounterExample, RefinementObjects)
checkReachability' reachabilityProperty pp refinements = do
        optRefine <- opt optRefinementType
        case optRefine of
            RefAll -> do
                r <- checkSat $ checkReachabilityCompleteSat reachabilityProperty pp
                case r of
                    Nothing -> return (Nothing, refinements)
                    Just (m0,m1,m2,x1,x2,_,_,_,_) -> return (Nothing, refinements)
            _ -> do
                r <- checkSat $ checkStrongConsensusSat reachabilityProperty pp refinements
                case r of
                    Nothing -> return (Nothing, refinements)
                    Just c -> do
                        case optRefine of
                                RefDefault ->
                                    let refinementMethods = [TrapRefinement, SiphonRefinement, UTrapRefinement, USiphonRefinement]
                                    in refineReachability reachabilityProperty pp refinementMethods refinements c
                                RefList refinementMethods ->
                                    refineReachability reachabilityProperty pp refinementMethods refinements c
                                RefAll -> return (Nothing, refinements)


refineReachability :: ReachabilityProperty -> PopulationProtocol -> [RefinementType] -> RefinementObjects ->
        StrongConsensusCounterExample ->
        OptIO (Maybe StrongConsensusRealCounterExample, RefinementObjects)
refineReachability reachabilityProperty pp [] refinements c = do
  let (c0, c1, c2, _, _ ) = c
  reachSeq <- liftIO $ sequence [y | c' <- [c1, c2],
                                     let y = do
                                             ts <- PIO.getReachSequence pp c0 c'
                                             return (c0, ts, c')
                                ]
  let nonReachConstraints = [(c0, c') | (c0, Nothing, c') <- reachSeq]
  case nonReachConstraints of
    [] -> do
      let [(_, Just t1, _), (_, Just t2, _)] = reachSeq
      let c' = (c0, c1, c2, t1, t2)
      return (Just c', refinements)
    xs -> do
      let (traps, siphons, nonreach) = refinements
      let refinements' = (traps, siphons, xs ++ nonreach)
      checkReachability' reachabilityProperty pp refinements'

refineReachability reachabilityProperty pp (ref:refs) refinements c = do
        let refinementMethod = case ref of
                         TrapRefinement -> findTrapConstraintsSat
                         SiphonRefinement -> findSiphonConstraintsSat
                         UTrapRefinement -> findUTrapConstraintsSat
                         USiphonRefinement -> findUSiphonConstraintsSat
        r <- checkSatMin $ refinementMethod pp c
        case r of
            Nothing -> do
                refineReachability reachabilityProperty pp refs refinements c
            Just refinement -> do
                let (utraps, usiphons, nonreach) = refinements
                let refinements' = case ref of
                         TrapRefinement -> (refinement:utraps, usiphons, nonreach)
                         SiphonRefinement -> (utraps, refinement:usiphons, nonreach)
                         UTrapRefinement -> (refinement:utraps, usiphons, nonreach)
                         USiphonRefinement -> (utraps, refinement:usiphons, nonreach)
                checkReachability' reachabilityProperty pp refinements'


checkLayeredTermination :: PopulationProtocol -> OptIO PropResult
checkLayeredTermination pp = do
        let nonTrivialTriplets = filter (not . trivialTriplet) $ generateTriplets pp
        checkLayeredTermination' pp nonTrivialTriplets 1 $ genericLength $ transitions pp


checkLayeredTermination' :: PopulationProtocol -> [Triplet] -> Integer -> Integer -> OptIO PropResult
checkLayeredTermination' pp triplets k kmax = do
        verbosePut 1 $ "Checking layered termination with at most " ++ show k ++ " layers"
        r <- checkSatMin $ checkLayeredTerminationSat pp triplets k
        case r of
            Nothing ->
                if k < kmax then
                    checkLayeredTermination' pp triplets (k + 1) kmax
                else
                    return Unknown
            Just inv -> do
                invariant <- opt optInvariant
                when invariant $ printInvariant inv
                return Satisfied
