{-# LANGUAGE OverloadedStrings, MultiParamTypeClasses, FlexibleInstances #-}

module Data.PopulationProtocol.Presburger
( Formula(..),
  OpLike(..),
  QuantFormula(..),
  quantifiedVariables,
  innerFormula,
  Op(..),
  Term(..),
  evaluateFormula,
  printFormula,
  printQuantFormula
)

where

import Data.PopulationProtocol.Util (IVector, val, MapLike)
import Data.SBV
import Data.Hashable
import qualified Data.Text as T

data Term a =
          Var a
        | Const Int
        | Minus (Term a)
        | Term a :+: Term a
        | Term a :-: Term a
        | Term a :*: Term a
        deriving (Eq, Ord, Show)


instance Functor Term where
        fmap f (Var x) = Var (f x)
        fmap _ (Const c) = Const c
        fmap f (Minus t) = Minus (fmap f t)
        fmap f (t :+: u) = fmap f t :+: fmap f u
        fmap f (t :-: u) = fmap f t :-: fmap f u
        fmap f (t :*: u) = fmap f t :*: fmap f u

data Op = Gt | Ge | Eq | Ne | Le | Lt | ModEq Int deriving (Eq, Ord, Show)

data QuantFormula a = ExQuantFormula [a] (Formula a)
             deriving (Eq, Ord, Show)

data Formula a =
          FTrue | FFalse
        | LinearInequation (Term a) Op (Term a)
        | Neg (Formula a)
        | Formula a :&: Formula a
        | Formula a :|: Formula a
             deriving (Eq, Ord, Show)

infixr 3 :&:
infixr 2 :|:

quantifiedVariables :: QuantFormula a -> [a]
quantifiedVariables (ExQuantFormula xs _) = xs

innerFormula :: QuantFormula a -> Formula a
innerFormula (ExQuantFormula _ p) = p

printTerm :: Term T.Text -> T.Text
printTerm (Var x) = T.concat["C[", x, "]"]
printTerm (Const c) = T.pack $ show c
printTerm (Minus t) = T.concat ["-", printTerm t]
printTerm (t :+: u) = T.concat ["(", printTerm t, " + ", printTerm u, ")"]
printTerm (t :-: u) = T.concat ["(", printTerm t, " - ", printTerm u, ")"]
printTerm (t :*: u) = T.concat [ printTerm t, " * ", printTerm u]

printOp :: Op -> T.Text
printOp Gt = ">"
printOp Ge = ">="
printOp Eq = "="
printOp Ne = "!="
printOp Le = "<="
printOp Lt = "<"
printOp (ModEq k) = T.concat["%=_", T.pack (show k)]

printQuantFormula :: (QuantFormula T.Text) -> T.Text
printQuantFormula (ExQuantFormula [] p) =  printFormula p
printQuantFormula (ExQuantFormula ps p) = T.concat ["EXISTS ", T.unwords ps,  ": ", printFormula p]

printFormula :: (Formula T.Text) -> T.Text
printFormula FTrue = "true"
printFormula FFalse = "false"
printFormula (LinearInequation lhs op rhs) = T.concat [printTerm lhs, " ", printOp op, " ", printTerm rhs]
printFormula (Neg p) = T.concat ["!", "(", printFormula p, ")"]
printFormula (p :&: q) = T.concat ["(", printFormula p, " && ", printFormula q, ")"]
printFormula (p :|: q) = T.concat["(", printFormula p, " || ", printFormula q, ")"]


instance Functor QuantFormula where
        fmap f (ExQuantFormula xs p) = ExQuantFormula (fmap f xs) (fmap f p)

instance Functor Formula where
        fmap _ FTrue = FTrue
        fmap _ FFalse = FFalse
        fmap f (LinearInequation lhs op rhs) =
                LinearInequation (fmap f lhs) op (fmap f rhs)
        fmap f (Neg p) = Neg (fmap f p)
        fmap f (p :&: q) = fmap f p :&: fmap f q
        fmap f (p :|: q) = fmap f p :|: fmap f q

evalTerm :: (Num val, MapLike a key val) =>  Term key -> a -> val
evalTerm (Var x) c = val c x
evalTerm (Const i) c = fromIntegral i
evalTerm (Minus t) c = -(evalTerm t c)
evalTerm (t1 :+: t2) c = evalTerm t1 c + evalTerm t2 c
evalTerm (t1 :-: t2) c = evalTerm t1 c - evalTerm t2 c
evalTerm (t1 :*: t2) c = evalTerm t1 c * evalTerm t2 c

class (Num a, Boolean b) => OpLike a b where
  (|>|)  :: a -> a -> b
  (|>=|) :: a -> a -> b
  (|==|) :: a -> a -> b
  (|/=|) :: a -> a -> b
  (|<=|) :: a -> a -> b
  (|<|)  :: a -> a -> b
  modEq :: a -> a -> a -> b

instance (Num a, OrdSymbolic a, SDivisible a) => OpLike a (SBV Bool) where
  (|>|)  = (.>)
  (|>=|) = (.>=)
  (|==|) = (.==)
  (|/=|) = (./=)
  (|<=|) = (.<=)
  (|<|)  = (.<)
  modEq k x y = sMod x k .== sMod y k

instance OpLike Int Bool where
  (|>|)  = (>)
  (|>=|) = (>=)
  (|==|) = (==)
  (|/=|) = (/=)
  (|<=|) = (<=)
  (|<|)  = (<)
  modEq k x y = x `mod` k == y `mod` k

opToOperator :: (OpLike a b) => Op -> (a -> a -> b)
opToOperator op = case op of
                  Gt      -> (|>|)
                  Ge      -> (|>=|)
                  Eq      -> (|==|)
                  Ne      -> (|/=|)
                  Le      -> (|<=|)
                  Lt      -> (|<|)
                  ModEq k -> modEq (fromIntegral k)

evaluateFormula :: (MapLike c key val, OpLike val b) => Formula key -> c -> b
evaluateFormula FTrue  _ = true
evaluateFormula FFalse  _ = false
evaluateFormula (LinearInequation lhs op rhs) c = (opToOperator op) (evalTerm lhs c) (evalTerm rhs c)
evaluateFormula (Neg p) c = bnot $ evaluateFormula p c
evaluateFormula (p1 :&: p2) c = (evaluateFormula p1 c) &&& (evaluateFormula p2 c)
evaluateFormula (p1 :|: p2) c = (evaluateFormula p1 c) ||| (evaluateFormula p2 c)
