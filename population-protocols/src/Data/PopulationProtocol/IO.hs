{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}

module Data.PopulationProtocol.IO
( parseJSON,
  lolaAnyStateCoverability,
  lolaFairInfiniteCover,
  configuration2JSON,
  getReachSequence,
  RecordPP (..),
  recordPP2PopulationProtocol,
  RecordTransition,
  SimpleLabelShape,
  populationProtocol2recordPP,
  exmp
)
where

import Data.PopulationProtocol.Configuration
import Data.Aeson
import Data.Aeson.TH
import Data.Data (Typeable(..), Data(..))
import Control.Applicative ((<*),(*>),(<$>))
import Data.Functor.Identity
import Text.Parsec
import qualified Data.Set as S
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Text as T
import Text.Parsec.Expr
import Text.Parsec.Language (LanguageDef, emptyDef)
import qualified Text.Parsec.Token as Token
import Data.PopulationProtocol(PopulationProtocol, makePopulationProtocol, State, Transition, Configuration)
import qualified Data.PopulationProtocol as P
import Data.PopulationProtocol.Presburger
import Data.List (group, groupBy, sort)
import Data.Maybe (isJust)
import Data.Function (on)
import Data.PopulationProtocol.Util (val)
import qualified Data.Map.Strict as M
import qualified Data.Vector.Unboxed as V
import Data.List (intercalate)
import System.Process

type Parser a = Parsec String () a


languageDef :: LanguageDef ()
languageDef =
        emptyDef {
                 Token.commentStart    = "/*",
                 Token.commentEnd      = "*/",
                 Token.commentLine     = "//",
                 Token.identStart      = alphaNum <|> letter <|> char '_' <|> char '-' <|> char '(' <|> char ')' <|> char ',' <|> char ' ',
                 Token.identLetter     = alphaNum <|> letter <|> char '_' <|> char '-' <|> char '(' <|> char ')' <|> char ',' <|> char ' ',
                 Token.reservedNames   = ["true", "false", "EXISTS", "FORALL"],
                 Token.reservedOpNames = ["->", "<", "<=", "=", "!=", ">=", ">", "%=_",
                                          "+", "-", "*", "&&", "||", "!", ":"]
                 }

lexer :: Token.TokenParser ()
lexer = Token.makeTokenParser languageDef

identifier :: Parser String
identifier = Token.identifier lexer -- parses an identifier
stringLiteral :: Parser String
stringLiteral = Token.stringLiteral lexer -- parses a string literal
reserved :: String -> Parser ()
reserved   = Token.reserved   lexer -- parses a reserved name
reservedOp :: String -> Parser ()
reservedOp = Token.reservedOp lexer -- parses an operator
brackets :: Parser a -> Parser a
brackets   = Token.brackets   lexer -- parses p surrounded by brackets
braces :: Parser a -> Parser a
braces     = Token.braces     lexer -- parses p surrounded by braces
parens :: Parser a -> Parser a
parens     = Token.parens     lexer -- parses p surrounded by parenthesis
natural :: Parser Integer
natural    = Token.integer    lexer -- parses a natural number
integer :: Parser Integer
integer    = Token.integer    lexer -- parses an integer
comma :: Parser String
comma      = Token.comma       lexer -- parses a comma
whiteSpace :: Parser ()
whiteSpace = Token.whiteSpace lexer -- parses whitespace

safeUnlines :: [String] -> String
safeUnlines = unlines . map (++ "\r")

optionalCommaSep :: Parser a -> Parser [a]
optionalCommaSep p = many (p <* optional comma)

singleOrList :: Parser a -> Parser [a]
singleOrList p = braces (optionalCommaSep p) <|> (:[]) <$> p

numberOption :: Parser Integer
numberOption = option 1 (brackets natural)

ident :: Parser String
ident = (char 'C' *> brackets (identifier <|> stringLiteral)) <?> "identifier"

identList :: Parser [String]
identList = singleOrList ident

binary :: String -> (a -> a -> a) -> Assoc -> Operator String () Identity a
binary name fun = Infix  ( reservedOp name *> return fun )
prefix :: String -> (a -> a) -> Operator String () Identity a
prefix name fun = Prefix ( reservedOp name *> return fun )

termOperatorTable :: [[Operator String () Identity (Term String)]]
termOperatorTable =
        [ [ prefix "-" Minus ]
        , [ binary "*" (:*:) AssocLeft ]
        , [ binary "+" (:+:) AssocLeft, binary "-" (:-:) AssocLeft ]
        ]

termAtom :: Parser (Term String)
termAtom =  (Var <$> ident)
        <|> (Const . fromInteger <$> integer)
        <|> parens term
        <?> "basic term"

term :: Parser (Term String)
term = buildExpressionParser termOperatorTable termAtom <?> "term"

parseOp :: Parser Op
parseOp = (reservedOp "<" *> return Lt) <|>
          (reservedOp "<=" *> return Le) <|>
          (reservedOp "=" *> return Eq) <|>
          (reservedOp "!=" *> return Ne) <|>
          (reservedOp ">" *> return Gt) <|>
          (reservedOp ">=" *> return Ge) <|>
          (reservedOp "%=_" *> ((ModEq . fromInteger) <$> integer))


linIneq :: Parser (Formula String)
linIneq = do
        lhs <- term
        op <- parseOp
        rhs <- term
        return (LinearInequation lhs op rhs)

formOperatorTable :: [[Operator String () Identity (Formula String)]]
formOperatorTable =
        [ [ prefix "!" Neg ]
        , [ binary "&&" (:&:) AssocRight ]
        , [ binary "||" (:|:) AssocRight ]
        ]

formAtom :: Parser (Formula String)
formAtom =  try linIneq
        <|> (reserved "true" *> return FTrue)
        <|> (reserved "false" *> return FFalse)
        <|> parens formula
        <?> "basic formula"

formula :: Parser (Formula String)
formula = buildExpressionParser formOperatorTable formAtom <?> "formula"

quantFormula :: Parser (QuantFormula String)
quantFormula =
        (do
            reserved "EXISTS"
            xs <- optionalCommaSep ident
            reservedOp ":"
            p <- formula
            return (ExQuantFormula xs p)
        )
          <|>
        (ExQuantFormula [] <$> formula)

instance FromJSON (QuantFormula P.StateLabel) where
          parseJSON (String v) = do
                  let formula = parse quantFormula "" (T.unpack v)
                  case formula of
                    Left e -> fail ("Predicate formula not well-formed: " ++ show e)
                    Right r -> return (T.pack <$> r)
          parseJSON _ = fail "Expect string for predicate."

instance ToJSON (QuantFormula P.StateLabel) where
        toJSON phi = String $ printQuantFormula phi

instance FromJSON (Formula P.StateLabel) where
          parseJSON (String v) = do
                  let f = parse formula "" (T.unpack v)
                  case f of
                    Left e -> fail ("Predicate formula not well-formed: " ++ show e)
                    Right r -> return (T.pack <$> r)
          parseJSON _ = fail "Expect string for formula."

instance ToJSON (Formula P.StateLabel) where
        toJSON phi = String $ printFormula phi

class Succ a where
  successor :: a -> a

data SimpleLabelShape = Numb Integer -- (possibly signed) number
                      | RightNumbered String Integer -- e. "A2"
                      | LeftNumbered Integer String -- e.g. "2A"
                      | Indexed String Integer -- eg. "A_1"
                      | Misc String -- else
                        deriving (Eq, Ord, Show, Typeable, Data)

instance Succ SimpleLabelShape where
  successor (Numb i) = Numb (i+1)
  successor (RightNumbered str i) = RightNumbered str (i+1)
  successor (LeftNumbered i str) = LeftNumbered (i+1) str
  successor (Indexed str i) = Indexed str (i+1)
  successor (Misc str) = Misc str -- identical

data SeparatorType = Parantheses
                   | Brackets
                   | Braces
                    deriving (Eq, Ord, Show, Typeable, Data)

data ComposedLabelShape = ComposedLabelShape SeparatorType [ComposedLabelShape]
                        | BottomShape SimpleLabelShape
                          deriving (Eq, Ord, Show, Typeable, Data)

type Var = String
type LowerBound = String
type UpperBound = String
type UpperLowerBound = (Var, LowerBound, UpperBound)

-- liefert letzte Variable [(ComposedLabelShape, [UpperLowerBound], Var)]
-- f xs = let ps = permutations [0..length xs - 1] in

parseSimpleLabelNumber = Numb <$> integer

parseSimpleLabelRightNumbered = do
  leftStr <- stringLiteral
  rightNum <- integer
  return $ RightNumbered leftStr rightNum


parseSimpleLabelLeftNumbered = do
  leftNum <- integer
  rightStr <- stringLiteral
  return $ LeftNumbered leftNum rightStr


parseSimpleLabelIndexed = do
  leftPart <- stringLiteral
  char '_'
  rightPart <- integer
  return $ Indexed leftPart rightPart


parseSimpleLabelMisc = Misc <$> stringLiteral


parseSimpleLabelShape :: Parser SimpleLabelShape
parseSimpleLabelShape =   parseSimpleLabelNumber
                      <|> parseSimpleLabelLeftNumbered
                      <|> parseSimpleLabelRightNumbered
                      <|> parseSimpleLabelIndexed
                      <|> parseSimpleLabelMisc

parseComposedLabelShape :: Parser ComposedLabelShape
parseComposedLabelShape = let p = parseComposedLabelShape `sepBy` (char ',') in
  (ComposedLabelShape Brackets <$> (brackets p))
  <|> (ComposedLabelShape Parantheses <$> (parens p))
  <|> (ComposedLabelShape Braces <$> (braces p))
  <|> (BottomShape <$> parseSimpleLabelShape)


exmp = groupBy recursiveConstrCmp $ sort [ComposedLabelShape Brackets [BottomShape (Numb 1), BottomShape (Numb 2) ],
                                          ComposedLabelShape Brackets [BottomShape (Numb 5), BottomShape (Numb 7) ],
                                          ComposedLabelShape Brackets [BottomShape (Misc "5"), BottomShape (Numb 7) ],
                                          ComposedLabelShape Brackets [ComposedLabelShape Brackets []],
                                          ComposedLabelShape Parantheses []]


recursiveConstrCmp (ComposedLabelShape a xs) (ComposedLabelShape b ys) =
    a == b && length xs == length ys && all (\(a,b) -> recursiveConstrCmp a b) (zip xs ys)
recursiveConstrCmp (BottomShape a) (BottomShape b) = toConstr a == toConstr b
recursiveConstrCmp _ _ = False


-- toVarMap :: ComposedLabelShape
--          -> Int
--          -> (Int, Int, M.Map Int SimpleLabelShape)
-- toVarMap shape startVal
-- splitIntoConsecutiveRange :: (Succ a, Eq a) => [a] -> [[a]]
-- splitIntoConsecutiveRange = foldr update [] where
--   update new (xs:ys) = case xs of
--     (x:xs') -> if x == successor new then (new:x:xs'):ys else [new]:(x:xs'):ys
--     _ -> [[new]]
--   update new _ = [[new]]


data RecordTransition = RecordTransition {
        name :: T.Text,
        pre :: [T.Text],
        post :: [T.Text]
} deriving (Ord, Eq, Show)

data StateStyle = StateStyle{
        size :: Maybe Double,
        shape :: Maybe String,
        color :: Maybe String,
        fillcolor :: Maybe String
} deriving (Ord, Eq, Show)

data RecordPP = RecordPP {
        title :: T.Text,
        states :: [P.StateLabel],
        transitions :: [RecordTransition],
        initialStates :: [P.StateLabel],
        trueStates :: [P.StateLabel],
        predicate :: Maybe (QuantFormula P.StateLabel),
        leaders :: Maybe (M.Map P.StateLabel Int),
        statesStyle :: Maybe (M.Map P.StateLabel StateStyle),
        precondition ::Maybe (QuantFormula P.StateLabel),
        postcondition ::Maybe (QuantFormula P.StateLabel),
        description :: Maybe T.Text
} deriving (Ord, Eq, Show)

$(deriveJSON defaultOptions {omitNothingFields  = True} ''StateStyle)
$(deriveJSON defaultOptions {omitNothingFields  = True} ''RecordTransition)
$(deriveJSON defaultOptions {omitNothingFields  = True} ''RecordPP)


populationProtocol2recordPP :: PopulationProtocol -> RecordPP
populationProtocol2recordPP pp = RecordPP { title = P.name pp,
                                            states = [P.getStateLabel pp x | x <- P.states pp],
                                            transitions = [RecordTransition { name = P.getTransitionLabel pp t,
                                                                              pre = concat [replicate i (P.getStateLabel pp q) | (q, i)  <- P.transitionPre pp t],
                                                                              post = concat [replicate i (P.getStateLabel pp q) | (q, i)  <- P.transitionPost pp t]
                                                                            }
                                                            | t <- P.transitions pp],
                                            initialStates = [P.getStateLabel pp x | x <- S.toList (P.initialStates pp)],
                                            trueStates = [P.getStateLabel pp x | x <- S.toList (P.trueStates pp)],
                                            predicate = Just (P.predicate pp),
                                            leaders = P.leaders pp,
                                            statesStyle = Nothing,
                                            precondition = P.precondition pp,
                                            postcondition = P.postcondition pp,
                                            description = (P.descr pp)
                                          }

unique :: (Ord a) => [a] -> [a]
unique = map head . group . sort


recordPP2PopulationProtocol :: RecordPP -> PopulationProtocol
recordPP2PopulationProtocol r =
  makePopulationProtocol (title r) (states r) (map name (transitions r)) (initialStates r) (trueStates r) p (precondition r) (postcondition r) (leaders r) arcs (description r) where
        arcs = [(count (pre t), name t, count (post t)) |  t <- unique (transitions r)]
        count = map (\xs -> (head xs, length xs)) . group . sort
        p = case predicate r of Nothing -> ExQuantFormula [] FTrue
                                (Just p') -> p'


labelListToConfiguration :: PopulationProtocol -> [P.StateLabel] -> Configuration
labelListToConfiguration pp ls = V.fromList [length [l | l <- ls,  l == P.getStateLabel pp q] | q <- P.states pp]


instance FromJSON PopulationProtocol where
  parseJSON = fmap recordPP2PopulationProtocol . parseJSON


instance ToJSON PopulationProtocol where
  toJSON = toJSON . populationProtocol2recordPP


-- | Represents pair (population protocol, configuration) as Petri net in Lola syntax.
markedPP2Lola :: PopulationProtocol -> Configuration -> [String]
markedPP2Lola pp c = [ showPlaces,
                       showMarking c,
                       safeUnlines [showTransition t | t <- P.transitions pp]
                     ]
                      where
   showPlaces = "PLACE " ++ intercalate "," (("s" ++) . show <$> P.states pp) ++ ";"
   showMarking m = "MARKING " ++ intercalate "," ["s" ++ show s ++ ": " ++ show i | (s,i) <-  P.items m] ++ ";"
   showTransition t = "TRANSITION " ++ "t" ++ show t ++ "\r\n" ++
                      "CONSUME " ++ intercalate "," ["s" ++ show s ++ ": " ++ show i | (s, i) <- P.transitionPre pp t] ++ ";\r\n" ++
                      "PRODUCE " ++ intercalate "," ["s" ++ show s ++ ": " ++ show i | (s, i) <- P.transitionPost pp t] ++ ";\r\n"


-- | Represents configuration in Lola syntax.
marking2Lola :: Configuration -> String
marking2Lola c =
    lolaPred $ P.items c
  where
    lolaPred [] = "TRUE"
    lolaPred qs = "(" ++ intercalate " AND " [ "s" ++ show s ++ " = " ++ show i | (s, i) <- qs ] ++ ")"

-- | Object to parse JSON output from Lola
data LolaObject = LolaObject {
  result :: Bool,
  path :: Maybe [String]
} deriving Show

instance FromJSON LolaObject where
  parseJSON (Object o) = do
    analysis <- o .: "analysis"
    result <- analysis .: "result"
    path <- o .: "path"
    return LolaObject { result = result, path = path }
  parseJSON _ = fail "expected Lola object"

lolaCall :: PopulationProtocol -> Configuration -> String -> IO LolaObject
lolaCall pp c p = do
    str <- readCreateProcess (shell request) petriNet
    case decode (BS.pack str) of
        Nothing -> ioError (userError "invalid return result by Lola")
        Just lolaObject -> return lolaObject
  where
    request = lola ++ " --quiet --json=/dev/stdout --jsoninclude=path -f  \"" ++ p ++ "\""
    petriNet = safeUnlines $ markedPP2Lola pp c

-- | Calls Lola to obtain whether the predicate holds at some fixed configuration.
lolaPredicateHolds :: PopulationProtocol -> Configuration -> String -> IO Bool
lolaPredicateHolds pp c p = result <$> lolaCall pp c p

-- | Calls Lola to obtain whether, starting from some fixed configuration, a configuration
-- | satisfying p is reachable.
-- | Returns (IO Nothing) in case no such sequence exists.
lolaReachSequence :: PopulationProtocol -> Configuration -> String -> IO (Maybe [Transition])
lolaReachSequence pp c p = do
    lolaObject <- lolaCall pp c $ "REACHABLE " ++ p
    case (result lolaObject, path lolaObject) of
      (False, _) -> return Nothing
      (True, Nothing) -> return $ Just []
      (True, Just path) -> return $ Just $ map (read . tail) path

-- | Transforms a list of states qs to the predicate
--   "Is at least one state from qs present in a given configuration?" in Lola syntax.
anyStateToLolaPredicate :: [P.State] -> String
anyStateToLolaPredicate [] = "FALSE"
anyStateToLolaPredicate qs = "(" ++ intercalate " OR " ["s" ++ show q ++ " > 0 " | q <- qs] ++ ")"

-- | Calls Lola to compute whether, starting from some fixed configuration, a configuration
-- | is reachable that contains a state from a given list of states.
lolaAnyStateCoverability :: P.PopulationProtocol -> P.Configuration -> [P.State] -> IO Bool
lolaAnyStateCoverability pp c qs = isJust <$> (lolaReachSequence pp c $ anyStateToLolaPredicate qs)

-- | Calls Lola to obtain transition sequence through which one configuration is reachable from another.
-- | Returns (IO Nothing) in case no such sequence exists.
getReachSequence :: PopulationProtocol -> Configuration -> Configuration -> IO (Maybe [Transition])
getReachSequence pp c c' = lolaReachSequence pp c $ marking2Lola c'

-- | Calls Lola to check if in any fair sequence from the given configuration,
-- | infinitely many configurations contain a state from the given list of states.
-- | Returns (IO Nothing) in case no such sequence and configuration exists.
lolaFairInfiniteCover :: PopulationProtocol -> Configuration -> [P.State] -> IO Bool
lolaFairInfiniteCover pp c qs =
    lolaPredicateHolds pp c $ "(AG (EF " ++ anyStateToLolaPredicate qs ++ "))"

-- | Represents configuration as JSON object which stores multiplicities as counting vector.
configuration2JSON :: PopulationProtocol -> Configuration -> Value
configuration2JSON pp c = object [(P.getStateLabel pp q) .= i | (q, i) <- zip (P.states pp) (V.toList c)]
