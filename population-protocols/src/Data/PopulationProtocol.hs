{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-name-shadowing #-}

module Data.PopulationProtocol
where

import Math.Combinatorics.Exact.Binomial
import qualified Data.Bimap as BM
import qualified Data.Set as S
import Control.Arrow (first,(***))
import Data.List (sort,(\\))
import Data.Tuple (swap)
import Debug.Trace

import Data.PopulationProtocol.Util
import Data.PopulationProtocol.Presburger
import qualified Data.Text as T
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector as V
import qualified Data.IntMap as IM
import qualified Data.Map.Strict as M

type State = Int
type Transition = Int

type StateLabel = T.Text
type TransitionLabel = T.Text

type Configuration = IVector
type FlowVector = IVector
type Trap = [State]
type Siphon = [State]

data PopulationProtocol = PopulationProtocol {
          name :: T.Text
        , numOfStates :: Int -- ^ number of states
        , numOfTransitions :: Int -- ^ number of transitions
        , stateLabels :: BM.Bimap State StateLabel -- ^ bimap between states indices [0..(numOfStates - 1)] and state labels that serve as unique identifiers
        , transitionLabels :: BM.Bimap Transition TransitionLabel -- ^ bimap between transition indices [0..(numOfTransitions - 1)] and transition labels that serve as unique identifiers
        , initialStates :: S.Set State -- ^ set of initial states
        , trueStates :: S.Set State -- ^ set of states that are of opinion 'true'
        , predicate :: QuantFormula StateLabel -- ^ predicate computed by protocol
        , leaders :: Maybe (M.Map StateLabel Int)
        , precondition :: Maybe (QuantFormula StateLabel) -- ^ precondition for initial configuration that must be satisfied for the protocol to stably compute correct value
        , postcondition :: Maybe (QuantFormula StateLabel) -- ^ postcondition for terminal configuration
        , adjacencyQ :: V.Vector (IM.IntMap Int, IM.IntMap Int) -- ^ vector of size numOfStates; first component stores pre-transitions, second component stores post-transitions
        , adjacencyT :: V.Vector ([(State, Int)], [(State, Int)]) -- ^ vector of size numOfTransitions; first component stores pre-states, second component stores post-states
        , descr :: Maybe T.Text
} deriving (Show)


-- | Returns opinion (true/false) of a given state in a protocol.
opinion :: PopulationProtocol -> State -> Bool
opinion pp q = S.member q (trueStates pp)


-- | Returns a protocol's set of states of opinion false.
falseStates :: PopulationProtocol -> S.Set State
falseStates pp = S.fromList [q | q <- states pp, not (opinion pp q)]


-- | Returns a protocol's set of states of opinion b.
statesOfOpinion :: PopulationProtocol -> Bool -> [State]
statesOfOpinion pp b = S.toList ops where
  ops = if b then trueStates pp else falseStates pp


-- | Returns multiplicities of states consumed by a transition.
transitionPre :: PopulationProtocol -> Transition -> [(State, Int)]
transitionPre pp t = fst (V.unsafeIndex (adjacencyT pp) t)


-- | Returns multiplicities of states coming out of a transition.
transitionPost :: PopulationProtocol -> Transition -> [(State, Int)]
transitionPost pp t = snd (adjacencyT pp V.! t)


-- | Returns list of states consumed by a transition.
transitionPreStates :: PopulationProtocol -> Transition -> [State]
transitionPreStates  pp t = map fst $ transitionPre pp t


-- | Returns list of states coming out of a transition.
transitionPostStates :: PopulationProtocol -> Transition -> [State]
transitionPostStates pp t = map fst $ transitionPost pp t


-- | Returns list of transitions that have a given state in their preset
statePre :: PopulationProtocol -> State -> [(Transition, Int)]
statePre pp q = IM.toList $ fst (adjacencyQ pp V.! q)


-- | Returns list of transitions that have a given state in their postset
statePost :: PopulationProtocol -> State -> [(Transition, Int)]
statePost pp q = IM.toList $ snd (adjacencyQ pp V.! q)


-- | Returns list of transitions that have a given state among their input.
statePostList :: PopulationProtocol -> State -> [Transition]
statePostList pp q = map fst $ statePost pp q


-- | Returns list of transitions that have a given state among their output.
statePreList :: PopulationProtocol -> State -> [Transition]
statePreList pp q = map fst $ statePre pp q


-- | Shortcut to (unsafely) yield a state label for a given state index from [0..(numOfStates - 1)].
getStateLabel :: PopulationProtocol -> State -> StateLabel
getStateLabel pp q = stateLabels pp BM.! q


-- | Shortcut to (unsafely) yield a state index for a given state label.
getStateFromLabel :: PopulationProtocol -> StateLabel -> State
getStateFromLabel pp l = stateLabels pp BM.!> l


-- | Returns list of state indices.
states :: PopulationProtocol -> [State]
states pp = [0..(numOfStates pp - 1)]


-- | Returns list of transition indices.
transitions :: PopulationProtocol -> [Transition]
transitions pp = [0..(numOfTransitions pp - 1)]


-- | Yields list of initial states.
getInitialStates :: PopulationProtocol -> [State]
getInitialStates = S.toList . initialStates

-- | Yields counting vector of leaders.
getLeaders :: PopulationProtocol -> [(State, Int)]
getLeaders pp = [(q, i) | q <- (states pp), let i = numOfLeadersForState pp q]

-- | Yields number of leaders for a given state
numOfLeadersForState :: PopulationProtocol -> State -> Int
numOfLeadersForState pp q = case (leaders pp) of
    Nothing -> 0
    (Just m) -> M.findWithDefault 0 (getStateLabel pp q) m

-- | Yields list of states that may not belong to an initial configuration.
getNonInitialStates :: PopulationProtocol -> [State]
getNonInitialStates pp = [q | q <- states pp, not (S.member q (initialStates pp))]


-- | Returns list of states that are of opinion 'true'.
getTrueStates :: PopulationProtocol -> [State]
getTrueStates = S.toList  . trueStates


-- | Returns list of states that are of opinion 'false'.
getFalseStates :: PopulationProtocol -> [State]
getFalseStates = S.toList . falseStates


-- | Looks up transition label (unsafely) for a given transition index.
getTransitionLabel :: PopulationProtocol -> Transition -> TransitionLabel
getTransitionLabel pp t = transitionLabels pp BM.! t


-- | Looks up (unsafely) transition index for a given transition label.
getTransitionFromLabel :: PopulationProtocol -> TransitionLabel -> Transition
getTransitionFromLabel pp l = transitionLabels pp BM.!> l


-- | Creates population protocol from its constituent components.
makePopulationProtocol :: T.Text -> [StateLabel] -> [TransitionLabel] ->
        [StateLabel] -> [StateLabel] -> QuantFormula StateLabel ->
        Maybe (QuantFormula StateLabel) ->
        Maybe (QuantFormula StateLabel) ->
        Maybe (M.Map StateLabel Int) ->
        [([(StateLabel, Int)], TransitionLabel, [(StateLabel, Int)])] ->
        Maybe T.Text ->
        PopulationProtocol
makePopulationProtocol name states transitions initialStates trueStates  predicate precondition postcondition leaders arcs descr =
            PopulationProtocol {
                name = name,
                numOfStates = n,
                numOfTransitions = m,
                stateLabels = qLabels,
                transitionLabels = tLabels,
                initialStates = S.fromList [qLabels BM.!> q | q <- initialStates],
                trueStates = S.fromList [qLabels BM.!> q | q <- trueStates],
                predicate = predicate,
                precondition = precondition,
                postcondition = postcondition,
                leaders = leaders,
                adjacencyQ = adjQ,
                adjacencyT = V.map (\(x, y) -> (IM.toList x, IM.toList y)) adjT,
                descr = descr
            }
        where
            n = length states
            m = length transitions
            qLabels = BM.fromList $ zip [0..(n-1)] states
            tLabels = BM.fromList $ zip [0..(m-1)] transitions
            adjT = foldl insertArcIntoAdjT (V.replicate m (IM.empty, IM.empty)) (map arcFromLabels arcs)
            adjQ = V.fromList [(presQ q, postsQ q) | q <- [0..(n-1)]]
            arcFromLabels :: ([(StateLabel, Int)], TransitionLabel, [(StateLabel, Int)]) -> ([(State, Int)], Transition, [(State, Int)])
            arcFromLabels (xs, l, ys) = ( [(qLabels BM.!> a, b) | (a, b) <- xs] ,
                                          (tLabels BM.!> l),
                                          [(qLabels BM.!> a, b) | (a, b) <- ys]
                                        )
            insertArcIntoAdjT adjT (pre, t, post) = adjT V.// [(t, (IM.fromList pre, IM.fromList post))]
            presQ q = IM.fromList [(t, k) | t <- [0..(m-1)], let k = IM.findWithDefault 0 q (snd (adjT V.! t)), k /= 0]
            postsQ q = IM.fromList [(t, k) | t <- [0..(m-1)], let k = IM.findWithDefault 0 q (fst (adjT V.! t)), k /= 0]


-- | Applies transition to a configuration.
successorConfiguration :: PopulationProtocol -> Configuration -> Transition -> Configuration
successorConfiguration pp c t =  foldl (\c' f -> f c') c updates where
  changes = transitionPost pp t ++ [(q, -i) | (q, i) <- transitionPre pp t]
  updates = [\v -> U.unsafeUpd v [(q, (val v q) + i)] | (q, i) <- changes]


-- | Computes whether some transition is enabled at a given configuration.
isTransitionEnabledAtConfiguration :: PopulationProtocol -> Configuration -> Transition -> Bool
isTransitionEnabledAtConfiguration pp c t = and [val c q >= i | (q, i) <- transitionPre pp t]


-- | Returns list of transitions enabled at a given configuration.
enabledTransitionsAtConfiguration :: PopulationProtocol -> Configuration -> [Transition]
enabledTransitionsAtConfiguration pp c = [t | t <- transitions pp
                                            , isTransitionEnabledAtConfiguration pp c t]


-- | ?
numInteractionsPerTransition :: PopulationProtocol -> Configuration -> Transition -> Int
numInteractionsPerTransition pp c t = product [(if k == 1 then n else n^2) | (q, k) <- transitionPre pp t, let n = val c q]


-- | Represents configuration as counting vector of the form [(State, Int)].
items :: Configuration -> [(State, Int)]
items c = [(q, i) | (q, i) <- zip [0..(U.length c)] (U.toList c), i > 0]


-- | Turns list representation of counting vector into configuration.
configurationFromList :: Int -> [(State, Int)] -> Configuration
configurationFromList m insertions = (U.replicate m 0) U.// insertions


-- | ?
stateToLabelMap :: (MapLike c State n) => PopulationProtocol -> c -> M.Map StateLabel n
stateToLabelMap pp c = M.fromList [(getStateLabel pp q, i) | q <- states pp, let i = val c q]


-- | Reverses directions of transitions in a given protocol.
invertPopulationProtocol :: PopulationProtocol -> PopulationProtocol
invertPopulationProtocol pp = pp { adjacencyQ = V.map swap (adjacencyQ pp),
                                   adjacencyT = V.map swap (adjacencyT pp)
                                 }

-- | Explodes configuration to list of states preserving number of occurences of each state.
explode :: Configuration -> [State]
explode c = concatMap (\(x, y) -> replicate y x) $ items c

-- | Converts configuration to dictionary with StateLabel keys and number of occurences as values
toCountingVector :: PopulationProtocol -> Configuration -> M.Map StateLabel Int
toCountingVector pp c = M.fromList [(getStateLabel pp q, i) | (q, i) <- zip (states pp) (U.toList c)]
