import Distribution.Simple
import Distribution.System
import Distribution.Simple.UserHooks
import System.Process
import System.Directory

main = defaultMainWithHooks $ simpleUserHooks { preConf = \x y -> checkDependencies >> preConf simpleUserHooks x y}


checkDependencies :: IO ()
checkDependencies = do
    putStrLn "Dependency check skipped"
  --putStrLn "Checking dependencies..."
  --checkLolaInstalled
  --checkNpmInstalled
  --checkPylintInstalled
  --checkPythonInstalled
  --checkWebpackInstalled
  --checkZ3Installed
  --putStrLn "All dependencies satisfied."


checkPythonInstalled :: IO ()
checkPythonInstalled = return ()


checkLolaInstalled :: IO ()
checkLolaInstalled = do
  exec <- findExecutable "lola"
  case exec of
    (Just _) -> return ()
    Nothing -> error "Lola (http://service-technology.org/lola) is not installed on your machine. Abort."


checkWebpackInstalled :: IO ()
checkWebpackInstalled = do
  exec <- findExecutable "webpack"
  case exec of
    (Just _) -> return ()
    Nothing -> error "Webpack is not globally installed on your machine. Abort."
  return ()

checkNpmInstalled :: IO ()
checkNpmInstalled = do
  exec <- findExecutable "npm"
  case exec of
    (Just _) -> return ()
    Nothing -> error "Npm is not globally installed on your machine. Abort."
  return ()

checkPylintInstalled :: IO ()
checkPylintInstalled = return ()

checkZ3Installed :: IO ()
checkZ3Installed = do
  exec <- findExecutable "z3"
  case exec of
    (Just _) -> return ()
    Nothing -> error "Z3 is not globally installed on your machine. Abort."
  return ()
