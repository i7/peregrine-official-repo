{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Lib

import Data.Text.Lazy.Encoding (encodeUtf8)
import Data.Text.Lazy (pack)
import Network.HTTP.Types.Status (ok200)
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Servant.API
import Servant.Docs
import Servant.JS

import qualified Data.ByteString.Lazy as B
import qualified Data.Text as T
import qualified Data.Text.IO as T

type DocsAPI = PeregrineAPI :<|> Raw

peregrineAPI :: Proxy PeregrineAPI
peregrineAPI = Proxy

api :: Proxy DocsAPI
api = Proxy

apiDocs :: API
apiDocs = docs peregrineAPI

docsBS :: B.ByteString
docsBS = encodeUtf8 . pack . markdown $ docsWithIntros [intro] peregrineAPI
  where intro = DocIntro "Welcome" ["This is the Peregrine Web API.", "Enjoy!"]

apiJS3 :: T.Text
apiJS3 = jsForAPI peregrineAPI $ axios defAxiosOptions

server :: Server DocsAPI
server = backend :<|> serveDirectoryFileServer "../peregrine-frontend/build/" where --Tagged serveDocs :<|>
    serveDocs _ respond =
        respond $ responseLBS ok200 [plain] docsBS
    plain = ("Content-Type", "text/plain")

main :: IO ()
main = do
  putStrLn "Peregrine server started..."
  run 3001 $ serve api server --T.writeFile "/tmp/result.js" apiJS3
