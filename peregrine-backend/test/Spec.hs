{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec
import Data.Maybe (fromMaybe)
import Actions
import Data.PopulationProtocol
import Data.PopulationProtocol.IO (recordPP2PopulationProtocol)
import Control.Monad (forM_)
import qualified Data.Text as T
import qualified Data.Map.Strict as M

correctProtocolNames :: [FilePath]
correctProtocolNames = [
  "broadcast.py",
  "flock_basic.py",
  "flock_log.py",
  "flock_tower.py",
  "majority_basic.py",
  "remainder.py",
  "threshold.py"
  ]


incorrectProtocolNames :: [FilePath]
incorrectProtocolNames = [
  "majority_basic_broken.py",
  "flock_log_broken.py"
  ]


getProtocolFromFilename :: FilePath -> IO PopulationProtocol
getProtocolFromFilename name = do
  (Just params) <- getProtocolParametersFromFileName name
  (Just protocol) <- getProtocol name params
  return $ recordPP2PopulationProtocol protocol


main :: IO ()
main = hspec $ do
  correctBenchmark <- runIO $ sequence $ fmap getProtocolFromFilename correctProtocolNames
  incorrectBenchmark <- runIO $ sequence $ fmap getProtocolFromFilename incorrectProtocolNames
  let new = head correctBenchmark
  describe "Verify correct protocols (without stage graph)" $ do
    forM_ correctBenchmark $ \protocol -> do
      reply <- runIO $ verifyProtocol protocol
      it (T.unpack (name protocol)) $
        result reply `shouldBe` (Just Verified)
  describe "Verify correct protocols (with stage graph)" $ do
    forM_ correctBenchmark $ \protocol -> do
      reply <- runIO $ verifyStageGraph protocol
      it (T.unpack (name protocol)) $
        result reply `shouldBe` (Just Verified)
  describe "Disprove incorrect protocols (without stage graph)" $ do
    forM_ incorrectBenchmark $ \protocol -> do
      reply <- runIO $ verifyProtocol protocol
      it (T.unpack (name protocol)) $
        result reply `shouldBe` (Just Disproven)
  describe "Disprove correct protocols (with stage graph)" $ do
    forM_ incorrectBenchmark $ \protocol -> do
      reply <- runIO $ verifyStageGraph protocol
      it (T.unpack (name protocol)) $
        result reply `shouldBe` (Just Disproven)
  describe "Test creation/editing/deletion of protocol" $ do
    fName <- runIO $ saveProtocol "No source" "Test protocol" "testProtocol"
    runIO $ writeTitle fName "test-title"
    titles <- runIO getTitles
    let t = M.findWithDefault ("error":: T.Text) fName titles
    it "Title editing" $ do
      t `shouldBe` "test-title"
    runIO $ deleteProtocol fName
    newTitles <- runIO getTitles
    it "Delete protocol" $ do
      M.member fName newTitles `shouldBe` False
