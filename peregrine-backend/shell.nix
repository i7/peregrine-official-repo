let
  nixpkgsPinned = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/a7ceb2536ab11973c59750c4c48994e3064a75fa.tar.gz";
    sha256 = "0hka65f31njqpq7i07l22z5rs7lkdfcl4pbqlmlsvnysb74ynyg1";
  };
  pkgs = import nixpkgsPinned {
    overlays = [
      (final: previous: {
        lola = final.callPackage ../vm/lola.nix {};
      })
    ];
  };
in
with pkgs;
let
  # Important: we currently need a nixpkgs version that still contains ghc8.2.2.
  # This holds true for nixos.19.09 but no longer for 20.03.
  # As of writing, the ghc822 from mpickering's nur does not work: yields error
  #     Undefined symbol: major
  ghc = haskell.compiler.ghc822; # Keep in sync with the GHC version defined by stack.yaml!
in
  haskell.lib.buildStackProject {
    inherit ghc;
    name = "myEnv";
    # System dependencies used only at build-time go in here.
    nativeBuildInputs = [
    ];
    # System dependencies used at run-time go in here.
    buildInputs = [
      gmp
      zlib
      lola
      (python3.withPackages (ps: with ps; [
        z3
        lark-parser
        pylint
      ]))
    ];
  }
