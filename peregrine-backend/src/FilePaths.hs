module FilePaths where

import Data.List (find)
import Data.Maybe (fromJust)
import System.Directory (doesFileExist, listDirectory, removeFile, renameFile)
import System.Environment (getEnv)
import System.IO.Error (tryIOError)
import System.FilePath.Posix (splitExtensions, splitFileName)

execProtocolPath :: IO FilePath
execProtocolPath = return " ../python/execprotocol.py"


peregrine2Path :: IO FilePath
peregrine2Path = return " ../peregrine2.0/src/main.py"


protocolDir :: IO FilePath
protocolDir = return "../protocols/python/"


protocolTitlePath :: IO FilePath
protocolTitlePath = return "../protocols/titles.conf"

efficientRandomSamplesPath :: IO FilePath
efficientRandomSamplesPath = return "../efficientrandomsamples/dist/EfficientRandomSamples.jar"


protocolPathFromFileName :: FilePath -> IO String
protocolPathFromFileName fileName = do
  dir <- protocolDir
  return $ dir ++ fileName


-- | Returns value of environment variable, if it is set, otherwise default value is returned
getEnvWithDefault :: String -> String -> IO String
getEnvWithDefault var alternative = do
  e <- tryIOError $ getEnv var
  case e of
    (Left _) -> return alternative
    (Right x) -> return x


-- | Returns path to python 3.* executable defined in environment variable PEREGRINE_PYTHON.
--   If PEREGRINE_PYTHON is not set, then "python" is assumed as default value.
getPythonExecutablePath :: IO FilePath
getPythonExecutablePath = getEnvWithDefault "PEREGRINE_PYTHON" "python"

-- | Returns path to python 3.* executable defined in environment variable PEREGRINE_PYTHON3.
--   If PEREGRINE_PYTHON3 is not set, then "python3" is assumed as default value.
getPython3ExecutablePath :: IO FilePath
getPython3ExecutablePath = getEnvWithDefault "PEREGRINE_PYTHON3" "python3"


-- | Returns path to pylint executable defined in environment variable PEREGRINE_PYLINT.
--   If PEREGRINE_PYLINTis not set, then "pylint" is assumed as default value.
getPylintExecutablePath :: IO FilePath
getPylintExecutablePath = getEnvWithDefault "PEREGRINE_PYLINT" "pylint"

getJavaExecutablePath:: IO FilePath
getJavaExecutablePath = return "java"


getUniqueFileName :: FilePath -> IO FilePath
getUniqueFileName path = do
  b <- doesFileExist path
  if b then
    do
      let (dir, fileName) = splitFileName path
      let (name, ext) = splitExtensions fileName
      fst <$> fromJust <$> find (not . snd) <$> sequence [existPair (dir ++ name ++ show i ++ ext) | i <- [2..10000]]
  else
    return path


existPair :: FilePath -> IO (FilePath, Bool)
existPair path = do
  b <- doesFileExist path
  return (path, b)
