{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DuplicateRecordFields #-}

module Lib
    ( PeregrineAPI
    , backend
    ) where

import Control.Monad.IO.Class (liftIO)
import Data.List (intercalate)

import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.Map.Strict as M
import Servant
import GHC.Generics
import Data.Aeson
import Data.PopulationProtocol
import Data.PopulationProtocol.IO
import Data.PopulationProtocol.Presburger
import Servant.Docs

import Actions


type SimulationReqResult = Maybe [SimulationResult]


data SimulationReqBody = SimulationReqBody { sampleSize :: Int
                                           , minConfigSize :: Int
                                           , maxConfigSize :: Int
                                           , numSteps :: Integer
                                           , increaseAccuracy :: Bool
                                           , schedulingMode :: String
                                           , protocol :: PopulationProtocol
                                           } deriving (Generic)

data PredicateEvalReqBody = PredicateEvalReqBody {
  predicate :: Formula StateLabel,
  configuration :: M.Map StateLabel Int
} deriving (Generic)

data DeleteReqResult = SuccessfulDelete | DeleteFailure deriving (Generic)

data EditReqResult = SuccessfulEdit | EditFailure deriving (Generic)

type SaveReqResult = FilePath

data LintReqBody = LintReqBody {sourceCode :: T.Text} deriving (Generic)

data SaveReqBody = SaveReqBody { sourceCode :: T.Text
                               , title :: T.Text
                               , fileName :: FilePath
                               } deriving (Generic)

type EditReqBody = SaveReqBody

instance FromJSON SimulationReqBody;
instance ToJSON SimulationReqBody;

instance FromJSON PredicateEvalReqBody;
instance ToJSON PredicateEvalReqBody;

instance FromJSON DeleteReqResult;
instance ToJSON DeleteReqResult;

instance FromJSON EditReqResult;
instance ToJSON EditReqResult;

instance FromJSON LinterErrorMessage;
instance ToJSON LinterErrorMessage;

instance FromJSON LintReqBody;
instance ToJSON LintReqBody;

instance FromJSON SaveReqBody;
instance ToJSON SaveReqBody;

type PeregrineAPI = "params" :> QueryParam "fileName" FilePath :> Get '[JSON] ProtocolParameters
               :<|> "protocols" :> Get '[JSON] [ProtocolItem]
               :<|> "delete" :> QueryParam "fileName" FilePath :> Delete '[JSON] DeleteReqResult
               :<|> "edit" :> ReqBody '[JSON] EditReqBody :> Put '[JSON] EditReqResult
               :<|> "lint" :> ReqBody '[JSON] LintReqBody :> Put '[JSON] LinterResult
               :<|> "save" :> ReqBody '[JSON] SaveReqBody :> Post '[JSON] SaveReqResult
               :<|> "protocol" :> QueryParam "fileName" FilePath :> ReqBody '[JSON] ProtocolParameters :> Post '[JSON] RecordPP
               :<|> "simulation" :> ReqBody '[JSON] SimulationReqBody :>  Post '[JSON] SimulationReqResult
               :<|> "evaluate-predicate" :> ReqBody '[JSON] PredicateEvalReqBody :> Post '[JSON] Bool
               :<|> "verification" :> ReqBody '[JSON] PopulationProtocol :> Post '[JSON] VerificationReply
               :<|> "verification-stage-graphs" :> ReqBody '[JSON] PopulationProtocol :> Post '[JSON] VerificationReply
               :<|> "pre-post-verification" :> ReqBody '[JSON] PopulationProtocol :> Post '[JSON] VerificationReply

instance ToParam (QueryParam "fileName" String) where
  toParam _ =
    DocQueryParam "name"                     -- name
                  ["protocol1.py", "my_protocol.py"] -- example of values (not necessarily exhaustive)
                  "File name of source code file." -- description
                  Normal -- Normal, List or Flag

instance ToSample ProtocolParameters where
  toSamples _ = singleSample ProtocolParameters { scheme = Just (M.fromList [("threshold", ParamInt Param { values = [1, 2, 3]
                                                                                                    , defaultValue = (Just 2)
                                                                                                    , value = Nothing
                                                                                                    , descr = "This param does [...]"
                                                                                                    })])
                                                }

instance ToSample DeleteReqResult where
  toSamples _ = singleSample SuccessfulDelete


instance ToSample SaveReqBody where
  toSamples _ = singleSample SaveReqBody { sourceCode = "print \"Hello World \""
                                         , title = "My Protocol Title"
                                         , fileName = "myfile.py"
                                         }

instance ToSample EditReqResult where
  toSamples _ = singleSample SuccessfulEdit


instance ToSample LintReqBody where
  toSamples _ = singleSample LintReqBody {sourceCode = "print \"Hello World \""}

instance ToSample LinterErrorMessage where
  toSamples _ = singleSample LinterErrorMessage { line = 10
                                                , column = 5
                                                , evidence = "x = "
                                                , reason = "parse error"
                                                }

instance ToSample PredicateEvalReqBody where
  toSamples _ = singleSample PredicateEvalReqBody {
    predicate = FTrue,
    configuration = M.fromList [("Y", 3), ("X", 2)]
  }

instance ToSample Char where
  toSamples _ = singleSample 'A'

exampleProtocol :: PopulationProtocol
exampleProtocol = makePopulationProtocol "Example" ["a", "b"] ["t"] ["a"] ["b"] (ExQuantFormula [] FTrue) Nothing Nothing Nothing [([("a", 2)], "t", [("b", 2)])] Nothing

instance ToSample PopulationProtocol where
  toSamples _ = singleSample $ exampleProtocol

instance ToSample RecordPP where
  toSamples _ = singleSample $ populationProtocol2recordPP exampleProtocol

instance ToSample SimulationReqBody where
  toSamples _ = singleSample $ SimulationReqBody { sampleSize = 1000
                                                 , minConfigSize = 5
                                                 , maxConfigSize = 10
                                                 , numSteps = 10000
                                                 , increaseAccuracy = True
                                                 , schedulingMode = "UniformAgents"
                                                 , protocol = exampleProtocol
                                                 }

instance ToSample SimulationResult where
  toSamples _ = singleSample $ SimulationResult { startConfig = M.fromList [("q1", 3)]
                                                , endConfig = M.fromList [("q2", 3)]
                                                , steps = 2000
                                                , size = 3
                                                , convergence = 1
                                                }

instance ToSample VerificationReply where
  toSamples _ = singleSample $  VerificationReply { status = Actions.Success
                                                  , result = Just Verified
                                                  , counterExample = Nothing
                                                  , stageGraph = Nothing
                                                  }
instance ToSample ProtocolItem where
  toSamples _ = singleSample $ ProtocolItem { fileName = "myprotocol.py"
                                            , sourceCode = "print \"Hello World \""
                                            , title = "Title of My Protocol"
                                            }
backend :: Server PeregrineAPI
backend = paramsRequestHandler
     :<|> protocolItemsRequestHandler
     :<|> deleteRequestHandler
     :<|> editRequestHandler
     :<|> lintRequestHandler
     :<|> saveRequestHandler
     :<|> protocolRequestHandler
     :<|> simulationRequestHandler
     :<|> evaluatePredicateRequestHandler
     :<|> verificationRequestHandler
     :<|> stageGraphVerificationRequestHandler
     :<|> prePostVerificationRequestHandler


paramsRequestHandler :: Maybe FilePath -> Handler ProtocolParameters
paramsRequestHandler maybeName =
  case maybeName of
    Just name -> do
      r <- liftIO $ getProtocolParametersFromFileName name
      case r of
        (Just params) -> return params
        _             -> throwError err400 { errBody = B.pack ("Returning parameters failed for filename " ++ name) }
    Nothing -> nonOptionalError ["'name'"]


protocolItemsRequestHandler :: Handler [ProtocolItem]
protocolItemsRequestHandler = liftIO getProtocols


protocolRequestHandler :: Maybe FilePath -> ProtocolParameters -> Handler RecordPP
protocolRequestHandler maybeName params =
  case maybeName of
    (Just name) -> do
      r <- liftIO $ getProtocol name params
      case r of
        (Just pp) -> return pp
        Nothing   -> throwError err400 { errBody = B.pack "Returning protocol failed." }
    _ -> nonOptionalError ["'name'"]


simulationRequestHandler :: SimulationReqBody -> Handler SimulationReqResult
simulationRequestHandler req = liftIO $ simulateProtocol (protocol req) (sampleSize req) (minConfigSize req) (maxConfigSize req) (numSteps req) (increaseAccuracy req) (schedulingMode req)

evaluatePredicateRequestHandler :: PredicateEvalReqBody -> Handler Bool
evaluatePredicateRequestHandler req = return $ evaluateFormula (predicate (req :: PredicateEvalReqBody)) (configuration (req :: PredicateEvalReqBody))

verificationRequestHandler :: PopulationProtocol -> Handler VerificationReply
verificationRequestHandler = liftIO . verifyProtocol

stageGraphVerificationRequestHandler :: PopulationProtocol -> Handler VerificationReply
stageGraphVerificationRequestHandler  = liftIO . verifyStageGraph

prePostVerificationRequestHandler :: PopulationProtocol -> Handler VerificationReply
prePostVerificationRequestHandler = liftIO . prePostVerifyProtocol

deleteRequestHandler :: Maybe FilePath -> Handler DeleteReqResult
deleteRequestHandler maybeName =
  case maybeName of
    Just name -> do
      liftIO $ deleteProtocol name
      return SuccessfulDelete
    Nothing -> nonOptionalError ["'fileName'"]


editRequestHandler :: EditReqBody -> Handler EditReqResult
editRequestHandler editReqBody = do
  liftIO $ editProtocol (sourceCode (editReqBody :: EditReqBody)) (title (editReqBody :: EditReqBody)) (fileName (editReqBody :: EditReqBody))
  return SuccessfulEdit


lintRequestHandler :: LintReqBody -> Handler LinterResult
lintRequestHandler lintReqBody = do
  r <- liftIO $ lintCode $ sourceCode (lintReqBody :: LintReqBody)
  case r of
    (Just linterResult) -> return linterResult
    Nothing             -> return []


saveRequestHandler :: SaveReqBody -> Handler SaveReqResult
saveRequestHandler saveReqBody = liftIO $ saveProtocol (sourceCode (saveReqBody :: SaveReqBody)) (title (saveReqBody :: SaveReqBody)) (fileName (saveReqBody :: SaveReqBody))


nonOptionalError xs = throwError err400 {errBody = B.pack msg} where
  msg = if length xs == 1 then
          "Field " ++ head xs ++ " is not optional."
        else
          "Fields " ++ intercalate " and " xs ++ " are not optional."
