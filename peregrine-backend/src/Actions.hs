{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Actions where

import Control.Applicative ((<|>))
import Control.Monad (replicateM)
import Control.Monad.Reader (runReaderT)
import Data.Aeson ( Array
                  , FromJSON (..)
                  , Object
                  , ToJSON (..)
                  , Value (..)
                  , decode
                  , eitherDecode
                  , encode
                  , object
                  , (.:)
                  , (.=)
                  )
import Data.Aeson.TH
import Data.Maybe (fromJust, fromMaybe, isJust, catMaybes, listToMaybe)
import Data.PopulationProtocol ( PopulationProtocol
                               , StateLabel
                               , TransitionLabel
                               , Configuration
                               , numOfStates
                               , getStateFromLabel
                               , configurationFromList
                               , getStateLabel
                               , getTransitionLabel
                               , toCountingVector
                               , statesOfOpinion
                               )
import Data.PopulationProtocol.IO ( RecordPP
                                  , RecordTransition
                                  , getReachSequence
                                  , lolaFairInfiniteCover
                                  )
import Data.PopulationProtocol.Options (startOptions)
import Data.PopulationProtocol.Property
import Data.PopulationProtocol.StrongConsensus (ReachabilityProperty(..))
import Data.PopulationProtocol.Util (sumVal)
import Data.PopulationProtocol.Verification
import Data.Scientific (isFloating, toBoundedInteger, toRealFloat)
import GHC.Generics
import System.Directory (listDirectory, removeFile)
import System.FilePath.Posix (takeFileName)
import System.IO.Error (tryIOError)
import System.IO.Temp (getCanonicalTemporaryDirectory, writeTempFile)
import System.Process (readCreateProcess, shell)
import System.Timeout (timeout)

import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.HashMap.Strict as HM
import qualified Data.Map.Strict as M
import qualified Data.Vector as V
import qualified Data.PopulationProtocol.Statistics as S
import qualified System.IO.Strict as SIO

import FilePaths

data LinterErrorMessage = LinterErrorMessage { line :: Int
                                             , column :: Int
                                             , evidence :: T.Text
                                             , reason :: T.Text
                                             } deriving (Generic)

type LinterResult = [LinterErrorMessage]

data ProtocolItem = ProtocolItem { fileName :: FilePath
                                 , sourceCode :: T.Text
                                 , title :: T.Text
                                 } deriving (Generic)

data RequestStatus = Timeout
                   | Success
                   | Error deriving (Show)

data VerificationResult = Unsure
                        | Verified
                        | Disproven deriving (Ord, Eq, Show)

data CounterExample = CounterExample { startConfig :: CountingVector
                                     , endConfig :: CountingVector
                                     , t  :: [TransitionLabel]
                                     , expectedConsensusValue :: Bool
                                     } deriving (Show, Generic)

data Param a = Param { values :: [a]
                     , defaultValue :: Maybe a
                     , value :: Maybe a
                     , descr :: T.Text
                     } deriving (Show)


data ParamType = ParamBool (Param Bool)
               | ParamDouble (Param Double)
               | ParamInt (Param Int)
               | ParamText (Param T.Text)
                 deriving (Show)


data ProtocolParameters = ProtocolParameters { scheme :: Maybe (M.Map String ParamType)
                                             } deriving (Show, Generic)


type CountingVector = M.Map StateLabel Int

data SimulationResult = SimulationResult { startConfig :: CountingVector
                                         , endConfig :: CountingVector
                                         , steps :: Integer
                                         , size :: Int
                                         , convergence :: Int
                                         } deriving (Generic)


data StateValuePair = StateValuePair { state :: T.Text
                                     , value :: Int
                                     } deriving (Show, Generic)

type StageConstraint = [[StateValuePair]]
type StageCertificate = [StateValuePair]
type StageConfiguration = M.Map T.Text Int

data Stage = Stage {
  id :: T.Text,
  failed :: Bool,
  speed :: Maybe String,
  parent :: Maybe String,
  constraint :: Maybe StageConstraint,
  certificate :: Maybe StageCertificate,
  potReachFromConfiguration :: M.Map T.Text Int,
  configuration :: M.Map T.Text Int,
  reachSequence :: Maybe [TransitionLabel],
  configurationUnstable :: Maybe Bool,
  checkedProperty :: Maybe String,
  deadTransitions :: Maybe [RecordTransition],
  eventuallyDeadTransitions :: Maybe [RecordTransition]
} deriving (Show, Generic)

data StageGraph = StageGraph { stages :: [Stage]
                             , speed :: T.Text
                             } deriving (Show, Generic)

data Peregrine2Response = Peregrine2Response { graph :: StageGraph
                                             , verified :: Bool
} deriving (Show, Generic)

data VerificationReply = VerificationReply { status :: RequestStatus
                                           , result :: Maybe VerificationResult
                                           , counterExample :: Maybe CounterExample
                                           , stageGraph :: Maybe StageGraph
                                           } deriving (Generic)

$(deriveJSON defaultOptions { sumEncoding = UntaggedValue } ''ParamType)
$(deriveJSON defaultOptions { sumEncoding = UntaggedValue } ''Param)

instance FromJSON StateValuePair
instance ToJSON StateValuePair

instance FromJSON CounterExample
instance ToJSON CounterExample

instance FromJSON Stage
instance ToJSON Stage

instance FromJSON ProtocolItem
instance ToJSON ProtocolItem

instance FromJSON StageGraph
instance ToJSON StageGraph

instance FromJSON Peregrine2Response
instance ToJSON Peregrine2Response

instance ToJSON VerificationReply where
  toJSON verificationReply = object $ statusAttribute ++ resultAttribute ++ counterExampleAttribute ++ speedAttribute where
    statusAttribute = ["status" .= show (status verificationReply)]
    resultAttribute = case result verificationReply of
                        Nothing -> []
                        Just verificationResult -> ["result" .= show verificationResult]
    counterExampleAttribute = case counterExample verificationReply of
                                Nothing -> []
                                (Just ex) -> ["counterExample" .= toJSON ex]
    speedAttribute = case stageGraph verificationReply of
                        Nothing -> []
                        Just x -> ["stageGraph" .= toJSON x]

instance FromJSON ProtocolParameters;
instance ToJSON ProtocolParameters;

instance FromJSON SimulationResult;
instance ToJSON SimulationResult;

timeoutVal :: Int
timeoutVal = 50*6000000 -- set timeout of request to 5 minutes

getTitles :: IO (M.Map FilePath T.Text)
getTitles = do
  titleFile <- protocolTitlePath
  e <- tryIOError $ SIO.readFile titleFile
  case e of
    (Left _) -> return M.empty
    (Right content) -> return $ M.fromList $ read content


getProtocolParametersFromFileName :: FilePath -> IO (Maybe ProtocolParameters)
getProtocolParametersFromFileName fileName = do
  python <- getPythonExecutablePath
  execProtocol <- execProtocolPath
  protocolPath <- protocolPathFromFileName fileName
  let query = python ++ " " ++ execProtocol ++ " \"" ++ protocolPath ++ "\""
  result <- tryIOError $ readCreateProcess (shell query) ""
  case result of
    (Right x) -> return $ decode (B.pack x)
    (Left _) -> return Nothing


getProtocol :: FilePath -> ProtocolParameters -> IO (Maybe RecordPP)
getProtocol fileName parameters = do
  python <- getPythonExecutablePath
  execProtocol <- execProtocolPath
  protocolPath <- protocolPathFromFileName fileName
  let params = show (B.unpack (encode parameters))
  let query = python ++ " " ++ execProtocol ++ " " ++ protocolPath ++ " " ++ params
  result <- tryIOError $ readCreateProcess (shell query) ""
  case result of
    (Right x) -> return $ decode (B.pack x)
    (Left _) -> return Nothing


getProtocols :: IO ([ProtocolItem])
getProtocols = do
  protocolFiles <- getProtocolFiles
  protocolFileNames <- getProtocolFileNames
  codes <- mapM TIO.readFile protocolFiles
  titles <- getTitles
  return [ProtocolItem { fileName = f
                       , sourceCode = c
                       , title = M.findWithDefault (T.pack f) f titles
                       } | (f, c) <- zip protocolFileNames codes]


saveProtocol :: T.Text -> T.Text -> FilePath -> IO FilePath
saveProtocol sourceCode title fileName = do
  protocolPath <- protocolPathFromFileName fileName
  f <- getUniqueFileName protocolPath
  TIO.writeFile f sourceCode
  writeTitle f title
  return $ takeFileName f


writeTitle :: FilePath -> T.Text -> IO ()
writeTitle filePath title = do
  titles <- getTitles
  let newTitles = M.insert filePath title titles
  titleFile <- protocolTitlePath
  TIO.writeFile titleFile $ T.pack $ show $ M.toList newTitles


simulateProtocol :: PopulationProtocol -> Int -> Int -> Int -> Integer -> Bool -> String -> IO (Maybe [SimulationResult])
simulateProtocol pp sampleSize minConfigSize maxConfigSize numSteps increaseAccuracy schedulingMode = do
  java <- getJavaExecutablePath
  jar <- efficientRandomSamplesPath
  let paramJson = object [
        "protocol" .= pp,
        "sampleSize" .= sampleSize,
        "minConfigSize" .= minConfigSize,
        "maxConfigSize" .= maxConfigSize,
        "numSteps" .= numSteps,
        "increaseAccuracy" .= increaseAccuracy,
        "schedulingMode" .= schedulingMode ]
  let params = show (B.unpack (encode paramJson ))
  tmpDir <- getCanonicalTemporaryDirectory
  fileName <- writeTempFile tmpDir "request" $ params
  let query = java ++ " -jar " ++ jar ++ " " ++ fileName
  result <- tryIOError $ readCreateProcess (shell query) ""
  case result of
    (Right x) -> return $ decode (B.pack x)
    (Left _) -> return Nothing


editProtocol ::  T.Text -> T.Text -> FilePath -> IO ()
editProtocol sourceCode title fileName = do
  writeTitle fileName title
  protocolPath <- protocolPathFromFileName fileName
  TIO.writeFile protocolPath sourceCode


deleteProtocol :: FilePath -> IO ()
deleteProtocol fileName = do
  protocolPath <- protocolPathFromFileName fileName
  removeFile protocolPath


lintCode :: T.Text -> IO (Maybe LinterResult)
lintCode sourceCode = do
  tmpDir <- getCanonicalTemporaryDirectory
  fileName <- writeTempFile tmpDir "lint" $ T.unpack sourceCode
  pylint <- getPylintExecutablePath
  let query = pylint ++ " --additional-builtins=Utils " ++ fileName ++ " --output-format=json  2>! || true"
  result <- tryIOError $ readCreateProcess (shell query) ""
  case result of
    (Right x) -> return $ simplifyLinterOutput <$> decode (B.pack x)
    (Left _) -> return Nothing


simplifyLinterOutput :: Value -> LinterResult
simplifyLinterOutput (Array a) = map toLinterErrorMessage $ filterError $ V.toList a
simplifyLinterOutput _ = []

isErrorValue (Object o) = HM.lookupDefault (String "") ("type" :: T.Text) o == String "error"
isErrorValue _ = False
filterError = filter isErrorValue
toLinterErrorMessage (Object o) = LinterErrorMessage { line = valToInt (o HM.! "line")
                                                     , column = valToInt (o HM.! "column")
                                                     , evidence = valToText (o HM.! "obj")
                                                     , reason = valToText (o HM.! "message")
                                                     }
toLinterErrorMessage _ = error "Wrong format"


verifyTerminalProperty :: ReachabilityProperty -> PopulationProtocol -> IO VerificationReply
verifyTerminalProperty reachabilityProperty pp = do
  strongConsensusResult <- timeout timeoutVal $ runReaderT (checkReachability reachabilityProperty pp) startOptions
  case strongConsensusResult of
    Just (Just (c0, c1, c2, ts1, ts2), _) -> do
      let s = case reachabilityProperty of
                Correctness -> doesSatisfy pp c1
                _           -> True
      let (c, ts) = if s then (c1, ts1) else (c2, ts2)
      let ex =  CounterExample { startConfig = toCountingVector pp c0
                               , endConfig = toCountingVector pp c
                               , t =  map (getTransitionLabel pp) ts
                               , expectedConsensusValue = doesSatisfy pp c0
                               }
      return VerificationReply { status = Success
                               , result = Just  Disproven
                               , counterExample = Just ex
                               , stageGraph = Nothing
                               }
    Just _ ->  return VerificationReply { status = Success
                                        , result = Just Verified
                                        , counterExample = Nothing
                                        , stageGraph = Nothing
                                        }
    Nothing -> return VerificationReply { status = Timeout
                                        , result = Nothing
                                        , counterExample = Nothing
                                        , stageGraph = Nothing
                                        }


verifyLayeredTermination :: PopulationProtocol -> IO VerificationReply
verifyLayeredTermination pp = do
  layeredTerminationResult <- timeout timeoutVal $ runReaderT (checkLayeredTermination pp) startOptions
  case layeredTerminationResult of
    Nothing         -> return VerificationReply { status = Timeout
                                                , result = Nothing
                                                , counterExample = Nothing
                                                , stageGraph = Nothing
                                                }
    (Just Satisfied) -> return VerificationReply { status = Success
                                                 , result = Just Verified
                                                 , counterExample = Nothing
                                                 , stageGraph = Nothing
                                                 }
    _                -> return VerificationReply { status = Success
                                                 , result = Just Unsure
                                                 , counterExample = Nothing
                                                 , stageGraph = Nothing
                                                 }


chainVerificationRequest :: (PopulationProtocol -> IO VerificationReply)
                         -> (PopulationProtocol -> IO VerificationReply)
                         -> (PopulationProtocol -> IO VerificationReply)
chainVerificationRequest r1 r2 pp = do
  res1 <- r1 pp
  case status res1 of
    Timeout -> return res1
    Success -> case result res1 of
      (Just Verified) -> r2 pp
      _               -> return res1


verifyProtocol :: PopulationProtocol -> IO VerificationReply
verifyProtocol = chainVerificationRequest (verifyTerminalProperty Correctness) verifyLayeredTermination

-- TODO potentially unsafe
property2Bool :: Maybe String -> Bool
property2Bool (Just "TRUE_CONSENSUS") = True
property2Bool (Just "FALSE_CONSENSUS") = False

stageConfiguration2Configuration :: PopulationProtocol -> StageConfiguration -> Configuration
stageConfiguration2Configuration pp sc =
    configurationFromList (numOfStates pp) $ map toPair $ M.assocs sc
  where
    toPair (label, value) = (getStateFromLabel pp label, value)

updateStage :: PopulationProtocol -> Stage -> IO Stage
updateStage pp stage = do
    reachSequence <- getReachSequence pp c c'
    let labelledSequence = map (getTransitionLabel pp) <$> reachSequence
    unstable <- lolaFairInfiniteCover pp c' $ statesOfOpinion pp $ not expectedConsensusValue
    return stage { reachSequence = labelledSequence, configurationUnstable = Just unstable }
  where
    c = stageConfiguration2Configuration pp $ potReachFromConfiguration stage
    c' = stageConfiguration2Configuration pp $ configuration (stage :: Stage)
    expectedConsensusValue = property2Bool $ checkedProperty stage

-- adds counter examples to failed stages of stage graph
updateStageGraph :: PopulationProtocol -> StageGraph -> IO StageGraph
updateStageGraph pp graph = do
    updatedStages <- mapM updateIfFailed $ stages graph
    return $ graph { stages = updatedStages }
  where
    updateIfFailed stage = if failed stage then updateStage pp stage else return stage

hasCounterExample :: StageGraph -> Bool
hasCounterExample graph =
    any valid $ stages graph
  where
    valid stage = isJust (reachSequence stage) && (configurationUnstable stage == Just True)

verifyStageGraph :: PopulationProtocol -> IO VerificationReply
verifyStageGraph pp = do
  tmpDir <- getCanonicalTemporaryDirectory
  let protocolString = B.unpack (encode pp)
  fileName <- writeTempFile tmpDir "peregrine2request" $ protocolString
  python <- getPython3ExecutablePath
  peregrine2 <- peregrine2Path
  let query = python ++ " " ++  peregrine2 ++ "  ../peregrine2.0/protocols/from_peregrine1.py '[\"" ++ fileName ++ "\"]' -e speed -eager -ex"
  putStrLn query
  result <- tryIOError $ readCreateProcess (shell query) ""
  either (\x -> putStrLn "IO Error while using peregrine2.0") putStrLn result
  case result of
    (Left _)    -> return VerificationReply { status = Error
                            , result = Nothing
                            , counterExample = Nothing
                            , stageGraph = Nothing
                            }
    (Right res) -> case (decode (B.pack res) :: Maybe Peregrine2Response) of
        (Just response) -> do
            graph' <- updateStageGraph pp $ graph response
            case verified response of
                               True -> return VerificationReply { status = Success
                                                 , result = Just Verified
                                                 , counterExample = Nothing
                                                 , stageGraph = Just graph'
                                                 }
                               False -> do
                                 if hasCounterExample graph' then
                                    return VerificationReply { status = Success
                                                             , result = Just Disproven
                                                             , counterExample = Nothing
                                                             , stageGraph = Just graph'
                                                             }
                                 else
                                    return VerificationReply { status = Success
                                                             , result = Just Unsure
                                                             , counterExample = Nothing
                                                             , stageGraph = Just graph'
                                                             }
        _               -> return VerificationReply { status = Error
                                                 , result = Nothing
                                                 , counterExample = Nothing
                                                 , stageGraph = Nothing
                                                 }


prePostVerifyProtocol :: PopulationProtocol -> IO VerificationReply
prePostVerifyProtocol = chainVerificationRequest (verifyTerminalProperty PrePostCondition) verifyLayeredTermination

getProtocolFiles :: IO [FilePath]
getProtocolFiles = do
  pDir <- protocolDir
  map (pDir ++) <$> getProtocolFileNames


getProtocolFileNames :: IO [FilePath]
getProtocolFileNames = protocolDir >>= listDirectory


valToInt (Number n) = fromJust $ toBoundedInteger n
valToInt _ = error "Wrong format, expected Int."

valToDouble (Number n) = (toRealFloat n) :: Double
valToDouble _ = error "Wrong format, expected Double."

valToText (String s) = s
valToText _ = error "Wrong format, expected Text."
