package src.efficientrandomsamples;

/**
 * @author Philipp Meyer
 *
 */
public class TreeConfiguration {
	final int[] tree;
	final int offset;

	/**
	 * Create a new configuration with n states backed by a lookup tree.
	 */
	public TreeConfiguration(int n) {
		tree = new int[2 * n - 1];
		offset = n - 1;
	}

	/**
	 * Get the total number of agents.
	 */
	public int size() {
		return tree[0];
	}

	/**
	 * Get the number of agents in state q.
	 */
	public int get(int q) {
		return tree[offset + q];
	}

	/**
	 * Set the number of agents in state q.
	 */
	public void set(int q, int agents) {
		update(q, agents - get(q));
	}

	/**
	 * Change the number of agents in state q by diff.
	 */
	public void update(int q, int diff) {
		int index = q + offset;
		tree[index] += diff;
		while (index > 0) {
			index = (index - 1) / 2; // parent
			tree[index] += diff;
			if (tree[index] < 0) {
				throw new ArithmeticException("State " + q + " has value " + tree[index]);
			}
		}
	}

	/**
	 * Get the state of the agent with number a.
	 */
	public int getState(int a) {
		int index = 0;
		while (index < offset) {
			int left = 2 * index + 1; // left child
			int a_left = tree[left];
			if (a < a_left) {
				index = left;
			} else {
				index = 2 * index + 2; // right child
				a -= a_left;
			}
		}
		return index - offset;
	}
}