package src.efficientrandomsamples;

import com.google.gson.JsonObject;

/**
 * @author Philip Offtermatt
 * 
 */
public class SimulationConfiguration {
	/**
	 * The number of runs that will be simulated.
	 */
	public int sampleSize;
	/**
	 * The minimal size of configurations to be simulated. Ignored when a specific
	 * initial configuration is given.
	 */
	public int minConfigSize;
	/**
	 * The maximal size of configurations to be simulated. Ignored when a specific
	 * initial configuration is given.
	 */
	public int maxConfigSize;
	/**
	 * The maximal number of steps to simulate in one run.
	 */
	public int numSteps;
	/**
	 * Indicates whether lola should be called to resolve situations where it is not
	 * clear whether a reached consensus will last.
	 */
	public boolean increaseAccuracy;
	/**
	 * The protocol to simulate. See the PopulationProtocol class for more
	 * information.
	 */
	public PopulationProtocol protocol;
	/**
	 * Can be either "UniformTransitions" or "UniformAgents". Uniform transitions
	 * will choose uniformly at random among all enabled transitions to choose the
	 * next configuration, while uniform agents will first choose two agents in the
	 * population uniformly at random, and then choose one of the transitions for
	 * the chosen pair.
	 */
	public String schedulingMode = "UniformTransitions";
	/**
	 * Optional: Can be used to specify an initial configuration that should be
	 * used. Supersedes minimal/maximal config sizes when used. Can safely not be
	 * included in the json request if no initial configuration should be specified.
	 */
	public JsonObject initialConfiguration;

	@Override
	public String toString() {
		return "SimulationConfiguration [sampleSize=" + sampleSize + ", minConfigSize=" + minConfigSize
				+ ", maxConfigSize=" + maxConfigSize + ", numSteps=" + numSteps + ", increaseAccuracy="
				+ increaseAccuracy + ", protocol=" + protocol + ", schedulingMode=" + schedulingMode + "]";
	}
}