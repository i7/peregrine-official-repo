package src.efficientrandomsamples;

import com.google.gson.JsonObject;

public class SimulationLoopResult {

	private int convergence;
	private int steps;

	public int getConvergence() {
		return convergence;
	}

	public int getStepsToConvergence() {
		return steps;
	}

	public SimulationLoopResult(int consensus, int stepsToConsensus) {
		this.convergence = consensus;
		this.steps = stepsToConsensus;
	}

	public JsonObject getConvergenceJson() {
		JsonObject result = new JsonObject();
		result.addProperty("convergence", convergence);
		return result;
	}

	public JsonObject getStepsJson() {
		JsonObject result = new JsonObject();
		result.addProperty("steps", steps);
		return result;
	}
}