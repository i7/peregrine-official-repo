package src.efficientrandomsamples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import src.efficientrandomsamples.predicateparser.PredicateParser;

/**
 * @author Philip Offtermatt
 *
 */
public class PopulationProtocol {
	/**
	 * A textual description of the protocol.
	 */
	public String description;
	/**
	 * An array of initial states.
	 * Naturally, must be a subset of states.
	 */
	public String[] initialStates;
	/**
	 * A postcondition, that should hold in the final configuration when in the initial configuration,
	 * the precondition held.
	 * !NOTE: not yet supported.
	 */
	public String postcondition;
	/**
	 * A precondition, such that if this condition holds in the initial configuration,
	 * then the postconition must hold in the final configuration.
	 * !NOTE: not yet supported.
	 */
	public String precondition;
	/**
	 * The predicate this protocol computes. Used to determine whether a run
	 * converges to the correct consensus.
	 * For documentation on how this should look,
	 * consider the Peregrine documentation.
	 */
	public String predicate;
	/**
	 * An array of string identifiers of states.
	 */
	public String[] states;
	/**
	 * The title of the protocol.
	 */
	public String title;
	/**
	 * An array of states that should have an output of 'true'. All other states
	 * are automatically considered to output 'false'.
	 * Must be a subset of states.
	 */
	public String[] trueStates;

	/**
	 * A set of transitions.
	 */
	public Transition[] transitions;

	/**
	 * Initial leader configuration
	 */
	public HashMap<String, Integer> leaders;

	/**
	 * Automatically populated when calling {@link #buildMap()}. Maps state identifier strings to integer identifiers.
	 */
	public HashMap<String, Integer> statesToIndices;

	/**
	 *  Maps a pair of agents to
	 */
	public Map<Pair<Integer, Integer>, List<Pair<Integer, Integer>>> transitionMap;

	// Used when generating initial populations
	private Random randomizer = new Random();

	public void setRandomizer(Random randomizer) {
		this.randomizer = randomizer;
	}

	public String buildLolaString(TreeConfiguration conf) {
		// Order of the parts is important. Places, then marking, then transitions
		StringBuilder result = new StringBuilder();
		// Add places
		result.append("PLACE ");
		// treat states array as stream, then map each element q to s
		try {
			result.append(Arrays.asList(states).stream().map(state -> "s" + state).collect(Collectors.joining(",")));
		} catch (NullPointerException e) {
			System.err.println("The protocol might not have states.");
			throw e;
		}
		result.append(";");
		// Add configuration
		result.append("MARKING ");
		try {
			result.append(Arrays.asList(states).stream()
					.map(state -> "s" + state + ": " + conf.get(statesToIndices.get(state)))
					.collect(Collectors.joining(",")));
		} catch (NullPointerException e) {
			System.err.println("Was protocol.buildMap called before this protocol was used to build a string?");
			throw e;
		}
		result.append(";");
		// Add transitions
		try {
			result.append(Arrays.asList(transitions).stream().map(transition -> transition.buildLolaString())
					.collect(Collectors.joining(" ")));
		} catch (NullPointerException e) {
			System.err.println("The protocol might not have transitions.");
			throw e;
		}
		return result.toString();
	}

	public void buildMap() {
		statesToIndices = new HashMap<String, Integer>();
		for (int i = 0; i < states.length; i++) {
			statesToIndices.put(states[i], i);
		}

		transitionMap = new HashMap<Pair<Integer, Integer>, List<Pair<Integer, Integer>>>();
		for (Transition t : transitions) {
			Pair<Integer, Integer> pre = new Pair<Integer, Integer>(statesToIndices.get(t.pre[0]),
					statesToIndices.get(t.pre[1]));
			Pair<Integer, Integer> post = new Pair<Integer, Integer>(statesToIndices.get(t.post[0]),
					statesToIndices.get(t.post[1]));
			List<Pair<Integer, Integer>> currentPostList = transitionMap.getOrDefault(pre,
					new ArrayList<Pair<Integer, Integer>>());
			currentPostList.add(post);
			// if it was not in before, i.e. we got the default from getOrDefault, we need
			// to put it now.
			transitionMap.put(pre, currentPostList); // TODO if this is too slow, can easily replace by only adding when
														// necessary
		}
	}

	public int checkConsensusWithLola(TreeConfiguration conf, boolean expectedConsensus) {
		int consensus = checkConsensus(conf, expectedConsensus);
		if (consensus == Simulation.POSSIBLY_CORRECT_CONSENSUS || consensus == Simulation.POSSIBLY_WRONG_CONSENSUS) {
			String lolaString = buildLolaString(conf);
			// build predicate string
			String predicateString;
			// if consensus might be wrong, check if correct states are possible again
			if (consensus == Simulation.POSSIBLY_WRONG_CONSENSUS) {
				// need to check whether there will be any states that output the correct result
				predicateString = buildPredicateString(
						expectedConsensus ? Arrays.asList(this.trueStates) : this.getFalseStates());
			} else {
				// if consensus might be correct, check for wrong states
				// get false states
				predicateString = buildPredicateString(
						expectedConsensus ? this.getFalseStates() : Arrays.asList(this.trueStates));
			}
			boolean predicateReachable = true;
			// call lola
			Runtime r = Runtime.getRuntime();
			try {

				File lolaInputFile = new File("lola_string.txt");
				try {
					FileWriter fileWriter = new FileWriter(lolaInputFile);
					fileWriter.write(lolaString);
					fileWriter.close();

					// does not seem to work on windows, since windows does not know /dev/stdout
					ProcessBuilder pb = new ProcessBuilder("lola", "--json=/dev/stdout", "--quiet", "-f",
							"REACHABLE " + predicateString, "lola_string.txt");
					pb.redirectOutput(Redirect.PIPE);
					Process p = pb.start();
					p.waitFor();

					BufferedReader lolaReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
					String lolaResponse = lolaReader.readLine();

					JsonElement lolaResult = new JsonParser().parse(lolaResponse);
					predicateReachable = lolaResult.getAsJsonObject().get("analysis").getAsJsonObject().get("result")
							.getAsBoolean();

					// predicate reachable means consensus is not stable yet, i.e. no consensus
					if (predicateReachable) {
						return Simulation.NO_CONSENSUS;
					} else {
						// consensus must be either possibly wrong or possibly correct, but predicate is
						// not reachable, therefore, it is actually a "real" consensus
						return consensus == Simulation.POSSIBLY_CORRECT_CONSENSUS ? Simulation.CORRECT_CONSENSUS
								: Simulation.WRONG_CONSENSUS;
					}
				} finally {
					lolaInputFile.delete();
				}

			} catch (IOException | InterruptedException e) {
				System.err.println("Could not call lola");
				e.printStackTrace();
				return consensus; // return the possibly wrong/possibly correct consensus, since we can't verify
									// further
			}
		} else {
			// consensus is definitely wrong, definitely correct or there is no consensus
			return consensus; // no need to call lola
		}
	}

	private List<String> getFalseStates() {
		return Arrays.asList(this.states).stream().filter(state -> !Arrays.asList(trueStates).contains(state))
				.collect(Collectors.toList());
	}

	public String buildPredicateString(List<String> states) {
		StringBuilder result = new StringBuilder();
		result.append("(");
		result.append(states.stream().map(state -> "s" + state + " > 0").collect(Collectors.joining(" OR ")));
		result.append(")");
		return result.toString();
	}

	public int checkConsensus(TreeConfiguration conf, boolean expectedConsensus) {
		// check if transitions are enabled
		boolean isTerminal = isTerminalConfiguration(conf);

		boolean trueConsensusPossible = true;
		boolean falseConsensusPossible = true;

		for (String qString : this.states) {
			int stateNumber = this.statesToIndices.get(qString);
			if (conf.get(stateNumber) > 0) {
				if (Arrays.asList(this.trueStates).contains(qString)) {
					falseConsensusPossible = false;
				} else {
					trueConsensusPossible = false;
				}
			}
		}
		if (trueConsensusPossible && !falseConsensusPossible) {
			return isTerminal ? (expectedConsensus ? Simulation.CORRECT_CONSENSUS : Simulation.WRONG_CONSENSUS)
					: (expectedConsensus ? Simulation.POSSIBLY_CORRECT_CONSENSUS : Simulation.POSSIBLY_WRONG_CONSENSUS);
		} else if (!trueConsensusPossible && falseConsensusPossible) {
			return isTerminal ? (!expectedConsensus ? Simulation.CORRECT_CONSENSUS : Simulation.WRONG_CONSENSUS)
					: (!expectedConsensus ? Simulation.POSSIBLY_CORRECT_CONSENSUS
							: Simulation.POSSIBLY_WRONG_CONSENSUS);
		}
		// (trueConsensusPossible && falseConsensusPossible) || (!trueConsensusPossible
		// && !falseConsensusPossible))
		return Simulation.NO_CONSENSUS;
	}

	public boolean evaluatePredicateForConfiguration(TreeConfiguration conf) {
		String substitutedPredicate = this.predicate;

		substitutedPredicate = this.substitueCounts(conf, substitutedPredicate);

		return PredicateParser.evaluatePredicate(substitutedPredicate);
	}

	private String substitueCounts(TreeConfiguration conf, String predicate) {
		for (String state : this.states) {
			predicate = predicate.replace("C[" + state + "]", "" + conf.get(statesToIndices.get(state)));
		}

		Pattern pattern = Pattern.compile(
				"C\\[" + PredicateParser.WHITESPACE_PATTERN + "(\\w*)" + PredicateParser.WHITESPACE_PATTERN + "\\]");
		Matcher m = pattern.matcher(predicate);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String stateName = m.group(1);

			m.appendReplacement(sb, "" + (conf.get(this.statesToIndices.get(stateName))));
		}
		m.appendTail(sb);
		return sb.toString();
	}

	public boolean isTerminalConfiguration(TreeConfiguration conf) {
		for (Transition t : this.transitions) {
			if ((t.pre[0].equals(t.post[0]) && t.pre[1].equals(t.post[1]))
					|| (t.pre[1].equals(t.post[0]) && t.pre[0].equals(t.post[1]))) {
				// if transition is silent, it does not matter whether it is enabled
				continue;
			}
			if (isTransitionEnabled(t, conf)) {
				// a transition is enabled, not terminal
				return false;
			}
		}
		// no transitions are enabled
		return true;
	}

	public boolean isTransitionEnabled(Transition t, TreeConfiguration conf) {
		if (t.pre[0].equals(t.pre[1])) {
			return conf.get(this.statesToIndices.get(t.pre[0])) >= 2;
		} else {
			return conf.get(this.statesToIndices.get(t.pre[0])) > 0 && conf.get(this.statesToIndices.get(t.pre[1])) > 0;
		}
	}

	public TreeConfiguration getRandomInitialPopulation(int minConfigSize, int maxConfigSize) {
		// ignore precondition for now
		// uniformly sampling so that the result equals a sum is not trivial.
		// see https://stackoverflow.com/a/8064754 for a reference as to how it is done
		// here

		TreeConfiguration initialTree = new TreeConfiguration(this.states.length);

		// when minConfigSize > maxConfigSize, swap the bounds, so that we can still get
		// configs. Weird behaviour, but follows the behaviour of the previous backend.
		if (minConfigSize > maxConfigSize) {
			int tmp = maxConfigSize;
			maxConfigSize = minConfigSize;
			minConfigSize = tmp;
		}

		// need at least 2 agents, otherwise a protocol does not make much sense
		if (minConfigSize <= 1) {
			minConfigSize = 2;
		}

		// randomizer.nextInt(0) throws an exception, so when minConfigSize ==
		// maxConfigSize, make sure not to random.
		int populationSize = minConfigSize == maxConfigSize ? minConfigSize
				: minConfigSize + randomizer.nextInt(maxConfigSize - minConfigSize);
		int[] randomNumbers = new int[this.initialStates.length + 1];

		for (int i = 0; i < this.initialStates.length - 1; i++) {
			randomNumbers[i] = randomizer.nextInt(populationSize);
		}
		randomNumbers[this.initialStates.length - 1] = 0;
		randomNumbers[this.initialStates.length] = populationSize;
		Arrays.sort(randomNumbers);

		for (int i = 0; i < randomNumbers.length - 1; i++) {
			int stateNumber = this.statesToIndices.get(this.initialStates[i]);
			int amount = randomNumbers[i + 1] - randomNumbers[i];
			initialTree.set(stateNumber, amount);
		}

		return initialTree;
	}

	public JsonObject getJsonOfConfiguration(TreeConfiguration configuration) {
		JsonObject result = new JsonObject();
		for (String state : states) {
			result.addProperty(state, configuration.get(statesToIndices.get(state)));
		}
		return result;
	}

	public TreeConfiguration getInitialConfigurationFromJson(JsonObject initialConfiguration) {
		TreeConfiguration result = new TreeConfiguration(this.states.length);

		for (Entry<String, JsonElement> entry : initialConfiguration.entrySet()) {
			String state = entry.getKey();
			int multiplicity = entry.getValue().getAsInt();
			result.set(this.statesToIndices.get(state), multiplicity);
		}
		return result;
	}
}
