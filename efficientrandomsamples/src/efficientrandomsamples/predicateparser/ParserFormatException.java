package src.efficientrandomsamples.predicateparser;

public class ParserFormatException extends RuntimeException {

	public ParserFormatException(String message) {
		super(message);
	}

}
