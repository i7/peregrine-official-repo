package src.efficientrandomsamples.predicateparser;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PredicateParser {

	public final static String BOOLEAN_PATTERN = "(true|false)";
	public final static String INTEGER_PATTERN = "(-?\\d+)";
	public final static String WHITESPACE_PATTERN = "\\s*";

	final static Function<String, Boolean> BOOLEAN_FROM_STRING_FUN = x -> Boolean.parseBoolean(x);
	final static Function<String, Integer> INTEGER_FROM_STRING_FUN = x -> Integer.parseInt(x);

	/**
	 * 
	 * @param predicate
	 * @param leftParameterType  either "boolean" or "number"
	 * @param rightParameterType either "boolean" or "number"
	 * @param operatorPattern
	 * @param function
	 * @return
	 */
	public static String searchReplace(String predicate, String leftParameterType, String rightParameterType,
			String operatorPattern, BiFunction function) {
		String leftPattern;
		String rightPattern;
		Function leftArgumentParser;
		Function rightArgumentParser;

		if (leftParameterType.equals("boolean")) {
			leftPattern = BOOLEAN_PATTERN;
			leftArgumentParser = BOOLEAN_FROM_STRING_FUN;
		} else if (leftParameterType.equals("number")) {
			leftPattern = INTEGER_PATTERN;
			leftArgumentParser = INTEGER_FROM_STRING_FUN;
		} else {
			throw new IllegalArgumentException("Left parameter type " + leftParameterType + " is not known!");
		}

		if (rightParameterType.equals("boolean")) {
			rightPattern = BOOLEAN_PATTERN;
			rightArgumentParser = BOOLEAN_FROM_STRING_FUN;
		} else if (rightParameterType.equals("number")) {
			rightPattern = INTEGER_PATTERN;
			rightArgumentParser = INTEGER_FROM_STRING_FUN;
		} else {
			throw new IllegalArgumentException("Right parameter type " + rightParameterType + " is not known!");
		}

		Pattern pattern = Pattern
				.compile(leftPattern + WHITESPACE_PATTERN + operatorPattern + WHITESPACE_PATTERN + rightPattern);
		Matcher m = pattern.matcher(predicate);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String firstParam = m.group(1);
			String secondParam = m.group(2);

			m.appendReplacement(sb, ""
					+ (function.apply(leftArgumentParser.apply(firstParam), rightArgumentParser.apply(secondParam))));
		}
		m.appendTail(sb);

		// apply operator recursively until no change anymore
		return sb.toString();
	}

	private static String resolveOperator(String operator, String predicate) {
		String currentPredicate;
		do {
			currentPredicate = predicate;
			predicate = cleanUpNegationAndBrackets(predicate);
			switch (operator) {
			case ("="):
				predicate = searchReplace(predicate, "number", "number", "=", equals);
				break;
			case ("!="):
				predicate = searchReplace(predicate, "number", "number", "!=", nEquals);
				break;
			case (">="):
				predicate = searchReplace(predicate, "number", "number", ">=", greaterEquals);
				break;
			case ("<="):
				predicate = searchReplace(predicate, "number", "number", "<=", lesserEquals);
				break;
			case (">"):
				predicate = searchReplace(predicate, "number", "number", ">", greater);
				break;
			case ("<"):
				predicate = searchReplace(predicate, "number", "number", "<", lesser);
				break;
			case ("%="):
				predicate = handleModulo(predicate);
				break;
			case ("&&"):
				predicate = searchReplace(predicate, "boolean", "boolean", "&&", and);
				break;
			case ("||"):
				predicate = searchReplace(predicate, "boolean", "boolean", "\\|\\|", or);
				break;
			case ("->"):
				predicate = searchReplace(predicate, "boolean", "boolean", "->", implies);
				break;
			case ("+"):
				predicate = searchReplace(predicate, "number", "number", "\\+", plus);
				break;
			case ("-"):
				predicate = searchReplace(predicate, "number", "number", "-", minus);
				break;
			case ("*"):
				predicate = searchReplace(predicate, "number", "number", "\\*", mul);
				break;
			default:
				throw new UnsupportedOperationException("Operator \"" + operator + "\" could not be resolved.");
			}
		} while (!predicate.equals(currentPredicate));
		return predicate;
	}

	public final static BiFunction<Integer, Integer, Boolean> equals = (x, y) -> x == y;
	public final static BiFunction<Integer, Integer, Boolean> nEquals = (x, y) -> x != y;
	public final static BiFunction<Integer, Integer, Boolean> greaterEquals = (x, y) -> x >= y;
	public final static BiFunction<Integer, Integer, Boolean> lesserEquals = (x, y) -> x <= y;
	public final static BiFunction<Integer, Integer, Boolean> greater = (x, y) -> x > y;
	public final static BiFunction<Integer, Integer, Boolean> lesser = (x, y) -> x < y;

	public final static BiFunction<Boolean, Boolean, Boolean> and = (x, y) -> x && y;
	public final static BiFunction<Boolean, Boolean, Boolean> or = (x, y) -> x || y;
	public final static BiFunction<Boolean, Boolean, Boolean> implies = (x, y) -> (!x) || y;

	public final static BiFunction<Integer, Integer, Integer> plus = (x, y) -> x + y;
	public final static BiFunction<Integer, Integer, Integer> minus = (x, y) -> x - y;
	public final static BiFunction<Integer, Integer, Integer> mul = (x, y) -> x * y;

	public static boolean evaluatePredicate(String predicate) {
		// remove whitespace at the beginning/end - this whitespace would interfere with
		// recognizing when we are done
		predicate = predicate.trim();

		String initialPredicate = predicate;

		// need to at the very most have one loop for each symbol in the predicate
		// -> very maximal upper bound
		int maxTries = predicate.length();
		int tries = 0;
		while (true) {
			tries++;
			if (tries > maxTries) {
				throw new ParserFormatException("When parsing predicate \"" + initialPredicate
						+ "\", could not resolve further. Stopped at: \"" + predicate + "\"");
			}

			// operators need to be resolved in order of precedence - e.g.
			// arithmetic operations need to be resolved before
			// comparison operations, and multiplication needs to
			// be resolved before addition

			// arithmetic operations
			predicate = resolveOperator("*", predicate);
			predicate = resolveOperator("-", predicate);
			predicate = resolveOperator("+", predicate);

			// comparison operations
			predicate = resolveOperator("=", predicate);
			predicate = resolveOperator("!=", predicate);
			predicate = resolveOperator(">=", predicate);
			predicate = resolveOperator("<=", predicate);
			predicate = resolveOperator(">", predicate);
			predicate = resolveOperator("<", predicate);
			predicate = resolveOperator("%=", predicate);

			// boolean operations
			predicate = resolveOperator("&&", predicate);
			predicate = resolveOperator("||", predicate);
			predicate = resolveOperator("->", predicate);

			if (predicate.equals("true")) {
				return true;
			}
			if (predicate.equals("false")) {
				return false;
			}
		}
	}

	private static String cleanUpNegationAndBrackets(String predicate) {
		// initialize current predicate
		String currentPredicate;
		do {
			currentPredicate = predicate;
			predicate = removeBrackets(predicate);
			predicate = handleNegation(predicate);
		} while (predicate != currentPredicate);
		return predicate;
	}

	private static String handleNegation(String predicate) {
		// boolean negation
		predicate = predicate.replaceAll("!!", "");
		predicate = predicate.replaceAll("!\\s*false", "true");
		predicate = predicate.replaceAll("!\\s*true", "false");

		// arithmetic negation
		predicate = predicate.replaceAll("(((%=_\\d)|[+\\-)(=&|><\\*])\\s*)?(--)", "$1");

		return predicate;
	}

	private static String removeBrackets(String predicate) {
		predicate = predicate.replaceAll("\\(\\s*(" + BOOLEAN_PATTERN + "|" + INTEGER_PATTERN + ")\\s*\\)", "$1");
		return predicate;
	}

	private static String handleModulo(String predicate) {
		Pattern pattern = Pattern
				.compile(INTEGER_PATTERN + WHITESPACE_PATTERN + "%=_(\\d+)" + WHITESPACE_PATTERN + INTEGER_PATTERN);
		Matcher m = pattern.matcher(predicate);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			Integer lefthandSide = Integer.parseInt(m.group(1));
			Integer modulo = Integer.parseInt(m.group(2));
			Integer righthandSide = Integer.parseInt(m.group(3));

			m.appendReplacement(sb, "" + (lefthandSide % modulo == righthandSide % modulo));
		}
		m.appendTail(sb);
		return sb.toString();
	}

}
