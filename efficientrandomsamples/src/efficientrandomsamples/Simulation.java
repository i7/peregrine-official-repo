package src.efficientrandomsamples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

// immutable pair
class Pair<F, S> {
	private F first;
	private S second;

	public Pair(F val1, S val2) {
		first = val1;
		second = val2;
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((second == null) ? 0 : second.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (second == null) {
			if (other.second != null)
				return false;
		} else if (!second.equals(other.second))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pair(" + first + "," + second + ")";
	}
}

public class Simulation {

	private SimulationConfiguration config;
	public final Random randomizer;

	/**
	 * Denotes the int that is returned when a consensus is correct and lasting
	 */
	public static final int CORRECT_CONSENSUS = 0;
	/**
	 * Denotes the int that is returned when a consensus is correct but not lasting
	 */
	public static final int POSSIBLY_CORRECT_CONSENSUS = 1;
	/**
	 * Denotes the int that is returned when there is no consensus
	 */
	public static final int NO_CONSENSUS = 2;
	/**
	 * Denotes the int that is returned when a consensus is incorrect but not
	 * lasting
	 */
	public static final int POSSIBLY_WRONG_CONSENSUS = 3;
	/**
	 * Denotes the int that is returned when a consensus is incorrect and lasting
	 */
	public static final int WRONG_CONSENSUS = 4;

	// TODO set
	public static final String lola = "path_to_lola";

	public Simulation(SimulationConfiguration newConfig, Random newRandomizer) {
		this.config = newConfig;
		this.randomizer = newRandomizer;
	}

	/*
	 * The transition should be taken from a lookup in a map created from the
	 * protocol. The key (q1, q2) can be combined into a single integer key = q1 +
	 * q2*n, where n is the number of states. Instead of a HashMap, also a
	 * 'List<Transition>[] trans' array of size n*n can be used such that
	 * 'trans[q1+q2*n]' contains the transition(s) enabled by q1,q2, which is then
	 * empty if only silent transitions are enabled. This is probably the most
	 * efficient way. The key could be normalized such that q1 <= q2 so that (q1,q2)
	 * and (q2,q1) map to the same value. Returns False if a silent transition was
	 * chosen, True if a nonsilent transition was chosen
	 */
	public boolean applyUpdate(TreeConfiguration conf, int q1, int q2) {
		// for testing purposes: implementation with hashmap
		List<Pair<Integer, Integer>> possibilities = config.protocol.transitionMap.get(new Pair(q1, q2));
		if (possibilities == null) {
			// only silent transitions available
			return false;
		}

		Pair<Integer, Integer> chosenPost = possibilities.get(randomizer.nextInt(possibilities.size()));
		int q3 = chosenPost.getFirst();
		int q4 = chosenPost.getSecond();

		conf.update(q1, -1);
		conf.update(q2, -1);
		conf.update(q3, 1);
		conf.update(q4, 1);

		if ((q1 == q3 && q2 == q4) || (q1 == q4 && q2 == q3)) {
			// was a silent transition, but was still given as a transition. Could happen if
			// silent transitions are explicitly given.
			return false;
		} else {
			return true;
		}
	}

	public SimulationLoopResult handleLoop(TreeConfiguration conf, PopulationProtocol protocol, int numSteps) {
		return handleLoop(conf, protocol, numSteps, true);
	}

	public SimulationLoopResult handleLoop(TreeConfiguration conf, PopulationProtocol protocol, int numSteps,
			boolean uniformAgentScheduling) {
		// start step at 1 since to be consistent with silent transition counter, the
		// first step has to be 1

		int step = 1;

		// count how many silent transitions have been applied in a row.
		// When there have been at least silentTransitionThreshold many, check if there
		// is a consensus.
		int silentTransitionsCounter = 0;

		int silentTransitionThreshold = (int) Math.min(conf.size(), Math.ceil(numSteps / 10));

		boolean expectedConsensus = protocol.evaluatePredicateForConfiguration(conf);

		//Now add leaders to conf
		if(protocol.leaders != null){
			protocol.leaders.forEach((l, num) -> {
				int i = protocol.statesToIndices.get(l);
				int newVal = conf.get(i) + num;
				conf.set(i, newVal);
			});
		}

		for (; step < numSteps; ++step) {
			Pair<Integer, Integer> selectedAgents = selectAgents(protocol, conf, uniformAgentScheduling);
			if (selectedAgents == null) {
				// there must not be a valid pair of agents to be chosen (i.e. there are no
				// enabled non-silent transitions for
				// uniform transition scheduling - can stop now
				return new SimulationLoopResult(
						this.config.increaseAccuracy ? protocol.checkConsensusWithLola(conf, expectedConsensus)
								: protocol.checkConsensus(conf, expectedConsensus),
						step - silentTransitionsCounter);
			}
			if ((selectedAgents.getFirst() == selectedAgents.getSecond() && conf.get(selectedAgents.getFirst()) < 2)
					|| conf.get(selectedAgents.getFirst()) < 1) {
				throw new ArithmeticException(
						"State " + selectedAgents.getFirst() + " only has " + conf.get(selectedAgents.getFirst())
								+ " agents in it, but was chosen as a pre. Chosen agents: " + selectedAgents.getFirst()
								+ ", " + selectedAgents.getSecond());
			}

			if (conf.get(selectedAgents.getSecond()) < 1) {
				throw new ArithmeticException("State " + selectedAgents.getSecond() + " only has "
						+ conf.get(selectedAgents.getSecond()) + " agents in it, but was chosen as a pre");
			}

			boolean silentTransition = !applyUpdate(conf, selectedAgents.getFirst(), selectedAgents.getSecond());
			if (!silentTransition) {
				silentTransitionsCounter = 0;
			}

			if (silentTransition) {
				if (++silentTransitionsCounter - silentTransitionThreshold == 0) {
					if (this.config.increaseAccuracy) {
						int currentConsensus = protocol.checkConsensusWithLola(conf, expectedConsensus);
						if (currentConsensus == CORRECT_CONSENSUS || currentConsensus == WRONG_CONSENSUS) {
							return new SimulationLoopResult(currentConsensus, step - silentTransitionsCounter);
						}
					} else {
						// if not increaseAccuracy, check after 'silentTransitionThreshold' many silent
						// transitions whether there are
						// still transitions enabled. If not, can abort now
						if (protocol.isTerminalConfiguration(conf)) {
							int currentConsensus = protocol.checkConsensus(conf, expectedConsensus);
							if (currentConsensus == CORRECT_CONSENSUS || currentConsensus == WRONG_CONSENSUS) {
								return new SimulationLoopResult(currentConsensus, step - silentTransitionsCounter);
							}
						}
					}
				}
			}
		}

		return new SimulationLoopResult(
				this.config.increaseAccuracy ? protocol.checkConsensusWithLola(conf, expectedConsensus)
						: protocol.checkConsensus(conf, expectedConsensus),
				step - silentTransitionsCounter);
	}

	/**
	 * Selects agents from the given configuration with the specified scheduling
	 * mode.
	 *
	 * @param protocol
	 * @param conf
	 * @param uniformAgentScheduling True for uniform agent scheduling, false for
	 *                               uniform transition scheduling
	 * @return null if no valid pair could be found (no non-silent transition is
	 *         enabled for uniform transition scheduling), else a pair of two
	 *         integers, which are the state indices of the chosen agents
	 */
	private Pair<Integer, Integer> selectAgents(PopulationProtocol protocol, TreeConfiguration conf,
			boolean uniformAgentScheduling) {
		Pair<Integer, Integer> selectedAgents;
		if (uniformAgentScheduling) {
			int a1 = randomizer.nextInt(conf.size());
			int q1 = conf.getState(a1);
			int a2 = randomizer.nextInt(conf.size() - 1); // adjust so that we dont pick the same agent twice
			a2 = a2 >= a1 ? a2 + 1 : a2; // adjust the chosen agent if the number is at least as large as the already
											// chosen agent
			int q2 = conf.getState(a2);
			selectedAgents = new Pair<Integer, Integer>(q1, q2);
		} else { // Rules scheduling: pick uniformly at random among enabled transitions
			List<Transition> enabledTransitions = Arrays.asList(protocol.transitions).stream()
					.filter(transition -> protocol.isTransitionEnabled(transition, conf)).collect(Collectors.toList());
			if (enabledTransitions.size() == 0) {
				// no transitions enabled - can return any two agents, so return first two
				return null;
			}
			Transition selectedTransition = enabledTransitions.get(randomizer.nextInt(enabledTransitions.size()));
			selectedAgents = new Pair<Integer, Integer>(protocol.statesToIndices.get(selectedTransition.pre[0]),
					protocol.statesToIndices.get(selectedTransition.pre[1]));
		}
		return selectedAgents;
	}

	public static SimulationConfiguration parseJSon(String jsonString) {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		builder.setLenient();

		Gson gson = builder.create();
		SimulationConfiguration config = gson.fromJson(jsonString, SimulationConfiguration.class);

		// build a more efficient map from the transitions
		config.protocol.buildMap();

		return config;
	}

	/**
	 * Takes a request in json format and simulates the specified protocol with its parameters.
	 * One instance of the SimulationConfiguration class is expected to be encoded in the request.
	 * See that class for more details on the expected parameters.
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		Random randomizer = new Random();

		final String program_name = "Simulation";

		if (args.length < 1) {
			System.out
					.println(String.format(
							"Usage: java %s filepath \n where filepath is the path to a file containg the "
									+ "actual request string."
									+ "Request must be in the form sent by peregrine for simulation requests.",
							program_name));
			return;
		}
		File f = new File(args[0]);
		BufferedReader reader = new BufferedReader(new FileReader(f));
		String request = reader.readLine();

		// remove description from request. It is not needed, and can be problematic for
		// gson parsing
		request = request.replaceAll("\\\\\\\"description\\\\\\\":\\\\\\\"[^\\\"]*\\\\\\\"", "\"description\":\"\"");

		// remove leading and trailing quotation marks
		if(request.charAt(0) == '"') {
			request = request.substring(1, request.length() - 1);
		}

		// unescape escaped quotes (replaces \" by ")
		request = request.replace("\\\"", "\"");

		SimulationConfiguration config = parseJSon(request);

		// set consistent random generator to be reproducible
		config.protocol.setRandomizer(randomizer);

		Simulation simulation = new Simulation(config, randomizer);

		JsonArray resultJsonArray = new JsonArray(config.sampleSize);

		for (int i = 0; i < config.sampleSize; i++) {

			// either use provided initial configuration or generate one according to
			// specified min/max sizes
			TreeConfiguration initialConfiguration;
			if (config.initialConfiguration == null) {
				// use specified sizes
				initialConfiguration = config.protocol.getRandomInitialPopulation(config.minConfigSize,
						config.maxConfigSize);
			} else {
				// use provided configuration
				initialConfiguration = config.protocol.getInitialConfigurationFromJson(config.initialConfiguration);

			}

			JsonObject loopJson = new JsonObject();
			loopJson.addProperty("size", initialConfiguration.size());
			loopJson.add("startConfig", config.protocol.getJsonOfConfiguration(initialConfiguration));

			SimulationLoopResult loopResult = simulation.handleLoop(initialConfiguration, config.protocol,
					config.numSteps, config.schedulingMode.equals("UniformAgents"));

			loopJson.add("endConfig", config.protocol.getJsonOfConfiguration(initialConfiguration));
			loopJson.addProperty("convergence", loopResult.getConvergence());
			loopJson.addProperty("steps", loopResult.getStepsToConvergence());

			resultJsonArray.add(loopJson);
		}
		System.out.println(resultJsonArray.toString());
	}
}
