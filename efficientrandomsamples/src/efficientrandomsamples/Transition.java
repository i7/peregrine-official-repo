package src.efficientrandomsamples;

import java.util.Arrays;

public class Transition {
	public String name;
	public String[] post;
	public String[] pre;

	@Override
	public String toString() {
		return "Transition [name=" + name + ", post=" + Arrays.toString(post) + ", pre=" + Arrays.toString(pre) + "]";
	}

	public String buildLolaString() {
		StringBuilder result = new StringBuilder();
		result.append("TRANSITION \"");
		result.append(name.replace(",", ".").replace(" ", "-"));
		result.append("\"");

		result.append(" CONSUME ");
		if (pre[0].equals(pre[1])) {
			result.append("s");
			result.append(pre[0]);
			result.append(": 2;");
		} else {
			result.append("s");
			result.append(pre[0]);
			result.append(": 1,");
			result.append("s");
			result.append(pre[1]);
			result.append(": 1;");
		}

		result.append(" PRODUCE ");
		if (post[0].equals(post[1])) {
			result.append("s");
			result.append(post[0]);
			result.append(": 2;");
		} else {
			result.append("s");
			result.append(post[0]);
			result.append(": 1,");
			result.append("s");
			result.append(post[1]);
			result.append(": 1;");
		}
		return result.toString();
	}
}