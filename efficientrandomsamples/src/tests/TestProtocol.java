package src.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import src.efficientrandomsamples.Simulation;
import src.efficientrandomsamples.SimulationConfiguration;
import src.efficientrandomsamples.TreeConfiguration;

class TestProtocol {

	private SimulationConfiguration config;

	@BeforeEach
	public void setUp() {
		String jsonString = "{\"minConfigSize\":200,\"maxConfigSize\":200,\"sampleSize\":2,\"numSteps\":2000,\"schedulingMode\":\"UniformMode\",\"increaseAccuracy\":false,\"protocol\":{\"transitions\":[{\"pre\":[\"Y\",\"N\"],\"post\":[\"y\",\"n\"],\"name\":\"Y, N -> y, n\"},{\"pre\":[\"Y\",\"n\"],\"post\":[\"Y\",\"y\"],\"name\":\"Y, n -> Y, y\"},{\"pre\":[\"N\",\"y\"],\"post\":[\"N\",\"n\"],\"name\":\"N, y -> N, n\"},{\"pre\":[\"y\",\"n\"],\"post\":[\"y\",\"y\"],\"name\":\"y, n -> y, y\"}],\"states\":[\"Y\",\"N\",\"y\",\"n\"],\"precondition\":null,\"initialStates\":[\"Y\",\"N\"],\"predicate\":\"C[Y] >= C[N]\",\"trueStates\":[\"Y\",\"y\"],\"title\":\"Majority voting protocol\",\"postcondition\":null,\"description\":\"This protocol takes a majority vote. More precisely,\\n                          it computes whether there are initially more agents\\n                          in state Y than N.\"}}";
		this.config = Simulation.parseJSon(jsonString);
	}

	@Test
	public void testCorrectConsensus() {

		TreeConfiguration population = new TreeConfiguration(100);
		population.set(config.protocol.statesToIndices.get("Y"), 50);
		population.set(config.protocol.statesToIndices.get("y"), 50);

		assertEquals(Simulation.CORRECT_CONSENSUS, config.protocol.checkConsensus(population, true));
	}

	@Test
	public void testWrongConsensus() {

		TreeConfiguration population = new TreeConfiguration(100);
		population.set(config.protocol.statesToIndices.get("N"), 50);
		population.set(config.protocol.statesToIndices.get("n"), 50);

		assertEquals(Simulation.WRONG_CONSENSUS, config.protocol.checkConsensus(population, true));
	}

	@Test
	public void testNoConsensus() {

		TreeConfiguration population = new TreeConfiguration(100);
		population.set(config.protocol.statesToIndices.get("y"), 50);
		population.set(config.protocol.statesToIndices.get("n"), 50);

		assertEquals(Simulation.NO_CONSENSUS, config.protocol.checkConsensus(population, true));
	}

	@Test
	public void testTerminal() {

		TreeConfiguration population = new TreeConfiguration(100);
		population.set(config.protocol.statesToIndices.get("N"), 50);
		population.set(config.protocol.statesToIndices.get("n"), 50);

		assertTrue(config.protocol.isTerminalConfiguration(population));
	}

	@Test
	public void testNotTerminal() {

		TreeConfiguration population = new TreeConfiguration(100);
		population.set(config.protocol.statesToIndices.get("N"), 50);
		population.set(config.protocol.statesToIndices.get("y"), 50);

		assertFalse(config.protocol.isTerminalConfiguration(population));
	}
}
