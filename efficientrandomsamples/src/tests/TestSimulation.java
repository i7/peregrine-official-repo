package src.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.efficientrandomsamples.Simulation;
import src.efficientrandomsamples.SimulationConfiguration;
import src.efficientrandomsamples.SimulationLoopResult;
import src.efficientrandomsamples.TreeConfiguration;

class TestSimulation {

	private SimulationConfiguration config;
	private Simulation simulation;

	@BeforeEach
	public void setUp() {
		String jsonString = "{\"minConfigSize\":200,\"maxConfigSize\":200,\"sampleSize\":2,\"numSteps\":2000,\"schedulingMode\":\"UniformMode\",\"increaseAccuracy\":false,\"protocol\":{\"transitions\":[{\"pre\":[\"Y\",\"N\"],\"post\":[\"y\",\"n\"],\"name\":\"Y, N -> y, n\"},{\"pre\":[\"Y\",\"n\"],\"post\":[\"Y\",\"y\"],\"name\":\"Y, n -> Y, y\"},{\"pre\":[\"N\",\"y\"],\"post\":[\"N\",\"n\"],\"name\":\"N, y -> N, n\"},{\"pre\":[\"y\",\"n\"],\"post\":[\"y\",\"y\"],\"name\":\"y, n -> y, y\"}],\"states\":[\"Y\",\"N\",\"y\",\"n\"],\"precondition\":null,\"initialStates\":[\"Y\",\"N\"],\"predicate\":\"C[Y] >= C[N]\",\"trueStates\":[\"Y\",\"y\"],\"title\":\"Majority voting protocol\",\"postcondition\":null,\"description\":\"This protocol takes a majority vote. More precisely,\\n                          it computes whether there are initially more agents\\n                          in state Y than N.\"}}";
		this.config = Simulation.parseJSon(jsonString);
		simulation = new Simulation(this.config, new Random(50));
	}

	@Test
	public void testImmediateCorrectConsensus() {

		TreeConfiguration population = new TreeConfiguration(200);
		population.set(config.protocol.statesToIndices.get("Y"), 150);
		population.set(config.protocol.statesToIndices.get("y"), 50);

		SimulationLoopResult result = simulation.handleLoop(population, config.protocol, 500);

		assertEquals(Simulation.CORRECT_CONSENSUS, result.getConvergence());
		assertEquals(0, result.getStepsToConvergence());
	}

	@Test
	public void testCorrectConsensus() {

		TreeConfiguration population = new TreeConfiguration(200);
		population.set(config.protocol.statesToIndices.get("Y"), 190);
		population.set(config.protocol.statesToIndices.get("N"), 10);

		assertEquals(Simulation.CORRECT_CONSENSUS,
				simulation.handleLoop(population, config.protocol, 50000).getConvergence());
	}

	@Test
	public void testNoConsensus() {

		TreeConfiguration population = new TreeConfiguration(200);
		population.set(config.protocol.statesToIndices.get("Y"), 150);
		population.set(config.protocol.statesToIndices.get("n"), 50);

		assertEquals(Simulation.NO_CONSENSUS, simulation.handleLoop(population, config.protocol, 20).getConvergence());
	}

	@Test
	public void testLogarithmicFlockOfBirds() {
		String logarithmicJson = "{\"minConfigSize\":1,\"maxConfigSize\":25,\"sampleSize\":900,\"numSteps\":6500,\"schedulingMode\":\"UniformAgents\",\"increaseAccuracy\":false,\"protocol\":{\"title\":\"Logarithmic flock-of-birds protocol\",\"states\":[\"0\",\"1\",\"2\",\"4\",\"8\",\"10\"],\"transitions\":[{\"name\":\"0, 10 -> 10, 10\",\"pre\":[\"0\",\"10\"],\"post\":[\"10\",\"10\"]},{\"name\":\"1, 1 -> 2, 0\",\"pre\":[\"1\",\"1\"],\"post\":[\"2\",\"0\"]},{\"name\":\"1, 10 -> 10, 10\",\"pre\":[\"1\",\"10\"],\"post\":[\"10\",\"10\"]},{\"name\":\"2, 2 -> 4, 0\",\"pre\":[\"2\",\"2\"],\"post\":[\"4\",\"0\"]},{\"name\":\"2, 8 -> 10, 10\",\"pre\":[\"2\",\"8\"],\"post\":[\"10\",\"10\"]},{\"name\":\"2, 10 -> 10, 10\",\"pre\":[\"2\",\"10\"],\"post\":[\"10\",\"10\"]},{\"name\":\"4, 4 -> 8, 0\",\"pre\":[\"4\",\"4\"],\"post\":[\"8\",\"0\"]},{\"name\":\"4, 8 -> 10, 10\",\"pre\":[\"4\",\"8\"],\"post\":[\"10\",\"10\"]},{\"name\":\"4, 10 -> 10, 10\",\"pre\":[\"4\",\"10\"],\"post\":[\"10\",\"10\"]},{\"name\":\"8, 8 -> 10, 10\",\"pre\":[\"8\",\"8\"],\"post\":[\"10\",\"10\"]},{\"name\":\"8, 10 -> 10, 10\",\"pre\":[\"8\",\"10\"],\"post\":[\"10\",\"10\"]}],\"initialStates\":[\"1\"],\"trueStates\":[\"10\"],\"predicate\":\"C[1] >= 10\",\"leaders\":null,\"precondition\":null,\"postcondition\":null,\"description\":\"xxx\"}}";
		SimulationConfiguration conf = Simulation.parseJSon(logarithmicJson);
		Simulation logarithmicSimulation = new Simulation(conf, new Random(500));

		TreeConfiguration population = new TreeConfiguration(conf.protocol.states.length);
		population.set(conf.protocol.statesToIndices.get("1"), 9);

		SimulationLoopResult result = logarithmicSimulation.handleLoop(population, conf.protocol, conf.numSteps);
		assertEquals(Simulation.CORRECT_CONSENSUS, result.getConvergence());
	}

	@Test
	public void testLola() {
		String json = "{\"minConfigSize\":1,\"maxConfigSize\":25,\"sampleSize\":1,\"numSteps\":65000,\"schedulingMode\":\"UniformMode\",\"increaseAccuracy\":true,\"protocol\":{\"title\":\"Remainder protocol\",\"states\":[\"0\",\"1\",\"2\",\"3\",\"4\",\"5\",\"f\",\"y\"],\"transitions\":[{\"name\":\"0, 0 -> 0, f\",\"pre\":[\"0\",\"0\"],\"post\":[\"0\",\"f\"]},{\"name\":\"0, 1 -> 1, y\",\"pre\":[\"0\",\"1\"],\"post\":[\"1\",\"y\"]},{\"name\":\"0, 2 -> 2, f\",\"pre\":[\"0\",\"2\"],\"post\":[\"2\",\"f\"]},{\"name\":\"0, 3 -> 3, f\",\"pre\":[\"0\",\"3\"],\"post\":[\"3\",\"f\"]},{\"name\":\"0, 4 -> 4, f\",\"pre\":[\"0\",\"4\"],\"post\":[\"4\",\"f\"]},{\"name\":\"0, 5 -> 5, f\",\"pre\":[\"0\",\"5\"],\"post\":[\"5\",\"f\"]},{\"name\":\"0, y -> 0, f\",\"pre\":[\"0\",\"y\"],\"post\":[\"0\",\"f\"]},{\"name\":\"1, 1 -> 2, f\",\"pre\":[\"1\",\"1\"],\"post\":[\"2\",\"f\"]},{\"name\":\"1, 2 -> 3, f\",\"pre\":[\"1\",\"2\"],\"post\":[\"3\",\"f\"]},{\"name\":\"1, 3 -> 4, f\",\"pre\":[\"1\",\"3\"],\"post\":[\"4\",\"f\"]},{\"name\":\"1, 4 -> 5, f\",\"pre\":[\"1\",\"4\"],\"post\":[\"5\",\"f\"]},{\"name\":\"1, 5 -> 0, f\",\"pre\":[\"1\",\"5\"],\"post\":[\"0\",\"f\"]},{\"name\":\"1, f -> 1, y\",\"pre\":[\"1\",\"f\"],\"post\":[\"1\",\"y\"]},{\"name\":\"2, 2 -> 4, f\",\"pre\":[\"2\",\"2\"],\"post\":[\"4\",\"f\"]},{\"name\":\"2, 3 -> 5, f\",\"pre\":[\"2\",\"3\"],\"post\":[\"5\",\"f\"]},{\"name\":\"2, 4 -> 0, f\",\"pre\":[\"2\",\"4\"],\"post\":[\"0\",\"f\"]},{\"name\":\"2, 5 -> 1, y\",\"pre\":[\"2\",\"5\"],\"post\":[\"1\",\"y\"]},{\"name\":\"2, y -> 2, f\",\"pre\":[\"2\",\"y\"],\"post\":[\"2\",\"f\"]},{\"name\":\"3, 3 -> 0, f\",\"pre\":[\"3\",\"3\"],\"post\":[\"0\",\"f\"]},{\"name\":\"3, 4 -> 1, y\",\"pre\":[\"3\",\"4\"],\"post\":[\"1\",\"y\"]},{\"name\":\"3, 5 -> 2, f\",\"pre\":[\"3\",\"5\"],\"post\":[\"2\",\"f\"]},{\"name\":\"3, y -> 3, f\",\"pre\":[\"3\",\"y\"],\"post\":[\"3\",\"f\"]},{\"name\":\"4, 4 -> 2, f\",\"pre\":[\"4\",\"4\"],\"post\":[\"2\",\"f\"]},{\"name\":\"4, 5 -> 3, f\",\"pre\":[\"4\",\"5\"],\"post\":[\"3\",\"f\"]},{\"name\":\"4, y -> 4, f\",\"pre\":[\"4\",\"y\"],\"post\":[\"4\",\"f\"]},{\"name\":\"5, 5 -> 4, f\",\"pre\":[\"5\",\"5\"],\"post\":[\"4\",\"f\"]},{\"name\":\"5, y -> 5, f\",\"pre\":[\"5\",\"y\"],\"post\":[\"5\",\"f\"]}],\"initialStates\":[\"0\",\"1\",\"2\",\"3\",\"4\",\"5\"],\"trueStates\":[\"1\",\"y\"],\"predicate\":\"(((((0 * C[0] + 1 * C[1]) + 2 * C[2]) + 3 * C[3]) + 4 * C[4]) + 5 * C[5]) %=_6 1\",\"leaders\":null,\"precondition\":null,\"postcondition\":null,\"description\":\"This protocol tests whether0*C[0] + 1*C[1] + ... + (m-1)*C[m-1]  c (mod m).\"}}";
		SimulationConfiguration conf = Simulation.parseJSon(json);
		Simulation sim = new Simulation(conf, new Random(500));

		TreeConfiguration population = new TreeConfiguration(conf.protocol.states.length);
		population.set(conf.protocol.statesToIndices.get("0"), 9);
	}

	@Test
	public void testFlockOfBirds() {
		String json = "{\"sampleSize\":900,\"maxConfigSize\":25,\"protocol\":{\"transitions\":[{\"pre\":[\"0\",\"10\"],\"post\":[\"10\",\"10\"],\"name\":\"0, 10 -> 10, 10\"},{\"pre\":[\"1\",\"1\"],\"post\":[\"0\",\"2\"],\"name\":\"1, 1 -> 2, 0\"},{\"pre\":[\"1\",\"10\"],\"post\":[\"10\",\"10\"],\"name\":\"1, 10 -> 10, 10\"},{\"pre\":[\"2\",\"2\"],\"post\":[\"0\",\"4\"],\"name\":\"2, 2 -> 4, 0\"},{\"pre\":[\"2\",\"8\"],\"post\":[\"10\",\"10\"],\"name\":\"2, 8 -> 10, 10\"},{\"pre\":[\"2\",\"10\"],\"post\":[\"10\",\"10\"],\"name\":\"2, 10 -> 10, 10\"},{\"pre\":[\"4\",\"4\"],\"post\":[\"0\",\"8\"],\"name\":\"4, 4 -> 8, 0\"},{\"pre\":[\"4\",\"8\"],\"post\":[\"10\",\"10\"],\"name\":\"4, 8 -> 10, 10\"},{\"pre\":[\"4\",\"10\"],\"post\":[\"10\",\"10\"],\"name\":\"4, 10 -> 10, 10\"},{\"pre\":[\"8\",\"8\"],\"post\":[\"10\",\"10\"],\"name\":\"8, 8 -> 10, 10\"},{\"pre\":[\"8\",\"10\"],\"post\":[\"10\",\"10\"],\"name\":\"8, 10 -> 10, 10\"}],\"states\":[\"0\",\"1\",\"2\",\"4\",\"8\",\"10\"],\"precondition\":null,\"initialStates\":[\"1\"],\"leaders\":null,\"predicate\":\"C[1] >= 10\",\"trueStates\":[\"10\"],\"title\":\"Logarithmic flock-of-birds protocol\",\"postcondition\":null,\"description\":\"xxx\"},\"increaseAccuracy\":false,\"numSteps\":650,\"minConfigSize\":1}";
		SimulationConfiguration conf = Simulation.parseJSon(json);
		Simulation sim = new Simulation(conf, new Random(500));

		TreeConfiguration population = new TreeConfiguration(conf.protocol.states.length);
		population.set(conf.protocol.statesToIndices.get("0"), 9);
	}

	@Test
	public void testMain() throws IOException {
		String filepath = "test_data/FlockOfBirds.json";
		Simulation.main(new String[] { filepath });
	}
	
	@Test
	public void testMainWithInitialConfiguration() throws IOException {
		String filepath = "test_data/FlockOfBirdsWithInitialConfig.json";
		Simulation.main(new String[] { filepath });
	}
}
