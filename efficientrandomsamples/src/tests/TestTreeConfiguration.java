package src.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import src.efficientrandomsamples.Simulation;
import src.efficientrandomsamples.SimulationConfiguration;
import src.efficientrandomsamples.TreeConfiguration;

class TestTreeConfiguration {

	private SimulationConfiguration config;

	@BeforeEach
	public void setUp() {
		String jsonString = "{\"minConfigSize\":200,\"maxConfigSize\":200,\"sampleSize\":2,\"numSteps\":2000,\"schedulingMode\":\"UniformMode\",\"increaseAccuracy\":false,\"protocol\":{\"transitions\":[{\"pre\":[\"Y\",\"N\"],\"post\":[\"y\",\"n\"],\"name\":\"Y, N -> y, n\"},{\"pre\":[\"Y\",\"n\"],\"post\":[\"Y\",\"y\"],\"name\":\"Y, n -> Y, y\"},{\"pre\":[\"N\",\"y\"],\"post\":[\"N\",\"n\"],\"name\":\"N, y -> N, n\"},{\"pre\":[\"y\",\"n\"],\"post\":[\"y\",\"y\"],\"name\":\"y, n -> y, y\"}],\"states\":[\"Y\",\"N\",\"y\",\"n\"],\"precondition\":null,\"initialStates\":[\"Y\",\"N\"],\"predicate\":\"C[Y] >= C[N]\",\"trueStates\":[\"Y\",\"y\"],\"title\":\"Majority voting protocol\",\"postcondition\":null,\"description\":\"This protocol takes a majority vote. More precisely,\\n                          it computes whether there are initially more agents\\n                          in state Y than N.\"}}";
		this.config = Simulation.parseJSon(jsonString);
	}

	@Test
	public void testSize() {

		TreeConfiguration population = new TreeConfiguration(4);
		population.set(0, 20);
		population.set(3, 20);

		assertEquals(20, population.get(0));
		assertEquals(20, population.get(3));
	}
}
