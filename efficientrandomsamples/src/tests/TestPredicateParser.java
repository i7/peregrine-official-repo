package src.tests;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import src.efficientrandomsamples.predicateparser.ParserFormatException;
import src.efficientrandomsamples.predicateparser.PredicateParser;

class TestPredicateParser {

	@Test
	public void testConstants() {
		assertTrue(PredicateParser.evaluatePredicate("true"));
		assertFalse(PredicateParser.evaluatePredicate("false"));
	}

	@Test
	public void testBooleanOperations() {
		assertTrue(PredicateParser.evaluatePredicate("true && true"));
		assertFalse(PredicateParser.evaluatePredicate("!false && false"));
		assertFalse(PredicateParser.evaluatePredicate("!((!false && true) && true)"));
		assertTrue(PredicateParser.evaluatePredicate("false -> true"));
	}

	@Test
	public void testArithmeticOperations() {
		assertTrue(PredicateParser.evaluatePredicate("(3>2)"));
		assertFalse(PredicateParser.evaluatePredicate("(3=2)"));
		assertTrue(PredicateParser.evaluatePredicate("(3+2=5)"));
		assertTrue(PredicateParser.evaluatePredicate("(4*3=12) && (4-3=1)"));
		assertFalse(PredicateParser.evaluatePredicate("(10+5+5+10*5=10)"));
		assertTrue(PredicateParser.evaluatePredicate("(5*3=2) -> (false || true)"));
		assertFalse(PredicateParser.evaluatePredicate("!(3>2) && !!!5*(2*(4*(4*(4*4*4*4))))=3"));
	}

	@Test
	public void testModulo() {
		assertTrue(PredicateParser.evaluatePredicate("6 %=_3 3"));
		assertTrue(PredicateParser.evaluatePredicate("10 %=_4 30"));
		assertTrue(PredicateParser.evaluatePredicate("10+5+7 %=_5 10+10+10-3"));

	}

	@Test
	public void largeTests() {
		assertTrue(PredicateParser
				.evaluatePredicate("true || (!(!(10+5+7 %=_5 10+10+10-3)) && !!!(!(!(!!true && false)) && 5 "
						+ "!= 3 || !! 2 + 4 >= 10))"));
		assertTrue(PredicateParser
				.evaluatePredicate("false || (!(!(10+5+7 %=_5 10+10+10-3)) && !!!(!!!(!(!!true && (true -> "
						+ "(false || 5 = 3)))) && 5 != 3 || !! 2 + 4 >= 10))"));
	}

	@Test
	public void testLargeModuloPredicate() {
		// similar to modulo predicate, with m=100
		assertTrue(PredicateParser
				.evaluatePredicate("((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((("
						+ "(((((((((((((((((((((((((((0 * 0)+ 1 * 1)+ 2 * 2)+ 3 * 3)+ 4 * 4)+ 5 * "
						+ "5)+ 6 * 6)+ 7 * 7)+ 8 * 8)+ 9 * 9)+ 10 * 10)+ 11 * 11)+ 12 * 12)+ 13 * 13"
						+ ")+ 14 * 14)+ 15 * 15)+ 16 * 16)+ 17 * 17)+ 18 * 18)+ 19 * 19)+ 20 * 20)+"
						+ " 21 * 21)+ 22 * 22)+ 23 * 23)+ 24 * 24)+ 25 * 25)+ 26 * 26)+ 27 * 27)+ "
						+ "28 * 28)+ 29 * 29)+ 30 * 30)+ 31 * 31)+ 32 * 32)+ 33 * 33)+ 34 * 34)+ 35"
						+ " * 35)+ 36 * 36)+ 37 * 37)+ 38 * 38)+ 39 * 39)+ 40 * 40)+ 41 * 41)+ 42 *"
						+ " 42)+ 43 * 43)+ 44 * 44)+ 45 * 45)+ 46 * 46)+ 47 * 47)+ 48 * 48)+ 49 * 49"
						+ ")+ 50 * 50)+ 51 * 51)+ 52 * 52)+ 53 * 53)+ 54 * 54)+ 55 * 55)+ 56 * 56)+"
						+ " 57 * 57)+ 58 * 58)+ 59 * 59)+ 60 * 60)+ 61 * 61)+ 62 * 62)+ 63 * 63)+ 64"
						+ " * 64)+ 65 * 65)+ 66 * 66)+ 67 * 67)+ 68 * 68)+ 69 * 69)+ 70 * 70)+ 71 * "
						+ "71)+ 72 * 72)+ 73 * 73)+ 74 * 74)+ 75 * 75)+ 76 * 76)+ 77 * 77)+ 78 * 78)+"
						+ " 79 * 79)+ 80 * 80)+ 81 * 81)+ 82 * 82)+ 83 * 83)+ 84 * 84)+ 85 * 85)+ 86 "
						+ "* 86)+ 87 * 87)+ 88 * 88)+ 89 * 89)+ 90 * 90)+ 91 * 91)+ 92 * 92)+ 93 * 93"
						+ ")+ 94 * 94)+ 95 * 95)+ 96 * 96)+ 97 * 97)+ 98 * 98)+ 99 * 99) %=_100 50"));
	}
	
	@Test
	public void testNegativeNumbers() {
		assertTrue(PredicateParser.evaluatePredicate("-4 = 0-4"));
		assertTrue(PredicateParser.evaluatePredicate("-1 * 5 = -5"));
		assertTrue(PredicateParser.evaluatePredicate("--1 - 5 = -4"));
	}

	@Test
	public void testException() {
		assertThrows(ParserFormatException.class, () -> PredicateParser.evaluatePredicate("6 = 3 = 3"));
	}
}
