from multiset import Multiset
from protocol import Protocol
from transition import Transition

def name():
    return "EvDead_too_weak"

# small example where EvDead is insufficient

def generate():
    Q = {"A", "B", "c", "d"}
    T = {Transition(("A", "B"), ("A", "A")),
         Transition(("A", "c"), ("B", "c")),
         Transition(["c"], ["d"]),
         Transition(["d"], ["c"])}
    S = {"A", "B"}
    I = {q: q for q in Q}
    O = {q: 1 for q in Q}

    L = Multiset(["c"])

    p = "true"
    post = "(= A 0)"

    return Protocol(Q, T, S, I, O, p, leaders=L, postcondition=post)
