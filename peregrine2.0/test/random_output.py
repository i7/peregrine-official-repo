from protocol import Protocol
from transition import Transition

def name():
    return "Random_output (i.e. not well-specified)"

def generate():
    Q = {"0", "1"}
    T = {Transition(("0", "1"), ("0", "0")),
         Transition(("0", "1"), ("1", "1"))}
    S = {"0", "1"}
    I = {"0": "0", "1": "1"}
    O = {"0": 0, "1": 1}
    p = "true"

    return Protocol(Q, T, S, I, O, p)
