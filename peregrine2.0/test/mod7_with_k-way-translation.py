from protocol import Protocol
from transition import Transition
from multiset import Multiset

def name():
    return "x + y % 7 = 0 (logaritmic, k-way trans translated)"

def generate():
    Q = {"x0+", "x0-", "x1", "x2", "x4", "x8", "X", "Y", "d", "a", "b"}
    T = {Transition(("X", "x0+"), ("x1", "x0+")),
         Transition(("X", "x0-"), ("x1", "x0-")),
         Transition(("Y", "x0+"), ("x1", "x0+")),
         Transition(("Y", "x0-"), ("x1", "x0-")),
         Transition(("x1", "x1"), ("x2", "x0-")),
         Transition(("x2", "x0+"), ("x1", "x1")),
         Transition(("x2", "x0-"), ("x1", "x1")),
         Transition(("x2", "x2"), ("x4", "x0-")),
         Transition(("x4", "x0+"), ("x2", "x2")),
         Transition(("x4", "x0-"), ("x2", "x2")),
         Transition(("x4", "x4"), ("x8", "x0-")),
         Transition(("x8", "x0+"), ("x4", "x4")),
         Transition(("x8", "x0-"), ("x4", "x4")),
         Transition(("x1", "x0+"), ("x1", "x0-")),
         Transition(("x2", "x0+"), ("x2", "x0-")),
         Transition(("x4", "x0+"), ("x4", "x0-")),
         Transition(("x8", "x0+"), ("x8", "x0-")),
         Transition(("x0-", "x0+"), ("x0+", "x0+")),
         Transition(("x1", "x2"), ("d", "a")),
         Transition(("d", "a"), ("x1", "x2")),
         Transition(("a", "x4"), ("b", "x0+")),
         Transition(("d", "b"), ("x0+", "x0+"))}
    S = {"X", "Y"}
    I = {q: q for q in Q}
    O = {q: True if q == "x0+" else False for q in Q}
    p = "(= (mod (+ X Y) 7) 0)"
    L = Multiset(["x0+"] * 10)

    return Protocol(Q, T, S, I, O, p, leaders=L)
