from multiset import Multiset
from protocol import Protocol
from transition import Transition

def name():
    return "PotReach_iterative_too_weak"

# small example of "modulo_log_new.py" where iterative PotReach is insufficient

def generate():
    Q = {"q0", "q1", "q2", "qt", "qf"}
    T = {Transition(("q1", "q1"), ("q2", "q0")),
         Transition(("q2", "q0"), ("q1", "q1")),
         Transition(("q2", "qf"), ("q2", "qt")),
         Transition(("q2", "q1", "qt"), ("q0", "q0", "qf"))}
    S = {"q1"}
    I = {q: q for q in Q}
    O = {q: 1 for q in Q}

    L = Multiset(["qf"])

    p = "true"
    pre = "(< (mod q1 3) 2)"
    post = "(= qt 0)"

    return Protocol(Q, T, S, I, O, p, leaders=L, precondition=pre, postcondition=post)
