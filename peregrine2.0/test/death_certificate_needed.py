from protocol import Protocol
from transition import Transition

def name():
    return "Needs a death certificate not a siphon for splitting"

def generate():
    Q = {"0", "1", "2"}
    T = {Transition(("0", "0", "0"), ("1", "1", "2")),
         Transition(("0", "0", "0"), ("1", "2", "2")),
         Transition(("1", "1"), ("1", "1")),
         Transition(("2", "2"), ("2", "2")),
         }
    S = {"x"}
    I = {"x": "0", "q1": 1, "q2": 2}
    O = {"0": 0, "1": 1, "2": 0}
    p = "true"
    pre = "(= x 3)"

    return Protocol(Q, T, S, I, O, p, precondition=pre)
