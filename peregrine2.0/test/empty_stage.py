from protocol import Protocol
from transition import Transition

def name():
    return "Protocol producing empty stage when not using backwards coverability"

def generate():
    Q = {"1", "2", "3"}
    T = {Transition(pre=("1"), post=("2")),
         Transition(pre=("2"), post=("3")),
         Transition(pre=("2", "3"), post=("1", "3"))}
    S = {"x"}
    I = {"x": "1", "q1": "2", "q2": "3"}
    O = {"1": 0, "2": 1, "3": 1}
    p = "true"
    pre = "(= x 1)"

    return Protocol(Q, T, S, I, O, p, precondition=pre)
