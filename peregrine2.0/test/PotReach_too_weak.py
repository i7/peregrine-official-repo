from multiset import Multiset
from protocol import Protocol
from transition import Transition

def name():
    return "PotReach_too_weak"

# small example of "modulo_log_new.py" where PotReach is insufficient

def generate():
    Q = {"q0", "q1", "q2", "qt", "qf"}
    T = {Transition(("q1", "q1"), ("q2", "q0")),
         Transition(("q2", "q0"), ("q1", "q1")),
         Transition(("q2", "qf"), ("q2", "qt"))}
    S = {"q1"}
    I = {q: q for q in Q}
    O = {q: (1 if q == "qt" else 0) for q in Q}

    L = Multiset(["qf"] + ["q0"] * 10)

    p = "true"
    pre = "(= q1 1)"
    post = "(= qt 0)"

    return Protocol(Q, T, S, I, O, p, leaders=L, preconditions=pre, postconditions=post)
