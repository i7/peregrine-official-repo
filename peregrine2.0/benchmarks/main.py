# -*- coding: utf-8 -*-
import argparse
import json
import multiprocessing
import shlex
import sys
import resource

DESCRIPTION = "Benchmarking utility."

sys.path.append("..")
sys.path.append("../src/")
from src.main import execute, args_parser

def benchmark_single(parameter_string, timeout=None):
    args = args_parser().parse_args(shlex.split("../" + parameter_string))

    # change path for referenced files by adding "../" to simulate execution in parent directory
    # -> same parameter string as normal call
    args.hoaparser[0] = "../" + args.hoaparser[0]
    if args.buchi:
        args.buchi[0] = "../" + args.buchi[0]

    def run():
        print(execute(args))

    process = multiprocessing.Process(target=run)

    process.start()
    process.join(timeout)

    if process.is_alive():
        process.terminate()
        process.join()
        print(json.dumps({"timeout": timeout, "call": parameter_string, "protocol_path": args.protocol[0],
               "protocol_parameters": args.protocol[1] if len(args.protocol) > 1 else ""}))
        return False
    return True



if __name__ == "__main__":
    parser = argparse.ArgumentParser()    

    parser.add_argument("config", metavar="config", type=str,
                        help=("benchmarks JSON configuration file"))
    parser.add_argument("-t", "--timeout", type=int, metavar="sec", default=3600,
                        help=("Set the timeout (in sec) for each entry. (default: 3600)"))
    parser.add_argument("-RAM", "--RAM_limit", type=int, metavar="LIMIT", default=0,
                        help=("Set an optional hard limit for RAM usage (in MB)."))

    args  = parser.parse_args()

    if args.RAM_limit > 0:
        resource.setrlimit(resource.RLIMIT_AS, (args.RAM_limit * 1024 * 1024, args.RAM_limit * 1024 * 1024))

    print("[")

    file = args.config
    first = True
    global_para = ""
    skipping = False
    with open(file) as f:
        line = f.readline()
        while line:
            if len(line) == 0 or line.isspace() or line[0] == '#':  # empty or comment -> skip
                line = f.readline()
                skipping = False
                continue
            if line[0] == '!':  # empty or comment -> skip
                global_para = " " + line[1:]
                line = f.readline()
                continue
            if not first:
                print(",", end = " ")
            worked = benchmark_single(line.strip(), args.timeout if not skipping else 1)
            if not worked:
                skipping = True
            first = False
            line = f.readline()

    print("]")
