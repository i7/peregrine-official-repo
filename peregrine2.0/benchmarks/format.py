# -*- coding: utf-8 -*-
import argparse
import json


def table(data):
    TEMPLATE = "  {:40} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {} & {}\\\\"

    print("\\begin{tabular}{|r|r|r|r|r|r|r|r|r|r|r|r|r|r|}")
    print("  \\hline")
    print(TEMPLATE.format("Protocol", "$|Q|$", "$|T|$", "Property", "Result", "Time", "Speed", "Stages", "Roots", "Width", "Depth", "-over", "-e", "parameters"))
    print("  \\hline")

    def sanitize(s):
        return s.replace('"[', "``[").replace(']"', "]''").replace('_', "\\_")\
            .replace("->", "$\\rightarrow$").replace("&", "\\&")

    for d in data:

        parameters = sanitize(d["protocol_parameters"])
        path = sanitize(d["protocol_path"])

        if "timeout" not in d:
            name = d["protocol"]["name"]
            states = d["protocol"]["|Q|"]
            transitions = f'{d["protocol"]["orig_|T|"]} ({d["protocol"]["|T|"]})'
            property = d["ltl"][0] if d["ltl"] \
                else sanitize(d["buchi"][0]) if d["buchi"] \
                else "pre -> post" if "output_check" in d["settings"] and d["settings"]["output_check"] == "no check"  \
                else "consensus" if "output_check" in d["settings"] and d["settings"]["output_check"] == "termination" \
                else "correctness" if "output_check" in d["settings"] and d["settings"]["output_check"] == "correctness" \
                else "-"

            result = "{\\color{green} verified}" if d["verified"] else "{\\color{red} not verified} "
            t = d["elapsed"]["graphs"]
            time = "$<1s$" if float(t) < 1 else f'${round(float(t))}s$'
            speed = "$" + d["graph"]["speed_pretty"] + "$"
            stages = d["graph"]["nr_of_stages"]
            roots = d["graph"]["nr_of_roots"]
            width = d["graph"]["rn_of_terminal_stages"]
            depth = d["graph"]["depth"]
            over = d["settings"]["deadoverapprox"] if "deadoverapprox" in d["settings"] else "-"
            ev = d["settings"]["eventually_dead_method"].replace("->", "$\\rightarrow$")

        else:
            name = sanitize(d["call"])
            states = ""
            transitions = ""
            property = ""
            result = "{\\color{red} timeout} "
            time = "timeout"
            speed =  ""
            stages =  ""
            roots =  ""
            width =  ""
            depth =  ""
            over = ""
            ev = ""

        print(TEMPLATE.format(name, states, transitions, property, result, time, speed, stages, roots, width, depth, over, ev, parameters))

    print("  \\hline")
    print("\\end{tabular}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser() 

    parser.add_argument("data", metavar="data", type=str,
                        help=("benchmarks data filename"))

    args = parser.parse_args()

    with open(args.data) as data_file:
        data = json.load(data_file)

    print("\\documentclass[border = 12pt]{standalone}")
    print("\\usepackage[utf8]{inputenc}")
    print("\\usepackage{color}")
    print("\\usepackage{multirow}")
    print("\\begin{document}")
    print("\\centering")
    table(data)
    print("\\end{document}")
