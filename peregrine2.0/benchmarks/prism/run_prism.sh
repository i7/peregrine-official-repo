#!/bin/bash

RESULT_FILE=results.csv
OUT_DIR=results
memorylimit=8
timelimit=3600

#parse command line arguments
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -t|--timelimit)
    timelimit="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--memorylimit)
    memorylimit="$2"
    shift # past argument
    shift # past value
    ;;
    -b|--benchmark)
    benchmarking=true
    RESULT_FILE=$2
    shift # past argument
    shift # past value
    ;;
    -o|--output)
    OUT_DIR=$2
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    show_help=true
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [ "$show_help" == true ]; then
    echo "Usage: $0 [FLAGS] [OPTIONS] <BENCHMARK_DIR> <PROPERTY_FILE>"
    echo "FLAGS:"
    echo "    -h, --help                Display this help"
    echo "OPTIONS:"
    echo "    -o, --output <OUT_DIR>    Output all intermediate files to OUT_DIR"
    echo "    -b, --benchmark <RESULTS> Output benchmarking information to RESULTS"
    echo "    -m, --memorylimit <LIMIT> Enforce a memory limit of LIMIT GB"
    echo "    -t, --timelimit <LIMIT>   Enforce a time limit of LIMIT seconds"
    echo "ARGS:"
    echo "    <BENCHMARK_DIR>           The directory containing the benchmark files"
    echo "    <PROPERTY_FILE>           The property file to use"
    exit 0
fi

if [[ ${#POSITIONAL[@]} -lt 2 ]]; then
    echo "Error: No benchmark directory and property file specified"
    exit 1
fi

BENCHMARK_DIR=${POSITIONAL[0]}
PROPERTY_FILE=${POSITIONAL[1]}

if [[ ! $memorylimit =~ ^[1-9][0-9]*$ ]]; then
    echo "Error: The memory limit has to be a positive integer: $memorylimit"
    exit 1
fi

if [[ ! $timelimit =~ ^[1-9][0-9]*$ ]]; then
    echo "Error: The time limit has to be a positive integer: $timelimit"
    exit 1
fi

if [ ! -d $BENCHMARK_DIR ]; then
    echo "Error: The benchmark directory does not exist or is not a folder: $BENCHMARK_DIR"
    exit 1
fi
if [ ! -f $PROPERTY_FILE ]; then
    echo "Error: The property file does not exist or is not a file: $PROPERTY_FILE"
    exit 1
fi

# calucate memory limits from GB to byte
mem_soft=$(($memorylimit * 1024 * 1024))
mem_hard=$(($mem_soft + 1024))
# extra time limit for hard kill
time_hard=10

# path and options for prism
PRISM=prism
PRISM_OPTIONS=''

function run_prism {
    MODEL_FILE=$1

    BASE=$(basename ${MODEL_FILE%.pm})
    OUT_FILE=$OUT_DIR/$BASE.out

    echo -n "$MODEL_FILE" >>$RESULT_FILE
    runtime="$(date +%s%N)"
    (
        set -o pipefail
        ulimit -S -v $mem_soft
        ulimit -H -v $mem_hard
        echo $PRISM $PRISM_OPTIONS $MODEL_FILE $PROPERTY_FILE
        timeout -k $time_hard $timelimit $PRISM $PRISM_OPTIONS $MODEL_FILE $PROPERTY_FILE 2>&1 | tee $OUT_FILE
    ) 2>/dev/null
    result=$?
    runtime=$(( ($(date +%s%N)-runtime) / 1000000 ))
    #runtime=$(bc -l <<< "scale=2; $runtime/1000000000")
    if grep -q "Error" $OUT_FILE; then
        runresult='ERROR'
    elif [[ $result -eq 0 ]]; then
        runresult="SUCCESS"
    elif [[ $result -eq 124 ]] || [[ result -eq 137 ]]; then
        runresult='TIMEOUT'
    else
        runresult='ERROR'
    fi
    echo ",$runresult,$runtime" >>$RESULT_FILE
}

mkdir -p $OUT_DIR

for FILE in $(find $BENCHMARK_DIR -mindepth 1 -type f -name "*.pm" | sort); do
    echo "Benchmarking $FILE with property file $PROPERTY_FILE"
    run_prism $FILE
done
