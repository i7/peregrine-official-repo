dtmc
global flag1  : [0..1];
global flag2  : [0..1];
module process1
q1 : [1..7];
[] (q1=1) -> (q1'=2) & (flag1'=0);
[] (q1=2) -> (q1'=3);
[] (q1=3) -> (q1'=4) & (flag1'=1);
[] (q1=4) -> (q1'=5);
[] (q1=5) & (flag2=0) -> (q1'=6);
[] (q1=6) -> (q1'=7) & (flag1'=0);
[] (q1=7) -> (q1'=1);
endmodule
module process2
q2 : [1..7];
[] (q2=1) -> (q2'=2) & (flag2'=0);
[] (q2=2) & (flag1=0) -> (q2'=3);
[] (q2=2) & (flag1=1) -> (q2'=1);
[] (q2=3) -> (q2'=4) & (flag2'=1);
[] (q2=4) & (flag1=0) -> (q2'=5);
[] (q2=4) & (flag1=1) -> (q2'=1);
[] (q2=5) -> (q2'=6);
[] (q2=6) -> (q2'=7) & (flag2'=0);
[] (q2=7) -> (q2'=1);
endmodule
label "cs" = (q1=6);
init
(q1=1) & (q2=1)
endinit
