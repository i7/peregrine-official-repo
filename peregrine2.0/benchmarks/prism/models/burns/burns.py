import sys

n = int(sys.argv[1])

print('dtmc')

for i in range(1,n+1):
    print(f'global flag{i}  : [0..1];')

for i in range(1,n+1):
    print(f'module process{i}')

    # state q for each process i is 1,2,3,4,5,6,7
    print(f'q{i} : [1..7];')

    print(f"[] (q{i}=1) -> (q{i}'=2) & (flag{i}'=0);")

    print(f"[] (q{i}=2)",end='')
    for j in range(1,i):
        print(f" & (flag{j}=0)", end='')
    print(f" -> (q{i}'=3);")

    for j in range(1,i):
        print(f"[] (q{i}=2) & (flag{j}=1) -> (q{i}'=1);")

    print(f"[] (q{i}=3) -> (q{i}'=4) & (flag{i}'=1);")

    print(f"[] (q{i}=4)",end='')
    for j in range(1,i):
        print(f" & (flag{j}=0)", end='')
    print(f" -> (q{i}'=5);")

    for j in range(1,i):
        print(f"[] (q{i}=4) & (flag{j}=1) -> (q{i}'=1);")

    print(f"[] (q{i}=5)",end='')
    for j in range(i+1,n+1):
        print(f" & (flag{j}=0)", end='')
    print(f" -> (q{i}'=6);")

    print(f"[] (q{i}=6) -> (q{i}'=7) & (flag{i}'=0);")

    print(f"[] (q{i}=7) -> (q{i}'=1);")

    print('endmodule')

# label - first process in critical section
print('label "cs" = (q1=6);')

# initial states (each process starts in state 0)
print('init')
print("(q1=1)", end='')
for i in range(2,n+1):
    print(f" & (q{i}=1)", end='')
print()
print('endinit')

