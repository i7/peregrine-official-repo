// Israeli-Jalfon self stabilising algorithm
// dxp/gxn 10/06/02

dtmc

// variables to represent whether a process has a token or not
// note they are global because they can be updated by other processes
global q1  : [0..1];
global q2  : [0..1];
global q3  : [0..1];
global q4  : [0..1];
global q5  : [0..1];
global q6  : [0..1];
global q7  : [0..1];
global q8  : [0..1];
global q9  : [0..1];
global q10  : [0..1];
global q11  : [0..1];
global q12  : [0..1];
global q13  : [0..1];
global q14  : [0..1];
global q15  : [0..1];
global q16  : [0..1];
global q17  : [0..1];
global q18  : [0..1];
global q19  : [0..1];
global q20  : [0..1];
global q21  : [0..1];
global q22  : [0..1];
global q23  : [0..1];
global q24  : [0..1];
global q25  : [0..1];
global q26  : [0..1];
global q27  : [0..1];
global q28  : [0..1];
global q29  : [0..1];
global q30  : [0..1];
global q31  : [0..1];
global q32  : [0..1];
global q33  : [0..1];
global q34  : [0..1];
global q35  : [0..1];
global q36  : [0..1];
global q37  : [0..1];
global q38  : [0..1];
global q39  : [0..1];
global q40  : [0..1];
global q41  : [0..1];
global q42  : [0..1];
global q43  : [0..1];
global q44  : [0..1];
global q45  : [0..1];
global q46  : [0..1];
global q47  : [0..1];
global q48  : [0..1];
global q49  : [0..1];
global q50  : [0..1];
global q51  : [0..1];
global q52  : [0..1];
global q53  : [0..1];
global q54  : [0..1];
global q55  : [0..1];
global q56  : [0..1];
global q57  : [0..1];
global q58  : [0..1];
global q59  : [0..1];
global q60  : [0..1];
global q61  : [0..1];
global q62  : [0..1];
global q63  : [0..1];
global q64  : [0..1];
global q65  : [0..1];
global q66  : [0..1];
global q67  : [0..1];
global q68  : [0..1];
global q69  : [0..1];
global q70  : [0..1];

// module of process 1
module process1
	
	[] (q1=1) -> 0.5 : (q1'=0) & (q70'=1) + 0.5 : (q1'=0) & (q2'=1);
	
endmodule

// add further processes through renaming
module process2 = process1 [ q1=q2, q2=q3, q70=q1 ] endmodule
module process3 = process1 [ q1=q3, q2=q4, q70=q2 ] endmodule
module process4 = process1 [ q1=q4, q2=q5, q70=q3 ] endmodule
module process5 = process1 [ q1=q5, q2=q6, q70=q4 ] endmodule
module process6 = process1 [ q1=q6, q2=q7, q70=q5 ] endmodule
module process7 = process1 [ q1=q7, q2=q8, q70=q6 ] endmodule
module process8 = process1 [ q1=q8, q2=q9, q70=q7 ] endmodule
module process9 = process1 [ q1=q9, q2=q10, q70=q8 ] endmodule
module process10 = process1 [ q1=q10, q2=q11, q70=q9 ] endmodule
module process11 = process1 [ q1=q11, q2=q12, q70=q10 ] endmodule
module process12 = process1 [ q1=q12, q2=q13, q70=q11 ] endmodule
module process13 = process1 [ q1=q13, q2=q14, q70=q12 ] endmodule
module process14 = process1 [ q1=q14, q2=q15, q70=q13 ] endmodule
module process15 = process1 [ q1=q15, q2=q16, q70=q14 ] endmodule
module process16 = process1 [ q1=q16, q2=q17, q70=q15 ] endmodule
module process17 = process1 [ q1=q17, q2=q18, q70=q16 ] endmodule
module process18 = process1 [ q1=q18, q2=q19, q70=q17 ] endmodule
module process19 = process1 [ q1=q19, q2=q20, q70=q18 ] endmodule
module process20 = process1 [ q1=q20, q2=q21, q70=q19 ] endmodule
module process21 = process1 [ q1=q21, q2=q22, q70=q20 ] endmodule
module process22 = process1 [ q1=q22, q2=q23, q70=q21 ] endmodule
module process23 = process1 [ q1=q23, q2=q24, q70=q22 ] endmodule
module process24 = process1 [ q1=q24, q2=q25, q70=q23 ] endmodule
module process25 = process1 [ q1=q25, q2=q26, q70=q24 ] endmodule
module process26 = process1 [ q1=q26, q2=q27, q70=q25 ] endmodule
module process27 = process1 [ q1=q27, q2=q28, q70=q26 ] endmodule
module process28 = process1 [ q1=q28, q2=q29, q70=q27 ] endmodule
module process29 = process1 [ q1=q29, q2=q30, q70=q28 ] endmodule
module process30 = process1 [ q1=q30, q2=q31, q70=q29 ] endmodule
module process31 = process1 [ q1=q31, q2=q32, q70=q30 ] endmodule
module process32 = process1 [ q1=q32, q2=q33, q70=q31 ] endmodule
module process33 = process1 [ q1=q33, q2=q34, q70=q32 ] endmodule
module process34 = process1 [ q1=q34, q2=q35, q70=q33 ] endmodule
module process35 = process1 [ q1=q35, q2=q36, q70=q34 ] endmodule
module process36 = process1 [ q1=q36, q2=q37, q70=q35 ] endmodule
module process37 = process1 [ q1=q37, q2=q38, q70=q36 ] endmodule
module process38 = process1 [ q1=q38, q2=q39, q70=q37 ] endmodule
module process39 = process1 [ q1=q39, q2=q40, q70=q38 ] endmodule
module process40 = process1 [ q1=q40, q2=q41, q70=q39 ] endmodule
module process41 = process1 [ q1=q41, q2=q42, q70=q40 ] endmodule
module process42 = process1 [ q1=q42, q2=q43, q70=q41 ] endmodule
module process43 = process1 [ q1=q43, q2=q44, q70=q42 ] endmodule
module process44 = process1 [ q1=q44, q2=q45, q70=q43 ] endmodule
module process45 = process1 [ q1=q45, q2=q46, q70=q44 ] endmodule
module process46 = process1 [ q1=q46, q2=q47, q70=q45 ] endmodule
module process47 = process1 [ q1=q47, q2=q48, q70=q46 ] endmodule
module process48 = process1 [ q1=q48, q2=q49, q70=q47 ] endmodule
module process49 = process1 [ q1=q49, q2=q50, q70=q48 ] endmodule
module process50 = process1 [ q1=q50, q2=q51, q70=q49 ] endmodule
module process51 = process1 [ q1=q51, q2=q52, q70=q50 ] endmodule
module process52 = process1 [ q1=q52, q2=q53, q70=q51 ] endmodule
module process53 = process1 [ q1=q53, q2=q54, q70=q52 ] endmodule
module process54 = process1 [ q1=q54, q2=q55, q70=q53 ] endmodule
module process55 = process1 [ q1=q55, q2=q56, q70=q54 ] endmodule
module process56 = process1 [ q1=q56, q2=q57, q70=q55 ] endmodule
module process57 = process1 [ q1=q57, q2=q58, q70=q56 ] endmodule
module process58 = process1 [ q1=q58, q2=q59, q70=q57 ] endmodule
module process59 = process1 [ q1=q59, q2=q60, q70=q58 ] endmodule
module process60 = process1 [ q1=q60, q2=q61, q70=q59 ] endmodule
module process61 = process1 [ q1=q61, q2=q62, q70=q60 ] endmodule
module process62 = process1 [ q1=q62, q2=q63, q70=q61 ] endmodule
module process63 = process1 [ q1=q63, q2=q64, q70=q62 ] endmodule
module process64 = process1 [ q1=q64, q2=q65, q70=q63 ] endmodule
module process65 = process1 [ q1=q65, q2=q66, q70=q64 ] endmodule
module process66 = process1 [ q1=q66, q2=q67, q70=q65 ] endmodule
module process67 = process1 [ q1=q67, q2=q68, q70=q66 ] endmodule
module process68 = process1 [ q1=q68, q2=q69, q70=q67 ] endmodule
module process69 = process1 [ q1=q69, q2=q70, q70=q68 ] endmodule
module process70 = process1 [ q1=q70, q2=q1, q70=q69 ] endmodule

// cost - 1 in each state (expected steps)
rewards "steps"
	true : 1;
endrewards

// formula, for use here and in properties: number of tokens
formula num_tokens = q1+q2+q3+q4+q5+q6+q7+q8+q9+q10+q11+q12+q13+q14+q15+q16+q17+q18+q19+q20+q21+q22+q23+q24+q25+q26+q27+q28+q29+q30+q31+q32+q33+q34+q35+q36+q37+q38+q39+q40+q41+q42+q43+q44+q45+q46+q47+q48+q49+q50+q51+q52+q53+q54+q55+q56+q57+q58+q59+q60+q61+q62+q63+q64+q65+q66+q67+q68+q69+q70;

// label - stable configurations (1 token)
label "stable" = num_tokens=1;

// initial states (at least one token)
init
	num_tokens >= 1
endinit

