// petersons mutex algorithm

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

global flag1 : [0..1];
global flag2 : [0..1];
global turn : [1..2];


// module for process 1
module process1

	// step variable for process 1
	x1 : [0..4];


	// set flag
	[]  (x1=0) -> (x1'=1) & (flag1'=1);
    // set turn
	[]  (x1=1) -> (x1'=2) & (turn'=1);
    // wait
	[]  (x1=2) & ((flag2=0) | (turn=2)) -> (x1'=3);
    // critical section
	[]  (x1=3) -> (x1'=4);
    // leave CS & unset flag
	[] (x1=4) -> (x1'=0) & (flag1'=0);
	
endmodule

// module for process 2
module process2

	// step variable for process 2
	x2 : [0..4];

	// set flag
	[]  (x2=0) -> (x2'=1) & (flag2'=1);
    // set turn
	[]  (x2=1) -> (x2'=2) & (turn'=1);
    // wait
	[]  (x2=2) & ((flag1=0)|(turn=1)) -> (x2'=3);
    // critical section
	[]  (x2=3) -> (x2'=4);
    // leave CS & unset flag
	[] (x2=4) -> (x2'=0) & (flag2'=0);

endmodule

// set of initial states: all (i.e. any possible initial configuration of tokens)
init
	(x1=0)&(x2=0)&(flag1=0)&(flag2=0)
endinit

// formula, for use in properties: critical section
formula cs1 = (x1=3);
formula cs2 = (x2=3);

