import sys

n = int(sys.argv[1])

print('dtmc')

for i in range(1,n+1):
    print(f'global pc{i}  : [1..7];')
    print(f'global w{i}  : [0..1];')
    print(f'global s{i}  : [0..1];')

for i in range(1,n+1):
    print(f'module process{i}')

    print(f"[] (pc{i}=1)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & (s{j}=0)", end='')
    print(f" -> (pc{i}'=2);")

    print(f"[] (pc{i}=2) -> (pc{i}'=3) & (w{i}'=1) & (s{i}'=1);")

    for j in range(1,n+1):
        if i != j:
            print(f"[] (pc{i}=3) & !(pc{j}=1) & !(pc{j}=2) -> (pc{i}'=4) & (s{i}'=0);")

    print(f"[] (pc{i}=3)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & ((pc{j}=1) | (pc{j}=2))", end='')
    print(f" -> (pc{i}'=5) & (w{i}'=0);")

    for j in range(1,n+1):
        if i != j:
            print(f"[] (pc{i}=4) & (s{j}=1) & (w{j}=0) -> (pc{i}'=5) & (w{i}'=0) & (s{i}'=1);")

    print(f"[] (pc{i}=5)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & (w{j}=0)", end='')
    print(f" -> (pc{i}'=6);")

    print(f"[] (pc{i}=6)",end='')
    for j in range(1,i):
        print(f" & ((s{j}=0) | (w{j}=0))", end='')
    print(f" -> (pc{i}'=7);")

    print(f"[] (pc{i}=7) -> (pc{i}'=1) & (s{i}'=0);")

    print('endmodule')

# label - first process in critical section
print('label "cs" = (pc1=7);')

# initial states (each process starts in state 0)
print('init')
print("(pc1=1) & (w1=0) & (s1=0)", end='')
for i in range(2,n+1):
    print(f" & (pc{i}=1) & (w{i}=0) & (s{i}=0)", end='')
print()
print('endinit')

