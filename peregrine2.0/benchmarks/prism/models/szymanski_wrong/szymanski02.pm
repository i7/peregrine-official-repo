dtmc
global pc1  : [1..7];
global w1  : [0..1];
global s1  : [0..1];
global pc2  : [1..7];
global w2  : [0..1];
global s2  : [0..1];
module process1
[] (pc1=1) & (s2=0) -> (pc1'=2);
[] (pc1=2) -> (pc1'=3) & (w1'=1) & (s1'=1);
[] (pc1=3) & !(pc2=1) & !(pc2=2) -> (pc1'=4) & (s1'=0);
[] (pc1=3) & ((pc2=1) | (pc2=2)) -> (pc1'=5) & (w1'=0);
[] (pc1=4) & (s2=1) & (w2=0) -> (pc1'=5) & (w1'=0) & (s1'=1);
[] (pc1=5) & (w2=0) -> (pc1'=6);
[] (pc1=6) -> (pc1'=7);
[] (pc1=7) -> (pc1'=1) & (s1'=0);
endmodule
module process2
[] (pc2=1) & (s1=0) -> (pc2'=2);
[] (pc2=2) -> (pc2'=3) & (w2'=1) & (s2'=1);
[] (pc2=3) & !(pc1=1) & !(pc1=2) -> (pc2'=4) & (s2'=0);
[] (pc2=3) & ((pc1=1) | (pc1=2)) -> (pc2'=5) & (w2'=0);
[] (pc2=4) & (s1=1) & (w1=0) -> (pc2'=5) & (w2'=0) & (s2'=1);
[] (pc2=5) & (w1=0) -> (pc2'=6);
[] (pc2=6) & ((s1=0) | (w1=0)) -> (pc2'=7);
[] (pc2=7) -> (pc2'=1) & (s2'=0);
endmodule
label "cs" = (pc1=7);
init
(pc1=1) & (w1=0) & (s1=0) & (pc2=1) & (w2=0) & (s2=0)
endinit
