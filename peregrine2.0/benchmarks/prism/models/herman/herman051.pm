// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// module for process 1
module process1

	// Boolean variable for process 1
	x1 : [0..1];
	
	[step]  (x1=x51) -> 0.5 : (x1'=0) + 0.5 : (x1'=1);
	[step] !(x1=x51) -> (x1'=x51);
	
endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x51=x1 ] endmodule
module process3 = process1 [ x1=x3, x51=x2 ] endmodule
module process4 = process1 [ x1=x4, x51=x3 ] endmodule
module process5 = process1 [ x1=x5, x51=x4 ] endmodule
module process6 = process1 [ x1=x6, x51=x5 ] endmodule
module process7 = process1 [ x1=x7, x51=x6 ] endmodule
module process8 = process1 [ x1=x8, x51=x7 ] endmodule
module process9 = process1 [ x1=x9, x51=x8 ] endmodule
module process10 = process1 [ x1=x10, x51=x9 ] endmodule
module process11 = process1 [ x1=x11, x51=x10 ] endmodule
module process12 = process1 [ x1=x12, x51=x11 ] endmodule
module process13 = process1 [ x1=x13, x51=x12 ] endmodule
module process14 = process1 [ x1=x14, x51=x13 ] endmodule
module process15 = process1 [ x1=x15, x51=x14 ] endmodule
module process16 = process1 [ x1=x16, x51=x15 ] endmodule
module process17 = process1 [ x1=x17, x51=x16 ] endmodule
module process18 = process1 [ x1=x18, x51=x17 ] endmodule
module process19 = process1 [ x1=x19, x51=x18 ] endmodule
module process20 = process1 [ x1=x20, x51=x19 ] endmodule
module process21 = process1 [ x1=x21, x51=x20 ] endmodule
module process22 = process1 [ x1=x22, x51=x21 ] endmodule
module process23 = process1 [ x1=x23, x51=x22 ] endmodule
module process24 = process1 [ x1=x24, x51=x23 ] endmodule
module process25 = process1 [ x1=x25, x51=x24 ] endmodule
module process26 = process1 [ x1=x26, x51=x25 ] endmodule
module process27 = process1 [ x1=x27, x51=x26 ] endmodule
module process28 = process1 [ x1=x28, x51=x27 ] endmodule
module process29 = process1 [ x1=x29, x51=x28 ] endmodule
module process30 = process1 [ x1=x30, x51=x29 ] endmodule
module process31 = process1 [ x1=x31, x51=x30 ] endmodule
module process32 = process1 [ x1=x32, x51=x31 ] endmodule
module process33 = process1 [ x1=x33, x51=x32 ] endmodule
module process34 = process1 [ x1=x34, x51=x33 ] endmodule
module process35 = process1 [ x1=x35, x51=x34 ] endmodule
module process36 = process1 [ x1=x36, x51=x35 ] endmodule
module process37 = process1 [ x1=x37, x51=x36 ] endmodule
module process38 = process1 [ x1=x38, x51=x37 ] endmodule
module process39 = process1 [ x1=x39, x51=x38 ] endmodule
module process40 = process1 [ x1=x40, x51=x39 ] endmodule
module process41 = process1 [ x1=x41, x51=x40 ] endmodule
module process42 = process1 [ x1=x42, x51=x41 ] endmodule
module process43 = process1 [ x1=x43, x51=x42 ] endmodule
module process44 = process1 [ x1=x44, x51=x43 ] endmodule
module process45 = process1 [ x1=x45, x51=x44 ] endmodule
module process46 = process1 [ x1=x46, x51=x45 ] endmodule
module process47 = process1 [ x1=x47, x51=x46 ] endmodule
module process48 = process1 [ x1=x48, x51=x47 ] endmodule
module process49 = process1 [ x1=x49, x51=x48 ] endmodule
module process50 = process1 [ x1=x50, x51=x49 ] endmodule
module process51 = process1 [ x1=x51, x51=x50 ] endmodule

// cost - 1 in each state (expected number of steps)
rewards "steps"
	
	true : 1;
	
endrewards

// set of initial states: all (i.e. any possible initial configuration of tokens)
init true endinit

// formula, for use in properties: number of tokens
// (i.e. number of processes that have the same value as the process to their left)
formula num_tokens = (x1=x2?1:0)+(x2=x3?1:0)+(x3=x4?1:0)+(x4=x5?1:0)+(x5=x6?1:0)+(x6=x7?1:0)+(x7=x8?1:0)+(x8=x9?1:0)+(x9=x10?1:0)+(x10=x11?1:0)+(x11=x12?1:0)+(x12=x13?1:0)+(x13=x14?1:0)+(x14=x15?1:0)+(x15=x16?1:0)+(x16=x17?1:0)+(x17=x18?1:0)+(x18=x19?1:0)+(x19=x20?1:0)+(x20=x21?1:0)+(x21=x22?1:0)+(x22=x23?1:0)+(x23=x24?1:0)+(x24=x25?1:0)+(x25=x26?1:0)+(x26=x27?1:0)+(x27=x28?1:0)+(x28=x29?1:0)+(x29=x30?1:0)+(x30=x31?1:0)+(x31=x32?1:0)+(x32=x33?1:0)+(x33=x34?1:0)+(x34=x35?1:0)+(x35=x36?1:0)+(x36=x37?1:0)+(x37=x38?1:0)+(x38=x39?1:0)+(x39=x40?1:0)+(x40=x41?1:0)+(x41=x42?1:0)+(x42=x43?1:0)+(x43=x44?1:0)+(x44=x45?1:0)+(x45=x46?1:0)+(x46=x47?1:0)+(x47=x48?1:0)+(x48=x49?1:0)+(x49=x50?1:0)+(x50=x51?1:0)+(x51=x1?1:0);
