// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// module for process 1
module process1

	// Boolean variable for process 1
	x1 : [0..1];
	
	[step]  (x1=x41) -> 0.5 : (x1'=0) + 0.5 : (x1'=1);
	[step] !(x1=x41) -> (x1'=x41);
	
endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x41=x1 ] endmodule
module process3 = process1 [ x1=x3, x41=x2 ] endmodule
module process4 = process1 [ x1=x4, x41=x3 ] endmodule
module process5 = process1 [ x1=x5, x41=x4 ] endmodule
module process6 = process1 [ x1=x6, x41=x5 ] endmodule
module process7 = process1 [ x1=x7, x41=x6 ] endmodule
module process8 = process1 [ x1=x8, x41=x7 ] endmodule
module process9 = process1 [ x1=x9, x41=x8 ] endmodule
module process10 = process1 [ x1=x10, x41=x9 ] endmodule
module process11 = process1 [ x1=x11, x41=x10 ] endmodule
module process12 = process1 [ x1=x12, x41=x11 ] endmodule
module process13 = process1 [ x1=x13, x41=x12 ] endmodule
module process14 = process1 [ x1=x14, x41=x13 ] endmodule
module process15 = process1 [ x1=x15, x41=x14 ] endmodule
module process16 = process1 [ x1=x16, x41=x15 ] endmodule
module process17 = process1 [ x1=x17, x41=x16 ] endmodule
module process18 = process1 [ x1=x18, x41=x17 ] endmodule
module process19 = process1 [ x1=x19, x41=x18 ] endmodule
module process20 = process1 [ x1=x20, x41=x19 ] endmodule
module process21 = process1 [ x1=x21, x41=x20 ] endmodule
module process22 = process1 [ x1=x22, x41=x21 ] endmodule
module process23 = process1 [ x1=x23, x41=x22 ] endmodule
module process24 = process1 [ x1=x24, x41=x23 ] endmodule
module process25 = process1 [ x1=x25, x41=x24 ] endmodule
module process26 = process1 [ x1=x26, x41=x25 ] endmodule
module process27 = process1 [ x1=x27, x41=x26 ] endmodule
module process28 = process1 [ x1=x28, x41=x27 ] endmodule
module process29 = process1 [ x1=x29, x41=x28 ] endmodule
module process30 = process1 [ x1=x30, x41=x29 ] endmodule
module process31 = process1 [ x1=x31, x41=x30 ] endmodule
module process32 = process1 [ x1=x32, x41=x31 ] endmodule
module process33 = process1 [ x1=x33, x41=x32 ] endmodule
module process34 = process1 [ x1=x34, x41=x33 ] endmodule
module process35 = process1 [ x1=x35, x41=x34 ] endmodule
module process36 = process1 [ x1=x36, x41=x35 ] endmodule
module process37 = process1 [ x1=x37, x41=x36 ] endmodule
module process38 = process1 [ x1=x38, x41=x37 ] endmodule
module process39 = process1 [ x1=x39, x41=x38 ] endmodule
module process40 = process1 [ x1=x40, x41=x39 ] endmodule
module process41 = process1 [ x1=x41, x41=x40 ] endmodule

// cost - 1 in each state (expected number of steps)
rewards "steps"
	
	true : 1;
	
endrewards

// set of initial states: all (i.e. any possible initial configuration of tokens)
init true endinit

// formula, for use in properties: number of tokens
// (i.e. number of processes that have the same value as the process to their left)
formula num_tokens = (x1=x2?1:0)+(x2=x3?1:0)+(x3=x4?1:0)+(x4=x5?1:0)+(x5=x6?1:0)+(x6=x7?1:0)+(x7=x8?1:0)+(x8=x9?1:0)+(x9=x10?1:0)+(x10=x11?1:0)+(x11=x12?1:0)+(x12=x13?1:0)+(x13=x14?1:0)+(x14=x15?1:0)+(x15=x16?1:0)+(x16=x17?1:0)+(x17=x18?1:0)+(x18=x19?1:0)+(x19=x20?1:0)+(x20=x21?1:0)+(x21=x22?1:0)+(x22=x23?1:0)+(x23=x24?1:0)+(x24=x25?1:0)+(x25=x26?1:0)+(x26=x27?1:0)+(x27=x28?1:0)+(x28=x29?1:0)+(x29=x30?1:0)+(x30=x31?1:0)+(x31=x32?1:0)+(x32=x33?1:0)+(x33=x34?1:0)+(x34=x35?1:0)+(x35=x36?1:0)+(x36=x37?1:0)+(x37=x38?1:0)+(x38=x39?1:0)+(x39=x40?1:0)+(x40=x41?1:0)+(x41=x1?1:0);
