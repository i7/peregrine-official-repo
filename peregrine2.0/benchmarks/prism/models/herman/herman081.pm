// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// module for process 1
module process1

	// Boolean variable for process 1
	x1 : [0..1];
	
	[step]  (x1=x81) -> 0.5 : (x1'=0) + 0.5 : (x1'=1);
	[step] !(x1=x81) -> (x1'=x81);
	
endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x81=x1 ] endmodule
module process3 = process1 [ x1=x3, x81=x2 ] endmodule
module process4 = process1 [ x1=x4, x81=x3 ] endmodule
module process5 = process1 [ x1=x5, x81=x4 ] endmodule
module process6 = process1 [ x1=x6, x81=x5 ] endmodule
module process7 = process1 [ x1=x7, x81=x6 ] endmodule
module process8 = process1 [ x1=x8, x81=x7 ] endmodule
module process9 = process1 [ x1=x9, x81=x8 ] endmodule
module process10 = process1 [ x1=x10, x81=x9 ] endmodule
module process11 = process1 [ x1=x11, x81=x10 ] endmodule
module process12 = process1 [ x1=x12, x81=x11 ] endmodule
module process13 = process1 [ x1=x13, x81=x12 ] endmodule
module process14 = process1 [ x1=x14, x81=x13 ] endmodule
module process15 = process1 [ x1=x15, x81=x14 ] endmodule
module process16 = process1 [ x1=x16, x81=x15 ] endmodule
module process17 = process1 [ x1=x17, x81=x16 ] endmodule
module process18 = process1 [ x1=x18, x81=x17 ] endmodule
module process19 = process1 [ x1=x19, x81=x18 ] endmodule
module process20 = process1 [ x1=x20, x81=x19 ] endmodule
module process21 = process1 [ x1=x21, x81=x20 ] endmodule
module process22 = process1 [ x1=x22, x81=x21 ] endmodule
module process23 = process1 [ x1=x23, x81=x22 ] endmodule
module process24 = process1 [ x1=x24, x81=x23 ] endmodule
module process25 = process1 [ x1=x25, x81=x24 ] endmodule
module process26 = process1 [ x1=x26, x81=x25 ] endmodule
module process27 = process1 [ x1=x27, x81=x26 ] endmodule
module process28 = process1 [ x1=x28, x81=x27 ] endmodule
module process29 = process1 [ x1=x29, x81=x28 ] endmodule
module process30 = process1 [ x1=x30, x81=x29 ] endmodule
module process31 = process1 [ x1=x31, x81=x30 ] endmodule
module process32 = process1 [ x1=x32, x81=x31 ] endmodule
module process33 = process1 [ x1=x33, x81=x32 ] endmodule
module process34 = process1 [ x1=x34, x81=x33 ] endmodule
module process35 = process1 [ x1=x35, x81=x34 ] endmodule
module process36 = process1 [ x1=x36, x81=x35 ] endmodule
module process37 = process1 [ x1=x37, x81=x36 ] endmodule
module process38 = process1 [ x1=x38, x81=x37 ] endmodule
module process39 = process1 [ x1=x39, x81=x38 ] endmodule
module process40 = process1 [ x1=x40, x81=x39 ] endmodule
module process41 = process1 [ x1=x41, x81=x40 ] endmodule
module process42 = process1 [ x1=x42, x81=x41 ] endmodule
module process43 = process1 [ x1=x43, x81=x42 ] endmodule
module process44 = process1 [ x1=x44, x81=x43 ] endmodule
module process45 = process1 [ x1=x45, x81=x44 ] endmodule
module process46 = process1 [ x1=x46, x81=x45 ] endmodule
module process47 = process1 [ x1=x47, x81=x46 ] endmodule
module process48 = process1 [ x1=x48, x81=x47 ] endmodule
module process49 = process1 [ x1=x49, x81=x48 ] endmodule
module process50 = process1 [ x1=x50, x81=x49 ] endmodule
module process51 = process1 [ x1=x51, x81=x50 ] endmodule
module process52 = process1 [ x1=x52, x81=x51 ] endmodule
module process53 = process1 [ x1=x53, x81=x52 ] endmodule
module process54 = process1 [ x1=x54, x81=x53 ] endmodule
module process55 = process1 [ x1=x55, x81=x54 ] endmodule
module process56 = process1 [ x1=x56, x81=x55 ] endmodule
module process57 = process1 [ x1=x57, x81=x56 ] endmodule
module process58 = process1 [ x1=x58, x81=x57 ] endmodule
module process59 = process1 [ x1=x59, x81=x58 ] endmodule
module process60 = process1 [ x1=x60, x81=x59 ] endmodule
module process61 = process1 [ x1=x61, x81=x60 ] endmodule
module process62 = process1 [ x1=x62, x81=x61 ] endmodule
module process63 = process1 [ x1=x63, x81=x62 ] endmodule
module process64 = process1 [ x1=x64, x81=x63 ] endmodule
module process65 = process1 [ x1=x65, x81=x64 ] endmodule
module process66 = process1 [ x1=x66, x81=x65 ] endmodule
module process67 = process1 [ x1=x67, x81=x66 ] endmodule
module process68 = process1 [ x1=x68, x81=x67 ] endmodule
module process69 = process1 [ x1=x69, x81=x68 ] endmodule
module process70 = process1 [ x1=x70, x81=x69 ] endmodule
module process71 = process1 [ x1=x71, x81=x70 ] endmodule
module process72 = process1 [ x1=x72, x81=x71 ] endmodule
module process73 = process1 [ x1=x73, x81=x72 ] endmodule
module process74 = process1 [ x1=x74, x81=x73 ] endmodule
module process75 = process1 [ x1=x75, x81=x74 ] endmodule
module process76 = process1 [ x1=x76, x81=x75 ] endmodule
module process77 = process1 [ x1=x77, x81=x76 ] endmodule
module process78 = process1 [ x1=x78, x81=x77 ] endmodule
module process79 = process1 [ x1=x79, x81=x78 ] endmodule
module process80 = process1 [ x1=x80, x81=x79 ] endmodule
module process81 = process1 [ x1=x81, x81=x80 ] endmodule

// cost - 1 in each state (expected number of steps)
rewards "steps"
	
	true : 1;
	
endrewards

// set of initial states: all (i.e. any possible initial configuration of tokens)
init true endinit

// formula, for use in properties: number of tokens
// (i.e. number of processes that have the same value as the process to their left)
formula num_tokens = (x1=x2?1:0)+(x2=x3?1:0)+(x3=x4?1:0)+(x4=x5?1:0)+(x5=x6?1:0)+(x6=x7?1:0)+(x7=x8?1:0)+(x8=x9?1:0)+(x9=x10?1:0)+(x10=x11?1:0)+(x11=x12?1:0)+(x12=x13?1:0)+(x13=x14?1:0)+(x14=x15?1:0)+(x15=x16?1:0)+(x16=x17?1:0)+(x17=x18?1:0)+(x18=x19?1:0)+(x19=x20?1:0)+(x20=x21?1:0)+(x21=x22?1:0)+(x22=x23?1:0)+(x23=x24?1:0)+(x24=x25?1:0)+(x25=x26?1:0)+(x26=x27?1:0)+(x27=x28?1:0)+(x28=x29?1:0)+(x29=x30?1:0)+(x30=x31?1:0)+(x31=x32?1:0)+(x32=x33?1:0)+(x33=x34?1:0)+(x34=x35?1:0)+(x35=x36?1:0)+(x36=x37?1:0)+(x37=x38?1:0)+(x38=x39?1:0)+(x39=x40?1:0)+(x40=x41?1:0)+(x41=x42?1:0)+(x42=x43?1:0)+(x43=x44?1:0)+(x44=x45?1:0)+(x45=x46?1:0)+(x46=x47?1:0)+(x47=x48?1:0)+(x48=x49?1:0)+(x49=x50?1:0)+(x50=x51?1:0)+(x51=x52?1:0)+(x52=x53?1:0)+(x53=x54?1:0)+(x54=x55?1:0)+(x55=x56?1:0)+(x56=x57?1:0)+(x57=x58?1:0)+(x58=x59?1:0)+(x59=x60?1:0)+(x60=x61?1:0)+(x61=x62?1:0)+(x62=x63?1:0)+(x63=x64?1:0)+(x64=x65?1:0)+(x65=x66?1:0)+(x66=x67?1:0)+(x67=x68?1:0)+(x68=x69?1:0)+(x69=x70?1:0)+(x70=x71?1:0)+(x71=x72?1:0)+(x72=x73?1:0)+(x73=x74?1:0)+(x74=x75?1:0)+(x75=x76?1:0)+(x76=x77?1:0)+(x77=x78?1:0)+(x78=x79?1:0)+(x79=x80?1:0)+(x80=x81?1:0)+(x81=x1?1:0);
