dtmc
global flag1  : [0..2];
global flag2  : [0..2];
global flag3  : [0..2];
global flag4  : [0..2];
global p  : [1..4];
module process1
q1 : [1..8];
[] (q1=1) -> (q1'=2) & (flag1'=1);
[] (q1=2) & !(p=1) -> (q1'=8);
[] (q1=2) & (p=1) -> (q1'=4);
[] (q1=8) & (p=1) & (flag1=0) -> (q1'=3);
[] (q1=8) & (p=2) & (flag2=0) -> (q1'=3);
[] (q1=8) & (p=3) & (flag3=0) -> (q1'=3);
[] (q1=8) & (p=4) & (flag4=0) -> (q1'=3);
[] (q1=8) & (p=1) & (flag1=0) -> (q1'=3);
[] (q1=8) & (p=2) & (flag2=0) -> (q1'=3);
[] (q1=8) & (p=3) & (flag3=0) -> (q1'=3);
[] (q1=8) & (p=4) & (flag4=0) -> (q1'=3);
[] (q1=8) & (p=1) & (flag1=0) -> (q1'=3);
[] (q1=8) & (p=2) & (flag2=0) -> (q1'=3);
[] (q1=8) & (p=3) & (flag3=0) -> (q1'=3);
[] (q1=8) & (p=4) & (flag4=0) -> (q1'=3);
[] (q1=3) -> (q1'=4) & (p'=1);
[] (q1=4) -> (q1'=5) & (flag1'=2);
[] (q1=5) & (flag2=2) -> (q1'=1);
[] (q1=5) & (flag3=2) -> (q1'=1);
[] (q1=5) & (flag4=2) -> (q1'=1);
[] (q1=5) & !(flag2=2) & !(flag3=2) & !(flag4=2) -> (q1'=6);
[] (q1=6) -> (q1'=7) & (flag1'=0);
[] (q1=7) -> (q1'=1);
endmodule
module process2
q2 : [1..8];
[] (q2=1) -> (q2'=2) & (flag2'=1);
[] (q2=2) & !(p=2) -> (q2'=8);
[] (q2=2) & (p=2) -> (q2'=4);
[] (q2=8) & (p=1) & (flag1=0) -> (q2'=3);
[] (q2=8) & (p=2) & (flag2=0) -> (q2'=3);
[] (q2=8) & (p=3) & (flag3=0) -> (q2'=3);
[] (q2=8) & (p=4) & (flag4=0) -> (q2'=3);
[] (q2=8) & (p=1) & (flag1=0) -> (q2'=3);
[] (q2=8) & (p=2) & (flag2=0) -> (q2'=3);
[] (q2=8) & (p=3) & (flag3=0) -> (q2'=3);
[] (q2=8) & (p=4) & (flag4=0) -> (q2'=3);
[] (q2=8) & (p=1) & (flag1=0) -> (q2'=3);
[] (q2=8) & (p=2) & (flag2=0) -> (q2'=3);
[] (q2=8) & (p=3) & (flag3=0) -> (q2'=3);
[] (q2=8) & (p=4) & (flag4=0) -> (q2'=3);
[] (q2=3) -> (q2'=4) & (p'=2);
[] (q2=4) -> (q2'=5) & (flag2'=2);
[] (q2=5) & (flag1=2) -> (q2'=1);
[] (q2=5) & (flag3=2) -> (q2'=1);
[] (q2=5) & (flag4=2) -> (q2'=1);
[] (q2=5) & !(flag1=2) & !(flag3=2) & !(flag4=2) -> (q2'=6);
[] (q2=6) -> (q2'=7) & (flag2'=0);
[] (q2=7) -> (q2'=1);
endmodule
module process3
q3 : [1..8];
[] (q3=1) -> (q3'=2) & (flag3'=1);
[] (q3=2) & !(p=3) -> (q3'=8);
[] (q3=2) & (p=3) -> (q3'=4);
[] (q3=8) & (p=1) & (flag1=0) -> (q3'=3);
[] (q3=8) & (p=2) & (flag2=0) -> (q3'=3);
[] (q3=8) & (p=3) & (flag3=0) -> (q3'=3);
[] (q3=8) & (p=4) & (flag4=0) -> (q3'=3);
[] (q3=8) & (p=1) & (flag1=0) -> (q3'=3);
[] (q3=8) & (p=2) & (flag2=0) -> (q3'=3);
[] (q3=8) & (p=3) & (flag3=0) -> (q3'=3);
[] (q3=8) & (p=4) & (flag4=0) -> (q3'=3);
[] (q3=8) & (p=1) & (flag1=0) -> (q3'=3);
[] (q3=8) & (p=2) & (flag2=0) -> (q3'=3);
[] (q3=8) & (p=3) & (flag3=0) -> (q3'=3);
[] (q3=8) & (p=4) & (flag4=0) -> (q3'=3);
[] (q3=3) -> (q3'=4) & (p'=3);
[] (q3=4) -> (q3'=5) & (flag3'=2);
[] (q3=5) & (flag1=2) -> (q3'=1);
[] (q3=5) & (flag2=2) -> (q3'=1);
[] (q3=5) & (flag4=2) -> (q3'=1);
[] (q3=5) & !(flag1=2) & !(flag2=2) & !(flag4=2) -> (q3'=6);
[] (q3=6) -> (q3'=7) & (flag3'=0);
[] (q3=7) -> (q3'=1);
endmodule
module process4
q4 : [1..8];
[] (q4=1) -> (q4'=2) & (flag4'=1);
[] (q4=2) & !(p=4) -> (q4'=8);
[] (q4=2) & (p=4) -> (q4'=4);
[] (q4=8) & (p=1) & (flag1=0) -> (q4'=3);
[] (q4=8) & (p=2) & (flag2=0) -> (q4'=3);
[] (q4=8) & (p=3) & (flag3=0) -> (q4'=3);
[] (q4=8) & (p=4) & (flag4=0) -> (q4'=3);
[] (q4=8) & (p=1) & (flag1=0) -> (q4'=3);
[] (q4=8) & (p=2) & (flag2=0) -> (q4'=3);
[] (q4=8) & (p=3) & (flag3=0) -> (q4'=3);
[] (q4=8) & (p=4) & (flag4=0) -> (q4'=3);
[] (q4=8) & (p=1) & (flag1=0) -> (q4'=3);
[] (q4=8) & (p=2) & (flag2=0) -> (q4'=3);
[] (q4=8) & (p=3) & (flag3=0) -> (q4'=3);
[] (q4=8) & (p=4) & (flag4=0) -> (q4'=3);
[] (q4=3) -> (q4'=4) & (p'=4);
[] (q4=4) -> (q4'=5) & (flag4'=2);
[] (q4=5) & (flag1=2) -> (q4'=1);
[] (q4=5) & (flag2=2) -> (q4'=1);
[] (q4=5) & (flag3=2) -> (q4'=1);
[] (q4=5) & !(flag1=2) & !(flag2=2) & !(flag3=2) -> (q4'=6);
[] (q4=6) -> (q4'=7) & (flag4'=0);
[] (q4=7) -> (q4'=1);
endmodule
label "cs" = (q1=6);
init
(q1=1) & (q2=1) & (q3=1) & (q4=1)
endinit
