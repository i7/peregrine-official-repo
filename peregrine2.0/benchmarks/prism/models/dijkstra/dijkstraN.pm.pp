#const N#
// MUX array mutex algorithm

dtmc

// flag flagi for each process i is 0 or 1
#for i=1:N#
global flag#i#  : [0..1];
#end#

#for i=1:N#
// module of process #i#
module process#i#

    // state q for each process i is 1,2,3,4,5,6,7
    q#i# : [1..7];

	[] (q#i#=1) -> (q#i#'=2) & (flag#i#'=0);
	[] (q#i#=2)#for j=1:N#  & (flag#j#=0) #end# -> (q#i#'=3);

endmodule
#end#

// label - first process in critical section
label "cs" = (q1=2);

// initial states (each process starts in state 0, first process starts with token)
init
    (q1=0) & (t1=1)#for i=2:N# & (q#i#=0) & (t#i#=0)#end#
endinit

