import sys

n = int(sys.argv[1])

print('dtmc')

for i in range(1,n+1):
    print(f'global flag{i}  : [0..2];')
print(f'global p  : [1..{n}];')

for i in range(1,n+1):
    print(f'module process{i}')

    # state q for each process i is 1,2,3,4,5,6,7, and 8 between lines 2 and 3
    print(f'q{i} : [1..8];')

    print(f"[] (q{i}=1) -> (q{i}'=2) & (flag{i}'=1);")

    print(f"[] (q{i}=2) & !(p={i}) -> (q{i}'=8);")
    print(f"[] (q{i}=2) & (p={i}) -> (q{i}'=4);")

    for j in range(1,n+1):
        if i != j:
            for p in range(1,n+1):
                print(f"[] (q{i}=8) & (p={p}) & (flag{p}=0) -> (q{i}'=3);")

    print(f"[] (q{i}=3) -> (q{i}'=4) & (p'={i});")

    print(f"[] (q{i}=4) -> (q{i}'=5) & (flag{i}'=2);")

    for j in range(1,n+1):
        if i != j:
            print(f"[] (q{i}=5) & (flag{j}=2) -> (q{i}'=1);")

    print(f"[] (q{i}=5)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & !(flag{j}=2)", end='')
    print(f" -> (q{i}'=6);")

    print(f"[] (q{i}=6) -> (q{i}'=7) & (flag{i}'=0);")

    print(f"[] (q{i}=7) -> (q{i}'=1);")

    print('endmodule')

# label - first process in critical section
print('label "cs" = (q1=6);')

# initial states (each process starts in state 0)
print('init')
print("(q1=1)", end='')
for i in range(2,n+1):
    print(f" & (q{i}=1)", end='')
print()
print('endinit')

