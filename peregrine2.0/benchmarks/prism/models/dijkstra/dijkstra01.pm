dtmc
global flag1  : [0..2];
global p  : [1..1];
module process1
q1 : [1..8];
[] (q1=1) -> (q1'=2) & (flag1'=1);
[] (q1=2) & !(p=1) -> (q1'=8);
[] (q1=2) & (p=1) -> (q1'=4);
[] (q1=3) -> (q1'=4) & (p'=1);
[] (q1=4) -> (q1'=5) & (flag1'=2);
[] (q1=5) -> (q1'=6);
[] (q1=6) -> (q1'=7) & (flag1'=0);
[] (q1=7) -> (q1'=1);
endmodule
label "cs" = (q1=6);
init
(q1=1)
endinit
