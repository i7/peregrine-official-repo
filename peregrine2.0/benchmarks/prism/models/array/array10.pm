// MUX array mutex algorithm

dtmc

// state qi for each process i is 0,1 or 2 (0 stands # for `waiting', 1 for `idle', 2 for `in critical section')
// token ti for each process i is 0 or 1 (0 stands for `empty', 1 for `with token')
global q1  : [0..2];
global t1  : [0..1];
global q2  : [0..2];
global t2  : [0..1];
global q3  : [0..2];
global t3  : [0..1];
global q4  : [0..2];
global t4  : [0..1];
global q5  : [0..2];
global t5  : [0..1];
global q6  : [0..2];
global t6  : [0..1];
global q7  : [0..2];
global t7  : [0..1];
global q8  : [0..2];
global t8  : [0..1];
global q9  : [0..2];
global t9  : [0..1];
global q10  : [0..2];
global t10  : [0..1];

// module of process 1
module process1

    // waiting -> idle
	[] (q1=0) & !((t1=1) & (q2=1) & (t2=0)) -> (q1'=1);
    // give token to next process (or enter idle state)
	[] (q1=0) & (t1=1) & (q2=1) & (t2=0) ->
        0.5 : (q1'=0) & (t1'=0) & (q2'=1) & (t2'=1) +
        0.5 : (q1'=1) & (t1'=1) & (q2'=1) & (t2'=0);
    // idle -> critical section (if process has token)
	[] (q1=1) & (t1=1) -> (q1'=2) & (t1'=1);
    // critical section -> waiting
	[] (q1=2) -> (q1'=0);

endmodule

// add further processes through renaming
module process2 = process1 [ q1=q2, t1=t2, q2=q3, t2=t3 ] endmodule
module process3 = process1 [ q1=q3, t1=t3, q2=q4, t2=t4 ] endmodule
module process4 = process1 [ q1=q4, t1=t4, q2=q5, t2=t5 ] endmodule
module process5 = process1 [ q1=q5, t1=t5, q2=q6, t2=t6 ] endmodule
module process6 = process1 [ q1=q6, t1=t6, q2=q7, t2=t7 ] endmodule
module process7 = process1 [ q1=q7, t1=t7, q2=q8, t2=t8 ] endmodule
module process8 = process1 [ q1=q8, t1=t8, q2=q9, t2=t9 ] endmodule
module process9 = process1 [ q1=q9, t1=t9, q2=q10, t2=t10 ] endmodule
module process10 = process1 [ q1=q10, t1=t10, q2=q1, t2=t1 ] endmodule

// label - first process in critical section
label "cs" = (q1=2);

// initial states (each process starts in state 0, first process starts with token)
init
    (q1=0) & (t1=1) & (q2=0) & (t2=0) & (q3=0) & (t3=0) & (q4=0) & (t4=0) & (q5=0) & (t5=0) & (q6=0) & (t6=0) & (q7=0) & (t7=0) & (q8=0) & (t8=0) & (q9=0) & (t9=0) & (q10=0) & (t10=0)
endinit

