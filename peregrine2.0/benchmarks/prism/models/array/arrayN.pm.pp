#const N#
// MUX array mutex algorithm

dtmc

// state qi for each process i is 0,1 or 2 (0 stands # for `waiting', 1 for `idle', 2 for `in critical section')
// token ti for each process i is 0 or 1 (0 stands for `empty', 1 for `with token')
#for i=1:N#
global q#i#  : [0..2];
global t#i#  : [0..1];
#end#

// module of process 1
module process1

    // waiting -> idle
	[] (q1=0) & !((t1=1) & (q2=1) & (t2=0)) -> (q1'=1);
    // give token to next process (or enter idle state)
	[] (q1=0) & (t1=1) & (q2=1) & (t2=0) ->
        0.5 : (q1'=0) & (t1'=0) & (q2'=1) & (t2'=1) +
        0.5 : (q1'=1) & (t1'=1) & (q2'=1) & (t2'=0);
    // idle -> critical section (if process has token)
	[] (q1=1) & (t1=1) -> (q1'=2) & (t1'=1);
    // critical section -> waiting
	[] (q1=2) -> (q1'=0);

endmodule

// add further processes through renaming
#for i=2:N#
module process#i# = process1 [ q1=q#i#, t1=t#i#, q2=q#func(mod,i,N)+1#, t2=t#func(mod,i,N)+1# ] endmodule
#end#

// label - first process in critical section
label "cs" = (q1=2);

// initial states (each process starts in state 0, first process starts with token)
init
    (q1=0) & (t1=1)#for i=2:N# & (q#i#=0) & (t#i#=0)#end#
endinit

