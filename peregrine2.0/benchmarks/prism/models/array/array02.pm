// MUX array mutex algorithm

dtmc

// state qi for each process i is 0,1 or 2 (0 stands # for `waiting', 1 for `idle', 2 for `in critical section')
// token ti for each process i is 0 or 1 (0 stands for `empty', 1 for `with token')
global q1  : [0..2];
global t1  : [0..1];
global q2  : [0..2];
global t2  : [0..1];

// module of process 1
module process1

    // waiting -> idle
	[] (q1=0) & !((t1=1) & (q2=1) & (t2=0)) -> (q1'=1);
    // give token to next process (or enter idle state)
	[] (q1=0) & (t1=1) & (q2=1) & (t2=0) ->
        0.5 : (q1'=0) & (t1'=0) & (q2'=1) & (t2'=1) +
        0.5 : (q1'=1) & (t1'=1) & (q2'=1) & (t2'=0);
    // idle -> critical section (if process has token)
	[] (q1=1) & (t1=1) -> (q1'=2) & (t1'=1);
    // critical section -> waiting
	[] (q1=2) -> (q1'=0);

endmodule

// add further processes through renaming
module process2 = process1 [ q1=q2, t1=t2, q2=q1, t2=t1 ] endmodule

// label - first process in critical section
label "cs" = (q1=2);

// initial states (each process starts in state 0, first process starts with token)
init
    (q1=0) & (t1=1) & (q2=0) & (t2=0)
endinit

