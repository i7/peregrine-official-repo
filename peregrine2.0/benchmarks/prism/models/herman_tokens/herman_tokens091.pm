// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// variables to represent whether a process has a token or not
// note they are global because they can be updated by other processes
global x1  : [0..1];
global x2  : [0..1];
global x3  : [0..1];
global x4  : [0..1];
global x5  : [0..1];
global x6  : [0..1];
global x7  : [0..1];
global x8  : [0..1];
global x9  : [0..1];
global x10  : [0..1];
global x11  : [0..1];
global x12  : [0..1];
global x13  : [0..1];
global x14  : [0..1];
global x15  : [0..1];
global x16  : [0..1];
global x17  : [0..1];
global x18  : [0..1];
global x19  : [0..1];
global x20  : [0..1];
global x21  : [0..1];
global x22  : [0..1];
global x23  : [0..1];
global x24  : [0..1];
global x25  : [0..1];
global x26  : [0..1];
global x27  : [0..1];
global x28  : [0..1];
global x29  : [0..1];
global x30  : [0..1];
global x31  : [0..1];
global x32  : [0..1];
global x33  : [0..1];
global x34  : [0..1];
global x35  : [0..1];
global x36  : [0..1];
global x37  : [0..1];
global x38  : [0..1];
global x39  : [0..1];
global x40  : [0..1];
global x41  : [0..1];
global x42  : [0..1];
global x43  : [0..1];
global x44  : [0..1];
global x45  : [0..1];
global x46  : [0..1];
global x47  : [0..1];
global x48  : [0..1];
global x49  : [0..1];
global x50  : [0..1];
global x51  : [0..1];
global x52  : [0..1];
global x53  : [0..1];
global x54  : [0..1];
global x55  : [0..1];
global x56  : [0..1];
global x57  : [0..1];
global x58  : [0..1];
global x59  : [0..1];
global x60  : [0..1];
global x61  : [0..1];
global x62  : [0..1];
global x63  : [0..1];
global x64  : [0..1];
global x65  : [0..1];
global x66  : [0..1];
global x67  : [0..1];
global x68  : [0..1];
global x69  : [0..1];
global x70  : [0..1];
global x71  : [0..1];
global x72  : [0..1];
global x73  : [0..1];
global x74  : [0..1];
global x75  : [0..1];
global x76  : [0..1];
global x77  : [0..1];
global x78  : [0..1];
global x79  : [0..1];
global x80  : [0..1];
global x81  : [0..1];
global x82  : [0..1];
global x83  : [0..1];
global x84  : [0..1];
global x85  : [0..1];
global x86  : [0..1];
global x87  : [0..1];
global x88  : [0..1];
global x89  : [0..1];
global x90  : [0..1];
global x91  : [0..1];

// module for process 1
module process1

	[]  (x1=1) & (x2=0) -> (x1'=0) & (x2'=1);
	[]  (x1=1) & (x2=1) -> (x1'=0) & (x2'=0);

endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x2=x3 ] endmodule
module process3 = process1 [ x1=x3, x2=x4 ] endmodule
module process4 = process1 [ x1=x4, x2=x5 ] endmodule
module process5 = process1 [ x1=x5, x2=x6 ] endmodule
module process6 = process1 [ x1=x6, x2=x7 ] endmodule
module process7 = process1 [ x1=x7, x2=x8 ] endmodule
module process8 = process1 [ x1=x8, x2=x9 ] endmodule
module process9 = process1 [ x1=x9, x2=x10 ] endmodule
module process10 = process1 [ x1=x10, x2=x11 ] endmodule
module process11 = process1 [ x1=x11, x2=x12 ] endmodule
module process12 = process1 [ x1=x12, x2=x13 ] endmodule
module process13 = process1 [ x1=x13, x2=x14 ] endmodule
module process14 = process1 [ x1=x14, x2=x15 ] endmodule
module process15 = process1 [ x1=x15, x2=x16 ] endmodule
module process16 = process1 [ x1=x16, x2=x17 ] endmodule
module process17 = process1 [ x1=x17, x2=x18 ] endmodule
module process18 = process1 [ x1=x18, x2=x19 ] endmodule
module process19 = process1 [ x1=x19, x2=x20 ] endmodule
module process20 = process1 [ x1=x20, x2=x21 ] endmodule
module process21 = process1 [ x1=x21, x2=x22 ] endmodule
module process22 = process1 [ x1=x22, x2=x23 ] endmodule
module process23 = process1 [ x1=x23, x2=x24 ] endmodule
module process24 = process1 [ x1=x24, x2=x25 ] endmodule
module process25 = process1 [ x1=x25, x2=x26 ] endmodule
module process26 = process1 [ x1=x26, x2=x27 ] endmodule
module process27 = process1 [ x1=x27, x2=x28 ] endmodule
module process28 = process1 [ x1=x28, x2=x29 ] endmodule
module process29 = process1 [ x1=x29, x2=x30 ] endmodule
module process30 = process1 [ x1=x30, x2=x31 ] endmodule
module process31 = process1 [ x1=x31, x2=x32 ] endmodule
module process32 = process1 [ x1=x32, x2=x33 ] endmodule
module process33 = process1 [ x1=x33, x2=x34 ] endmodule
module process34 = process1 [ x1=x34, x2=x35 ] endmodule
module process35 = process1 [ x1=x35, x2=x36 ] endmodule
module process36 = process1 [ x1=x36, x2=x37 ] endmodule
module process37 = process1 [ x1=x37, x2=x38 ] endmodule
module process38 = process1 [ x1=x38, x2=x39 ] endmodule
module process39 = process1 [ x1=x39, x2=x40 ] endmodule
module process40 = process1 [ x1=x40, x2=x41 ] endmodule
module process41 = process1 [ x1=x41, x2=x42 ] endmodule
module process42 = process1 [ x1=x42, x2=x43 ] endmodule
module process43 = process1 [ x1=x43, x2=x44 ] endmodule
module process44 = process1 [ x1=x44, x2=x45 ] endmodule
module process45 = process1 [ x1=x45, x2=x46 ] endmodule
module process46 = process1 [ x1=x46, x2=x47 ] endmodule
module process47 = process1 [ x1=x47, x2=x48 ] endmodule
module process48 = process1 [ x1=x48, x2=x49 ] endmodule
module process49 = process1 [ x1=x49, x2=x50 ] endmodule
module process50 = process1 [ x1=x50, x2=x51 ] endmodule
module process51 = process1 [ x1=x51, x2=x52 ] endmodule
module process52 = process1 [ x1=x52, x2=x53 ] endmodule
module process53 = process1 [ x1=x53, x2=x54 ] endmodule
module process54 = process1 [ x1=x54, x2=x55 ] endmodule
module process55 = process1 [ x1=x55, x2=x56 ] endmodule
module process56 = process1 [ x1=x56, x2=x57 ] endmodule
module process57 = process1 [ x1=x57, x2=x58 ] endmodule
module process58 = process1 [ x1=x58, x2=x59 ] endmodule
module process59 = process1 [ x1=x59, x2=x60 ] endmodule
module process60 = process1 [ x1=x60, x2=x61 ] endmodule
module process61 = process1 [ x1=x61, x2=x62 ] endmodule
module process62 = process1 [ x1=x62, x2=x63 ] endmodule
module process63 = process1 [ x1=x63, x2=x64 ] endmodule
module process64 = process1 [ x1=x64, x2=x65 ] endmodule
module process65 = process1 [ x1=x65, x2=x66 ] endmodule
module process66 = process1 [ x1=x66, x2=x67 ] endmodule
module process67 = process1 [ x1=x67, x2=x68 ] endmodule
module process68 = process1 [ x1=x68, x2=x69 ] endmodule
module process69 = process1 [ x1=x69, x2=x70 ] endmodule
module process70 = process1 [ x1=x70, x2=x71 ] endmodule
module process71 = process1 [ x1=x71, x2=x72 ] endmodule
module process72 = process1 [ x1=x72, x2=x73 ] endmodule
module process73 = process1 [ x1=x73, x2=x74 ] endmodule
module process74 = process1 [ x1=x74, x2=x75 ] endmodule
module process75 = process1 [ x1=x75, x2=x76 ] endmodule
module process76 = process1 [ x1=x76, x2=x77 ] endmodule
module process77 = process1 [ x1=x77, x2=x78 ] endmodule
module process78 = process1 [ x1=x78, x2=x79 ] endmodule
module process79 = process1 [ x1=x79, x2=x80 ] endmodule
module process80 = process1 [ x1=x80, x2=x81 ] endmodule
module process81 = process1 [ x1=x81, x2=x82 ] endmodule
module process82 = process1 [ x1=x82, x2=x83 ] endmodule
module process83 = process1 [ x1=x83, x2=x84 ] endmodule
module process84 = process1 [ x1=x84, x2=x85 ] endmodule
module process85 = process1 [ x1=x85, x2=x86 ] endmodule
module process86 = process1 [ x1=x86, x2=x87 ] endmodule
module process87 = process1 [ x1=x87, x2=x88 ] endmodule
module process88 = process1 [ x1=x88, x2=x89 ] endmodule
module process89 = process1 [ x1=x89, x2=x90 ] endmodule
module process90 = process1 [ x1=x90, x2=x91 ] endmodule
module process91 = process1 [ x1=x91, x2=x1 ] endmodule

// formula, for use here and in properties: number of tokens
formula num_tokens = x1+x2+x3+x4+x5+x6+x7+x8+x9+x10+x11+x12+x13+x14+x15+x16+x17+x18+x19+x20+x21+x22+x23+x24+x25+x26+x27+x28+x29+x30+x31+x32+x33+x34+x35+x36+x37+x38+x39+x40+x41+x42+x43+x44+x45+x46+x47+x48+x49+x50+x51+x52+x53+x54+x55+x56+x57+x58+x59+x60+x61+x62+x63+x64+x65+x66+x67+x68+x69+x70+x71+x72+x73+x74+x75+x76+x77+x78+x79+x80+x81+x82+x83+x84+x85+x86+x87+x88+x89+x90+x91;

// set of initial states: at least one token
init
	func(mod,num_tokens,2) = 1
endinit

// label - stable configurations (1 token)
label "stable" = num_tokens=1;
