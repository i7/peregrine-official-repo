#const N#
// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// variables to represent whether a process has a token or not
// note they are global because they can be updated by other processes
#for i=1:N#
global x#i#  : [0..1];
#end#

// module for process 1
module process1

	[]  (x1=1) & (x2=0) -> (x1'=0) & (x2'=1);
	[]  (x1=1) & (x2=1) -> (x1'=0) & (x2'=0);

endmodule

// add further processes through renaming
#for i=2:N#
module process#i# = process1 [ x1=x#i#, x2=x#func(mod,i,N)+1# ] endmodule
#end#

// formula, for use here and in properties: number of tokens
formula num_tokens = #+ i=1:N#x#i##end#;

// set of initial states: at least one token
init
	func(mod,num_tokens,2) = 1
endinit

// label - stable configurations (1 token)
label "stable" = num_tokens=1;
