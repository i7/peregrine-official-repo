// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// variables to represent whether a process has a token or not
// note they are global because they can be updated by other processes
global x1  : [0..1];
global x2  : [0..1];
global x3  : [0..1];
global x4  : [0..1];
global x5  : [0..1];
global x6  : [0..1];
global x7  : [0..1];
global x8  : [0..1];
global x9  : [0..1];
global x10  : [0..1];
global x11  : [0..1];

// module for process 1
module process1

	[]  (x1=1) & (x2=0) -> (x1'=0) & (x2'=1);
	[]  (x1=1) & (x2=1) -> (x1'=0) & (x2'=0);

endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x2=x3 ] endmodule
module process3 = process1 [ x1=x3, x2=x4 ] endmodule
module process4 = process1 [ x1=x4, x2=x5 ] endmodule
module process5 = process1 [ x1=x5, x2=x6 ] endmodule
module process6 = process1 [ x1=x6, x2=x7 ] endmodule
module process7 = process1 [ x1=x7, x2=x8 ] endmodule
module process8 = process1 [ x1=x8, x2=x9 ] endmodule
module process9 = process1 [ x1=x9, x2=x10 ] endmodule
module process10 = process1 [ x1=x10, x2=x11 ] endmodule
module process11 = process1 [ x1=x11, x2=x1 ] endmodule

// formula, for use here and in properties: number of tokens
formula num_tokens = x1+x2+x3+x4+x5+x6+x7+x8+x9+x10+x11;

// set of initial states: at least one token
init
	func(mod,num_tokens,2) = 1
endinit

// label - stable configurations (1 token)
label "stable" = num_tokens=1;
