// herman's self stabilising algorithm [Her90]
// gxn/dxp 13/07/02

// the procotol is synchronous with no nondeterminism (a DTMC)
dtmc

// variables to represent whether a process has a token or not
// note they are global because they can be updated by other processes
global x1  : [0..1];
global x2  : [0..1];
global x3  : [0..1];
global x4  : [0..1];
global x5  : [0..1];
global x6  : [0..1];
global x7  : [0..1];
global x8  : [0..1];
global x9  : [0..1];
global x10  : [0..1];
global x11  : [0..1];
global x12  : [0..1];
global x13  : [0..1];
global x14  : [0..1];
global x15  : [0..1];
global x16  : [0..1];
global x17  : [0..1];
global x18  : [0..1];
global x19  : [0..1];
global x20  : [0..1];
global x21  : [0..1];
global x22  : [0..1];
global x23  : [0..1];
global x24  : [0..1];
global x25  : [0..1];
global x26  : [0..1];
global x27  : [0..1];
global x28  : [0..1];
global x29  : [0..1];
global x30  : [0..1];
global x31  : [0..1];
global x32  : [0..1];
global x33  : [0..1];
global x34  : [0..1];
global x35  : [0..1];
global x36  : [0..1];
global x37  : [0..1];
global x38  : [0..1];
global x39  : [0..1];
global x40  : [0..1];
global x41  : [0..1];

// module for process 1
module process1

	[]  (x1=1) & (x2=0) -> (x1'=0) & (x2'=1);
	[]  (x1=1) & (x2=1) -> (x1'=0) & (x2'=0);

endmodule

// add further processes through renaming
module process2 = process1 [ x1=x2, x2=x3 ] endmodule
module process3 = process1 [ x1=x3, x2=x4 ] endmodule
module process4 = process1 [ x1=x4, x2=x5 ] endmodule
module process5 = process1 [ x1=x5, x2=x6 ] endmodule
module process6 = process1 [ x1=x6, x2=x7 ] endmodule
module process7 = process1 [ x1=x7, x2=x8 ] endmodule
module process8 = process1 [ x1=x8, x2=x9 ] endmodule
module process9 = process1 [ x1=x9, x2=x10 ] endmodule
module process10 = process1 [ x1=x10, x2=x11 ] endmodule
module process11 = process1 [ x1=x11, x2=x12 ] endmodule
module process12 = process1 [ x1=x12, x2=x13 ] endmodule
module process13 = process1 [ x1=x13, x2=x14 ] endmodule
module process14 = process1 [ x1=x14, x2=x15 ] endmodule
module process15 = process1 [ x1=x15, x2=x16 ] endmodule
module process16 = process1 [ x1=x16, x2=x17 ] endmodule
module process17 = process1 [ x1=x17, x2=x18 ] endmodule
module process18 = process1 [ x1=x18, x2=x19 ] endmodule
module process19 = process1 [ x1=x19, x2=x20 ] endmodule
module process20 = process1 [ x1=x20, x2=x21 ] endmodule
module process21 = process1 [ x1=x21, x2=x22 ] endmodule
module process22 = process1 [ x1=x22, x2=x23 ] endmodule
module process23 = process1 [ x1=x23, x2=x24 ] endmodule
module process24 = process1 [ x1=x24, x2=x25 ] endmodule
module process25 = process1 [ x1=x25, x2=x26 ] endmodule
module process26 = process1 [ x1=x26, x2=x27 ] endmodule
module process27 = process1 [ x1=x27, x2=x28 ] endmodule
module process28 = process1 [ x1=x28, x2=x29 ] endmodule
module process29 = process1 [ x1=x29, x2=x30 ] endmodule
module process30 = process1 [ x1=x30, x2=x31 ] endmodule
module process31 = process1 [ x1=x31, x2=x32 ] endmodule
module process32 = process1 [ x1=x32, x2=x33 ] endmodule
module process33 = process1 [ x1=x33, x2=x34 ] endmodule
module process34 = process1 [ x1=x34, x2=x35 ] endmodule
module process35 = process1 [ x1=x35, x2=x36 ] endmodule
module process36 = process1 [ x1=x36, x2=x37 ] endmodule
module process37 = process1 [ x1=x37, x2=x38 ] endmodule
module process38 = process1 [ x1=x38, x2=x39 ] endmodule
module process39 = process1 [ x1=x39, x2=x40 ] endmodule
module process40 = process1 [ x1=x40, x2=x41 ] endmodule
module process41 = process1 [ x1=x41, x2=x1 ] endmodule

// formula, for use here and in properties: number of tokens
formula num_tokens = x1+x2+x3+x4+x5+x6+x7+x8+x9+x10+x11+x12+x13+x14+x15+x16+x17+x18+x19+x20+x21+x22+x23+x24+x25+x26+x27+x28+x29+x30+x31+x32+x33+x34+x35+x36+x37+x38+x39+x40+x41;

// set of initial states: at least one token
init
	func(mod,num_tokens,2) = 1
endinit

// label - stable configurations (1 token)
label "stable" = num_tokens=1;
