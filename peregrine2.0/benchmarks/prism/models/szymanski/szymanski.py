import sys

n = int(sys.argv[1])

print('dtmc')


#Entry protocol
#s1: flag[self] ← 1                    #Standing outside waiting room
#s2: await(all flag[1..N] ∈ {0, 1, 2}) #Wait for open door
#s3: flag[self] ← 3                    #Standing in doorway
#s4: if any flag[1..N] = 1:            #Another process is waiting to enter
#s5:   flag[self] ← 2                  #Waiting for other processes to enter
#s6:   await(any flag[1..N] = 4)       #Wait for a process to enter and close the door
#s7: flag[self] ← 4                    #The door is closed
#s8: await(all flag[1..self-1] ∈ {0, 1})   #Wait for everyone of lower ID to finish exit protocol
#s9: critical section
#    await(all flag[self+1..N] ∈ {0, 1, 4}) #Ensure everyone in the waiting room has
                                            #realized that the door is supposed to be closed
#s10:flag[self] ← 0 #Leave. Reopen door if nobody is still in the waiting room



for i in range(1,n+1):
    print(f'global flag{i}  : [0..4];')

for i in range(1,n+1):
    print(f'module process{i}')

    print(f'pc{i}  : [1..10];')



    print(f"[] (pc{i}=1) -> (pc{i}'=2) & (flag{i}'=1);")

    print(f"[] (pc{i}=2)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & (flag{j}<=2)", end='')
    print(f" -> (pc{i}'=3);")

    print(f"[] (pc{i}=3) -> (pc{i}'=4) & (flag{i}'=3);")

    for j in range(1,n+1):
        if i != j:
            print(f"[] (pc{i}=4) & (flag{j}=1) -> (pc{i}'=5);")

    print(f"[] (pc{i}=4)",end='')
    for j in range(1,n+1):
        if j != i:
            print(f" & !(flag{j}=1)", end='')
    print(f" -> (pc{i}'=7);")

    print(f"[] (pc{i}=5) -> (pc{i}'=6) & (flag{i}'=2);")

    for j in range(1,n+1):
        if i != j:
            print(f"[] (pc{i}=6) & (flag{j}=4) -> (pc{i}'=7);")

    print(f"[] (pc{i}=7) -> (pc{i}'=8) & (flag{i}'=4);")

    print(f"[] (pc{i}=8)",end='')
    for j in range(1,i):
        print(f" & (flag{j}<=1)", end='')
    print(f" -> (pc{i}'=9);")


    print(f"[] (pc{i}=9)",end='')
    for j in range(i+1,n+1):
        print(f" & ((flag{j}<=1) | (flag{j}=4))", end='')
    print(f" -> (pc{i}'=10);")

    print(f"[] (pc{i}=10) -> (pc{i}'=1) & (flag{i}'=0);")

    print('endmodule')

# label - first process in critical section
print('label "cs" = (pc1=9);')

# initial states (each process starts in state 0)
print('init')
print("(pc1=1) & (flag1=0)", end='')
for i in range(2,n+1):
    print(f" & (pc{i}=1) & (flag{i}=0)", end='')
print()
print('endinit')

