dtmc
global flag1  : [0..4];
global flag2  : [0..4];
global flag3  : [0..4];
global flag4  : [0..4];
global flag5  : [0..4];
global flag6  : [0..4];
module process1
pc1  : [1..10];
[] (pc1=1) -> (pc1'=2) & (flag1'=1);
[] (pc1=2) & (flag2<=2) & (flag3<=2) & (flag4<=2) & (flag5<=2) & (flag6<=2) -> (pc1'=3);
[] (pc1=3) -> (pc1'=4) & (flag1'=3);
[] (pc1=4) & (flag2=1) -> (pc1'=5);
[] (pc1=4) & (flag3=1) -> (pc1'=5);
[] (pc1=4) & (flag4=1) -> (pc1'=5);
[] (pc1=4) & (flag5=1) -> (pc1'=5);
[] (pc1=4) & (flag6=1) -> (pc1'=5);
[] (pc1=4) & !(flag2=1) & !(flag3=1) & !(flag4=1) & !(flag5=1) & !(flag6=1) -> (pc1'=7);
[] (pc1=5) -> (pc1'=6) & (flag1'=2);
[] (pc1=6) & (flag2=4) -> (pc1'=7);
[] (pc1=6) & (flag3=4) -> (pc1'=7);
[] (pc1=6) & (flag4=4) -> (pc1'=7);
[] (pc1=6) & (flag5=4) -> (pc1'=7);
[] (pc1=6) & (flag6=4) -> (pc1'=7);
[] (pc1=7) -> (pc1'=8) & (flag1'=4);
[] (pc1=8) -> (pc1'=9);
[] (pc1=9) & ((flag2<=1) | (flag2=4)) & ((flag3<=1) | (flag3=4)) & ((flag4<=1) | (flag4=4)) & ((flag5<=1) | (flag5=4)) & ((flag6<=1) | (flag6=4)) -> (pc1'=10);
[] (pc1=10) -> (pc1'=1) & (flag1'=0);
endmodule
module process2
pc2  : [1..10];
[] (pc2=1) -> (pc2'=2) & (flag2'=1);
[] (pc2=2) & (flag1<=2) & (flag3<=2) & (flag4<=2) & (flag5<=2) & (flag6<=2) -> (pc2'=3);
[] (pc2=3) -> (pc2'=4) & (flag2'=3);
[] (pc2=4) & (flag1=1) -> (pc2'=5);
[] (pc2=4) & (flag3=1) -> (pc2'=5);
[] (pc2=4) & (flag4=1) -> (pc2'=5);
[] (pc2=4) & (flag5=1) -> (pc2'=5);
[] (pc2=4) & (flag6=1) -> (pc2'=5);
[] (pc2=4) & !(flag1=1) & !(flag3=1) & !(flag4=1) & !(flag5=1) & !(flag6=1) -> (pc2'=7);
[] (pc2=5) -> (pc2'=6) & (flag2'=2);
[] (pc2=6) & (flag1=4) -> (pc2'=7);
[] (pc2=6) & (flag3=4) -> (pc2'=7);
[] (pc2=6) & (flag4=4) -> (pc2'=7);
[] (pc2=6) & (flag5=4) -> (pc2'=7);
[] (pc2=6) & (flag6=4) -> (pc2'=7);
[] (pc2=7) -> (pc2'=8) & (flag2'=4);
[] (pc2=8) & (flag1<=1) -> (pc2'=9);
[] (pc2=9) & ((flag3<=1) | (flag3=4)) & ((flag4<=1) | (flag4=4)) & ((flag5<=1) | (flag5=4)) & ((flag6<=1) | (flag6=4)) -> (pc2'=10);
[] (pc2=10) -> (pc2'=1) & (flag2'=0);
endmodule
module process3
pc3  : [1..10];
[] (pc3=1) -> (pc3'=2) & (flag3'=1);
[] (pc3=2) & (flag1<=2) & (flag2<=2) & (flag4<=2) & (flag5<=2) & (flag6<=2) -> (pc3'=3);
[] (pc3=3) -> (pc3'=4) & (flag3'=3);
[] (pc3=4) & (flag1=1) -> (pc3'=5);
[] (pc3=4) & (flag2=1) -> (pc3'=5);
[] (pc3=4) & (flag4=1) -> (pc3'=5);
[] (pc3=4) & (flag5=1) -> (pc3'=5);
[] (pc3=4) & (flag6=1) -> (pc3'=5);
[] (pc3=4) & !(flag1=1) & !(flag2=1) & !(flag4=1) & !(flag5=1) & !(flag6=1) -> (pc3'=7);
[] (pc3=5) -> (pc3'=6) & (flag3'=2);
[] (pc3=6) & (flag1=4) -> (pc3'=7);
[] (pc3=6) & (flag2=4) -> (pc3'=7);
[] (pc3=6) & (flag4=4) -> (pc3'=7);
[] (pc3=6) & (flag5=4) -> (pc3'=7);
[] (pc3=6) & (flag6=4) -> (pc3'=7);
[] (pc3=7) -> (pc3'=8) & (flag3'=4);
[] (pc3=8) & (flag1<=1) & (flag2<=1) -> (pc3'=9);
[] (pc3=9) & ((flag4<=1) | (flag4=4)) & ((flag5<=1) | (flag5=4)) & ((flag6<=1) | (flag6=4)) -> (pc3'=10);
[] (pc3=10) -> (pc3'=1) & (flag3'=0);
endmodule
module process4
pc4  : [1..10];
[] (pc4=1) -> (pc4'=2) & (flag4'=1);
[] (pc4=2) & (flag1<=2) & (flag2<=2) & (flag3<=2) & (flag5<=2) & (flag6<=2) -> (pc4'=3);
[] (pc4=3) -> (pc4'=4) & (flag4'=3);
[] (pc4=4) & (flag1=1) -> (pc4'=5);
[] (pc4=4) & (flag2=1) -> (pc4'=5);
[] (pc4=4) & (flag3=1) -> (pc4'=5);
[] (pc4=4) & (flag5=1) -> (pc4'=5);
[] (pc4=4) & (flag6=1) -> (pc4'=5);
[] (pc4=4) & !(flag1=1) & !(flag2=1) & !(flag3=1) & !(flag5=1) & !(flag6=1) -> (pc4'=7);
[] (pc4=5) -> (pc4'=6) & (flag4'=2);
[] (pc4=6) & (flag1=4) -> (pc4'=7);
[] (pc4=6) & (flag2=4) -> (pc4'=7);
[] (pc4=6) & (flag3=4) -> (pc4'=7);
[] (pc4=6) & (flag5=4) -> (pc4'=7);
[] (pc4=6) & (flag6=4) -> (pc4'=7);
[] (pc4=7) -> (pc4'=8) & (flag4'=4);
[] (pc4=8) & (flag1<=1) & (flag2<=1) & (flag3<=1) -> (pc4'=9);
[] (pc4=9) & ((flag5<=1) | (flag5=4)) & ((flag6<=1) | (flag6=4)) -> (pc4'=10);
[] (pc4=10) -> (pc4'=1) & (flag4'=0);
endmodule
module process5
pc5  : [1..10];
[] (pc5=1) -> (pc5'=2) & (flag5'=1);
[] (pc5=2) & (flag1<=2) & (flag2<=2) & (flag3<=2) & (flag4<=2) & (flag6<=2) -> (pc5'=3);
[] (pc5=3) -> (pc5'=4) & (flag5'=3);
[] (pc5=4) & (flag1=1) -> (pc5'=5);
[] (pc5=4) & (flag2=1) -> (pc5'=5);
[] (pc5=4) & (flag3=1) -> (pc5'=5);
[] (pc5=4) & (flag4=1) -> (pc5'=5);
[] (pc5=4) & (flag6=1) -> (pc5'=5);
[] (pc5=4) & !(flag1=1) & !(flag2=1) & !(flag3=1) & !(flag4=1) & !(flag6=1) -> (pc5'=7);
[] (pc5=5) -> (pc5'=6) & (flag5'=2);
[] (pc5=6) & (flag1=4) -> (pc5'=7);
[] (pc5=6) & (flag2=4) -> (pc5'=7);
[] (pc5=6) & (flag3=4) -> (pc5'=7);
[] (pc5=6) & (flag4=4) -> (pc5'=7);
[] (pc5=6) & (flag6=4) -> (pc5'=7);
[] (pc5=7) -> (pc5'=8) & (flag5'=4);
[] (pc5=8) & (flag1<=1) & (flag2<=1) & (flag3<=1) & (flag4<=1) -> (pc5'=9);
[] (pc5=9) & ((flag6<=1) | (flag6=4)) -> (pc5'=10);
[] (pc5=10) -> (pc5'=1) & (flag5'=0);
endmodule
module process6
pc6  : [1..10];
[] (pc6=1) -> (pc6'=2) & (flag6'=1);
[] (pc6=2) & (flag1<=2) & (flag2<=2) & (flag3<=2) & (flag4<=2) & (flag5<=2) -> (pc6'=3);
[] (pc6=3) -> (pc6'=4) & (flag6'=3);
[] (pc6=4) & (flag1=1) -> (pc6'=5);
[] (pc6=4) & (flag2=1) -> (pc6'=5);
[] (pc6=4) & (flag3=1) -> (pc6'=5);
[] (pc6=4) & (flag4=1) -> (pc6'=5);
[] (pc6=4) & (flag5=1) -> (pc6'=5);
[] (pc6=4) & !(flag1=1) & !(flag2=1) & !(flag3=1) & !(flag4=1) & !(flag5=1) -> (pc6'=7);
[] (pc6=5) -> (pc6'=6) & (flag6'=2);
[] (pc6=6) & (flag1=4) -> (pc6'=7);
[] (pc6=6) & (flag2=4) -> (pc6'=7);
[] (pc6=6) & (flag3=4) -> (pc6'=7);
[] (pc6=6) & (flag4=4) -> (pc6'=7);
[] (pc6=6) & (flag5=4) -> (pc6'=7);
[] (pc6=7) -> (pc6'=8) & (flag6'=4);
[] (pc6=8) & (flag1<=1) & (flag2<=1) & (flag3<=1) & (flag4<=1) & (flag5<=1) -> (pc6'=9);
[] (pc6=9) -> (pc6'=10);
[] (pc6=10) -> (pc6'=1) & (flag6'=0);
endmodule
label "cs" = (pc1=9);
init
(pc1=1) & (flag1=0) & (pc2=1) & (flag2=0) & (pc3=1) & (flag3=0) & (pc4=1) & (flag4=0) & (pc5=1) & (flag5=0) & (pc6=1) & (flag6=0)
endinit
