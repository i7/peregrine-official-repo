#!/bin/bash


for bench in herman_tokens ij lr peterson array burns dijkstra szymanski; do
    echo "Running benchmarks for $bench"
    ./run_prism.sh models/$bench models/$bench/$bench.pctl -b results_$bench.csv
done
