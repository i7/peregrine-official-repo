## Prerequisites

Compile the HOAF parser to enable verification with LTL and Buchi:

```
> cd ../jhoafparser
> ant
> cd ../benchmarks
```

## Reproducing benchmarks

The CAV'20 benchmarks can be reproduced by running:

```
> python3 main.py benchmark.txt -RAM 8192 | tee benchmark.out
```

The results can be converted into a LaTeX file with a table by running:

```
> python3 format.py benchmark.out | tee benchmark.tex
```

To produce and inspect the results as PDF file, first convert the .tex file and then open a PDF viewer. E.g. like this:

```
> pdflatex benchmark.tex
> evince benchmark.pdf
```

## New benchmarks

New benchmarks can be obtained by writing a new text file .
Each line in the file is a single call of the program.
The syntax is identical to the normal command line call.

E.g. if you want to call 

```
python3 src/main.py protocols/flock_log_reverse_k.py "[127]" -over -1
```

then add the following line to your benchmark file:

```
protocols/flock_log_reverse_k.py "[127]" -over -1
```
