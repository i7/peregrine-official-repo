# -*- coding: utf-8 -*-
import argparse
import json


def table(data):

    def sanitize(s):
        return s.replace('"[', "``[").replace(']"', "]''").replace('_', "\\_")\
            .replace("->", "$\\rightarrow$").replace("&", "\\&")

    TEMPLATE = "  {} & {} & {} & {}\\\\ "

    print("\\begin{tabular}{|r|r|r|r|r|r|r|r|r|r|r|r|r|r|}")
    print("  \\hline")
    print(TEMPLATE.format("Param./Processes", "$|Q|$", "$|T|$", "Time"))

    last_path = None

    for i in range(len(data)):
        d = data[i]

        if "timeout" not in d:
            path = sanitize(d["protocol_path"])
            if path != last_path:
                used_over = "" if d["settings"]["overapproximation_parameter"] == -1 else " {\\color{red} *}"
                print("  \\hline \\hline")
                print(f'\\multicolumn{{4}}{{|l|}}{{{d["protocol"]["family_name"] + used_over}}} \\\\ \\hline')

            parameters = "{" + sanitize(d["protocol_parameters"]) + "}"
            if i < len(data) - 1 and sanitize(data[i+1]["protocol_path"]) == path and "timeout" in data[i+1]:
                parameters = parameters + f' (TO: {sanitize(data[i+1]["protocol_parameters"])})'
            states = d["protocol"]["|Q|"]
            transitions = f'{d["protocol"]["orig_|T|"]}'
            t = d["elapsed"]["graphs"]
            time = "$<1s$" if float(t) < 1 else f'${round(float(t))}s$'

            print(TEMPLATE.format(parameters, states, transitions, time))
            last_path = path

    print("  \\hline")
    print("\\end{tabular}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser() 

    parser.add_argument("data", metavar="data", type=str,
                        help=("benchmarks data filename"))

    args = parser.parse_args()

    with open(args.data) as data_file:
        data = json.load(data_file)
        
    print("\\documentclass[border = 12pt]{standalone}")
    print("\\usepackage[utf8]{inputenc}")
    print("\\usepackage{color}")
    print("\\usepackage{multirow}")
    print("\\begin{document}")
    print("\\centering")
    table(data)
    print("\\end{document}")
    
