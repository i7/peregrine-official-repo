# -*- coding: utf-8 -*-
from enum import IntEnum


class Speed(IntEnum):
    ZERO          = 0
    QUADRATIC     = 100
    QUADRATIC_LOG = 200
    CUBIC         = 300
    POLYNOMIAL    = 400
    EXPONENTIAL   = 500
    NO_BOUND      = 1000


def pretty_speed(speed):
    labels = {None: "?",
              Speed.ZERO: "0",
              Speed.QUADRATIC: "n^2",
              Speed.QUADRATIC_LOG: "n^2 log(n)",
              Speed.CUBIC: "n^3",
              Speed.POLYNOMIAL: "poly(n)",
              Speed.EXPONENTIAL: "exp(n log n)",
              Speed.NO_BOUND: "no bound"}

    return labels[speed]


def speedToJSON(speed):
    if speed == Speed.ZERO:
        return "ZERO"
    if speed == Speed.QUADRATIC:
        return "QUADRATIC"
    if speed == Speed.QUADRATIC_LOG:
        return "QUADRATIC_LOG"
    if speed == Speed.CUBIC:
        return "CUBIC"
    if speed == Speed.POLYNOMIAL:
        return "POLYNOMIAL"
    if speed == Speed.EXPONENTIAL:
        return "EXPONENTIAL"
    return "NO_BOUND"
