# -*- coding: utf-8 -*-
import z3

nextID = 0  # global counter; allows to name Z3 vars differently ever time


def next_id():
    global nextID
    res = nextID
    nextID = nextID + 1         # increase by 1 -> new ID is different
    return res


def next_state_vars(protocol):
    id = next_id()
    return {q: z3.Int(f'_C{id}({q})') for q in protocol.states}


def next_alphabet_vars(protocol):
    id = next_id()
    return {a: z3.Int(f'_alphabet_{id}_{a}') for a in protocol.alphabet}


def z3name(q):  # safe names for states
    return "q" + z3safe(q)


def z3safe(s):  # safe encoding of strings that is as (human) readable as possible
    res = ""
    for c in str(s):
        if c == "_":
            res = res + "__"
        elif c == "(":
            res = res + "_L_"
        elif c == ")":
            res = res + "_R_"
        elif c == "[":
            res = res + "_LL_"
        elif c == "]":
            res = res + "_RR_"
        elif c == "{":
            res = res + "_LLL_"
        elif c == "}":
            res = res + "_RRR_"
        elif c in "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ":
            res = res + c
        else:
            res = res + "_" + str(ord(c)) + "_"
    return res



def is_sat(result):
    if result == z3.sat:
        return True
    elif result == z3.unsat:
        return False
    else:
        raise Exception("Unknown sat result")       # Throw error if Z3 can't decide query


def declsFor(state_vars, protocol):
    return {name: state_vars[q] for name, q in protocol.stateFromName.items()}


def parse_predicate(predicate, decls):
    #decls = {name: state_vars[state] for name, state in input_mapping.items()}
    ast = z3.parse_smt2_string(f"(assert {predicate})", decls=decls)
    if isinstance(ast, z3.z3.AstVector):
        return ast[0]
    else:
        return ast


def pretty_predicate(pred):
    if z3.is_var(pred):
        return str(pred)
    elif z3.is_app(pred):
        n = pred.num_args()
        if z3.is_not(pred):
            assert n == 1
            return ("¬({})".format(pretty_predicate(pred.arg(0))))
        if z3.is_and(pred):
            return ("{}".format('∧'.join('({})'.format(pretty_predicate(pred.arg(i))) for i in range(n))))
        elif z3.is_or(pred):
            return ("{}".format('∨'.join(pretty_predicate(pred.arg(i)) for i in range(n))))
        elif z3.is_add(pred):
            return ("{}".format('+'.join(pretty_predicate(pred.arg(i)) for i in range(n))))
        elif z3.is_sub(pred):
            return ("{}".format('-'.join(pretty_predicate(pred.arg(i)) for i in range(n))))
        elif z3.is_mul(pred):
            return ("{}".format('*'.join(pretty_predicate(pred.arg(i)) for i in range(n))))
        elif z3.is_mod(pred):
            assert n == 2
            return ("({})%{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_le(pred):
            assert n == 2
            return ("{}≤{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_lt(pred):
            assert n == 2
            return ("{}<{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_eq(pred):
            assert n == 2
            return ("{}={}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_distinct(pred):
            assert n == 2
            return ("{}≠{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_gt(pred):
            assert n == 2
            return ("{}>{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_ge(pred):
            assert n == 2
            return ("{}≥{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        elif z3.is_mod(pred):
            assert n == 2
            return ("{}%{}".format(pretty_predicate(pred.arg(0)), pretty_predicate(pred.arg(1))))
        else:
            return str(pred)
    else:
        return str(pred)


def rename_vars(pred, rename):
    if isinstance(pred, int):                         # normal int -> keep
        return pred
    if z3.is_const(pred) and z3.is_int_value(pred):   # constant -> keep
        return pred
    if z3.is_const(pred) and z3.is_int(pred):         # variable -> rename
        return rename(pred)
    if z3.is_app(pred):         # functions -> rebuild
        n = pred.num_args()
        if z3.is_not(pred):
            assert n == 1
            return z3.Not(rename_vars(pred.children()[0], rename))
        if z3.is_and(pred):
            return z3.And([rename_vars(c, rename) for c in pred.children()])
        elif z3.is_or(pred):
            return z3.Or([rename_vars(c, rename) for c in pred.children()])
        elif z3.is_add(pred):
            return z3.Sum([rename_vars(c, rename) for c in pred.children()])
        else:
            f = pred.decl()
            assert f.arity() == n
            return f([rename_vars(c, rename) for c in pred.children()])

    assert False, "encountered problem renaming variables in presburger formula"


def copy_pred(pred, excluded_vars={}):
    def rename(var):
        return var if var in excluded_vars else z3.Int(str(var) + "_copy")

    return rename_vars(pred, rename)
