class StableTerminationProperty:
    def __init__(self, precondition = None, postconditions={}, name="StableTermination"):
        self._precondition = precondition
        self._postconditions = dict(postconditions)   # dict: postcondition -> name (e.g. "True", "False", ... or "post")

        self.name = name    # name of stable termination property: TrueConsensus / FalseConsensus / Consensus / StableTermination

    @property
    def precondition(self):
        return self._precondition

    @property
    def postconditions(self):
        return self._postconditions

    def __str__(self):
        return ("(pre = {}, post = {}, name = {})"
                ).format(self._precondition, self._postconditions, self.name)

    def __repr__(self):
        return str(self)


