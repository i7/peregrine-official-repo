# -*- coding: utf-8 -*-
from Log import log


def export(stage_graph, filename, examples=False):
    try:
        from graph_tool.draw import graphviz_draw
        from graph_tool import Graph
    except ImportError:
        log("WARNING: Graph was not export to PDF because graph_tool is NOT installed!")
        return

    SIZE = (200, 150)
    FONT_SIZE = 40.0

    graph = Graph(directed=True)
    vertices = {}

    def add(stage):
        vertex = graph.add_vertex()
        index = graph.vertex_index[vertex]
        vertices[index] = stage
        for child in stage.children:
            child_index = add(child)
            graph.add_edge(index, child_index)
        return index

    for root in stage_graph.roots:
        add(root)

    labels = graph.new_vertex_property("string")
    colors = graph.new_vertex_property("string")

    for i in vertices:
        stage = vertices[i]
        labels[i] = stage.__str__(examples)
        colors[i] = "gold" if stage.failed else \
            "white" if stage.empty else \
                "deepskyblue1" if stage.satisfied_post is not None and stage.stable_termination_property.postconditions[
                    stage.satisfied_post] is "True" else \
                    "firebrick1" if stage.satisfied_post is not None and \
                                    stage.stable_termination_property.postconditions[
                                        stage.satisfied_post] is "False" else \
                        "azure2"
        # other good colors: mediumorchid1 darkslategray

    graphviz_draw(graph, layout="dot",
                  vprops={"shape": "circle", "label": labels, "fillcolor": colors,
                          "fontsize": FONT_SIZE},
                  output=filename, size=SIZE, ratio="auto", vsize=0.5)
