# -*- coding: utf-8 -*-
from collections import Counter
import z3


class Multiset(Counter):
    def __le__(self, other):
        return all(c <= other[q] for q, c in self.items())

    def __lt__(self, other):
        return self <= other and self != other

    def __ge__(self, other):
        return all(self[q] >= c for q, c in other.items())

    def __gt__(self, other):
        return self >= other and self != other

    def __hash__(self):
        return hash(frozenset(self.items()))

    # Plus only keeping positive values
    def __add__(self, other):
        return Multiset(Counter.__add__(self, other))

    # Minus only keeping positive values
    def __sub__(self, other):
        return Multiset(Counter.__sub__(self, other))

    def __str__(self):
        return "<{}>".format(', '.join("{}".format(q) for q in self.elements()))

    def __repr__(self):
        return str(self)

    def size(self):
        return sum(self.values())

    def as_set(self):
        return set(self.elements())

class Vector(Counter):
    def __le__(self, other):
        return all(c <= other[q] for q, c in self.items())

    def __lt__(self, other):
        return self <= other and self != other

    def __ge__(self, other):
        return all(self[q] >= c for q, c in other.items())

    def __gt__(self, other):
        return self >= other and self != other

    def __hash__(self):
        return hash(frozenset(self.items()))

    def __add__(self, other):
        result = Vector()
        for q in set(self) | set(other):
            c = self[q] + other[q]
            if c != 0:
                result[q] = c
        return result

    def __sub__(self, other):
        result = Vector()
        for q in set(self) | set(other):
            c = self[q] - other[q]
            if c != 0:
                result[q] = c
        return result

    def __str__(self):
        return "<{}>".format(', '.join('{}: {:+}'.format(q, c) for q, c in self.items()))

    def __repr__(self):
        return str(self)

# union of the upwards closure of a finite amount of multisets
class UpwardSet():
    def __init__(self, sets=None):
        if sets is None:
            self._sets = set()
        else:
            self._sets = set(sets)

    def copy(self):
        return UpwardSet(sets=self._sets)

    def __str__(self):
        return "{}".format(' v '.join('|{}|'.format(x) for x in self._sets))

    def __repr__(self):
        return str(self)

    def __iter__(self):
        return iter(self._sets)

    def __contains__(self, x):
        return x in self._sets or any(y <= x for y in self._sets)

    # Merges self and other
    def __ior__(self, other):
        self._sets.difference_update({ x for x in self._sets if x in other })
        self._sets.update({ x for x in other._sets if not x in self })
        return self

    def __or__(self, other):
        new_set = UpwardSet(self._sets)
        new_set |= other
        return new_set

    # Removes other from self
    def __isub__(self, other):
        self._sets.difference_update({ x for x in self._sets if x in other })
        return self

    def __sub__(self, other):
        new_set = UpwardSet(self._sets)
        new_set -= other
        return new_set

    def __len__(self):
        return len(self._sets)

    def __bool__(self):
        return bool(self._sets)

    # Add element x if not contained
    # Returns if x was added
    def add(self, x):
        to_remove = set()

        for y in self._sets:
            if y <= x:
                return False
            elif x <= y:
                to_remove.add(y)

        self._sets.add(x)
        self._sets.difference_update(to_remove)
        return True

    # z3 constaint: true if C in UpwardSet
    def z3_constraint_in(self, state_vars):
        return z3.Or([z3.And([state_vars[q] >= M[q] for q in M]) for M in self])

    # z3 constaint: true if C is not in UpwardSet
    def z3_constraint_out(self, state_vars):
        return z3.Not(self.z3_constraint_in(state_vars))
