# -*- coding: utf-8 -*-
import z3

from Log import log
from constraints import constraint_dead_approx, constraint_potentially_reachable, constraint_initial_configuration
from z3_util import next_state_vars, is_sat, parse_predicate, copy_pred


def compute_predicate_directly(protocol, steps=-1):
    log("trying to compute predicate...")
    # version 1: using Dead(T_b)
    reverse_state_vars = next_state_vars(protocol)
    state_varsT = next_state_vars(protocol)
    consensusT, exactT, _ = constraint_dead_approx(protocol, state_varsT,
                                        protocol.transitions - protocol.transitions_with_output(True), steps)
    state_varsF = next_state_vars(protocol)
    consensusF, exactF, _ = constraint_dead_approx(protocol, state_varsF,
                                        protocol.transitions - protocol.transitions_with_output(False), steps)

    if exactF and exactT:
        solver = z3.Solver()
        solver.add(consensusT)
        solver.add(consensusF)
        solver.add(constraint_potentially_reachable(protocol, reverse_state_vars, state_varsT))
        solver.add(constraint_potentially_reachable(protocol, reverse_state_vars, state_varsF))
        solver.add(constraint_initial_configuration(protocol, reverse_state_vars))
        solver.add(z3.Sum([reverse_state_vars[q] for q in protocol.states]) >= 1)
        if is_sat(solver.check()):
            model = solver.model()
            log(f'a config in approx that is in True and False: {({q: model[reverse_state_vars[q]].as_long() for q in protocol.states})}')
            return None
        else:
            log("YAY, approx is exact -> predicate computed!!!")
            pred = z3.And(consensusT, constraint_potentially_reachable(protocol, reverse_state_vars, state_varsT), constraint_initial_configuration(protocol, reverse_state_vars))
            predNeg = z3.And(consensusF, constraint_potentially_reachable(protocol, reverse_state_vars, state_varsF), constraint_initial_configuration(protocol, reverse_state_vars))
            log(f'predicate size: {len(str(pred))}')
            return pred, predNeg

def compute_predicate_with_stage_graph(protocol, graph):
    leafsT = {s for s in graph.stages_without_successor if s.output == True}
    leafsF = {s for s in graph.stages_without_successor if s.output == False}
    if len(leafsT) == 0:
        log("predicate: False")
        return False, True
    elif len(leafsF) == 0:
        log("predicate: True")
        return True, False
    else:
        initial_state_vars = list(leafsT)[0].initial_state_vars
        solver = z3.Solver()
        solver.add(z3.Or( [s.presburger for s in leafsT] ))
        solver.add(z3.Or( [copy_pred(s.presburger, excluded_vars=initial_state_vars.values()) for s in leafsF] ))
        solver.add(z3.Sum([initial_state_vars[q] for q in protocol.states]) >= 1)
        if is_sat(solver.check()):
            model = solver.model()
            log(f'a config in approx (v2) that is in True and False: {({q: model[initial_state_vars[q]].as_long() for q in protocol.states})}')
            return None
        else:
            log("YAY, approx (v2) is exact -> predicate computed using stage graph!!!")
            pred = z3.And(z3.Or( [s.presburger for s in leafsT] ))
            predNeg = z3.And(z3.Or( [s.presburger for s in leafsF] ))
            log(f'predicate size: {len(str(pred))}')
            return pred, predNeg


def check_pred_equivalence(protocol, pred, predNeg, actual_pred, initial_state_vars):
    solver = z3.Solver()
    solver.add(z3.Or(z3.And(pred, z3.Not(actual_pred)), z3.And(predNeg, actual_pred)))
    solver.add(z3.Sum([initial_state_vars[q] for q in protocol.states]) >= 1)
    if is_sat(solver.check()):
        log(f'the computed predicate is wrong!?')
        model = solver.model()
        log(f'config: {({q: model[initial_state_vars[q]].as_long() for q in protocol.states})}')
        return False
    else:
        log(f'the computed predicate is right :)')
        return True



    # # ------------------------------------------
    # # compute predicate for protocol
    # if settings.output_check == "termination" and not graph.failed:
    #     log("trying to compute predicate...")
    #     # version 1: using Dead(T_b)
    #     reverse_state_vars = next_state_vars(protocol)
    #     state_varsT = next_state_vars(protocol)
    #     consensusT, exactT = constraint_dead_approx(protocol, state_varsT,
    #                                         protocol.transitions - protocol.transitions_with_output(True), 8)
    #     state_varsF = next_state_vars(protocol)
    #     consensusF, exactF = constraint_dead_approx(protocol, state_varsF,
    #                                         protocol.transitions - protocol.transitions_with_output(False), 8)
    #
    #     if exactF and exactT:
    #         log("... using v1")
    #         solver = z3.Solver()
    #         solver.add(consensusT)
    #         solver.add(consensusF)
    #         solver.add(constraint_potentially_reachable(protocol, reverse_state_vars, state_varsT))
    #         solver.add(constraint_potentially_reachable(protocol, reverse_state_vars, state_varsF))
    #         solver.add(constraint_initial_configuration(protocol, reverse_state_vars))
    #         solver.add(z3.Sum([reverse_state_vars[q] for q in protocol.states]) >= 1)
    #         if is_sat(solver.check()):
    #             model = solver.model()
    #             log(f'a config in approx that is in True and False: {({q: model[reverse_state_vars[q]].as_long() for q in protocol.states})}')
    #         else:
    #             log("YAY, approx is exact -> predicate computed!!!")
    #             pred = z3.And(consensusT, constraint_potentially_reachable(protocol, reverse_state_vars, state_varsT), constraint_initial_configuration(protocol, reverse_state_vars))
    #             log(f'predicate size: {len(str(pred))}')
    #             predNeg = z3.And(consensusF, constraint_potentially_reachable(protocol, reverse_state_vars, state_varsF), constraint_initial_configuration(protocol, reverse_state_vars))
    #             actual_pred = parse_predicate(protocol.predicate_string, reverse_state_vars, protocol.input_mapping)
    #             solver = z3.Solver()
    #             solver.add(z3.Or(z3.And(pred, z3.Not(actual_pred)), z3.And(predNeg, actual_pred)))
    #             solver.add(z3.Sum([reverse_state_vars[q] for q in protocol.states]) >= 1)
    #             if is_sat(solver.check()):
    #                 log(f'the computed predicate is wrong!?')
    #                 model = solver.model()
    #                 log(f'config: {({q: model[reverse_state_vars[q]].as_long() for q in protocol.states})}')
    #             else:
    #                 log(f'the computed predicate is right :)')
    #
    #     else:
    #         # version 2: using stages
    #         log("... using v2")
    #         leafsT = {s for s in graph.stages_without_successor if s.output == True}
    #         leafsF = {s for s in graph.stages_without_successor if s.output == False}
    #         if len(leafsT) == 0:
    #             log("predicate: False")
    #         elif len(leafsF) == 0:
    #             log("predicate: True")
    #         else:
    #             initial_state_vars = list(leafsT)[0].initial_state_vars
    #             solver = z3.Solver()
    #             solver.add(z3.Or( [s.presburger for s in leafsT] ))
    #             solver.add(z3.Or( [copy_pred(s.presburger, excluded_vars=initial_state_vars.values()) for s in leafsF] ))
    #             solver.add(z3.Sum([initial_state_vars[q] for q in protocol.states]) >= 1)
    #             if is_sat(solver.check()):
    #                 model = solver.model()
    #                 log(f'a config in approx (v2) that is in True and False: {({q: model[initial_state_vars[q]].as_long() for q in protocol.states})}')
    #             else:
    #                 log("YAY, approx (v2) is exact -> predicate computed using stage graph!!!")
    #                 pred = z3.And(z3.Or( [s.presburger for s in leafsT] ))
    #                 log(f'predicate size: {len(str(pred))}')
    #                 predNeg = z3.And(z3.Or( [s.presburger for s in leafsF] ))
    #                 actual_pred = parse_predicate(protocol.predicate_string, initial_state_vars, protocol.input_mapping)
    #                 solver = z3.Solver()
    #                 solver.add(z3.Or(z3.And(pred, z3.Not(actual_pred)), z3.And(predNeg, actual_pred)))
    #                 solver.add(z3.Sum([initial_state_vars[q] for q in protocol.states]) >= 1)
    #                 if is_sat(solver.check()):
    #                     log(f'the computed predicate is wrong!?')
    #                     model = solver.model()
    #                     log(f'config: {({q: model[initial_state_vars[q]].as_long() for q in protocol.states})}')
    #                 else:
    #                     log(f'the computed predicate is right :)')
    # # ------------------------------------------