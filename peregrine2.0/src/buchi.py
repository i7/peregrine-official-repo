from transition import Transition
from multiset import Multiset
from protocol import Protocol
from Log import log
import json

class BuchiTransition:
    def __init__(self, pre, tags, post, final=False):
        self.pre = pre
        self.allowedTags = set(tags)
        self.post = post
        self.final = final

    def __str__(self):
        return "({}-{}->{},final: {})".format(self.pre, self.allowedTags, self.post, self.final)

    def __repr__(self):
        return str(self)

class Buchi:
    def __init__(self, states, aps, transitions, initial):
        self._states = frozenset(states)
        self._aps = frozenset(aps)
        self._transitions  = frozenset(transitions)
        self._initial = frozenset(initial)

    @property
    def states(self):
        return self._states

    @property
    def aps(self):
        return self._aps

    @property
    def transitions(self):
        return self._transitions

    @property
    def initial(self):
        return self._initial

    #@property
    #def str_state_mapping(self):
    #    return {f'_B{q}': q for q in self.states}

    #@property
    #def state_str_mapping(self):
    #    return {q: f'_B{q}' for q in self.states}

    def __str__(self):
        return ("Q = {}\n"
                "Σ = {}\n"
                "T = {}\n"
                "I = {}\n"
                ).format(set(self._states),
                                 set(self._aps),
                                 set(self._transitions),
                                 self._initial)

    def __repr__(self):
        return str(self)

