# -*- coding: utf-8 -*-
from multiset import Multiset
from stable_termination_property import StableTerminationProperty
from transition import Transition
from z3_util import z3name



class Protocol:
    def __init__(self,
                 states,
                 transitions,
                 name=None,
                 family_name=None,
                 initial_states=None,
                 leaders=None,
                 state_names=None,
                 precondition="true",
                 postcondition="true",
                 # handle: population protocol specifics
                 predicate=None,
                 alphabet_mapping=None,
                 output_mapping=None):
        # default arguments
        initial_states = {} if initial_states is None else initial_states
        leaders = Multiset() if leaders is None else leaders
        state_names = {} if state_names is None else state_names
        alphabet_mapping = {} if alphabet_mapping is None else alphabet_mapping
        output_mapping = {} if output_mapping is None else output_mapping

        # safe information
        self.states = frozenset(states)
        self.nameFromState = {q: state_names[q] if q in state_names else z3name(q) for q in self.states}    # state -> z3-safe string
        self.stateFromName = {name: q for q, name in self.nameFromState.items()}
        #print(self.nameFromState)
        assert len(self.states) == len(set(self.nameFromState.values())), "a state name was not unique"
        assert not any([c in "()#<>-+%!/*-" for n in self.nameFromState.values() for c in n]), "state names are not z3-safe"
        assert not any([n[0] == "_" for n in self.nameFromState.values()]), "state names may not start with '_'"

        #self.transitions = frozenset(transitions)                                                    # dont add 1-ways
        self.transitions = frozenset(set(transitions) | {Transition([s], [s]) for s in self.states})  # add 1-way trans.

        self.name = name                                         # name of protocol
        self.family_name = family_name if family_name else name  # name of protocol family (for parameterized families)
        self.initial_states = frozenset(initial_states) | frozenset(alphabet_mapping.values())  # states that may have agents in initial configuration
        self.leaders = leaders if leaders is not None else Multiset()  # multiset of "leader" agents (always present)
        self.precondition = precondition                         # precondition over state names (and alphabet)
        self.postcondition = postcondition                       # postcondition over state names

        # Population protocol specific
        alphabet_mapping = {str(a): alphabet_mapping[a] for a in alphabet_mapping}
        self.alphabet = frozenset(alphabet_mapping.keys())      # set of z3-safe strings
        #print(self.alphabet)
        assert not any([c in "()#<>-+%!/*-" for a in self.alphabet for c in a]), "alphabet is not z3-safe"
        assert not any([a[0] == "_" for a in self.alphabet]), "elements of the alphabet may not start with '_'"
        assert not self.stateFromName.keys() & self.alphabet, f'alphabet and state names must be disjunct: {self.stateFromName.keys() & self.alphabet}'
        self.alphabet_mapping = alphabet_mapping                # dictionary mapping alphabet to states
        self.predicate = predicate                              # predicate (over alphabet)
        self.output_mapping = output_mapping                    # output for each state: True/False/undefined

    def add_precondition(self, pre):
        self.precondition = f'(and {self.precondition} {pre})'

    def add_postcondition(self, post):
        self.postcondition = f'(and {self.postcondition} {post})'

    def stable_termination_properties(self, output_check):
        if self.predicate is None or output_check == "no check":
            return [StableTerminationProperty(self.precondition, {self.postcondition: "post"}, name="STABLE_TERMINATION")]  # pre => FG post

        # NOTE: the first "true" for 'and' (or the first "false" for 'or') makes the smt syntax correct...
        #       ... in case there is no state with the needed output: "(and )" -> "(and true)"
        no_true_output = f'(and true {" ".join("(= " + self.nameFromState[q] + " 0)" for q in self.output_mapping if self.output_mapping[q] == True)})'
        some_true_output = f'(or false {" ".join("(> " + self.nameFromState[q] + " 0)" for q in self.output_mapping if self.output_mapping[q] == True)})'
        no_false_output = f'(and true {" ".join("(= " + self.nameFromState[q] + " 0)" for q in self.output_mapping if self.output_mapping[q] == False)})'
        some_false_output = f'(or false {" ".join("(> " + self.nameFromState[q] + " 0)" for q in self.output_mapping if self.output_mapping[q] == False)})'

        # OPTIMIZATION
        if any(q not in self.output_mapping or (self.output_mapping[q] != True and self.output_mapping[q] != False) for q in self.states):     # undefined output possible
            # stronger check needed!
            at_east_one_agent = f'(or {" ".join("(> " + self.nameFromState[q] + " 0)" for q in self.states)})'
            # -> force at least one agent at start
            pre = f'(and {at_east_one_agent} {self.precondition})'
            pre_pred_true = f'(and {at_east_one_agent} {self.precondition} {self.predicate})'
            pre_pred_false = f'(and {at_east_one_agent} {self.precondition} (not {self.predicate}))'
            # -> force at least one agent with correct output at the end
            post_consensus_true = f'(and {self.postcondition} {no_false_output} {some_true_output})'
            post_consensus_false = f'(and {self.postcondition} {no_true_output} {some_false_output})'
        else:
            # all states have output -> only check that wrong output is missing
            pre = self.precondition
            pre_pred_true = f'(and {self.precondition} {self.predicate})'
            pre_pred_false = f'(and {self.precondition} (not {self.predicate}))'
            post_consensus_true = f'(and {self.postcondition} {no_false_output})'
            post_consensus_false = f'(and {self.postcondition} {no_true_output})'

        if output_check == "correctness":
            return [StableTerminationProperty(pre_pred_false, {post_consensus_false: "False"}, name="FALSE_CONSENSUS"),
                    StableTerminationProperty(pre_pred_true, {post_consensus_true: "True"}, name="TRUE_CONSENSUS")]
        else:       # consensus / well-specification
            return [StableTerminationProperty(pre, {post_consensus_true: "True", post_consensus_false: "False"}, name="CONSENSUS")]

    def __str__(self):
        return ("Name: {}\n"
                "Q = {}\n"
                "T = {}\n"
                "L = {}\n"
                "initial = {}\n"
                "str(Q) = {}\n"
                "pre = {}\n"
                "post = {}\n"
                "A = {}\n"
                "O = {}\n"
                "pred = {}\n"
                ).format(self.name,
                         set(self.states),
                         set(self.transitions),
                         self.leaders,
                         set(self.initial_states),
                         self.nameFromState,
                         self.precondition,
                         self.postcondition,
                         self.alphabet_mapping,
                         self.output_mapping,
                         self.predicate)

    def __repr__(self):
        return str(self)
