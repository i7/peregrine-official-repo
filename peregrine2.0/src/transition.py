# -*- coding: utf-8 -*-
from multiset import Multiset, Vector
from functools import reduce
import operator

class Transition:
    def __init__(self, pre, post, name="", tags=None):
        self._pre     = Multiset(pre)
        self._post    = Multiset(post)
        self._name   = name
        self._tags    = tags if tags is not None else {"_"}

    @property
    def pre(self):
        return self._pre

    @property
    def post(self):
        return self._post

    @property
    def name(self):
        return self._name

    @property
    def tags(self):
        return self._tags

    @property
    def delta(self):
        return Vector(self.post) - Vector(self.pre)

    def silent(self):
        return (self.pre == self.post)

    def __contains__(self, q):
        return (q in self._pre or q in self._post)

    def __eq__(self, other):
        return (self._pre == other._pre and self._post == other._post)

    def __ne__(self, other):
        return not (self == other)

    def __hash__(self):
        return hash((self._pre, self._post))

    def __str__(self):
        return "{} → {}:{}".format(str(self._pre), str(self._post), str(set(self._tags)))

    def __repr__(self):
        return str(self)

    def toJSON(self):
        return {"pre": [q for q in self.pre.elements()], "post": [q for q in self.post.elements()], "name": self.name}

    #def output(self, output_mapping):
    #    if all(output_mapping[q] for q in self.pre):
    #        return True
    #    if all(not output_mapping[q] for q in self.pre):
    #        return False
    #    return None

    @staticmethod
    def seq_delta(sigma):
        return reduce(operator.add, (t.delta for t in sigma), Vector())

