# -*- coding: utf-8 -*-
import argparse
import importlib.util
import json
import time

import z3

from buchi_util import buchi_x_protocol, ltl2buchi, buchi_from_hoa_file
from constraints import constraint_initial_configuration, constraint_potentially_reachable, constraint_dead_approx
from multiset import UpwardSet, Multiset
from stage_graph import StageGraph, GraphSettings
import Log
from Log import log
from speed import pretty_speed
from stage_graph_utils import export
from z3_util import next_state_vars, is_sat, copy_pred, parse_predicate

DESCRIPTION = "Automatic Verification of non-silent Population Protocols"


def load_protocol(filename, args):
    log("Loading protocol from {}...".format(filename))

    spec = importlib.util.spec_from_file_location("", filename)
    gen = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(gen)
    protocol = gen.generate(*args)

    log(f'{protocol}')
    return protocol


def execute(args):
    Log.verbose = args.verbose

    # graph_settings
    settings = GraphSettings(eventually_dead_method=args.eventually_dead_method,
                             iterative_PotReach=args.iterative_PotReach,
                             update_dead_in_terminal=args.update_dead_in_terminal,
                             deadoverapprox=args.dead_overapproximation,
                             keep_splitted_stage=args.keep_splitted_stage)

    # load protocol
    start = time.process_time()
    prot_args = json.loads(args.protocol[1]) if len(args.protocol) > 1 else []
    protocol = load_protocol(args.protocol[0], prot_args)
    end = time.process_time()
    log(f'Protocol loaded ({len(protocol.states)} states, {len(protocol.transitions)} transitions).')
    load_time = end - start
    log("Loading took {:.4f} seconds.".format(load_time))

    # changes (i.e. pre / post / ltl / buchi)
    start = time.process_time()
    if args.additional_precondition:
        protocol.add_precondition(args.additional_precondition)
    if args.additional_postcondition:
        protocol.add_postcondition(args.additional_postcondition)
    if args.ltl is not None:
        log("Generating buchi for LTL formular !({}) and performing product...".format(args.ltl[0]))
        protocol = buchi_x_protocol(ltl2buchi(args.ltl[0], args.owl[0], args.hoaparser[0]), protocol)
        log("\n result of product:")
        log(protocol)
    if args.buchi is not None:
        log("Performing product construction with Buchi automaton...")
        protocol = buchi_x_protocol(buchi_from_hoa_file(args.buchi[0], args.hoaparser[0]), protocol)
        log("\n result of product:")
        log(protocol)

    log("")
    change_time = time.process_time() - start

    # get stable termination property (that is going to be verified)
    stable_termination_properties = protocol.stable_termination_properties(args.output_check)
    log("property to check:")
    log(f'{stable_termination_properties}')
    log("")

    # build graph
    start = time.process_time()
    log("Generating stage graph...")
    graph = StageGraph(protocol, stable_termination_properties, settings=settings)
    log("Stage graph generated. ")
    log(f'Generated {len(graph.stages)} stages and {len(graph.stages_without_successor)} terminal stages.')
    end = time.process_time()
    graph_time = end - start
    log("Generation took {:.4f} seconds.".format(graph_time))

    log("")
    log("Graph as JSON:")
    log(graph.toJSON())
    log("")

    # verified?
    log(f'Protocol {"verified" if not graph.failed else "could not be verified"}')

    # speed?
    speed = pretty_speed(graph.speed)
    log(f'Protocol speed: {speed}')

    # output
    output = {}
    output["verified"] = not graph.failed
    output["speed"] = speed
    output["elapsed"] = {"loading": load_time, "graphs": graph_time, "changes": change_time}
    output["protocol"] = {"name": protocol.name,
                          "family_name": protocol.family_name,
                          "|Q|": len(protocol.states),
                          "|T|": len(protocol.transitions),
                          "orig_|T|": len(protocol.transitions) - len(protocol.states),
                          "predicate": protocol.predicate,
                          "preconditions": protocol.precondition,
                          "postconditions": protocol.postcondition}
    output["protocol_path"] = args.protocol[0]
    output["protocol_parameters"] = args.protocol[1] if len(args.protocol) > 1 else ""
    output["settings"] = settings.__dict__
    output["ltl"] = args.ltl
    output["buchi"] = args.buchi
    output["additional_precondition"] = args.additional_precondition
    output["additional_postcondition"] = args.additional_postcondition
    output["graph"] = graph.toJSON(args.examples)

    if args.pdf is not None:
        export(graph, args.pdf[0], args.examples)

    return json.dumps(output)

def args_parser():
    # Create parser and parse arguments
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument("protocol", metavar="protocol", nargs="+", type=str,
                        help=("protocol filename and "
                              "optional arguments as JSON list"))
    parser.add_argument("-pdf", "--pdf", metavar="filename", nargs=1, type=str,
                        help="export stage graph as PDF to filename")
    parser.add_argument("-v", "--verbose",
                        help="enable verbosity", action="store_true")
    parser.add_argument("-k", "--keep_splitted_stage",
                        help="don't hide splitted stages", action="store_true")
    parser.add_argument("-pre", "--additional_precondition", type=str,
                        help="adds an additional precondition for initial configurations")
    parser.add_argument("-post", "--additional_postcondition", type=str,
                        help="adds an additional postcondition for initial configurations")
    parser.add_argument("-iPR", "--iterative_PotReach",
                        help="enable improved (iterative) potential reachablity",
                        action="store_true")
    parser.add_argument("-e", "--eventually_dead_method", default="ranking->layered", metavar='METHOD',
                        choices=["ranking", "layered", "ranking->layered", "ranking+layered", "all", "speed"],
                        help="method for finding eventually dead transitions")
    parser.add_argument("-doa", "--dead_overapproximation",
                        help="use overapproximation for dead transition constraint", action="store_true")
    parser.add_argument("-oc", "--output_check", default="correctness",
                        choices=["correctness", "termination", "no check"],
                        help="check correctness / check termination / ignore output")
    parser.add_argument("-eager", "--update_dead_in_terminal",
                        help="find already dead transitions in terminal stages",
                        action="store_true")
    parser.add_argument("-ex", "--examples",
                        help="find a (small) example configuration for each stage",
                        action="store_true")
    parser.add_argument("-parser", "--hoaparser", metavar="hoa_parser", nargs=1,
                        default=["jhoafparser/dist/Hoa2Peregrine.jar"], type=str,
                        help="location of hoa parser (Hoa2Peregrine.jar)")
    parser.add_argument("-owl", "--owl", metavar="owl", nargs=1,
                        default=["owl"], type=str,
                        help="Command to use owl.")
    ltl_buchi_group = parser.add_mutually_exclusive_group()
    ltl_buchi_group.add_argument("-ltl", "--ltl", metavar="ltl_string", nargs=1, type=str,
                                 help="Verify a LTL formula. The atomic prepositions are the labels of the transitions.")
    ltl_buchi_group.add_argument("-buchi", "--buchi", metavar="hoa_file", nargs=1, type=str,
                                 help="Verify that a given Buchi automaton can't have an accepting run. "
                                      "The input is a file in HOA formate. "
                                      "The atomic prepositions are the labels of the transitions.")
    return parser


if __name__ == "__main__":
    parser = args_parser()
    args = parser.parse_args()

    out = execute(args)

    if out is not None:
        print(out)
