# -*- coding: utf-8 -*-
import z3

from multiset import UpwardSet, Multiset
from stage import Stage
from speed import Speed, speedToJSON, pretty_speed
from collections import deque
from Log import log
from z3_util import next_state_vars, next_alphabet_vars
from constraints import constraint_stage, constraint_dead_approx, constraint_potentially_reachable_iterative, \
    constraint_potentially_reachable


class GraphSettings:
    def __init__(self,
                 eventually_dead_method="speed",
                 iterative_PotReach=False,
                 update_dead_in_terminal=False,
                 deadoverapprox=False,
                 keep_splitted_stage=False):
        self.eventually_dead_method = eventually_dead_method  # "ranking" / "layered" / "ranking+layered" / "ranking->layered" / "speed"
        self.iterative_PotReach = iterative_PotReach
        self.update_dead_in_terminal = update_dead_in_terminal
        self.deadoverapprox = deadoverapprox
        self.keep_splitted_stage = keep_splitted_stage


class StageGraph:
    def __init__(self, protocol, stable_termination_properties, settings=GraphSettings()):
        self.protocol = protocol
        self.stable_termination_properties = stable_termination_properties
        self.settings = settings

        # accumulated info of stages
        self.speed = Speed.ZERO
        self.failed = False
        self.depth = 0
        self.stages = set()
        self.roots = set()
        self.stages_without_successor = set()

        # start procedure that automatically constructs the stage graph
        self.construct_graph()

    def update_accumulated_info(self, stage):
        self.speed = max(self.speed, stage.speed)
        if not stage.terminal and stage.failed:
            self.failed = True
        self.depth = max(self.depth, stage.depth)
        self.stages.add(stage)
        if not stage.is_root():
            self.stages_without_successor.discard(stage.parent)
        self.stages_without_successor.add(stage)
        if stage.is_root():
            self.roots.add(stage)

    def construct_graph(self):
        unprocessed = deque()

        def add_root_stage(stable_termination_property):
            log(f'Adding root state "{stable_termination_property.name}"')
            alphabet_vars = next_alphabet_vars(self.protocol)
            state_vars_initial = next_state_vars(self.protocol)
            state_vars_intermediate = next_state_vars(self.protocol)
            state_vars_end = next_state_vars(self.protocol)
            root = Stage(self, stable_termination_property,
                         alphabet_vars, state_vars_initial, state_vars_intermediate, state_vars_end)
            unprocessed.append(root)

        # add roots
        for stable_termination_property in self.stable_termination_properties:
            add_root_stage(stable_termination_property)

        log("------------------------")
        # work set algorithm
        while unprocessed:
            stage = unprocessed.pop()     # get an unprocessed stage
            if stage.is_root():
                log(f'--> root {stage.stable_termination_property.name}')

            log("")
            log(f'working on stage: {stage.id}')
            log("start: terminal")
            terminal = stage.update_terminal()          # check if terminal
            log(f'end: terminal -> {terminal}')

            if (not terminal) or self.settings.update_dead_in_terminal:
                log("start: finding already dead transitions")
                stage.update_dead()
                log("end: finding already dead transitions")

            if not terminal:   # need to continue?
                # try to find substages
                log("start: eventually dead")
                new_dead, speed, certificate = stage.eventually_dead(self.settings.eventually_dead_method)
                log(f'end: eventually dead (found {len(new_dead)} new dead transitions with speed {speed})')
                if new_dead:    # found new_dead
                    substage = stage.newSuccessor()
                    substage.mark_as_dead(new_dead)
                    stage.speed = speed
                    stage.certificate = certificate
                    unprocessed.append(substage)       # add new substage
                    log("eventually dead added a substage...")
                else:
                    # otherwise: try split
                    log("start: split")
                    death_certificates = stage.split()       # split via death certificates
                    log("end: split")
                    if death_certificates is None or not death_certificates:
                        log("FAILED!")
                        if not stage.terminal:
                            stage.failed = True     # split failed
                            stage.speed = Speed.NO_BOUND
                    else:
                        stage.speed = Speed.ZERO
                        log(f'--> splitting current stage: replacing it with {len(death_certificates)} new stages')

                        # don't add this stage but replace it with substages
                        for death_certificate in death_certificates:
                            substage = stage.newSuccessor()
                            m = UpwardSet()
                            for q in death_certificate:
                                m.add(Multiset({q: death_certificate[q] + 1}))
                            substage.add_to_removed_upward_set(m)

                            # add transitions that are dead because of death_certificate
                            new_dead = {t for t in self.protocol.transitions if any([q in death_certificate and death_certificate[q] < t.pre[q] for q in t.pre])}
                            substage.mark_as_dead(new_dead)
                            
                            if self.settings.keep_splitted_stage:
                                stage.children.add(substage)
                            else:
                                if not stage.is_root():
                                    stage.parent.children.add(substage)
                                substage.parent = stage.parent  # replace stage
                                
                            ### #if not stage.is_root():
                            ### #    
                            ### stage.children.add(substage)
                            ### #substage.parent = stage  # replace stage
                            unprocessed.append(substage)
                        
                        if not self.settings.keep_splitted_stage:
                            if not stage.is_root() and stage in stage.parent.children:
                                stage.parent.children.remove(stage)   # replaced stage -> remove
                            continue  # don't update accumulated info for this stage as it is removed
            else:
                log(f'ended this branch')
                log("------------------------")

            # add to stage graph (CARE! do this last to update accumulated info in stage_graph correctly)
            self.update_accumulated_info(stage)

    def toJSON(self, examples=False):
        # generate JSON for stages
        stages = [stage.toJSON(examples=examples) for stage in self.stages]
        # sort by id
        stages.sort(key=lambda stage: stage["id"])

        return {
            "failed": 1 if self.failed else 0,
            "speed": speedToJSON(self.speed),
            "speed_pretty": pretty_speed(self.speed),
            "nr_of_stages": len(self.stages),
            "nr_of_roots": len(self.roots),
            "rn_of_terminal_stages": len(self.stages_without_successor),
            "depth": self.depth,
            "stages": stages
        }
