# -*- coding: utf-8 -*-
import itertools
import z3

from Log import log
from z3_util import parse_predicate, next_id, is_sat, declsFor
from multiset import Vector, UpwardSet, Multiset


def constraint_disabled_transitions(U, state_vars, delta=None):  # set of transitions U is disabled
    return z3.And([constraint_disabled_transition(t, state_vars, delta) for t in U])


def constraint_disabled_transition(t, state_vars, delta=None):  # transition t is disabled
    if delta is None:  # delta allows to add some offset to the configuration while checking
        delta = Vector()
    return z3.Or([state_vars[q] + delta[q] < t.pre[q] for q in t.pre])


def constraint_potentially_reachable(protocol, state_vars_initial, state_vars_end, dead_transitions=None):
    dead_transitions = set() if dead_transitions is None else dead_transitions

    def symbolic_largest_unmarked_siphon(states, transitions, r_var, q_var, x_var, trap=False):
        n = len(states)

        constraints = []
        # R is a siphon or trap
        for t in transitions:
            m1, m2 = (t.pre, t.post) if trap else (t.post, t.pre)
            constraints.append(z3.Implies(z3.And(x_var[t] > 0, z3.Or([r_var[r] == n for r in m1])),
                                          z3.Or([r_var[r] == n for r in m2])))

        # Constraints for transitions that prove that q is not part of the largest siphon or trap
        def excluded(q):
            for t in transitions:
                m1, m2 = (t.pre, t.post) if trap else (t.post, t.pre)
                if q in m1:
                    yield z3.And(x_var[t] > 0, z3.And([r_var[q_] < r_var[q] for q_ in m2]))

        # R is the largest empty siphon or trap
        for q in states:
            constraints.append(r_var[q] >= 0)
            constraints.append(r_var[q] <= n)
            constraints.append(z3.Implies(r_var[q] == n, q_var[q] == 0))
            constraints.append(z3.Implies(r_var[q] == 0, q_var[q] > 0))
            constraints.append(z3.Implies(z3.And(r_var[q] > 0, r_var[q] < n), z3.Or(list(excluded(q)))))

        return constraints

    ID = next_id()

    x_var = {t: z3.Int(f'_tX{ID}_{idx}') for idx, t in enumerate(protocol.transitions)}
    c0_siphon_var = {q: z3.Int(f'_siphon{ID}_{protocol.nameFromState[q]}') for q in protocol.states}
    c_trap_var = {q: z3.Int(f'_trap{ID}_{protocol.nameFromState[q]}') for q in protocol.states}

    constraints = []

    # add marking equation
    for t in protocol.transitions:
        constraints.append(x_var[t] >= 0)  # nr of times trans. t is fired can't be negative
    for t in dead_transitions:
        constraints.append(x_var[t] == 0)  # dead transitions cannot be fired
    for q in protocol.states:
        constraints.append(state_vars_end[q] >= 0)  # nr of agents in state can't be negative
    for q in protocol.states:
        constraints.append(state_vars_end[q] == state_vars_initial[q] +
                           z3.Sum([t.delta[q] * x_var[t] for t in protocol.transitions if
                                   q in t.delta]))  # marking equation

    # find largest unmarked siphon at C0 and largest unmarked trap at C
    n = len(protocol.states)
    constraints += symbolic_largest_unmarked_siphon(protocol.states, protocol.transitions, c0_siphon_var,
                                                    state_vars_initial, x_var, False)
    constraints += symbolic_largest_unmarked_siphon(protocol.states, protocol.transitions, c_trap_var, state_vars_end,
                                                    x_var, True)

    # trap/siphon constraints for states
    for q in protocol.states:
        constraints.append(z3.Implies(c0_siphon_var[q] == n, state_vars_end[q] == 0))
        constraints.append(z3.Implies(c_trap_var[q] == n, state_vars_initial[q] == 0))

    # trap/siphon constraints for transitions
    for t in protocol.transitions:
        constraints.append(z3.Implies(z3.Or([c0_siphon_var[r] == n for r in t.pre]), x_var[t] == 0))
        constraints.append(z3.Implies(z3.Or([c_trap_var[r] == n for r in t.post]), x_var[t] == 0))

    return z3.And(constraints)


def constraint_potentially_reachable_iterative(initial_config_constraint, protocol, state_vars_initial, state_vars_end, dead_transitions=None):
    dead_transitions = set() if dead_transitions is None else dead_transitions

    # call basic potential reachability
    constraint = constraint_potentially_reachable(protocol, state_vars_initial, state_vars_end, dead_transitions)

    # find already dead transitions
    new_dead_transitions = set()
    solver = z3.Solver()
    solver.add(initial_config_constraint)
    solver.add(constraint)
    for t in protocol.transitions - dead_transitions:
        # check if t is dead
        solver.push()
        solver.add(z3.Not(constraint_disabled_transition(t, state_vars_end)))
        sat = is_sat(solver.check())
        solver.pop()
        if not sat:
            new_dead_transitions.add(t)  # t is dead

    if not new_dead_transitions:
        return constraint   # no iteration needed
    else:
        # found transitions that are dead -> they may not occur in the PotReach's t-invariant
        log(f'PotReach: found {len(new_dead_transitions)} already dead transitions -> iterate...')
        return constraint_potentially_reachable_iterative(initial_config_constraint, protocol, state_vars_initial, state_vars_end, dead_transitions | new_dead_transitions)


def constraint_initial_configuration(protocol, alphabet_vars, state_vars):
    # initial states only AND force leaders
    constraints = []
    for q in protocol.states:
        if q in protocol.initial_states:
            constraints.append(state_vars[q] >= protocol.leaders[q])
        else:
            constraints.append(state_vars[q] == protocol.leaders[q])

    # handle alphabet mapping
    for a in protocol.alphabet:
        constraints.append(alphabet_vars[a] >= 0)   # only positive inputs
    for q in set(protocol.alphabet_mapping.values()):
        constraints.append(z3.Sum([alphabet_vars[a] for a in protocol.alphabet if protocol.alphabet_mapping[a] == q]) == state_vars[q])

    return z3.And(constraints)
    
def constraint_initial_and_pre(protocol, alphabet_vars, state_vars_initial, precondition):
    decls = declsFor(state_vars_initial, protocol)
    decls.update(alphabet_vars)

    constraints = []
    constraints.append(constraint_initial_configuration(protocol, alphabet_vars, state_vars_initial))   # initial configuration
    constraints.append(parse_predicate(precondition, decls))       # precondition

    return z3.And(constraints)


def constraint_stage(protocol, alphabet_vars, state_vars_initial, state_vars_intermidiate, state_vars_end, precondition, iterative_PotReach=False):
    constraints = [constraint_initial_and_pre(protocol, alphabet_vars, state_vars_initial)]

    # need inductivity -> add PotReach
    if iterative_PotReach:
        constraints.append(constraint_potentially_reachable_iterative(z3.And(constraints), protocol, state_vars_initial, state_vars_end))
    else:
        constraints.append(constraint_potentially_reachable(protocol, state_vars_initial, state_vars_end))

    return z3.And(constraints)


def constraints_ranking(protocol, dead):
    E = protocol.transitions - dead
    y_var = {q: z3.Int(f'_rank{protocol.nameFromState[q]}') for q in protocol.states}

    base_constraints = []
    # base constraints
    for q in protocol.states:
        base_constraints.append(y_var[q] >= 0)
    for t in E:
        base_constraints.append(z3.Sum([c * y_var[q] for q, c in t.delta.items()]) <= 0)

    # constraints that force transition t to be dead
    t_is_dead_constraints = {t: z3.Sum([c * y_var[q] for q, c in t.delta.items()]) < 0 for t in E}

    # constraints to exclude states (needed for fixpoint / fast check when calculating speed)
    q_is_excluded_constraints = {q: y_var[q] == 0 for q in protocol.states}

    # function to extract ranking function
    def get_ranking_function(model):
        return {q: model[y_var[q]].as_long() for q in protocol.states}

    return base_constraints, t_is_dead_constraints, q_is_excluded_constraints, get_ranking_function


def constraints_layered(protocol, dead):
    E = protocol.transitions - dead
    y_var = {q: z3.Int((f'_layer_rank{protocol.nameFromState[q]}')) for q in protocol.states}
    d_var = {t: z3.Int((f'_is_in_layer{idx}')) for idx, t in enumerate(E)}

    base_constraints = []
    for q in protocol.states:
        base_constraints.append(y_var[q] >= 0)
    for t in E:
        base_constraints.append(z3.Or(d_var[t] == 0, d_var[t] == 1))  # d_var is quasi-boolean (t is in layer or not)

    def reenabled(t, u, u_):
        return u_.pre <= t.pre + (u.pre - t.post)

    for t in E:  # if t is in layer => ranking function decreasing when firing t
        base_constraints.append(z3.Implies(d_var[t] == 1, z3.Sum([c * y_var[q] for q, c in t.delta.items()]) < 0))

    for t, u in itertools.product(E, E):  # make sure for t in layer: dis(t) => dead(t)
        if t != u and not any(reenabled(t, u, u_) for u_ in dead):
            U_ = (u_ for u_ in E if reenabled(t, u, u_))
            base_constraints.append(
                z3.Implies(z3.And(d_var[t] == 0, d_var[u] == 1), z3.Or([d_var[u_] == 1 for u_ in U_])))

    def t_is_dead(model, t):  # test, if t is dead in model
        return model.evaluate(d_var[t]) == 1

    def at_least_size_k(k):  # add constraint to force at least k dead transitions
        return z3.Sum([d_var[t] for t in E]) >= k

    def get_ranking_function(model):  # run to extract ranking function
        return {q: model[y_var[q]].as_long() for q in protocol.states}

    return base_constraints, at_least_size_k, t_is_dead, get_ranking_function


def constraints_layered_with_death_certificate(protocol, dead):
    E = protocol.transitions - dead
    y_var = {q: z3.Int(f'_l_y_{protocol.nameFromState[q]}') for q in protocol.states}        # invariant factors
    d_var = {t: z3.Int(f'_l_d{idx}') for idx, t in enumerate(E)}     # t in layer?

    # death_certificate C^w[q]
    dc_w_var = {q: z3.Int(f'_l_dc_w_{protocol.nameFromState[q]}') for q in protocol.states}     # death_certificate C^w[q] is omega?
    dc_var = {q: z3.Int(f'_l_dc_{protocol.nameFromState[q]}') for q in protocol.states}       # death_certificate C^w[q] if not omega?

    base_constraints = []
    for q in protocol.states:
        base_constraints.append(y_var[q] >= 0)  # factors are positive
        base_constraints.append(z3.Or(dc_w_var[q] == 0, dc_w_var[q] == 1))  # dc_w_var is quasi-boolean
        base_constraints.append(dc_var[q] >= 0)  # non-omega values of death certificate are positive
    for t in E:
        base_constraints.append(z3.Or(d_var[t] == 0, d_var[t] == 1))  # d_var is quasi-boolean

    def can_be_enabled(t):
        return z3.And([z3.Or(dc_w_var[q] == 1, t.pre[q] <= dc_var[q]) for q in t.pre])

    # condition 1)
    for t in E:  # if t is in layer => ranking function decreasing when firing t
        base_constraints.append(z3.Implies(d_var[t] == 1, z3.Sum([c * y_var[q] for q, c in t.delta.items()]) < 0))

    # condition 2)
    for t in E:
        # if t in layer => disabled by death certificate
        base_constraints.append(z3.Implies(d_var[t] == 1, z3.Not(can_be_enabled(t))))
    for t in E:
        # is death certificate: t enabled => does not increase non-omega places
        base_constraints.append(z3.Implies(can_be_enabled(t), z3.And([dc_w_var[q] == 1 for q in t.post if t.delta[q] > 0])))
    for t in E:
        c = z3.Implies(d_var[t] == 1,       # t \in U
                       z3.And( [ z3.Implies(dc_w_var[q] == 0,       # x = {q times (dc_var[q] + 1)}
                                            z3.Or([ z3.And(d_var[u] == 1,       # u \in U
                                                               z3.And([ z3.Or(t.post[r] >= u.pre[r], z3.And(r == q, dc_var[r] + 1 >= u.pre[r]))
                                                                        for r in protocol.states]))     # (t.post | x) >= r.pre
                                                    for u in E]))
                                 for q in protocol.states] ))
        base_constraints.append(c)

    def t_is_dead(model, t):  # test, if t is dead in model
        return model.evaluate(d_var[t]) == 1

    def at_least_size_k(k):  # add constraint to force at least k dead transitions
        return z3.Sum([d_var[t] for t in E]) >= k

    def get_ranking_function(model):  # run to extract ranking function
        return {q: model[y_var[q]].as_long() for q in protocol.states}

    def get_death_certificate(model):  # run to extract the used death certificate
        return {q: model[dc_var[q]].as_long() for q in protocol.states if model[dc_w_var[q]] == 0}

    return base_constraints, at_least_size_k, t_is_dead, get_ranking_function, get_death_certificate


def constraint_dead_approx(protocol, state_vars, U, overappoximation_parameter):
    # run backwards coverability algorithm
    # overappoximation_parameter = 0 -> Dis(U)
    # overappoximation_parameter > 0 -> U stays disabled for at least i transitions
    # overappoximation_parameter = -1 -> exact coverability
    E = protocol.transitions - U

    def new_predecessors(m, markings):
        predecessors = UpwardSet()
        for M in markings:
            for t in E:
                Mpre = (M - t.post) + t.pre
                if Mpre not in m:
                    predecessors.add(Mpre)
        return predecessors

    log(f'start: computing InductiveOverapproximation (with -over {overappoximation_parameter})')
    m = UpwardSet()
    new_markings = UpwardSet()

    for t in U:
        new_markings.add(t.pre)

    iteration = 0
    while new_markings and (overappoximation_parameter < 0 or iteration <= overappoximation_parameter):
        m |= new_markings
        log(f'.. adding {len(new_markings)} minimal markings (total: {len(m)})')
        new_markings = new_predecessors(m, new_markings)
        iteration += 1

    exact = True if not new_markings else False
    not_in_m = z3.And([z3.Or([state_vars[q] < M[q] for q in M]) for M in m])
    jsonCNF = [[{"state": q, "value": M[q] - 1} for q in M] for M in m]
    log(f'end: computing InductiveOverapproximation')
    return not_in_m, exact, jsonCNF


def constraint_configuration_of_stage_not_covered_by_certificates(stage, death_certificates):
    constraints = []
    constraints.append(stage.presburger())  # C is in stage

    def not_in_certificate(certificate):
        # certificate with 1 minimal set
        Cw_i = certificate  # -> just 1 minimal element Cw_i (if q not in Cw_i => omega)
        return z3.Or([stage.state_vars[q] > Cw_i[q] for q in Cw_i])

    for death_certificate in death_certificates:
        constraints.append(not_in_certificate(death_certificate))

    return z3.And(constraints)


def constraints_best_death_certificate(stage, k=-1):
    states = stage.protocol.states
    transitions = stage.protocol.transitions
    E = transitions - stage.dead
    n = len(states)
    if k < 0:
        k = max(t.pre[q] for t in transitions for q in t.pre)
    x_var = {q: z3.Int(f'_OmegaTime_{stage.protocol.nameFromState[q]}') for q in stage.protocol.states}  # time between 0 and |Q| when C(q) is set to omega

    base_constraints = []
    base_constraints.append(stage.presburger())                       # guess some C in stage

    # idea: Cw[q] = C[q] if x_var[q] = n else omega

    for q in states:
        base_constraints.append(x_var[q] >= 0)
        base_constraints.append(x_var[q] <= n)

        base_constraints.append(
            (x_var[q] < n)
            ==
            (z3.Or(
                stage.state_vars[q] >= k, 
                z3.Or(
                    [z3.And(
                        t.delta[q] > 0,  
                        z3.And(
                            [z3.Or(
                                stage.state_vars[q_] >= t.pre[q_], 
                                x_var[q_] < x_var[q]
                            ) for q_ in t.pre]
                        )
                    ) 
                    for t in E if q in t.post]
                )
            )
            )
        )

    base_constraints.append(
        z3.Or([z3.And(x_var[q] == n, t.pre[q] > stage.state_vars[q]) for t in E for q in t.pre]))  # Cw_disables something

    def not_in_other_certificate(certificate):
        # certificate with 1 minimal set
        Cw_i = certificate  # -> just 1 minimal element Cw_i (if q not in Cw_i => omega)
        return z3.Or([stage.state_vars[q] > Cw_i[q] for q in Cw_i])

    def more_omegas(omegas):
        sum_omegas = z3.Sum([z3.If(x_var[q] < n, 1, 0) for q in states])
        return sum_omegas > omegas

    def more_rest(omegas, rest):
        sum_omegas = z3.Sum([z3.If(x_var[q] < n, 1, 0) for q in states])
        same_omegas = sum_omegas == omegas
        sum_rest = z3.Sum([z3.If(x_var[q] == n, stage.state_vars[q], 0) for q in states])
        more_rest = sum_rest > rest
        return z3.And(same_omegas, more_rest)

    def get_death_certificate(model):  # run to extract ranking function
        return {q: model[stage.state_vars[q]].as_long() for q in states if model[x_var[q]].as_long() == n}

    return base_constraints, not_in_other_certificate, more_omegas, more_rest, get_death_certificate