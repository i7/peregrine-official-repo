verbose = False         # enable / disable logging
file = False            # enable / disable additional output of log as file

def log(message):
    global verbose

    if verbose:
        print(message)

        if file:
            f = open("log.txt", "a")
            f.write("{}\n".format(message))
            f.close()
