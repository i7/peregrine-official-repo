# -*- coding: utf-8 -*-
import pprint

from Log import log
import z3

from constraints import *
from multiset import UpwardSet
from z3_util import parse_predicate, is_sat, declsFor
from speed import pretty_speed, Speed, speedToJSON

nextStageID = 0  # global counter; for assigning a different ID to every stage


def next_stage_id():
    global nextStageID
    res = nextStageID
    nextStageID = nextStageID + 1  # increase by 1 -> new ID is different
    return res


class Stage:
    def __init__(self, stage_graph, stable_termination_property,
                 alphabet_vars, initial_state_vars, intermediate_state_vars, state_vars,
                 parent=None):
        self.id = next_stage_id()  # unique ID
        self.stage_graph = stage_graph  # pointer to stage_graph
        self.stable_termination_property = stable_termination_property
        self.alphabet_vars = alphabet_vars  # dict: alphabet -> z3.Int
        self.initial_state_vars = initial_state_vars  # dict: state -> z3.Int (#agents per state of initial configuration that lead to configuration of stage)
        self.intermediate_state_vars = intermediate_state_vars  # dict: state -> z3.Int (#agents per state of intermediate configuration where transitions are disabled)
        self.state_vars = state_vars  # dict: state -> z3.Int (#agents in state when in some configuration of stage)
        self.parent = parent  # pointer to parent in stage_graph

        # upward closed set of configurations that are not in the stage
        self.removed_upward_set = parent.removed_upward_set.copy() if not self.is_root() else UpwardSet()

        # help vars
        self.protocol = stage_graph.protocol  # pointer to protocol
        self.speed = Speed.NO_BOUND  # speed of transitions from this stage to a successor stage
        self.terminal = parent.terminal if not self.is_root() else False  # output as desired & postconditions hold?
        self.satisfied_post = parent.satisfied_post if not self.is_root() else None  # for graphical output
        self.failed = parent.failed if not self.is_root() else False  # failed the algorithm at this stage?
        self.empty = parent.empty if not self.is_root() else False  # was detected that the stage is empty?
        self.depth = parent.depth + 1 if not self.is_root() else 0  # depth in stage_graph
        self.dead = set(parent.dead) if not self.is_root() else set()  # dead transitions
        self.updated_terminal = False  # flag: only update terminal once
        self.updated_dead = False  # flag: only update dead transitions once
        self.children = set()  # set with all children (i.e. successors)
        self._exampleConfig = None  # minimal example (only calculated once)
        self._exampleConfigCalculated = False  # minimal example (only calculated once)
        self.certificate = []  # (JSON) certificate function for transition to the ONLY successor stage (only when found by EvDead)

    def newSuccessor(self):
        child = Stage(self.stage_graph,
                     self.stable_termination_property,
                     self.alphabet_vars,
                     self.initial_state_vars,
                     self.intermediate_state_vars,
                     self.state_vars,
                     parent=self)
        self.children.add(child)
        self._exampleConfigCalculated = False  # example may be in the substage -> invalid
        return child

    def presburger(self):
        constraints = [constraint_initial_and_pre(self.protocol, self.alphabet_vars, self.initial_state_vars, self.stable_termination_property.precondition)]
        if self.dead and self.stage_graph.settings.deadoverapprox:
            if self.stage_graph.settings.iterative_PotReach:
                constraints.append(constraint_potentially_reachable_iterative(z3.And(constraints), self.protocol, self.initial_state_vars, self.intermediate_state_vars))
                constraints.append(constraint_disabled_transitions(self.dead, self.intermediate_state_vars))
                constraints.append(constraint_potentially_reachable_iterative(z3.And(constraints), self.protocol, self.intermediate_state_vars, self.state_vars))
            else:
                constraints.append(constraint_potentially_reachable(self.protocol, self.initial_state_vars, self.intermediate_state_vars))
                constraints.append(constraint_disabled_transitions(self.dead, self.intermediate_state_vars))
                constraints.append(constraint_potentially_reachable(self.protocol, self.intermediate_state_vars, self.state_vars))
        else:
            if self.stage_graph.settings.iterative_PotReach:
                constraints.append(constraint_potentially_reachable_iterative(z3.And(constraints), self.protocol, self.initial_state_vars, self.state_vars))
            else:
                constraints.append(constraint_potentially_reachable(self.protocol, self.initial_state_vars, self.state_vars))
        
        constraints.append(self.removed_upward_set.z3_constraint_out(self.state_vars))
        
        return z3.And(constraints)

    def add_to_removed_upward_set(self, new):
        self._exampleConfigCalculated = False   # example is invalidated
        self.updated_dead = False
        self.updated_terminal = False
        alive = self.protocol.transitions - self.dead
        log(f'adding new elements to NotInUpperSet & making it inductive (for stage {self.id})')
        log(f'.. start: ({len(self.removed_upward_set)})    {self.removed_upward_set}')
        log(f'.. new: ({len(new)})    {new}')
        while new:
            self.removed_upward_set |= new
            log(f'.. adding {len(new)}    {new}      minimal markings (total: {len(self.removed_upward_set)}    {self.removed_upward_set})')
            next = UpwardSet()
            for M in new:
                for t in alive:
                    Mpre = (M - t.post) + t.pre
                    if Mpre not in self.removed_upward_set:
                        next.add(Mpre)
            new = next
        log(f'.. final: ({len(self.removed_upward_set)})    {self.removed_upward_set}')
        

    def mark_as_dead(self, new_dead):  # mark a set of transitions as dead
        new_dead -= self.dead
        self.dead |= new_dead
        if self.stage_graph.settings.deadoverapprox:
            return
        
        m = UpwardSet()
        for t in new_dead:
            m.add(t.pre)

        self.add_to_removed_upward_set(m)

    def is_root(self):
        return self.parent is None

    def satisfies(self, p):
        solver = z3.Solver()
        solver.add(z3.And(self.presburger(), z3.Not(p)))  # try to satisfy stage and negated predicate
        return not is_sat(solver.check())

    def update_terminal(self):
        if self.updated_terminal:  # only update once
            return self.terminal

        self.updated_terminal = True
        solver = z3.Solver()
        solver.add(self.presburger())

        # check if empty (-> trivially terminal)
        log(".. checking: empty")
        if not is_sat(solver.check()):
            self.terminal = True
            self.empty = True
            self.speed = Speed.ZERO
            log("stage is empty! -> terminal")
            return True

        # check terminal:
        decls = declsFor(self.state_vars, self.protocol)
        for postcondition, name in self.stable_termination_property.postconditions.items():
            # check: stage |= postcondition
            log(f'.. checking postcondition {name}')
            solver.push()  # don't use self.satisfies() to reuse solver -> faster
            solver.add(z3.Not(parse_predicate(postcondition, decls)))
            sat = is_sat(solver.check())
            solver.pop()
            if not sat:
                log(f'.. stage satisfies postcondition {name} -> terminal')
                self.satisfied_post = postcondition
                self.terminal = True
                self.speed = Speed.ZERO
                return True

        return False

    def update_dead(self):
        if self.updated_dead:  # only update once
            return

        log(".. finding already dead transitions")
        self.updated_dead = True
        solver = z3.Solver()
        solver.add(self.presburger())
        new_dead = set()

        for t in self.protocol.transitions - self.dead:
            # check: stage |= Dis(t)
            solver.push()  # don't use self.satisfies() to reuse solver -> faster
            solver.add(z3.Not(constraint_disabled_transition(t, self.state_vars)))
            sat = is_sat(solver.check())
            solver.pop()
            if not sat:
                log(f'.... found: {t}')
                new_dead.add(t)

        log(f'.. found {len(new_dead)} already dead transitions')
        self.mark_as_dead(new_dead)
        return

    def eventually_dead(self, method):  # returns list of dead transitions
        def dead_by_ranking(stage):
            log(".. running dead_by_ranking")
            solver = z3.Solver()

            base_constraints, t_is_dead_constraints, _, get_ranking_function = constraints_ranking(stage.protocol, stage.dead)
            for c in base_constraints:  # prep solver
                solver.add(c)

            D = set()
            certificate = {q: 0 for q in stage.protocol.states}
            for t in stage.protocol.transitions - stage.dead:  # check transition t
                if t in D:
                    continue
                solver.push()
                solver.add(t_is_dead_constraints[t])
                sat = is_sat(solver.check())
                if sat:
                    D.add(t)
                    model = solver.model()
                    Y = get_ranking_function(model)
                    for q in stage.protocol.states:
                        certificate[q] = certificate[q] + Y[q]
                    log(f'.... ranking: found trans. {t} (r(t) = {sum(t.delta[q] * Y[q] for q in t.pre | t.post)}) with ranking function {({q: Y[q] for q in Y if Y[q] != 0})}')
                    for t2 in stage.protocol.transitions - stage.dead - D:  # check if Y is ranking function for other transitions (OPTIMIZATION)
                        if sum(t2.delta[q] * Y[q] for q in t2.pre | t2.post) < 0:
                            D.add(t2)
                            log(f'...... and additionally: {t2} (r(t) = {sum(t2.delta[q] * Y[q] for q in t2.pre | t2.post)})')
                solver.pop()

            return frozenset(D), Speed.POLYNOMIAL, certificate

        def dead_by_layered(stage):
            log(".. running dead_by_layered")
            base_constraints, at_least_size_k, t_is_dead, get_ranking_function = constraints_layered(stage.protocol, stage.dead)

            E = stage.protocol.transitions - stage.dead
            solver = z3.Solver()  # prep solver
            for c in base_constraints:
                solver.add(c)

            D = set()
            certificate = dict()
            k = 1  # find largest layer
            while True:
                solver.push()
                solver.add(at_least_size_k(k))  # force size to be at least k
                sat = is_sat(solver.check())
                if sat:
                    model = solver.model()
                    solver.pop()
                    D = {t for t in E if t_is_dead(model, t)}  # determine transitions of layer
                    certificate = get_ranking_function(model)  # extract ranking function
                    k = len(D) + 1
                else:
                    solver.pop()
                    break

            if D:
                log(f'.... layered: found transitions {D} with ranking function {({q: certificate[q] for q in certificate if certificate[q] != 0})}')

            return frozenset(D), Speed.EXPONENTIAL, certificate

        def dead_by_layered_with_death_certificate(stage):
            log(".. running dead_by_layered_with_death_certificate")
            base_constraints, at_least_size_k, t_is_dead, get_ranking_function, get_death_certificate = constraints_layered_with_death_certificate(
                stage.protocol, stage.dead)

            E = stage.protocol.transitions - stage.dead
            solver = z3.Solver()  # prep solver
            for c in base_constraints:
                solver.add(c)

            D = set()
            certificate = dict()
            k = 1  # find largest layer
            while True:
                solver.push()
                solver.add(at_least_size_k(k))  # force size to be at least k
                sat = is_sat(solver.check())
                if sat:
                    model = solver.model()
                    solver.pop()
                    D = {t for t in E if t_is_dead(model, t)}  # determine transitions of layer
                    certificate = get_ranking_function(model)  # extract ranking function
                    k = len(D) + 1
                else:
                    solver.pop()
                    break

            if D:
                log(f'... layered w. DC: found transitions {D} with ranking function {({q: certificate[q] for q in certificate if certificate[q] != 0})} and death certificate {get_death_certificate(model)}')

            return frozenset(D), Speed.EXPONENTIAL, certificate

        def dead_with_speed(stage):  # returns: (dead, speed)
            log(". running dead_with_speed")

            def fast(stage, states_to_check, new_dead):  # fast check (see CONCUR18)
                checked = dict()
                for A in states_to_check:
                    enabled_transition_with_A = [z3.Not(constraint_disabled_transition(t, stage.state_vars)) for t in new_dead
                                                 if A in t.pre]
                    exists_enabled_transition_with_A = z3.Or(enabled_transition_with_A)
                    solver = z3.Solver()
                    solver.add(stage.presburger())
                    solver.add(z3.Not(constraint_disabled_transitions(new_dead, stage.state_vars)))
                    solver.add(stage.state_vars[A] > 0)
                    solver.add(z3.Not(exists_enabled_transition_with_A))
                    if is_sat(solver.check()):
                        checked[A] = False
                        return False, checked
                    else:
                        checked[A] = True

                return True, checked

            def fixpoint_algorithm(protocol, dead, new_dead):  # remove transitions that might be reenabled
                new_dead = set(new_dead)
                stable = (len(new_dead) == 0)
                while not stable:
                    to_remove = set()
                    for t_new in new_dead:
                        # check if 't_new' can be reenabled
                        for other in protocol.transitions - dead - new_dead:
                            # assume 'other' can reenable 't_new'
                            can_be_reenabled = True
                            for t_already_dead in new_dead | dead:
                                if t_already_dead.pre <= other.pre + (t_new.pre - other.post):
                                    # can't, because 't_new_other' would be enabled
                                    can_be_reenabled = False
                                    break

                            if can_be_reenabled:
                                to_remove.add(t_new)
                                break

                    if len(to_remove) > 0:
                        new_dead -= to_remove
                    else:
                        stable = True

                return new_dead

            new_dead, speed, certificate = dead_by_ranking(stage)  # try ranking
            if not new_dead:
                return dead_by_layered(stage)  # if ranking fails: layered -> c^n

            # -> at least n^c

            # fix_point algorithm
            log(".... running fixpoint algorithm")  # remove t that might be reenabled
            fixpoint_dead = fixpoint_algorithm(self.stage_graph.protocol, self.dead, new_dead)  # suc -> n^3

            if not fixpoint_dead:
                return new_dead, speed, certificate  # -> n^c from ranking

            # -> at least n³
            # try to show "fast" by finding ranking function that has special properties
            mem = dict()  # to memorize if fast check failed for given state
            base_constraints, t_is_dead_constraints, q_is_excluded_constraints, get_ranking_function = constraints_ranking(
                self.stage_graph.protocol, self.dead)
            while True:
                # compute some ranking function
                solver = z3.Solver()
                for c in base_constraints:
                    solver.add(c)
                for t in fixpoint_dead:
                    solver.add(t_is_dead_constraints[t])
                for q in {q for q in mem if not mem[q]}:
                    solver.add(q_is_excluded_constraints[q])

                log(".... trying to find fast ranking function...")
                if not is_sat(solver.check()):
                    return fixpoint_dead, Speed.CUBIC, certificate

                model = solver.model()
                certificate = get_ranking_function(model)  # computed ranking function

                log("...... checking a potential ranking function: fast?")  # check fast property for each state in ranking function
                was_fast, checked = fast(stage, {q for q in certificate if certificate[q] > 0 and q not in mem}, fixpoint_dead)
                if was_fast:
                    log(f'.... found fast ranking function {({q: certificate[q] for q in certificate if certificate[q] != 0})}')
                    return fixpoint_dead, Speed.QUADRATIC_LOG, certificate  # found "fast" ranking function -> n^2 log n
                else:
                    mem.update(checked)

        new_dead = set()
        speed = Speed.NO_BOUND
        certificate = {q: 0 for q in self.protocol.states}
        if method == "speed":
            new_dead, speed, certificate = dead_with_speed(self)
        elif method == "ranking":  # only ranking
            new_dead, speed, certificate = dead_by_ranking(self)
        elif method == "layered":  # only layered
            new_dead, speed, certificate = dead_by_layered(self)
        elif method == "ranking->layered":  # ranking (w. fallback lay.)
            new_dead, speed, certificate = dead_by_ranking(self)
            if not new_dead:
                new_dead, speed, certificate = dead_by_layered(self)
        elif method == "all":  # ranking -> lay. -> lay w. DC
            new_dead, speed, certificate = dead_by_ranking(self)
            if not new_dead:
                new_dead, speed, certificate = dead_by_layered(self)
            if not new_dead:
                new_dead, speed, certificate = dead_by_layered_with_death_certificate(self)

        # convert certificate to JSON
        certificate = [{"state": q, "value": certificate[q]} for q in certificate if certificate[q] != 0]
        # sort by state
        certificate.sort(key=lambda i: i['state'])
        return new_dead, speed, certificate

    def split(self):  # returns list of death_certificates

        def find_best_death_certificate():
            log(".... start finding best death certificate")
            death_certificate = None
            omegas = 0
            rest = 0

            log(f'...... trying to maximize the number of omegas...')
            while True:
                solver.push()
                solver.add(more_omegas(omegas))
                sat = is_sat(solver.check())
                if sat:
                    model = solver.model()
                    solver.pop()
                    death_certificate = get_death_certificate(model)
                    omegas = len(self.protocol.states) - len(death_certificate)
                    rest = sum(death_certificate.values())
                    log(f'...... found certificate with {omegas} omegas and rest {rest}')
                else:
                    solver.pop()
                    log(f'...... trying to maximize the rest...')
                    while True:
                        solver.push()
                        solver.add(more_rest(omegas, rest))
                        sat = is_sat(solver.check())
                        if sat:
                            model = solver.model()
                            solver.pop()
                            death_certificate = get_death_certificate(model)
                            rest = sum(death_certificate.values())
                            log(f'...... found certificate with {omegas} omegas and rest {rest}')
                        else:
                            solver.pop()
                            break
                    break

            log(f'.... best death vertificate is {death_certificate}')
            return death_certificate

        death_certificates = []
        base_constraints, not_in_other_certificate, more_omegas, more_rest, get_death_certificate = constraints_best_death_certificate(self)
        solver = z3.Solver()  # prep solver
        for c in base_constraints:
            solver.add(c)

        log(f'.. looking for first death certificate..')
        while True:

            death_certificate = find_best_death_certificate()

            if death_certificate is None:
                # no more configurations with other empty siphons
                break
            if death_certificate:
                # found some death certificate
                log(".. added death_certificate {} to the split".format(death_certificate))
                death_certificates.append(death_certificate)
                solver.add(not_in_other_certificate(death_certificate))
            else:
                log(".. death certificate: analog to largest empty siphon is empty set (i.e. found certificate only contains omegas)")
                return None

        log(f'.. death certificates in the split: {death_certificates}')

        # check that the split covers the hole stage
        solver = z3.Solver()
        solver.add(constraint_configuration_of_stage_not_covered_by_certificates(self, death_certificates))
        if is_sat(solver.check()):
            model = solver.model()
            log(f'split is not complete {({q: model.evaluate(self.state_vars[q]) for q in self.protocol.states if model.evaluate(self.state_vars[q]).as_long() > 0})} -> fail')
            return None

        return death_certificates

    def smallest_example(self):
        if self._exampleConfigCalculated:
            return self._exampleConfig

        self._exampleConfigCalculated = True

        log(f'calculating example configuration for stage with id {self.id}')
        solver = z3.Solver()
        solver.add(self.presburger())
        solver.add(z3.Sum([self.initial_state_vars[q] for q in self.protocol.states]) > 1)  # at least 2 agents...

        # find a config that is not already in a substage
        for substage in self.children:
            solver.add(substage.removed_upward_set.z3_constraint_in(self.state_vars))

        # if failed: find config that does not satisfy ("some") postcondition
        if self.failed:
            decls = declsFor(self.state_vars, self.protocol)
            for postcondition, name in self.stable_termination_property.postconditions.items():
                solver.add(z3.Not(parse_predicate(postcondition, decls)))

        if not is_sat(solver.check()):
            return None

        model = solver.model()
        initial = {q: model[self.initial_state_vars[q]].as_long() for q in self.protocol.states}
        k = sum(initial.values())
        k_min = 1

        # binary search for smallest k = |C|
        while True:
            if k == k_min:
                break
            k_test = int((k + k_min) / 2)
            solver.push()
            solver.add(z3.Sum([self.initial_state_vars[q] for q in self.protocol.states]) <= k_test)
            sat = is_sat(solver.check())
            if sat:
                model = solver.model()
                initial = {q: model[self.initial_state_vars[q]].as_long() for q in self.protocol.states}
                k = sum(initial.values())
            else:
                k_min = k_test + 1
            solver.pop()

        alpha = {a: model[self.alphabet_vars[a]].as_long() for a in self.protocol.alphabet}
        end = {q: model[self.state_vars[q]].as_long() for q in self.protocol.states}

        if self.is_root() and not self.failed:
            end = initial   # use initial configuration instead

        self._exampleConfig = alpha, initial, end
        log(f'-> {self._exampleConfig}')
        return self._exampleConfig

    def __str__(self, example=False):
        def pretty_set(S):
            return pprint.pformat(set(S)).replace("'", "") if len(S) > 0 else "{}"

        s = []
        if self.is_root():
            s.append("Root")

        s.append(f'ID: {self.id}')

        if self.speed is not None:
            s.append("Speed: {}".format(pretty_speed(self.speed)))

        new_dead = self.dead if self.is_root() else self.dead - self.parent.dead
        if len(new_dead) > 0:
            s.append("New Dead: {}".format(pretty_set(new_dead)))

        if self.terminal:
            s.append(f'Terminal! {"" if self.satisfied_post is None else " -> " + self.stable_termination_property.postconditions[self.satisfied_post]}')

        if self.failed:
            s.append("Failed!")

        if self.empty:
            s.append("Empty!")

        if example:
            smallest_example = self.smallest_example()
            if smallest_example is not None:
                alpha, C_in, C_end = smallest_example
                s.append(f'EXAMPLE: alpha: {({q: alpha[q] for q in alpha if alpha[q] > 0})} \n -> initial: {({q: C_in[q] for q in C_in if C_in[q] > 0})} \n -> end: {({q: C_end[q] for q in C_end if C_end[q] > 0})}')

        return '\n'.join(s)

    def __repr__(self):
        return str(self)

    def toJSON(self, examples=False):
        # calculate constraint JSON
        constraint = [[{"state": q, "value": M[q] - 1} for q in M]
                      for M in self.removed_upward_set]
        if self.is_root():
            constraint = []     # --> smaller constraint is good enough (i.e. only PotReach(init))
        # sort the constraint
        map(lambda x: x.sort(key=lambda i: i['state']), constraint)
        constraint.sort(key=lambda i: "" if len(i) == 0 else str(i[0]["state"]))
        constraint.sort(key=len)

        res = {
            "id": f'S{self.id}',
            "checkedProperty": self.stable_termination_property.name,
            "deadTransitions": [t.toJSON() for t in self.dead if not t.silent()],
            "failed": self.failed,
            "constraint": constraint
        }
        if not self.is_root():
            res["parent"] = f'S{self.parent.id}'
        if len(self.children) > 0:  # non-terminal (or failed), i.e. has children
            res["certificate"] = self.certificate
            res["eventuallyDeadTransitions"] = [t.toJSON() for t in
                                                set.intersection(*[child.dead for child in self.children]) - self.dead
                                                if not t.silent()]
            res["speed"] = speedToJSON(self.speed)
            res["speed_pretty"] = pretty_speed(self.speed)

        if examples:
            smallest_example = self.smallest_example()
            if smallest_example is not None:
                alpha, C_in, C_end = smallest_example
                res["configuration"] = {q: C_end[q] for q in C_end}
                res["potReachFromConfiguration"] = {q: C_in[q] for q in C_in}
            else:
                res["configuration"] = {}
                res["potReachFromConfiguration"] = {}

        return res
