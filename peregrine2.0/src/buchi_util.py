from buchi import Buchi, BuchiTransition
import subprocess
import os
import json
from multiset import Multiset
from transition import Transition
from protocol import Protocol
from Log import log


def ltl2buchi(ltl, owl, hoa2peregrine):
    ltl = f'!({ltl})'  # negate as we test for unsatisfiable
    intermediate = "owl.out"

    # remove old intermediate file
    if os.path.exists(intermediate):
        os.remove(intermediate)

    owl = owl.split(" ")
    owl_res = subprocess.run(
        owl + ["-O", intermediate, "-i", ltl, "ltl",
               "---", "simplify-ltl",
               "---", "ltl2ldba",
               "---", "minimize-aut",
               "---", "hoa"])
    if owl_res.returncode != 0:
        raise EnvironmentError("The Call to Owl failed. Make sure the LTL formula is correct and that "
                               "OWL is installed correctly at the specified location.")

    res = buchi_from_hoa_file(intermediate, hoa2peregrine)

    log(res)

    # remove intermediate file
    if os.path.exists(intermediate):
        os.remove(intermediate)

    return res


def buchi_from_hoa_file(file, hoa2peregrine):
    parsed = subprocess.check_output(["java", "-jar", hoa2peregrine, file])
    return buchi_from_json(parsed)


def buchi_from_json(json_string):
    j = json.loads(json_string)
    Q = {f'_B{q}' for q in range(j["nrofstates"])}
    aps = j["aps"]
    initial = {f'_B{q}' for q in j["initial"]}
    T = set()
    for t in j["transitions"]:
        T.add(BuchiTransition(f'_B{t["from"]}', t["aps"], f'_B{t["to"]}', t["final"]))
    return Buchi(Q, aps, T, initial)


# Product construction for a Buchi automaton and a protocol
def buchi_x_protocol(buchi, protocol):
    assert not buchi.states & protocol.states, f'there was a naming collision between the states of the buchi ' \
                                               f'automaton ({buchi.states}) and the states of the protocol ({protocol.states}). Please rename them.'

    assert "_fail" not in buchi.states and "_fail" not in protocol.states, \
        "The state '_fail' is reserved for the construction!"

    states = set(protocol.states | buchi.states)
    state_names = protocol.nameFromState    # buchi names are generated automatically
    name = f'Buchi X ({protocol.name})'
    family_name = f'Buchi X ({protocol.family_name})'
    initial_states=frozenset( protocol.initial_states | buchi.initial)

    accepting = set()
    failState = "_fail"
    needFailState = False

    # let the Buchi automaton observe the protocol
    T = set()
    for t in protocol.transitions:
        relevant_tags = t.tags & buchi.aps
        if not relevant_tags:
            relevant_tags = {"_"}
        for bq in buchi.states:
            handled = False
            for bt in buchi.transitions:
                if bt.pre != bq:
                    continue
                log(f't.tags:{t.tags} t.relevTags{relevant_tags} bt.allowed{bt.allowedTags}')
                if relevant_tags & bt.allowedTags:
                    pre = Multiset(t.pre)
                    pre[bt.pre] += 1
                    post = Multiset(t.post)
                    post[bt.post] += 1
                    new_t = Transition(pre, post, relevant_tags & bt.allowedTags)
                    T.add(new_t)

                    if bt.final:
                        accepting.add(new_t)

                    handled = True
            if not handled:
                needFailState = True
                pre = Multiset(t.pre)
                pre[bq] += 1
                post = Multiset(t.post)
                post[failState] += 1
                new_t = Transition(pre, post, t.tags | {"Fail"})
                T.add(new_t)

    if needFailState:
        states.add(failState)
        # add idle transitions
        for t in protocol.transitions:
            pre = Multiset(t.pre)
            pre[failState] += 1
            post = Multiset(t.post)
            post[failState] += 1
            new_t = Transition(pre, post, t.tags | {"Failed"})
            T.add(new_t)

    product = Protocol(states, T,
                       name=name,
                       family_name=family_name,
                       initial_states=initial_states,
                       leaders=protocol.leaders,
                       state_names=state_names,
                       precondition=protocol.precondition,
                       postcondition=protocol.postcondition,
                       predicate=protocol.predicate,
                       alphabet_mapping=protocol.alphabet_mapping,
                       output_mapping=protocol.output_mapping)

    # make sure there is exactly 1 agent in a initial state of the Buchi automaton
    product.add_precondition("(= (+ {}) 1)".format(' '.join([product.nameFromState[q] for q in buchi.initial])))

    # add postcondition to check
    # i.e. accepting transitions are disabled -> no accepting run in buchi automaton
    new_post = "(and {})".format(' '.join(
        ["(or {})".format(' '.join(
            ["(< {} {})".format(product.nameFromState[q], t.pre[q]) for q in t.pre] if t.pre.size() > 0 else "(< 1 0)"
        ))
            for t in accepting]
    )) if len(accepting) > 0 else "(< 0 1)"
    product.add_postcondition(new_post)

    return product
