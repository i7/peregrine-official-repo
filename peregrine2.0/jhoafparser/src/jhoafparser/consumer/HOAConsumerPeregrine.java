package jhoafparser.consumer;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import jhoafparser.ast.*;

/**
 * This {@code HOAConsumer} renders the method calls
 * to produce a valid HOA automaton output. 
 */
public class HOAConsumerPeregrine implements HOAConsumer {
    
    /** The output writer */
    protected Writer out;
    int biggest_state = -1;
    List<Transition> transitions = new LinkedList<Transition>();
    List<Integer> initial = new LinkedList<Integer>();
    List<String> aps = new LinkedList<>();
    HashMap<String, BooleanExpression<AtomLabel>> alias = new HashMap<>();

    public HOAConsumerPeregrine(OutputStream out) {
            this.out = new BufferedWriter(new OutputStreamWriter(out));
    }

    public static HOAConsumerFactory getFactory(final OutputStream stream)
    {
            return new HOAConsumerFactory() {
                    @Override
                    public HOAConsumer getNewHOAConsumer()
                    {
                            return new HOAConsumerPeregrine(stream);
                    }
            };
    }

    @Override
    public boolean parserResolvesAliases() {
            return false;
    }

    @Override
    public void notifyHeaderStart(String version) {

    }

    @Override
    public void setNumberOfStates(int numberOfStates) {
    }

    @Override
    public void addStartStates(List<Integer> stateConjunction) {
            initial = stateConjunction;
    }

    @Override
    public void addAlias(String name, BooleanExpression<AtomLabel> labelExpr) {
        alias.put(name, labelExpr);
    }

    @Override
    public void setAPs(List<String> aps) {
       for (String ap : aps)
           this.aps.add("\"" + ap + "\"");
       
       this.aps.add("\"_\"");
    }

    @Override
    public void setAcceptanceCondition(int numberOfSets,
                    BooleanExpression<AtomAcceptance> accExpr) {
    }

    @Override
    public void provideAcceptanceName(String name, List<Object> extraInfo) {
    }

    @Override
    public void setName(String name) {
    }

    @Override
    public void setTool(String name, String version) {
    }

    @Override
    public void addProperties(List<String> properties) {
    }

    @Override
    public void addMiscHeader(String name, List<Object> content) {
    }

    @Override
    public void notifyBodyStart() {
    }

    @Override
    public void addState(int id, String info, BooleanExpression<AtomLabel> labelExpr,
                    List<Integer> accSignature) throws HOAConsumerException {
        if (biggest_state < id) 
            biggest_state = id;
        if (accSignature != null)
            throw new HOAConsumerException("Accaptance: only transitions allowed!");
    }

    @Override
    public void addEdgeImplicit(int stateId, List<Integer> conjSuccessors,
                    List<Integer> accSignature) {
    }

    @Override
    public void addEdgeWithLabel(int stateId, BooleanExpression<AtomLabel> labelExpr,
                    List<Integer> conjSuccessors, List<Integer> accSignature) throws HOAConsumerException {
        int from = stateId;
        int to = conjSuccessors.get(0);
        Transition t = new Transition(from, to);

        // in a petrinet, only one AP can occure at once
        // --> find the set of APs that satify the label

        for (int i = 0; i < aps.size(); i++)
        {
            if (satForAp(labelExpr, i, alias))
                t.add(aps.get(i));
        }
        
        if (accSignature != null)
            t.makeFinal();

        transitions.add(t);
    }

    public static boolean satForAp(BooleanExpression<AtomLabel> labelExpr, int ap, Map<String, BooleanExpression<AtomLabel>> alias) throws HOAConsumerException {
        if (labelExpr.isTRUE()) return true;
        if (labelExpr.isFALSE()) return false;
        if (labelExpr.isNOT()) 
        {
            BooleanExpression<AtomLabel> child = labelExpr.getLeft() != null ? labelExpr.getLeft() : labelExpr.getRight();
            return !satForAp(child, ap, alias);
        }
        if (labelExpr.isAND()) 
            return satForAp(labelExpr.getLeft(), ap, alias) && satForAp(labelExpr.getRight(), ap, alias);
        if (labelExpr.isOR()) 
            return satForAp(labelExpr.getLeft(), ap, alias) || satForAp(labelExpr.getRight(), ap, alias);

        // should be ap
        AtomLabel a = labelExpr.getAtom();
        if (a == null)
            throw new HOAConsumerException("Found BooleanExpression that is not handled");

        if (a.isAlias()) {
            BooleanExpression<AtomLabel> newLabel = alias.get(a.getAliasName());
            if (newLabel == null)
                throw new HOAConsumerException("Unknown alias found!");
            else 
                return satForAp(newLabel, ap, alias);
        }

        return a.getAPIndex() == ap;
    }

    @Override
    public void notifyEndOfState(int stateId) throws HOAConsumerException
    {
    }

    @Override
    public void notifyEnd() {
            try {
                    out.write(this.toString());
                    out.flush();
            } catch (IOException e) {
            }
    }

    @Override
    public void notifyAbort() {
    }


    @Override
    public void notifyWarning(String warning) throws HOAConsumerException
    {
        System.err.println("Warning: "+warning);
    }

    public static String toJsonList(Collection c)
    {
        String res = "[";
        boolean first = true;
        for (Object o : c)
            if (first) {
                res += o;
                first = false;
            }
            else 
                res += ", " + o;
        res += "]";
        return res;
    }
    
    @Override
    public String toString() {
        String res = "{\"nrofstates\": " + (biggest_state + 1)
                + ", \"aps\": " + toJsonList(aps)
                + ", \"initial\": " + toJsonList(initial)
                + ", \"transitions\": " + toJsonList(transitions) + "}";
        return res;
    }

        
    private static class Transition {
        public int from, to;
        public boolean isFinal = false;
        public HashSet<String> aps = new HashSet<String>();
        
        public Transition(int from, int to) {
            this.from = from;
            this.to = to;
        }
        
        public void add(String ap){
            aps.add(ap);
        }

        private void makeFinal() {
            isFinal = true;
        }
        
        @Override
        public String toString(){
            return "{\"from\": " + from + ", \"aps\": " + toJsonList(aps) + ", \"to\": " + to + ", \"final\": " + isFinal + "}";
                
        }
    }

}

