package jhoafparser.examples;

import java.io.FileInputStream;
import jhoafparser.consumer.HOAConsumerPeregrine;
import jhoafparser.parser.HOAFParser;

public class PeregrineParser
{
    public static void main(String[] args) {
        try {
            String path = "hoa1.txt";
            if (args.length > 0) {
                path = args[0];
            }
            HOAFParser.parseHOA(new FileInputStream(path), new HOAConsumerPeregrine(System.out));
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

