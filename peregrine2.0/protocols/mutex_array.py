from protocol import Protocol
from transition import Transition
from multiset import Multiset

# Mux Array with states of the form (id, q1, qt) where: 0 <= id < n and q1 is 0,1 or 2 (0 stands
# for `waiting', 1 for `idle', 2 for `in critical section'), qt is 0 or 1 (0 stands for
# `empty', 1 for `with token')


# reference: L. Fribourg and H. Olsén, “Reachability sets of parameterized rings as regular languages”,
#               Electronic Notes in Theoretical Computer Science, vol. 9, p. 40, 1997.


def name(n):
    return "Mutex Array({})".format(n)


def generate(n):
    assert n >= 2, "the number of processes must be larger than 0"

    Q = set()
    L = Multiset()
    T = set()
    for i in range(n):
        Q |= {(i, 0, 0), (i, 0, 1), (i, 1, 0), (i, 1, 1), (i, 2, 0), (i, 2, 1)}

        L[(i, 0, (1 if i == 0 else 0))] += 1

        T.add(Transition([(i, 0, 0)], [(i, 1, 0)]))
        T.add(Transition([(i, 0, 1)], [(i, 1, 1)]))
        T.add(Transition([(i, 2, 0)], [(i, 0, 0)], tags={f'leave_{i}'}))
        T.add(Transition([(i, 2, 1)], [(i, 0, 1)], tags={f'leave_{i}'}))
        T.add(Transition([(i, 1, 1)], [(i, 2, 1)], tags={f'enter_{i}'}))

        T.add(Transition([(i, 0, 1), ((i+1)%n, 1, 0)], [(i, 0, 0), ((i+1)%n, 1, 1)]))   # give token to next process

    state_names = {(p, line, token): f'q{p}_{line}_{token}' for (p,line,token) in Q}

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Mutex: Array")

    return protocol
