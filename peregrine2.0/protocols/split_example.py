from protocol import Protocol
from transition import Transition

def name():
    return "Majority"

def generate():
    n = 5
    states = {f'{l}{v}' for l in "ab" for v in range(n)} | {"c"}
    transitions = {Transition(("c"), ("c"))} | {Transition((f'{"a"}{v}', f'{"b"}{v}'), (f'{"a"}{(v+1)%n}', f'{"b"}{(v+1)%n}')) for v in range(n)}
    O = {f'{l}{v}':0 for l in "ab" for v in range(n)}
    O["c"] = 1
    alphabet_mapping = {q:q for q in states}
    pre = "(or (and {}) (= c 0))".format(" ".join(f'(= {q} 0)' for q in states if q != "c"))
    #pre = "(or (and {}) (= c 0))".format(" ".join(f'(or (= a{v} 0) (= b{v} 0))' for v in range(n)))

    return Protocol(states, transitions,
                 name=name(),
                 predicate="(< c c)",
                 precondition=pre,
                 alphabet_mapping=alphabet_mapping,
                 output_mapping=O)

