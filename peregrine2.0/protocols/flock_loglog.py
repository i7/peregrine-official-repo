from protocol import Protocol
from transition import Transition
from multiset import Multiset

# Log-Log flock of birds protocol
# Following construction from:
# - Blondin, Esparza, Jaax: Large Flocks of Small Birds: On the Minimal Size of Population Protocols. 2018
# - Mayr, Meyer: The complexity of the word problems for commutative semigroups and polynomial ideals. 1982
# Modified to remove dead transitions and unchanged states

def name(n):
    return f'Log-log flock of birds ({n})'

def states(n, m):
    cur = { f's_{n}', f'f_{n}' }
    # only need states c1,c1,c3,c4,b2,b3,b4 in lower levels
    if n < m:
        cur |= { f'c1_{n}', f'c2_{n}', f'c3_{n}', f'c4_{n}' }
        cur |= { f'b1_{n}', f'b2_{n}', f'b3_{n}', f'b4_{n}' }
    else:
        cur |= { f'b_{n}' }
    if n > 0:
        cur |= states(n-1, m)
        cur |= { f'q1_{n}', f'q2_{n}', f'q3_{n}', f'q4_{n}' }
    return cur

def base_transitions(n, m):
    T = set()
    if n == 0:
        if n == m:
            T.add(Transition([f's_{n}'], [f'f_{n}', f'b_{n}', f'b_{n}']))
        else:
            for i in range(1,5):
                T.add(Transition([f's_{n}', f'c{i}_{n}'], [f'f_{n}', f'c{i}_{n}', f'b{i}_{n}', f'b{i}_{n}']))
    else:
        T.update(base_transitions(n-1, m))
        T.add(Transition([f's_{n}'], [f'q1_{n}', f's_{n-1}', f'c1_{n-1}'])) # (a)
        T.add(Transition([f'q1_{n}', f'f_{n-1}', f'c1_{n-1}', f'b1_{n-1}'], [f'q2_{n}', f's_{n-1}', f'c2_{n-1}'])) # (b)
        T.add(Transition([f'q2_{n}', f'f_{n-1}', f'c2_{n-1}'], [f'q3_{n}', f'f_{n-1}', f'c3_{n-1}'])) # (c)
        T.add(Transition([f'q3_{n}', f's_{n-1}', f'c3_{n-1}', f'b1_{n-1}'], [f'q2_{n}', f's_{n-1}', f'c2_{n-1}', f'b4_{n-1}'])) # (d)
        T.add(Transition([f'q3_{n}', f's_{n-1}', f'c3_{n-1}'], [f'q4_{n}', f'f_{n-1}', f'c4_{n-1}', f'b4_{n-1}'])) # (e)
        T.add(Transition([f'q4_{n}', f's_{n-1}', f'c4_{n-1}'], [f'f_{n}'])) # (f)
        # only need transitions (h)-(j) in lower levels, further highest level does not need c1
        if n == m:
            T.add(Transition([f'q2_{n}', f'f_{n-1}', f'b2_{n-1}'], [f'q2_{n}', f'b_{n}', f'f_{n-1}', f'b3_{n-1}'])) # (g)-(j)
        else:
            for i in range(1,5):
                T.add(Transition([f'q2_{n}', f'c{i}_{n}', f'f_{n-1}', f'b2_{n-1}'], [f'q2_{n}', f'c{i}_{n}', f'b{i}_{n}', f'f_{n-1}', f'b3_{n-1}'])) # (g)-(j)
    return T

def pad_and_reverse_transitions(T, x):
    U = set()
    for t in T:
        pre = t.pre
        post = t.post
        diff = pre.size() - post.size()
        if diff > 0:
            post[x] = diff
        elif diff < 0:
            pre[x] = -diff
        U.add(Transition(pre, post))
        U.add(Transition(post, pre))
    return U

def prot_states(n):
    Q = states(n, n)
    Q.add('x')
    return Q

def prot_transitions(n):
    return pad_and_reverse_transitions(base_transitions(n, n), 'x')

def generate(n):
    assert n >= 0, "n must be non-negative"

    Q = prot_states(n)
    T = prot_transitions(n)
    for q in Q:
        T.add(Transition([f'f_{n}', q], [f'f_{n}', f'f_{n}']))
    S = Q
    I = {q: q for q in Q}
    O = {q: (1 if q == f'f_{n}' else 0) for q in Q}
    L = Multiset([f's_{n}'])

    # TODO: find out exact c_n >= 2**(2**n) for at least some values of n
    c = 2**(2**n)+n
    p = f"true"

    init = []
    for q in Q:
        if q == 'x':
            init.append(f"(>= {q} {c})")
        elif q == f's_{n}':
            init.append(f"(= {q} 1)")
        else:
            init.append(f"(= {q} 0)")
    terminal = []
    for q in Q:
        if q != f'f_{n}':
            terminal.append(f"(= {q} 0)")

    pre = "(and {})".format(" ".join(init))
    post = "(and {})".format(" ".join(terminal))

    state_names = {q: q for q in Q}

    # TODO: specify predicate instead of pre/post
    protocol = Protocol(Q, T, initial_states=Q, state_names=state_names, precondition=pre, postcondition=post, name=name(n))

    return protocol
