import math
from protocol import Protocol
from transition import Transition
from multiset import Multiset

def name(a, c, m):
    return "Log Modulo ({}, {}, {})".format(a, c, m)

def generate(a, c, m):
    m = max(m, 2)
    c %= m

    def binlog(x):
        return 0 if x == 0 else math.ceil(math.log(abs(x), 2))

    n = binlog(m)

    def var(i):
        return "x{}".format(i)

    def bits(z):
        return (i for i in range(0, n + 1) if (z & 2 ** i))

    p0 = "0+"
    m0 = "0-"
    R = {p0, m0}

    def rep(z):
        if z > 0:
            return {2 ** i for i in bits(z)}
        elif z < 0:
            return {-(2 ** i) for i in bits(-z)}
        else:
            return {p0}

    Q = {2 ** i for i in range(0, n + 1)}
    X = { var(i) for i in range(len(a)) }

    states = X | Q | R

    I = { x: x for x in X }
    O = dict()
    O.update({q: 1 for q in {p0}})
    O.update({q: 0 for q in Q | {m0} | X})
    T = set()

    for i, b in enumerate(a):   # translate x_i to a_i
        post = rep(b)
        count = len(post) - 1
        for r in R:
            pre = { var(i): 1, r: count }
            T.add(Transition(pre, post))

    for i in range(0, n):
        T.add(Transition((2**i, 2**i), (2**(i+1), m0)))         # up_i
        for r in R:
            T.add(Transition((2**(i+1), r), ((2**i), 2**i)))    # down_i

    for i in range(0, n+1):
        T.add(Transition((2**i, p0), ((2**i), m0)))             #signal_i false
    T.add(Transition((m0, p0), (p0, p0)))                     #signal true

    T.add(Transition(rep(m), {p0: len(rep(m))}))                # mod

    p = "(= (mod (+ {}) {}) {})".format(' '.join("(* {} {})".format(a[i], var(i)) for i in range(len(a))), m, c)

    # add leaders
    L = Multiset(rep(m-c))
    if 0 == c:
        L = Multiset()
    L[p0] += 2*n+1

    return Protocol(states, T, leaders=L, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(a,c,m), family_name="succinct Remainder")
