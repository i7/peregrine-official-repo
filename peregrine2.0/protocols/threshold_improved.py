from protocol import Protocol
from transition import Transition

def name(a, c):
    return "Improved Threshold ({}, {})".format(a, c)

def generate(a, c):

    s1 = min(min(a), c - 1)
    s2 = max(max(a), c)

    # Helper functions
    def var(i):
        return "x{}".format(i)

    def is_boolean(q):
        return q == "t" or q == "f"

    def is_tuple(q):
        return isinstance(q, tuple)

    def is_value(q):
        return not is_boolean(q) and not is_tuple(q)

    def value(q):
        if is_boolean(q):
            return 0
        return q[0] if is_tuple(q) else q

    def output(q):
        if is_boolean(q):
            return q == "t"
        return q[1] if is_tuple(q) else value(q) >= c

    def f(m, n):
        return max(s1, min(s2, value(m) + value(n)))

    def g(m, n):
        return value(m) + value(n) - f(m, n)

    def postFor(x, y):
        if not is_value(x) and not is_value(y):
            return None
        xn = f(x, y)
        yn = g(x, y)
        if value(yn) == 0:
            yn = "t" if xn >= c else "f"
            return xn, yn
        if output(xn) != output(yn):
            yn = (yn, output(xn))
        return xn, yn

    neutral_state = "t" if 0 >= c else "f"
    initial_states = set(a)
    states = set()
    transitions = set()
    WS = initial_states | {neutral_state}  # start from initial and neutral state

    while len(WS) != 0:
        cur = WS.pop()
        states.add(cur)

        for other in states:
            pre = (cur, other)
            post = postFor(cur, other)
            if post is not None:
                new1, new2 = post
                t = Transition(pre, post)
                if (not t.silent()):
                    transitions.add(t)
                if new1 not in states:
                    WS.add(new1)
                if new2 not in states:
                    WS.add(new2)

    I = {var(i): a[i] for i in range(len(a))}
    O = {q: output(q) for q in states}
    p = "(>= (+ {}) {})".format(' '.join("(* {} {})".format(b, var(i)) for i, b in enumerate(a)), c)

    return Protocol(states, transitions, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(a,c), family_name="Threshold improved")
