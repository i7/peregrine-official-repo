from protocol import *
from transition import Transition
from multiset import Multiset
import operator
from lark import Lark, Transformer, v_args
from z3 import *
from Log import log

def generate_true():
    return Protocol({"T"}, set(), output_mapping={"T": 1}, predicate="(< 1 0)"), "T"


def generate_false():
    return Protocol({"F"}, set(), output_mapping={"F": 0}, predicate="(< 0 1)"), "F"


def generate_remainder(coeffs, c, m):
    m = m
    c %= m

    numeric = range(m)
    boolean = ["t", "f"]

    Q = set(numeric) | set(boolean)
    I = {var: coeff % m for var, coeff in coeffs}
    O = {q: 1 if (q == c or q == "t") else 0 for q in Q}
    T = set()
    p = "(= (mod (+ {}) {}) {})".format(' '.join("(* {} {})".format(coeff, var) for var, coeff in coeffs), m, c)

    for n in numeric:
        for n_ in numeric:
            pre = (n, n_)
            post = ((n + n_) % m, "t" if ((n + n_) % m) == c else "f")
            t = Transition(pre, post)
            if not t.silent():  # only add if not silent
                T.add(t)

        for b in boolean:
            pre = (n, b)
            post = (n, "t" if n == c else "f")
            t = Transition(pre, post)
            if not t.silent():  # only add if not silent
                T.add(t)

    return Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, name=f'Remainder({coeffs},{c},{m})',
                    family_name="automatic Remainder"), 0


def generate_threshold(coeffs, c, op, op_str):
    variable = [var for (var, coeff) in coeffs]
    coeffss = [coeff for (var, coeff) in coeffs]
    value_for = {var: coeff for (var, coeff) in coeffs}
    s1 = min(min(coeffss), c - 1)
    s2 = max(max(coeffss), c + 1)

    # Helper functions
    def is_boolean(q):
        return q == "T" or q == "F"

    def is_tuple(q):
        return isinstance(q, tuple)

    def is_value(q):
        return not is_boolean(q) and not is_tuple(q)

    def value(q):
        if is_boolean(q):
            return 0
        return q[0] if is_tuple(q) else q

    def output(q):
        if is_boolean(q):
            return q == "T"
        return q[1] if is_tuple(q) else op(value(q),c)

    def f(m, n):
        return max(s1, min(s2, value(m) + value(n)))

    def g(m, n):
        return value(m) + value(n) - f(m, n)

    def postFor(x, y):
        if not is_value(x) and not is_value(y):
            return None
        xn = f(x, y)
        yn = g(x, y)
        if value(yn) == 0:
            yn = "T" if op(xn,c) else "F"
            return xn, yn
        if output(xn) != output(yn):
            yn = (yn, output(xn))
        return xn, yn

    neutral_state = "T" if op(0,c) else "F"
    initial_states = set(coeffss)
    states = set()
    transitions = set()
    WS = initial_states | {neutral_state}  # start from initial and neutral state

    while len(WS) != 0:
        cur = WS.pop()
        states.add(cur)

        for other in states:
            pre = (cur, other)
            post = postFor(cur, other)
            if post is not None:
                new1, new2 = post
                t = Transition(pre, post)
                if (not t.silent()):
                    transitions.add(t)
                if new1 not in states:
                    WS.add(new1)
                if new2 not in states:
                    WS.add(new2)

    I = {var: coeff for (var, coeff) in coeffs}
    O = {q: output(q) for q in states}
    p = "({} (+ {}) {})".format(op_str, ' '.join("(* {} {})".format(value_for[var], var) for var in variable), c)

    return Protocol(states, transitions, alphabet_mapping=I, output_mapping=O, predicate=p, name=f'Threshold({coeffs},{c})', family_name="automatic Threshold"), neutral_state


def generate_neg(prot, n):
    return Protocol(prot.states, prot.transitions, alphabet_mapping=prot.nameFromState,
                    output_mapping={q: not prot.output_mapping[q] for q in prot.states},
                    predicate=f'(not {prot.predicate_string})',
                    name=f'Neg({prot.name})',
                    family_name=f'Neg({prot.family_name})'), n


def generateBinaryProt(prot1, n1, prot2, n2, op, op_str):
    Q = {(q1, q2) for q1 in prot1.states for q2 in prot2.states}

    # neutral state
    neutral_state = (n1, n2)

    I = {a: (prot1.alphabet_mapping[a] if a in prot1.alphabet else n1, prot2.alphabet_mapping[a] if a in prot2.alphabet else n2)
         for a in prot1.alphabet | prot2.alphabet}

    O = {(q1, q2): op(prot1.output_mapping[q1], prot2.output_mapping[q2])
         for q1, q2 in Q}

    p = f'({op_str} {prot1.predicate} {prot2.predicate})'

    # initial states
    initial = I.values()

    # build lookup structures for protocols
    def build_lookup(transitions):
        lookup = {}
        for t in transitions:
            if t.silent():
                continue
            if t.pre not in lookup:
                lookup[t.pre] = [t.post]
            else:
                map[t.pre].append(t.post)

        return lookup

    prot1_lookup = build_lookup(prot1.transitions)
    prot2_lookup = build_lookup(prot2.transitions)

    # generate reachable states and transitions using work set algorithm
    states = set()
    transitions = set()
    WS = set(initial) | {neutral_state}  # start from initial and neutral state

    while len(WS) != 0:
        cur = WS.pop()
        cur_1, cur_2 = cur
        states.add(cur)

        for other in states:
            other_1, other_2 = other
            pre_1 = Multiset([cur_1, other_1])
            pre_2 = Multiset([cur_2, other_2])
            posts_1 = [Multiset([cur_1, other_1])]
            if pre_1 in prot1_lookup:
                posts_1 = prot1_lookup[pre_1]
            posts_2 = [Multiset([cur_2, other_2])]
            if pre_2 in prot2_lookup:
                posts_2 = prot2_lookup[pre_2]
            for post1 in posts_1:
                cur_1_next, other_1_next = list(post1.elements())
                for post2 in posts_2:
                    cur_2_next, other_2_next = list(post2.elements())
                    cur_next = (cur_1_next, cur_2_next)
                    other_next = (other_1_next, other_2_next)
                    pre = Multiset([cur, other])
                    post = Multiset([cur_next, other_next])
                    t = Transition(pre, post)
                    if (not t.silent()):
                        transitions.add(t)
                    if cur_next not in states:
                        WS.add(cur_next)
                    if other_next not in states:
                        WS.add(other_next)

    return Protocol(Q, transitions, alphabet_mapping=I, output_mapping=O, predicate=p, name="automatic"), neutral_state


def presburgerToProtocol(formula):
    # help function
    def getCooefficient(expression):  # can be multiplication of variable with constant or just a single variable or an int value
        if expression.decl().name() == '*':
            if expression.children()[0].decl().name() != 'Int':
                raise ArgumentError("Muliplication is only allowed with a constant factor.")
            coeff = expression.children()[0].as_long()
            var = expression.children()[1].decl().name()
            return [(var, coeff)]
        if len(expression.children()) == 0:
            if expression.decl().name() == 'Int':   # int
                return expression.as_long()
            return [(expression.decl().name(), 1)]  # only variable
        raise ArgumentError("Expected (scaled) variable..." + str(formula))

    binary_ops = {
        "and": operator.iand,
        "or": operator.ior
    }

    comparisions = {
        "<": operator.lt,
        "<=": operator.le,
        "=": operator.eq,
        ">": operator.gt,
        ">=": operator.ge
    }

    name = formula.decl().name()

    if name == 'true':
        return generate_true()
    if name == 'false':
        return generate_false()
    if name == 'not':
        prot, n = presburgerToProtocol(formula.children()[0])
        return generate_neg(prot, n)
    if name in binary_ops:
        prot1, n1 = presburgerToProtocol(formula.children()[0])
        prot2, n2 = presburgerToProtocol(formula.children()[1])
        prot, n = generateBinaryProt(prot1, n1, prot2, n2, binary_ops[name], name)
        next = 2
        while next < len(formula.children()):
            prot_next, n_next = presburgerToProtocol(formula.children()[1])
            prot, n = generateBinaryProt(prot, n, prot_next, n_next, binary_ops[name], name)
            next = next + 1
        return prot, n
    if (name == '=' and formula.children()[0].decl().name() == 'mod'):  # mod
        if (formula.children()[0].children()[1].decl().name() != 'Int'):
            raise ArgumentError("The mode of a modulo expression has to be constant.")
        if (formula.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a modulo expression has to be constant.")
        c = formula.children()[1].as_long()
        m = formula.children()[0].children()[1].as_long()
        lhs = formula.children()[0].children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):
            for exp in lhs.children():
                cur_res = getCooefficient(exp)
                if not isinstance(cur_res, list):
                    c = (c - cur_res) % m
                else:
                    coeffs = coeffs + cur_res
        else:
            cur_res = getCooefficient(lhs)
            if not isinstance(cur_res, list):
                c = (c - cur_res) % m
            else:
                coeffs = cur_res
            coeffs = getCooefficient(lhs)

        return generate_remainder(coeffs, c, m)
    if (name in comparisions):
        if (formula.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a comparision has to be constant.")
        c = formula.children()[1].as_long()
        lhs = formula.children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):  # sum
            for exp in lhs.children():
                coeffs = coeffs + getCooefficient(exp)
        else:  # just 1 var
            coeffs = getCooefficient(lhs)

        return generate_threshold(coeffs, c, comparisions[name], name)


def parse_predicate(string):
    predicate_grammar = """
        ?start: pred

        ?pred: and_pred
             | pred "||" and_pred   -> op_or

        ?and_pred: bool_exp
             | and_pred "&&" bool_exp   -> op_and    

        ?bool_exp: comp
             | "!" bool_exp             -> op_not
             | "(" pred ")"      
             | ("True" | "true")        -> op_true
             | ("False" | "false")      -> op_false

        ?comp: exp "=" exp            -> eq
             | exp "!=" exp           -> neq
             | exp ">" exp            -> gt
             | exp "<" exp            -> lt
             | exp "<=" exp           -> le
             | exp ">=" exp           -> ge
             | exp "%" INT "=" exp    -> mod

        ?exp: product
             | exp "+" product   -> add
             | exp "-" product   -> sub

        ?product: atom
            | product "*" atom  -> mul

        ?atom: S_INT            -> number
             | "-" atom         -> neg
             | VAR              -> var
             | "(" exp ")"

        %import common.LCASE_LETTER -> VAR
        %import common.INT -> INT
        %import common.SIGNED_INT -> S_INT
        %import common.WS_INLINE

        %ignore WS_INLINE
    """

    @v_args(inline=True)  # Affects the signatures of the methods
    class GenerateZ3Predicate(Transformer):
        def op_and(self, l, r):
            return And(l, r)

        def op_or(self, l, r):
            return Or(l, r)

        def op_not(self, e):
            return Not(e)

        def op_true(self):
            return BoolVal(True)

        def op_false(self):
            return BoolVal(False)

        def eq(self, l, r):
            return l == r

        def neq(self, l, r):
            return l != r

        def gt(self, l, r):
            return l > r

        def lt(self, l, r):
            return l < r

        def ge(self, l, r):
            return l >= r

        def le(self, l, r):
            return l <= r

        def mod(self, l, m, r):
            return l % IntVal(str(m)) == r

        def add(self, l, r):
            return l + r

        def sub(self, l, r):
            return l - r

        def mul(self, l, r):
            return l * r

        def neg(self, e):
            return -e

        def var(self, e):
            return Int(str(e))

        def number(self, e):
            return IntVal(str(e))

    parser = Lark(predicate_grammar, parser='lalr', transformer=GenerateZ3Predicate())
    parse = parser.parse
    predicate = parse(string)

    # simplify formula (simplification only works with z3 4.8 and above)
    z3_version = get_version()
    do_symplify = True
    if do_symplify and (z3_version[0] > 5 or z3_version[0] == 4 and z3_version[1] >= 8):
        return simplify(predicate, arith_lhs=True)

    return predicate



def name(predicate):
    return "automatic for {}".format(predicate)


def generate(predicate):
    formula = parse_predicate(predicate)
    prot, n = presburgerToProtocol(formula)
    prot.name = name(predicate)
    return prot