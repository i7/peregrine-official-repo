import math
from protocol import Protocol
from transition import Transition
from multiset import Multiset
import re

def name(a, c):
    return "Log Threshold ({}, {})".format(a, c)

def generate(a, c):
    def binlog(x):
        return 0 if x == 0 else math.ceil(math.log(abs(x), 2))

    n = max(max(max(binlog(x) for x in a), binlog(c)), 1)

    def var(i):
        return "x{}".format(i)

    def bits(z):
        return ( i for i in range(0,n+1) if (z & 2**i) )

    def rep(z):
        if z > 0:
            return { 2**i for i in bits(z) }
        elif z < 0:
            return { -(2**i) for i in bits(-z) }
        else:
            return { '+0' }

    Qp = { 2**i for i in range(0, n+1) }
    Qm = { -(2**i) for i in range(0, n+1) }
    X = { var(i) for i in range(len(a)) }
    R = { '-0', '+0' }

    Q = X | Qp | Qm | R
    I = {x: x for x in X}
    O = dict()
    O.update({ q: 1 for q in Qp | { '+0' } })
    O.update({ q: 0 for q in Qm | { '-0' } | X })
    T = set()

    for i, b in enumerate(a):
        post = rep(b)
        count = len(post) - 1
        for r in R:
            pre = { var(i): 1, r: count }
            T.add(Transition(pre, post))

    for i in range(0, n+1):
        T.add(Transition((2**i, -(2**i)), ('+0', '-0')))

    for i in range(0, n):
        T.add(Transition((2**i, 2**i), (2**(i+1), '+0')))
        T.add(Transition((-(2**i), -(2**i)), (-(2**(i+1)), '-0')))
        for r in R:
            T.add(Transition((2**(i+1), r), ((2**i), 2**i)))
            T.add(Transition((-(2**(i+1)), r), ((-(2**i)), -(2**i))))

    for i in range(0, n+1):
        T.add(Transition((2**i, '-0'), ((2**i), '+0')))
        T.add(Transition((-(2**i), '+0'), ((-(2**i)), '-0')))
    T.add(Transition(('-0', '+0'), (('+0', '+0'))))

    p = "(>= (+ {}) {})".format(' '.join("(* {} {})".format(b, var(i)) for i, b in enumerate(a)), c)

    # add leaders
    L = Multiset(rep(-c))
    L['+0'] += 4*n+2

    return Protocol(Q, T, leaders=L, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(a,c), family_name="Threshold succinct")
