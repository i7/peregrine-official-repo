from protocol import Protocol
from transition import Transition

def name():
    return "Majority (no tie-breaker)"

def generate():
    states = {"A", "B", "a", "b"}
    transitions = {Transition(("A", "B"), ("a", "b")),
                   Transition(("A", "b"), ("A", "a")),
                   Transition(("B", "a"), ("B", "b"))}
    O = {"A": 0, "B": 1, "a": 0, "b": 1}
    p = "(< A B)"
    alphabet_mapping = {"A": "A", "B": "B"}
    pre = "(distinct A B)"

    return Protocol(states, transitions,
                 name=name(),
                 predicate=p,
                 precondition=pre,
                 alphabet_mapping=alphabet_mapping,
                 output_mapping=O)
