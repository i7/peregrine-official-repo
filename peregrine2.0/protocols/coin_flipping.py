from protocol import Protocol
from transition import Transition


def name():
    return "Coin Flipping"

# each agent has a coin with one side up (Head vs Tails)
# each turn, one random agent flips his coin

# show: GF (= H 0) and GF (= T 0)
#       i.e. infinitely often all heads and infinitely often all tails
# -> our method cannot show this as there is not transitions that can only be fired if (= T 0)


def generate():
    Q = {"qH", "qT"}
    T = {Transition(["qH"], ["qT"]),
         Transition(["qT"], ["qH"])}
    state_names = {"qH": "H", "qT": "T"}

    return Protocol(Q, T, initial_states=Q, state_names=state_names, name=name())
