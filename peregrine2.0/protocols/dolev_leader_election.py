from protocol import Protocol
from transition import Transition

# Algorithm A from Dolev D., Klawe M., Rodeh M.: An O(n log n) unidirectional distributed algorithm for extrema finding in a circle (1982)

def name(n):
    return f'Dolev Leader Election ({n})'

def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    T = set()
    # iterate over all processes v
    for v in range(1,n+1):
        # id of next process to the right
        w = (v % n) + 1

        # program states as in Algorithm A of paper: A0, A1, A2, P, H where A = Active, P = Passive and H = Halted
        Q |= {f's{v}_A0', f's{v}_A2', f's{v}_A1', f's{v}_P', f's{v}_H'}
        # variable max(v) in range {1..n+1}
        for i in range(1,n+1):
            Q |= {f'max{v}_{i}'}
        # variable left(v) in range {1..n+1}
        for i in range(1,n+1):
            Q |= {f'left{v}_{i}'}

        # queues m1(v) and m2(v) of incoming messages
        # only use bounded queues of size 1
        # values in range {1..n+1}, plus value for empty queue
        Q |= {f'm1_{v}_empty'}
        for j in range(1,n+1):
            Q |= {f'm1_{v}_{j}'}
        Q |= {f'm2_{v}_empty'}
        for j in range(1,n+1):
            Q |= {f'm2_{v}_{j}'}

        # A0: Send the message <1,max(v)>
        for i in range(1,n+1):
            T.add(Transition([f's{v}_A0', f'max{v}_{i}', f'm1_{w}_empty'], [f's{v}_A1', f'max{v}_{i}', f'm1_{w}_{i}']))

        # A1: If a message <1,i> arrives do as follows:
        # If i != max(v) then send message (m2,i) and assign i to left(v)
        # Otherwise, halt - max(v) is the global maximum
        for m1i in range(1,n+1):
            for maxv in range(1,n+1):
                if m1i == maxv:
                    T.add(Transition([f's{v}_A1', f'max{v}_{maxv}', f'm1_{v}_{m1i}'], [f's{v}_H', f'max{v}_{maxv}', f'm1_{v}_empty']))
                else:
                    for leftv in range(1,n+1):
                        T.add(Transition([f's{v}_A1', f'max{v}_{maxv}', f'm1_{v}_{m1i}', f'left{v}_{leftv}', f'm2_{w}_empty'], [f's{v}_A2', f'max{v}_{maxv}', f'm1_{v}_empty', f'left{v}_{m1i}', f'm2_{w}_{m1i}']))

        # A2: If a message <2,j> arrives do as follows:
        # If left(v) is greater than both j and max(v)
        #   then asign left(v) to max(v)
        #   and send the massige <1,max(v)>
        # Otherwise, become passive
        for m2j in range(1,n+1):
            for maxv in range(1,n+1):
                for leftv in range(1,n+1):
                    if leftv > max(m2j, maxv):
                        T.add(Transition([f's{v}_A2', f'max{v}_{maxv}', f'left{v}_{leftv}', f'm2_{v}_{m2j}', f'm1_{w}_empty'], [f's{v}_A1', f'max{v}_{leftv}', f'left{v}_{leftv}', f'm2_{v}_empty', f'm1_{w}_{leftv}']))
                    else:
                        T.add(Transition([f's{v}_A2', f'max{v}_{maxv}', f'left{v}_{leftv}', f'm2_{v}_{m2j}'], [f's{v}_P', f'max{v}_{maxv}', f'left{v}_{leftv}', f'm2_{v}_empty']))

        # P: Passive processes simply pass on unchanged whatever messages they receive
        for i in range(1,n+1):
            T.add(Transition([f's{v}_P', f'm1_{v}_{i}', f'm1_{w}_empty'], [f's{v}_P', f'm1_{v}_empty', f'm1_{w}_{i}']))
            T.add(Transition([f's{v}_P', f'm2_{v}_{i}', f'm2_{w}_empty'], [f's{v}_P', f'm2_{v}_empty', f'm2_{w}_{i}']))


    state_names = {q: q for q in Q}

    # constraint that there is exactly one agent in each state of R
    def empty_except(R):
        s = []
        for q in R:
            s.append(f'(= {q} 1)')
        for q in Q - R:
            s.append(f'(= {q} 0)')
        return "(and {})".format(" ".join(s))

    # initial states
    initial  = { f's{v}_A0' for v in range(1,n+1) }
    initial |= { f'max{v}_{v}' for v in range(1,n+1) }
    initial |= { f'left{v}_{1}' for v in range(1,n+1) }
    initial |= { f'm1_{v}_empty' for v in range(1,n+1) }
    initial |= { f'm2_{v}_empty' for v in range(1,n+1) }

    # number of processes in states A0, P and H
    A0 = "(+ {})".format(' '.join(f's{v}_P' for v in range(1,n+1)))
    P  = "(+ {})".format(' '.join(f's{v}_P' for v in range(1,n+1)))
    H  = "(+ {})".format(' '.join(f's{v}_H' for v in range(1,n+1)))

    # eventually one process halted as leader and all other processes are passive
    post = f'(and (= {H} 1) (= {P} {n-1}))'

    protocol = Protocol(Q, T, state_names=state_names, initial_states=Q, precondition=empty_except(initial), postcondition=post, name=name(n))

    return protocol

