from protocol import Protocol
from transition import Transition

# self_stabilizing protocol on ring structure

def name(n):
    return f'Israeli Jalfon ({n})'

def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    T = set()
    for i in range(n):
        Q |= {f'q{i}', f'q{i}e'}
        T.add(Transition([f'q{i}', f'q{(i-1)%n}'], [f'q{i}e', f'q{(i-1)%n}'], tags={"left", "elim"}))     # give to left, neighbor has token
        T.add(Transition([f'q{i}', f'q{(i-1)%n}e'], [f'q{i}e', f'q{(i-1)%n}'], tags={"left"}))              # give to left, neighbor has no token

        T.add(Transition([f'q{i}', f'q{(i+1)%n}'], [f'q{i}e', f'q{(i+1)%n}'], tags={"right", "elim"}))    # give to right, neighbor has token
        T.add(Transition([f'q{i}', f'q{(i+1)%n}e'], [f'q{i}e', f'q{(i+1)%n}'], tags={"right"}))             # give to right, neighbor has no token

    state_names = {q: q for q in Q}
    O = {q: True for q in Q}
    sum = "(+ {})".format(' '.join(f'q{i}' for i in range(n)))
    pre = "(and {} (> {} 0))".format(' '.join(f'(= (+ q{i} q{i}e) 1)' for i in range(n)), sum)
    post = f'(= {sum} 1)'

    protocol = Protocol(Q, T, state_names=state_names, initial_states=Q, precondition=pre, postcondition=post, name=name(n), family_name="Israeli Jalfon")

    return protocol
