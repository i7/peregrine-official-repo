from protocol import Protocol
from transition import Transition
from multiset import Multiset
from Log import log
from lark import Lark, Transformer, v_args
from z3_util import z3name
import json

def name(protocol):
    return protocol["title"]

def generate(fileName):
    with open(fileName) as file:
        protocol = json.load(file)
    Q = set(protocol["states"])
    nameFromState = {q: z3name(q) for q in Q}
    T = {Transition(t["pre"], t["post"]) for t in protocol["transitions"]}
    I = {f'i_{nameFromState[q]}': q for q in protocol["initialStates"]}
    O = {q: (q in protocol["trueStates"]) for q in protocol["states"]}
    L = Multiset()
    if "leaders" in protocol:
        L = Multiset({q: protocol["leaders"][q] for q in protocol["leaders"]})
        log(str(protocol["leaders"]))
        log(str(L))

    log(protocol["predicate"])
    p = peregrine_to_smt(protocol["predicate"], nameFromState)
    log(p)

    pre = "true"
    if "precondition" in protocol:
        pre = peregrine_to_smt(protocol["precondition"], nameFromState)

    res = Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, precondition=pre, leaders=L, name=name(protocol), family_name="from Peregrine1")
    log(res)
    return res

def peregrine_to_smt(string, nameFromState):
    predicate_grammar = """
        ?start: pred

        ?pred: and_pred
             | pred "||" and_pred   -> op_or

        ?and_pred: bool_exp
             | and_pred "&&" bool_exp   -> op_and    

        ?bool_exp: comp
             | "!" bool_exp             -> op_not
             | "(" pred ")"      
             | ("True" | "true")        -> op_true
             | ("False" | "false")      -> op_false

        ?comp: exp "=" exp            -> eq
             | exp "!=" exp           -> neq
             | exp ">" exp            -> gt
             | exp "<" exp            -> lt
             | exp "<=" exp           -> le
             | exp ">=" exp           -> ge
             | exp "%" INT "=" exp    -> mod

        ?exp: product
             | exp "+" product   -> add
             | exp "-" product   -> sub

        ?product: atom
            | product "*" atom  -> mul

        ?atom: S_INT            -> number
             | "-" atom         -> neg
             | "C[" /[^\\]]+/ "]"     -> var
             | "(" exp ")"

        %import common.LETTER -> LETTER
        %import common.DIGIT -> DIGIT
        %import common.INT -> INT
        %import common.SIGNED_INT -> S_INT
        %import common.WS_INLINE

        %ignore WS_INLINE
    """

    @v_args(inline=True)  # Affects the signatures of the methods
    class GenerateZ3Predicate(Transformer):
        def op_and(self, l, r):
            return f'(and {l} {r})'

        def op_or(self, l, r):
            return f'(or {l} {r})'

        def op_not(self, e):
            return f'(not {e})'

        def op_true(self):
            return f'(< 0 1)'

        def op_false(self):
            return f'(< 1 0)'

        def eq(self, l, r):
            return f'(= {l} {r})'

        def neq(self, l, r):
            return self.op_not(self.eq(l, r))

        def gt(self, l, r):
            return f'(> {l} {r})'

        def lt(self, l, r):
            return f'(< {l} {r})'

        def ge(self, l, r):
            return f'(>= {l} {r})'

        def le(self, l, r):
            return f'(<= {l} {r})'

        def mod(self, l, m, r):
            return f'(= (mod {l} {m}) (mod {r} {m}))'

        def add(self, l, r):
            return f'(+ {l} {r})'

        def sub(self, l, r):
            return f'(- {l} {r})'

        def mul(self, l, r):
            return f'(* {l} {r})'

        def neg(self, e):
            return self.mul("-1", e)

        def var(self, e):
            return f'i_{nameFromState[e]}'

        def number(self, e):
            return str(e)

    while string.find("%=_") != -1:
        mod_loc = string.find("%=_")
        number_loc = mod_loc
        while not string[number_loc].isdigit():
            number_loc = number_loc + 1
        number_end = number_loc
        while string[number_end].isdigit():
            number_end = number_end + 1

        string = string[0:mod_loc] + "%" + string[number_loc:number_end] + "=" + string[number_end:]

    parser = Lark(predicate_grammar, parser='lalr', transformer=GenerateZ3Predicate())
    parse = parser.parse
    predicate = parse(string)

    return predicate
