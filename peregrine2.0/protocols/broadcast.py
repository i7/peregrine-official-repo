from protocol import Protocol
from transition import Transition

def name():
    return "Broadcast"

def generate():
    Q = {"t", "f"}
    T = {Transition(("t", "f"), ("t", "t"))}
    I = {"t": "t", "f": "f"}
    O = {"t": 1, "f": 0}
    p = "(>= t 1)"

    return Protocol(Q, T, alphabet_mapping=I, predicate=p, output_mapping=O, name=name())
