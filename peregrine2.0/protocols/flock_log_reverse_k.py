import math
from protocol import Protocol
from transition import Transition

def name(n):
    return "Flock-of-birds logarithmic with k-way reverse transitions ({})".format(n)

def generate(n):
    i = int(math.log(n, 2))
    states = [0] + [2**j for j in range(0, i + 1)]
    transitions = []

    for q in states:
        tot  = q + q
        pre  = (q, q)

        if tot < n and q != 0 and tot in states:
            post = (tot, 0)
            transitions.append(Transition(pre, post))
            transitions.append(Transition(post, pre))
        elif tot == n:
            post = (n, n)
            transitions.append(Transition(pre, post))

    if n not in states:
        states.append(n)
        pre = []
        for j in states:
            if 0 < j and j < n and n & j:
                pre.append(j)
        post = [n for q in pre]
        transitions.append(Transition(pre, post))

    for j in states:
        if j < n:
            pre = (j, n)
            post = (n, n)
            transitions.append(Transition(pre, post))

    Q = states
    T = transitions
    I = {"x0": 0, "x1": 1}
    O = {i: (0 if i < n else 1) for i in Q}
    p = "(>= x1 {})".format(n)

    return Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(n), family_name="succinct reversible Flock with k-way")

