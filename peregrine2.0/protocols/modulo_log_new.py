import math
from protocol import Protocol
from transition import Transition
from multiset import Multiset

def name(a, c, m):
    return "new Log Modulo ({}, {}, {})".format(a, c, m)

# succinct modulo protocol from "Succinct Population Protocols for Presburger Arithmetic" (STACS20)
# correct, but we cannot verify it... EvDead to weak (for True) and PotReach to weak (for False)

def generate(a, c, m):
    m = max(m, 2)
    a = [i % m for i in a]
    c %= m

    t = "ot"
    f = "of"
    null = "z"

    def binlog(x):
        return 0 if x == 0 else math.ceil(math.log(abs(x), 2))

    n = binlog(m)

    def var(i):
        return "x{}".format(i)

    def bits(z):
        return (i for i in range(0, n + 1) if (z & 2 ** i))

    def rep(z):
        if z > 0:
            return {2 ** i for i in bits(z)}
        elif z < 0:
            return {-(2 ** i) for i in bits(-z)}
        else:
            return {null}

    N = {2 ** i for i in range(0, n + 1)} | {null}
    X = { var(i) for i in range(len(a)) }
    B = {t, f}

    Q = X | N | B
    state_names = {f'q{q}': q for q in Q}

    # helpers:
    initial_states = {f'q{null}', f'q{f}'}
    L = Multiset({f: 1, null: 2*n})

    I = {x:x for x in X}
    O = {q: (1 if q == t else 0 if q == f else "?") for q in Q}
    T = set()

    for i, b in enumerate(a):   # translate x_i to a_i
        post = rep(b)
        count = len(post) - 1
        pre = { var(i): 1, null: count }
        T.add(Transition(pre, post))

    for i in range(0, n):
        T.add(Transition((2**i, 2**i), (2**(i+1), null)))         # up_i
        T.add(Transition((2**(i+1), null), ((2**i), 2**i)))    # down_i

    T.add(Transition({t} | rep(m), {f: 1, null: len(rep(m))}))    # mod_t
    T.add(Transition({f} | rep(m), {f: 1, null: len(rep(m))}))    # mod_f

    T.add(Transition({f} | rep(c), {t} | rep(c)))    # equal
    T.add(Transition((t,f), (f,f)))    # false

    p = "(>= (mod (+ {}) {}) {})".format(' '.join("(* {} q{})".format(a[i], var(i)) for i in range(len(a))), m, c)

    # add helpers
    #pre = f'(and (>= qof 1) (>= qz {2*n}))'

    return Protocol(Q, T, leaders=L, initial_states=initial_states, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(a,c,m), family_name="succinct Remainder (STACS20)")
