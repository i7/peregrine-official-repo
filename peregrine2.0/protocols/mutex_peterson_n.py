from protocol import Protocol
from transition import Transition
from itertools import chain

# Fig. 3. Simple n process solution from Peterson G.L.: Myths about the mutual exclusion problem (1981)

#q[i]  := 0 for 1 ≤ i ≤ n
#turn[i] := 0 for 1 ≤ i ≤ n - 1

# s1: for j = 1 to n - 1 do
# s2_j: q[i] = j
# s3_j: turn[j] = i
# s4_j  for k=1 to i-1, i+1 to n do
# s5_j_k: if ( q[k] >= j and
# s6_j_k       turn[j] = i ) then
# s7_j_k:   goto s4_j
    #     fi
# s8_j_k: next
#       done
# s9_j: next
#     done
#s10: (* before critical section *)
#s11: critical section
#s12: q[i] = 0
#s13: goto s1

def name(n):
    return f'Peterson\'s mutual exclusion algorithm ({n})'

def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    T = set()
    # global variables q[1..n] with range {0..n-1}
    for i in range(1,n+1):
        for q in range(0,n):
            Q |= {f'q{i}_{q}'}
    # global variables turn[1..n-1] with range {1..n}
    for j in range(1,n):
        for turn in range(1,n+1):
            Q |= {f'turn{j}_{turn}'}

    # iterate over all processes i
    for i in range(1,n+1):
        # program states
        Q |= {f's{i}_1', f's{i}_10', f's{i}_12', f's{i}_13'}
        for j in range(1,n):
            Q |= {f's{i}_2_{j}', f's{i}_3_{j}', f's{i}_4_{j}', f's{i}_9{j}'}
            for k in range(1,n+1):
                if k != i:
                    Q |= {f's{i}_5_{j}_{k}', f's{i}_6_{j}_{k}', f's{i}_7_{j}_{k}', f's{i}_8_{j}_{k}'}

        # s1: for j = 1 to n - 1 do
        Q |= {f's{i}_1'}
        T.add(Transition([f's{i}_1'], [f's{i}_2_1']))
        for j in range(1,n):
            Q |= {f's{i}_2_{j}'}
            # s2_j: q[i] = j
            for q in range(0,n):
                T.add(Transition([f's{i}_2_{j}', f'q{i}_{q}'], [f's{i}_3_{j}', f'q{i}_{j}']))

            # s3_j: turn[j] = i
            Q |= {f's{i}_3_{j}'}
            for turn in range(1,n+1):
                T.add(Transition([f's{i}_3_{j}', f'turn{j}_{turn}'], [f's{i}_4_{j}', f'turn{j}_{i}']))

            # s4_j  for k=1 to i-1, i+1 to n do
            Q |= {f's{i}_4_{j}'}
            if i == 1:
                T.add(Transition([f's{i}_4_{j}'], [f's{i}_5_{j}_2']))
            else:
                T.add(Transition([f's{i}_4_{j}'], [f's{i}_5_{j}_1']))

            for k in chain(range(1,i), range(i+1,n+1)):
                # s5_j_k: if ( q[k] >= j and
                Q |= {f's{i}_5_{j}_{k}'}
                for q in range(0,n):
                    if q >= j:
                        T.add(Transition([f's{i}_5_{j}_{k}', f'q{k}_{q}'], [f's{i}_6_{j}_{k}', f'q{k}_{q}']))
                    else:
                        T.add(Transition([f's{i}_5_{j}_{k}', f'q{k}_{q}'], [f's{i}_8_{j}_{k}', f'q{k}_{q}']))

                # s6_j_k       turn[j] = i ) then
                Q |= {f's{i}_6_{j}_{k}'}
                for turn in range(1,n+1):
                    if turn == i:
                        T.add(Transition([f's{i}_6_{j}_{k}', f'turn{j}_{turn}'], [f's{i}_7_{j}_{k}', f'turn{j}_{turn}']))
                    else:
                        T.add(Transition([f's{i}_6_{j}_{k}', f'turn{j}_{turn}'], [f's{i}_8_{j}_{k}', f'turn{j}_{turn}']))

                # s7_j_k:   goto s4_j
                Q |= {f's{i}_7_{j}_{k}'}
                T.add(Transition([f's{i}_7_{j}_{k}'], [f's{i}_4_{j}']))

                # s8_j_k: next
                Q |= {f's{i}_8_{j}_{k}'}
                if k == i-1 and i == n or k == n:
                    T.add(Transition([f's{i}_8_{j}_{k}'], [f's{i}_9_{j}']))
                    pass
                elif k == i-1:
                    T.add(Transition([f's{i}_8_{j}_{k}'], [f's{i}_5_{j}_{i+1}']))
                    pass
                else:
                    T.add(Transition([f's{i}_8_{j}_{k}'], [f's{i}_5_{j}_{k+1}']))

            # s9_j: next
            Q |= {f's{i}_9_{j}'}
            if j == n-1:
                T.add(Transition([f's{i}_9_{j}'], [f's{i}_10']))
            else:
                T.add(Transition([f's{i}_9_{j}'], [f's{i}_2_{j+1}']))

        #s10: (* before critical section *)
        Q |= {f's{i}_10'}
        T.add(Transition([f's{i}_10'], [f's{i}_11'], tags={f'enter_{i-1}'}))

        #s11: critical section
        Q |= {f's{i}_11'}
        T.add(Transition([f's{i}_11'], [f's{i}_12'], tags={f'leave_{i-1}'}))

        #s12: q[i] = 0
        Q |= {f's{i}_12'}
        for q in range(0,n):
            T.add(Transition([f's{i}_12', f'q{i}_{q}'], [f's{i}_13', f'q{i}_0']))

        #s13: goto s1
        Q |= {f's{i}_13'}
        T.add(Transition([f's{i}_13'], [f's{i}_1']))

    state_names = {q: q for q in Q}

    # constraint that there is exactly one agent in each state of R
    def empty_except(R):
        s = []
        for q in R:
            s.append(f'(= {q} 1)')
        for q in Q - R:
            s.append(f'(= {q} 0)')
        return "(and {})".format(" ".join(s))

    # initial states
    initial  = { f's{i}_1' for i in range(1,n+1) }
    initial |= { f'q{i}_0' for i in range(1,n+1) }
    initial |= { f'turn{j}_1' for j in range(1,n) }

    # start in initial state
    pre = empty_except(initial)
    # eventually one process halted as leader and all other processes are passive

    protocol = Protocol(Q, T, state_names=state_names, precondition=pre, name=name(n), family_name="Mutex: Peterson for n")

    return protocol


