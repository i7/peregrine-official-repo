from protocol import Protocol
from transition import Transition
from multiset import Multiset

# Burns algorithm as population protocol as described in "Regular Model Checking by MARCUS NILSSON"

# Only the states 1, 3, 5 and 6 where modeled. The other states are implicitly modeled in the transitions.


def name(n):
    return "Mutex: Burns({})".format(n)


def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    for i in range(n):
        Q |= {(i, 1), (i, 2), (i, 3), (i, 4)}
        # q1: process in line 1
        # q2: process in line 3 (i.e. just set flag=1)
        # q3: process in line 5 (i.e. waiting for others with flag=1; flag is still 1)
        # q4: process in line 6 (i.e. critical section; flag is still 1)

    state_names = {(i, x): f'q_{i}_{x}' for i, x in Q}
    L = Multiset([(i, 1) for i in range(n)])

    T = set()
    for i in range(n):
        # q1 -> q2 (if all left have flag 0)
        T.add(Transition([(i,1)] + [(j,1) for j in range(i)], [(i,2)] + [(j,1) for j in range(i)]))

        # q2 -> q1 (if any left has flag 1)
        for j in range(i):
            T.add(Transition([(i,2), (j,2)], [(i,1), (j,2)]))
            T.add(Transition([(i,2), (j,3)], [(i,1), (j,3)]))
            T.add(Transition([(i,2), (j,4)], [(i,1), (j,4)]))

        # q2 -> q3 (if all left have flag 0)
        T.add(Transition([(i, 2)] + [(j, 1) for j in range(i)], [(i, 3)] + [(j, 1) for j in range(i)]))

        # q3 -> q4 (if all right have flag 0)
        T.add(Transition([(i, 3)] + [(j, 1) for j in range(i+1,n)], [(i, 4)] + [(j, 1) for j in range(i+1,n)],
                         tags={f'enter_{i}'}))

        # q4 -> q1 leave critical section
        T.add(Transition([(i, 4)], [(i, 1)], tags={f'leave_{i}'}))

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Mutex: Burns")

    return protocol
