from protocol import Protocol
from transition import Transition

def name():
    return f'Coffee Can'

def generate():
    Q = {"b", "w"}
    T = {Transition(["w", "w"], ["b"]),
         Transition(["b", "b"], ["b"]),
         Transition(["b", "w"], ["w"])}
    I = {q:q for q in Q}
    O = {"b": False, "w": True}
    pre = "(> (+ w b) 0)"
    p = "(= (mod w 2) 1)"

    return Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, precondition=pre, name=name())
