from protocol import Protocol
from transition import Transition
from multiset import Multiset
import itertools

# Szymanski algorithm as population protocol

# states of form "(process, position)"
# pos 0: flag=0 (i.e. in noncritical)
# pos 1: flag=1 (i.e. want to enter cs)
# pos 2: flag=2 (i.e. in waiting room + waits for others to enter)
# pos 3: flag=3 (i.e. in waiting room)
# pos 4: flag=4 (i.e. behind waiting room)
# pos 5: flag=4 (i.e. in critical section)
# pos 6: flag=4 (i.e. after critical section; starting "epilog")


# reference: B. K. Szymanski, “A simple solution to lamport’s concurrent programming
#               problem with linear wait”, in Proceedings of the 2nd international conference on
#               Supercomputing, ACM, 1988, pp. 621–626.


def name(n):
    return "Mutex: Szymanski({})".format(n)


def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    for i in range(n):
        Q |= {(i, flag) for flag in range(7)}

    state_names = {f'q{i}_{flag}': (i, flag) for (i, flag) in Q}
    L = Multiset([(i, 0) for i in range(n)])

    T = set()
    for i in range(n):
        T.add(Transition([(i,0)], [(i,1)]))     # request entry

        # entering waiting room (if flag_j < 3 for all j)
        for other in list(itertools.product([0, 1, 2], repeat=n-1)):
            valid_flag_config = [(j,other[j if j < i else j-1]) for j in range(n) if j != i]
            T.add(Transition(
                [(i, 1)] + valid_flag_config,
                [(i, 3)] + valid_flag_config))

        # other requests? --> wait
        for j in range(n):
            if j == i:
                continue
            T.add(Transition([(i,3), (j,1)], [(i,2), (j,1)]))
        # otherwise: close door in go through door_out
        for other in list(itertools.product([0, 2, 3, 4, 5, 6], repeat=n-1)):
            valid_flag_config = [(j,other[j if j < i else j-1]) for j in range(n) if j != i]
            T.add(Transition(
                [(i, 3)] + valid_flag_config,
                [(i, 4)] + valid_flag_config))

        # someone closed door_in --> stop_waiting
        for j in range(n):
            if j == i:
                continue
            T.add(Transition([(i, 2), (j, 4)], [(i, 4), (j, 4)]))
            T.add(Transition([(i, 2), (j, 5)], [(i, 4), (j, 5)]))
            T.add(Transition([(i, 2), (j, 6)], [(i, 4), (j, 6)]))

        # lowest behind door_out --> enter critical
        for other in list(itertools.product([0, 1], repeat=i)):
            valid_flag_config = [(j,other[j]) for j in range(i)]
            t = Transition(
                [(i, 4)] + valid_flag_config,
                [(i, 5)] + valid_flag_config,
                tags={f'enter_{i}'})
            T.add(t)

        # exit critical
        T.add(Transition([(i, 5)], [(i, 6)], tags={f'leave_{i}'}))

        # epilog: wait until all others that are larger left waiting_room
        for other in list(itertools.product([0, 1, 4, 5, 6], repeat=n-i-1)):
            valid_flag_config = [(j,other[j-i-1]) for j in range(i+1,n)]
            T.add(Transition(
                [(i, 6)] + valid_flag_config,
                [(i, 0)] + valid_flag_config))

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Mutex: Szymanski")

    return protocol
