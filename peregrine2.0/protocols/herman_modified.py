from protocol import Protocol
from transition import Transition
from multiset import Multiset

def name(n):
    return "Herman's algorithm with tokens ({})".format(n)

def generate(n):
    assert(n >= 3 and n % 2 == 1)

    Q = { (i, t) for i in range(n) for t in range(2) }

    state_names = { (i, t): "x{}t{}".format(i, t) for i in range(n) for t in range(2) }
    
    # L = Multiset([(i, 1) for i in range(n)])
    pre = "(and (= (mod (+ {}) 2) 1) {})".format(
            ' '.join("x{}t1".format(i) for i in range(n)),
            ' '.join("(= (+ x{}t1 x{}t0) 1)".format(i,i) for i in range(n))
        )
    post = "(= (+ {}) 1)".format(' '.join("x{}t1".format(i) for i in range(n)))

    T = set()
    for j in range(n):
        i = (j-1)%n

        # pass token to i with token, eliminate two tokens
        T.add(Transition(((i, 1), (j, 1)), ((i, 0), (j, 0))))

        # pass token to i without token
        T.add(Transition(((i, 0), (j, 1)), ((i, 1), (j, 0))))

    return Protocol(Q, T, state_names=state_names, initial_states=Q, precondition=pre, postcondition=post, name=name(n), family_name="Herman w. tokens")
