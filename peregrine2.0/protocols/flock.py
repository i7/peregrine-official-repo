from protocol import Protocol
from transition import Transition

def name(n):
    return "Flock-of-birds ({})".format(n)

def generate(n):
    Q = set(range(n + 1))
    T = set()
    I = {"x0": 0, "x1": 1}
    O = {i: (0 if i < n else 1) for i in Q}
    p = "(>= x1 {})".format(n)

    for i in range(n + 1):
        for j in range(n + 1):
            if i + j < n:
                t = Transition((i, j), (0, i + j))
            else:
                t = Transition((i, j), (n, n))
            if not t.silent():  # only add if not silent
                T.add(t)

    return Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(n), family_name="Flock (default)")
