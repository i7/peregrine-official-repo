from protocol import Protocol
from transition import Transition

def name(a, c):
    return "Threshold ({}, {})".format(a, c)

def generate(a, c):
    v_max = max(max(abs(x) for x in a), abs(c) + 1)

    def f(x, y):
        return max(-v_max, min(v_max, x + y))

    def g(x, y):
        return (x + y) - f(x, y)

    def b(x, y):
        return (f(x, y) >= c)

    def var(i):
        return "x{}".format(i)

    def label(q):
        return "{}{}{}{}".format("L" if q[0] else "P",
                                 "T" if q[2] else "F",
                                 "n" if q[1] < 0 else "p",
                                 abs(q[1]))

    Q = {(l, v, o) for l in [False, True] for v in range(-v_max, v_max+1)
                   for o in [False, True]}
    state_names = {q: label(q) for q in Q}
    I = {var(i): (True, a[i], a[i] >= c) for i in range(len(a))}
    O = {q: 1 if q[2] else 0 for q in Q}
    T = set()

    for p in Q:
        for q in Q:
            if p[0]:
                n, n_ = p[1], q[1]
                pre   = (p, q)
                post  = ((True,  f(n, n_), b(n, n_)),
                         (False, g(n, n_), b(n, n_)))

                t = Transition(pre,post)
                if not t.silent():  # only add non-silent transitions
                    T.add(t)

    p = "(>= (+ {}) {})".format(' '.join("(* {} {})".format(b, var(i)) for i, b in enumerate(a)), c)

    return Protocol(Q, T, state_names=state_names, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(a,c), family_name="Threshold")
