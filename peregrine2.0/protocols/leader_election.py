from protocol import Protocol
from transition import Transition
from multiset import Multiset

def name():
    return "Leader Election"

def generate():
    Q = {"a", "p"}
    T = {Transition(("a", "a"), ("a", "p"))}
    state_names = {"a": "a", "p": "p"}
    pre = "(> a 0)"
    post = "(= a 1)"

    return Protocol(Q, T, state_names=state_names, initial_states=Q, precondition=pre, postcondition=post, name=name())
