from protocol import Protocol
from transition import Transition

# kanban setup with n>0 parallel components (and 1 at the beginning and 1 at the end)


# reference: G. Ciardo and M. Tilgner, “On the use of kronecker operators for the solution of
#               generalized stochastic petri nets”, 1996
def generate(n):
    assert n >= 1, "the number of parallel processes must be larger than 0"

    def add_subnet(x, Q, T):
        Q |= {f'kanban_{x}', f'm_{x}', f'c_{x}', f'back_{x}', f'out_{x}'}
        T.add(Transition([f'm_{x}'], [f'c_{x}']))
        T.add(Transition([f'c_{x}'], [f'back_{x}']))
        T.add(Transition([f'c_{x}'], [f'out_{x}']))
        T.add(Transition([f'back_{x}'], [f'm_{x}']))

    Q = set()
    T = set()

    add_subnet("start", Q, T)
    T.add(Transition([f'kanban_start'], [f'm_start']))
    add_subnet("end", Q, T)
    test_t = Transition([f'out_end'], [f'kanban_end'], tags={"produce"})
    T.add(test_t)
    for i in range(n):
        add_subnet(i, Q, T)

    T.add(Transition([f'out_start'] + [f'kanban_{i}' for i in range(n)],
                     [f'kanban_start']+ [f'm_{i}' for i in range(n)]))
    T.add(Transition([f'kanban_end'] + [f'out_{i}' for i in range(n)],
                     [f'm_end'] + [f'kanban_{i}' for i in range(n)]))

    initial_states = {f'kanban_{name}' for name in list(range(n)) + ["start", "end"]}
    state_names = {q: q for q in Q}
    pre = "(and {})".format(" ".join(f'(> kanban_{x} 0)' for x in list(range(n)) + ["start", "end"]))

    protocol = Protocol(Q, T, state_names=state_names, initial_states=initial_states, precondition=pre, name=f'Kanban{n}', family_name="Kanban")

    return protocol
