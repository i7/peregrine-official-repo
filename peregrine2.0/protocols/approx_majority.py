from protocol import Protocol
from transition import Transition

# a.k.a. "Cell cycle switch" from "Fair Termination for Parameterized Probabilistic Concurrent System"

def name():
    return "Approximate majority (cell cycle switch)"

def generate():
    Q = {"Y", "N", "b"}
    T = {Transition(("Y", "b"), ("Y", "Y")),
         Transition(("Y", "N"), ("Y", "b")),
         Transition(("N", "Y"), ("N", "b")),
         Transition(("N", "b"), ("N", "N"))}
    alphabet_mapping = {"Y": "Y", "N": "N"}
    O = {"Y": 1, "N": 0}
    p = "(> Y N)"

    return Protocol(Q, T, name=name(), predicate=p, alphabet_mapping=alphabet_mapping, output_mapping=O)
