from protocol import Protocol
from transition import Transition
from multiset import Multiset

# dining philosophers problem solved with random chance (Lehmann_Rabin)

def name(n):
    return f'Lehmann Rabin for {n}'

def generate(n):
    assert n >= 2, "the number of philosophers must be larger than 2"

    Q = set()
    T = set()
    L = Multiset()
    for i in range(n):
        Q |= {f'q{i}',      # initial state (no forks)
              f'qTL{i}',    # decided to take 'left' first
              f'qTR{i}',    # decided to take 'right' first
              f'qL{i}',     # acquired 'left'
              f'qR{i}',     # acquired 'right'
              f'qG{i}',     # acquired both
              f'F{i}',      # fork (left of philosopher i) free
              f'NF{i}'}     # fork (left of philosopher i) taken

        T.add(Transition([f'q{i}'], [f'qTL{i}']))   # random choice to take 'left' first
        T.add(Transition([f'q{i}'], [f'qTR{i}']))   # random choice to take 'right' first

        T.add(Transition([f'qTL{i}', f'F{i}'], [f'qL{i}', f'NF{i}']))   # wait for 'left' fork and take it
        T.add(Transition([f'qTR{i}', f'F{(i+1)%n}'], [f'qR{i}', f'NF{(i+1)%n}']))   # wait for 'right' folk and take it

        # reset because other fork (right) is taken
        T.add(Transition([f'qL{i}', f'NF{i}', f'NF{(i+1)%n}'], [f'q{i}', f'F{i}', f'NF{(i+1)%n}']))
        # reset because other fork (left) is taken
        T.add(Transition([f'qR{i}', f'NF{(i+1)%n}', f'NF{i}'], [f'q{i}', f'F{(i+1)%n}', f'NF{i}']))

        # take other fork (right)
        T.add(Transition([f'qL{i}', f'F{(i+1)%n}'], [f'qG{i}', f'NF{(i+1)%n}']))
        # take other fork (left)
        T.add(Transition([f'qR{i}', f'F{i}'], [f'qG{i}', f'NF{i}']))

        # eat + put down both forks
        t = Transition([f'qG{i}', f'NF{i}', f'NF{(i+1)%n}'], [f'q{i}', f'F{i}', f'F{(i+1)%n}'], tags={f'eat_{i}'})
        T.add(t)

        L[f'q{i}'] += 1     # add philosopher
        L[f'F{i}'] += 1     # add fork

    state_names = {q: q for q in Q}

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Lehmann Rabin")

    return protocol
