from protocol import Protocol
from transition import Transition
from multiset import Multiset

# simple mutex algorithm with n protocols (p_i: waiting, cs_i: critical section)
# and a mutex place m (with 1 leader token)

def name(n):
    return f'simple mutex for {n}'

def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = {"m"}
    T = set()
    L = Multiset(["m"])
    for i in range(n):
        Q |= {f'p{i}', f'cs{i}'}
        T |= {Transition([f'p{i}', "m"], [f'cs{i}'], tags={f'enter_{i}'}),
              Transition([f'cs{i}'], [f'p{i}', "m"], tags={f'leave_{i}'})}
        L[f'p{i}'] += 1
    state_names = {q: q for q in Q}

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Mutex: 'simple'")

    return protocol
