from protocol import Protocol
from transition import Transition
from multiset import Multiset

# Peterson's mutex algorithm for 2 processes
# as described in "distributed algorithms - an intuitive approach" by Wan Fokkink

def name():
    return f'Peterson\'s mutex'

def generate():

    Q = {"q0", "q0F", "q0FW", "q0FC", "q1", "q1F", "q1FW", "q1FC", "w0", "w1"}
    L = Multiset(["w0", "q0", "q1"])
    T = set()

    # setting flag
    T.add(Transition(["q0"], ["q0F"]))
    T.add(Transition(["q1"], ["q1F"]))
    # setting wait
    T.add(Transition(["q0F", "w0"], ["q0FW", "w0"]))
    T.add(Transition(["q0F", "w1"], ["q0FW", "w0"]))
    T.add(Transition(["q1F", "w0"], ["q1FW", "w1"]))
    T.add(Transition(["q1F", "w1"], ["q1FW", "w1"]))
    # enter critical section
    T.add(Transition(["q0FW", "w1"], ["q0FC", "w1"], tags={"enter_0"}))
    T.add(Transition(["q0FW", "q1"], ["q0FW", "q1"], tags={"enter_0"}))
    T.add(Transition(["q1FW", "w0"], ["q1FC", "w0"], tags={"enter_1"}))
    T.add(Transition(["q1FW", "q0"], ["q1FW", "q0"], tags={"enter_1"}))
    # leave critical section
    T.add(Transition(["q0FC"], ["q0"], tags={"leave_0"}))
    T.add(Transition(["q1FC"], ["q1"], tags={"leave_1"}))

    state_names = {q: q for q in Q}

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name())

    return protocol
