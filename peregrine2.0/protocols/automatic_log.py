import itertools
from protocol import *
from transition import Transition
from multiset import Multiset
import operator
from lark import Lark, Transformer, v_args
from z3 import *
from Log import log

# TODO broken... not adapted to new protocol representation

class SOPP:
    def __init__(self, id, states, input_states, oT, oF, transitions, transitions_in, transitions_out,
                 alphabet, input_mapping, neutralState, b,
                 predicate, leaders = None):
        self.id = id
        self.states = frozenset(states)
        self.input_states = frozenset(input_states)
        self.oT = oT
        self.oF = oF
        self.transitions = frozenset(transitions)
        self.transitions_in = frozenset(transitions_in)
        self.transitions_out = frozenset(transitions_out)
        self.alphabet = frozenset(alphabet)
        self.input_mapping = dict(input_mapping)
        self.neutralState = neutralState
        self.b = b
        self.predicate = predicate
        self.leaders = Multiset(leaders)

    def to_2_way(self):
        states = set (self.states)
        transitions = set()
        transitions_in = set()
        transitions_out = set()

        t_id = 0 # for naming the new states
        input_states = self.input_states
        output_states = [self.oT, self.oF]

        def add(t):
            if len([s for s in input_states if t.pre[s] > 0 or t.post[s] > 0]) > 0:
                transitions_in.add(t)
                return

            if len([s for s in output_states if t.pre[s] > 0 or t.post[s] > 0]) > 0:
                transitions_out.add(t)
                return

            transitions.add(t)

        def two_way(t, t_id):
            pre = list(t.pre.elements())
            post = list(t.post.elements())
            k = len(pre)

            if k == 1:
                return ({}, {Transition([pre[0], self.neutralState], [post[0], self.neutralState])})

            if k <= 2:
                return ({}, {t})

            d = [f'_d_{t_id}_{i}' for i in range(1, k - 1)]
            a = [f'_a_{t_id}_{i}' for i in range(1, k)] # a[0] not used
            b = [f'_b_{t_id}_{i}' for i in range(1, k)] # b[0] not used

            forth = [Transition([pre[0],pre[1]], [d[0], a[1]])] + [Transition([a[i], pre[i+1]], [d[i], a[i+1]])
                                                                   for i in range(1, k-2)]
            forth_rev = [Transition(t.post, t.pre) for t in forth]
            back = [Transition([d[0],b[1]], [post[0], post[1]])] + [Transition([d[i], b[i+1]], [b[i], post[i+1]])
                                                                    for i in range(1, k-2)]
            success = [Transition([a[k-2], pre[k-1]], [b[k-2], post[k-1]])]

            return (set(d + a[1:] + b[1:]), set(forth + forth_rev + back + success))

        for t in self.transitions.union(self.transitions_in).union(self.transitions_out):
            new_states, new_transitions = two_way(t,t_id)
            if new_states:
                t_id += 1
            states = states.union(new_states)
            for t_new in new_transitions:
                add(t_new)

        return SOPP(self.id, states, self.input_states, self.oT, self.oF, transitions, transitions_in
                    , transitions_out, self.alphabet, self.input_mapping, self.neutralState, self.b, self.predicate
                    , self.leaders)


    def to_PP(self):

        states = set((s,o) for s in self.states.union(self.input_states) for o in [True, False])
        states.add((self.oT, True))
        states.add((self.oF, False))

        states_l = list(states)
        def hashh(x):
            # return "x{}".format(states_l.index(x))        # to shorten names
            return x

        input_mapping ={x: hashh((s, self.b)) for x,s in self.input_mapping.items()}
        leaders = Multiset([hashh((s,self.b)) for s in self.leaders.elements()])
        if self.b:
            leaders[hashh((self.oT, True))] += 1
        else:
            leaders[hashh((self.oF, False))] += 1

        output_mapping = {hashh((s,b)): b for (s,b) in states}

        t_normal = set()
        for t in self.transitions.union(self.transitions_in):
            pre = list(t.pre.elements())
            post = list(t.post.elements())
            k = len(pre)
            for bs in list(itertools.product([True, False], repeat=k)):
                t_normal.add(
                    Transition(
                        [hashh((pre[i], bs[i])) for i in range(0,k)],
                        [hashh((post[i], bs[i])) for i in range(0,k)]
                    )
                )

        t_out = set()
        for t in self.transitions_out:
            pre = list(t.pre.elements())
            post = list(t.post.elements())
            k = len(pre)
            for bs in list(itertools.product([True, False], repeat=k)):
                pre_bs = [True if pre[i] == self.oT else False if pre[i] == self.oF else bs[i] for i in range(k)]
                post_bs = [True if post[i] == self.oT else False if post[i] == self.oF else pre_bs[i] for i in range(k)]
                t_out.add(
                    Transition(
                        [hashh((pre[i], pre_bs[i])) for i in range(k)],
                        [hashh((post[i], post_bs[i])) for i in range(k)]
                    )
                )


        t_true = set(Transition([hashh((self.oT, True)), hashh((s,False))], [hashh((self.oT, True)), hashh((s,True))])
                     for (s,b) in states if not b and not s == self.oF)

        t_false = set(Transition([hashh((self.oF, False)), hashh((s, True))], [hashh((self.oF, False)), hashh((s, False))])
                     for (s, b) in states if b and not s == self.oT)

        transitions = t_normal.union(t_out.union(t_true.union(t_false)))

        return Protocol(set(hashh(s) for s in states), transitions, self.alphabet, input_mapping, output_mapping, predicate=self.predicate, leaders=leaders)

    def combine(self, other, operand=0):
        if operand == 0:  # AND
            id_mask = "({}^{})"
        else:  # OR
            id_mask = "({}V{})"
        id = id_mask.format(self.id, other.id)
        input_states = self.input_states.union(other.input_states)
        states = set(self.states.union(other.states))
        states.add(self.oT)
        states.add(self.oF)
        states.add(other.oT)
        states.add(other.oF)
        neutralState = self.neutralState
        oT = "T{}".format(id)
        oF = "F{}".format(id)
        transitions = set(self.transitions.union(other.transitions).union(self.transitions_out)
                          .union(other.transitions_out))
        transitions_in = set()
        for s in input_states:
            pre = Multiset([s])
            post = Multiset([neutralState])
            added = False
            for t in self.transitions_in.union(other.transitions_in):
                if t.pre[s] > 0:
                    if not added:
                        added = True
                        pre = t.pre
                        post = t.post
                    else:
                        pre = pre + t.pre
                        pre[s] -= 1
                        pre[neutralState] += 1
                        post = post + t.post
            transitions_in.add(Transition(pre, post))

        transitions_out = set()
        if operand == 0:  # AND
            transitions_out.add(Transition([oF, self.oT, other.oT], [oT, self.oT, other.oT]))
            transitions_out.add(Transition([oT, self.oF], [oF, self.oF]))
            transitions_out.add(Transition([oT, other.oF], [oF, other.oF]))
        else:  # OR
            transitions_out.add(Transition([oT, self.oF, other.oF], [oF, self.oF, other.oF]))
            transitions_out.add(Transition([oF, self.oT], [oT, self.oT]))
            transitions_out.add(Transition([oF, other.oT], [oT, other.oT]))

        alphabet = self.alphabet.union(other.alphabet)
        input_mapping = self.input_mapping.copy()
        input_mapping.update(other.input_mapping)
        leaders = self.leaders + other.leaders + Multiset({self.oT if self.b else self.oF,
                                                           other.oT if other.b else other.oF})

        if operand == 0:  # AND
            predicate_mask = "(and {} {})"
        else:  # OR
            predicate_mask = "(or {} {})"
        predicate = predicate_mask.format(self.predicate, other.predicate)

        return SOPP(id, states, input_states, oT, oF, transitions, transitions_in, transitions_out, alphabet
                    , input_mapping, neutralState, True, predicate, leaders)

    def gen_and(self, other):
        return self.combine(other, 0)

    def gen_or(self, other):
        return self.combine(other, 1)

    def gen_neg(self):
        id = f'!{self.id}'
        states = set(self.states)
        states.add(self.oT)
        states.add(self.oF)
        oT = "T{}".format(id)
        oF = "F{}".format(id)
        transitions = set(self.transitions.union(self.transitions_out))
        transitions_out = set()
        transitions_out.add(Transition([oT, self.oT], [oF, self.oT]))
        transitions_out.add(Transition([oF, self.oF], [oT, self.oF]))
        leaders = self.leaders + Multiset({self.oT if self.b else self.oF})
        predicate = f'(not {self.predicate})'

        return SOPP(id, states, self.input_states, oT, oF, transitions, self.transitions_in, transitions_out,
                    self.alphabet, self.input_mapping, self.neutralState, True, predicate, leaders)


def generate_true(id):
    return SOPP(id, set([0]), set(), f'T{id}', f'F{id}', set(), set(),
                set(),
                set(), set(), 0, True, "(< 0 1)")


def generate_false(id):
    return SOPP(id, set([0]), set(), f'T{id}', f'F{id}', set(), set(),
                set(),
                set(), set(), 0, False, "(< 1 0)")


def generate_remainder(id, variables, d, c, m):
    d = max(d, 2)
    variables = {var: (val % d) for var, val in variables.items() if (val % d) != 0}
    c %= d

    def size(x):
        if x == 0:
            return 0
        else:
            return math.floor(math.log(abs(x), 2)) + 1

    def bits(z):
        return (i for i in range(0, n + 1) if (z & 2 ** i))

    n = size(d)
    if m > 0:
        n += math.ceil(math.log(2*m, 2))

    alphabet = {var for var, val in variables.items()}
    input_states = {var for var, val in variables.items()}
    input_mapping = {var: var for var, val in variables.items()}
    states = {2**i: f'{2**i}_{id}' for i in range(n+1)}
    states[0] = 0
    oT = f'T{id}'
    oF = f'F{id}'
    b = True

    def rep(z):
        if z > 0:
            return [states[2 ** i] for i in bits(z)]
        elif z < 0:
            return [states[-(2 ** i)] for i in bits(-z)]
        else:
            return [0]

    transitions = set()
    for i in range(n):
        transitions.add(Transition([states[2**i], states[2**i]]  , [states[2**(i+1)], 0]))          # up_i
        transitions.add(Transition([states[2**(i+1)], 0]         , [states[2**i], states[2**i]]))   # down_i

    transitions_in = set()
    for var, val in variables.items():
        post = rep(val)
        pre = [var] + ([0] * (len(post) - 1))
        transitions_in.add(Transition(pre, post))                                                   # add


    transitions_out = set()
    for oX in [oT, oF]:
        transitions_out.add(Transition([oX] + rep(d) , [oT] + ([0] * len(rep(d)))))                 # mod

    for i in range(n+1):
        transitions_out.add(Transition([oT, states[2**i]] , [oF, states[2**i]]))                    # notzero_i

    leaders_shift = Multiset()
    if c != 0:
        leaders_shift = Multiset(rep(d - c))
    leaders_reservoir = Multiset()
    leaders_reservoir[0] = 2*n + 1
    leaders = leaders_shift + leaders_reservoir

    predicate = "(= (mod (+ {}) {}) {})".format(' '.join("(* {} {})".format(var, val) for var, val
                                                         in variables.items()), d, c)

    return SOPP(id, {name for val, name in states.items()}, input_states, oT, oF, transitions, transitions_in, transitions_out,
                 alphabet, input_mapping, 0, b, predicate, leaders)


def generate_threshold(id, variables, c, m, op, op_str):
    variables = {var: val for var, val in variables.items() if val != 0}

    def size(x):
        if x == 0:
            return 0
        else:
            return math.floor(math.log(abs(x), 2)) + 1

    def bits(z):
        return (i for i in range(0, n + 1) if (z & 2 ** i))

    b_max = max(1, abs(c), max([abs(val) for var, val in variables.items()]))
    n = size(b_max)
    if m > 0:
        n += math.ceil(math.log(2*m, 2))

    alphabet = {var for var, val in variables.items()}
    input_states = {var for var, val in variables.items()}
    input_mapping = {var: var for var, val in variables.items()}
    states = {2**i: f'{2**i}_{id}' for i in range(n+1)}
    states.update({-(2**i): f'-{2**i}_{id}' for i in range(n+1)})
    states[0] = 0
    oT = f'T{id}'
    oF = f'F{id}'
    b = op(0, 0)

    def rep(z):
        if z > 0:
            return [states[2 ** i] for i in bits(z)]
        elif z < 0:
            return [states[-(2 ** i)] for i in bits(-z)]
        else:
            return [0]

    transitions = set()
    for i in range(n):
        transitions.add(Transition([states[2**i], states[2**i]], [states[2**(i+1)], 0]))            # up+_i
        transitions.add(Transition([states[2**(i+1)], 0], [states[2**i], states[2**i]]))            # down+_i
        transitions.add(Transition([states[-(2**i)], states[-(2**i)]], [states[-(2**(i+1))], 0]))   # up-_i
        transitions.add(Transition([states[-(2**(i+1))], 0], [states[-(2**i)], states[-(2**i)]]))   # down-_i

    transitions_in = set()
    for var, val in variables.items():
        post = rep(val)
        pre = [var] + ([0] * (len(post) - 1))
        transitions_in.add(Transition(pre, post))                                                   # add


    transitions_out = set()
    for i in range(n+1):
        if op(2**i, 0):
            transitions_out.add(Transition([oF, states[2 ** i]], [oT, states[2 ** i]]))
        else:
            transitions_out.add(Transition([oT, states[2 ** i]], [oF, states[2 ** i]]))

        if op(-(2**i), 0):
            transitions_out.add(Transition([oF, states[-(2**i)]], [oT, states[-(2**i)]]))
        else:
            transitions_out.add(Transition([oT, states[-(2**i)]], [oF, states[-(2**i)]]))

        for oX in [oT, oF]:
            transitions_out.add(Transition([oX, states[2 ** i], states[-(2 ** i)]], [oT if op(0,0) else oF, 0, 0]))    # cancel_i

    leaders_shift = Multiset()
    if c != 0:
        leaders_shift = Multiset(rep(-c))
    leaders_reservoir = Multiset()
    leaders_reservoir[0] = 3*n + 1
    leaders = leaders_shift + leaders_reservoir

    predicate = "({} (+ {}) {})".format(op_str, ' '.join("(* {} {})".format(var, val) for var, val in variables.items()), c)

    return SOPP(id, {name for val, name in states.items()}, input_states, oT, oF, transitions, transitions_in,
                transitions_out, alphabet, input_mapping, 0, b, predicate, leaders)


def presburgerToProtocol(formula, options):
    # help function
    def getCooefficient(expression):  # can be multiplication of variable with constant or just a single variable or an int value
        if expression.decl().name() == '*':
            if expression.children()[0].decl().name() != 'Int':
                raise ArgumentError("Muliplication is only allowed with a constant factor.")
            coeff = expression.children()[0].as_long()
            var = expression.children()[1].decl().name()
            return [(var, coeff)]
        if len(expression.children()) == 0:
            if expression.decl().name() == 'Int':   # int
                return expression.as_long()
            return [(expression.decl().name(), 1)]  # only variable
        raise ArgumentError("Expected (scaled) variable..." + str(formula))

    comparisions = {
        "<": operator.lt,
        "<=": operator.le,
        "=": operator.eq,
        ">": operator.gt,
        ">=": operator.ge
    }

    name = formula.decl().name()

    if name == 'true':
        options["id"] = options["id"] + 1
        return generate_true(options["id"])
    if name == 'false':
        options["id"] = options["id"] + 1
        return generate_false(options["id"])
    if name == 'not':
        return presburgerToProtocol(formula.children()[0], options).gen_neg()
    if name == "and" or name == "or":
        p1 = presburgerToProtocol(formula.children()[0], options)
        p2 = presburgerToProtocol(formula.children()[1], options)
        res = p1.gen_and(p2) if name == "and" else p1.gen_or(p2)
        next = 2
        while next < len(formula.children()):
            p_next = presburgerToProtocol(formula.children()[1], options)
            res = res.gen_and(p_next) if name == "and" else next.gen_or(p_next)
            next = next + 1
        return res
    if (name == '=' and formula.children()[0].decl().name() == 'mod'):  # mod
        c = formula.children()[1].as_long()
        d = formula.children()[0].children()[1].as_long()
        lhs = formula.children()[0].children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):
            for exp in lhs.children():
                cur_res = getCooefficient(exp)
                if not isinstance(cur_res, list):
                    c = (c - cur_res) % d
                else:
                    coeffs = coeffs + cur_res
        else:
            cur_res = getCooefficient(lhs)
            if not isinstance(cur_res, list):
                c = (c - cur_res) % d
            else:
                coeffs = cur_res
            coeffs = getCooefficient(lhs)
            
        options["id"] = options["id"] + 1
        return generate_remainder(options["id"], {x_i: a_i for (x_i, a_i) in coeffs}, d, c, options["m"])
    if (name in comparisions):
        c = formula.children()[1].as_long()
        lhs = formula.children()[0]

        # gather coefficients
        coeffs = []
        if (lhs.decl().name() == '+'):  # sum
            for exp in lhs.children():
                coeffs = coeffs + getCooefficient(exp)
        else:  # just 1 var
            coeffs = getCooefficient(lhs)

        options["id"] = options["id"] + 1
        return generate_threshold(options["id"], {x_i: a_i for (x_i, a_i) in coeffs}, c
                                  , options["m"], comparisions[name], name)


def parse_predicate(string):
    predicate_grammar = """
        ?start: pred

        ?pred: and_pred
             | pred "||" and_pred   -> op_or

        ?and_pred: bool_exp
             | and_pred "&&" bool_exp   -> op_and    

        ?bool_exp: comp
             | "!" bool_exp             -> op_not
             | "(" pred ")"      
             | ("True" | "true")        -> op_true
             | ("False" | "false")      -> op_false

        ?comp: exp "=" exp            -> eq
             | exp "!=" exp           -> neq
             | exp ">" exp            -> gt
             | exp "<" exp            -> lt
             | exp "<=" exp           -> le
             | exp ">=" exp           -> ge
             | exp "%" INT "=" exp    -> mod

        ?exp: product
             | exp "+" product   -> add
             | exp "-" product   -> sub

        ?product: atom
            | product "*" atom  -> mul

        ?atom: S_INT            -> number
             | "-" atom         -> neg
             | VAR              -> var
             | "(" exp ")"

        %import common.LCASE_LETTER -> VAR
        %import common.INT -> INT
        %import common.SIGNED_INT -> S_INT
        %import common.WS_INLINE

        %ignore WS_INLINE
    """

    @v_args(inline=True)  # Affects the signatures of the methods
    class GenerateZ3Predicate(Transformer):
        def op_and(self, l, r):
            return And(l, r)

        def op_or(self, l, r):
            return Or(l, r)

        def op_not(self, e):
            return Not(e)

        def op_true(self):
            return BoolVal(True)

        def op_false(self):
            return BoolVal(False)

        def eq(self, l, r):
            return l == r

        def neq(self, l, r):
            return l != r

        def gt(self, l, r):
            return l > r

        def lt(self, l, r):
            return l < r

        def ge(self, l, r):
            return l >= r

        def le(self, l, r):
            return l <= r

        def mod(self, l, m, r):
            return l % IntVal(str(m)) == r

        def add(self, l, r):
            return l + r

        def sub(self, l, r):
            return l - r

        def mul(self, l, r):
            return l * r

        def neg(self, e):
            return -e

        def var(self, e):
            return Int(str(e))

        def number(self, e):
            return IntVal(str(e))

    parser = Lark(predicate_grammar, parser='lalr', transformer=GenerateZ3Predicate())
    parse = parser.parse
    predicate = parse(string)

    # simplify formula (simplification only works with z3 4.8 and above)
    z3_version = get_version()
    do_symplify = True
    if do_symplify and (z3_version[0] > 5 or z3_version[0] == 4 and z3_version[1] >= 8):
        return simplify(predicate, arith_lhs=True)
    
    return predicate


def count_base_protocols(formula):
    comparisions = {
        "<": operator.lt,
        "<=": operator.le,
        "=": operator.eq,
        ">": operator.gt,
        ">=": operator.ge
    }

    name = formula.decl().name()

    if name == 'true':
        return 0
    if name == 'false':
        return 0
    if name == 'not':
        return count_base_protocols(formula.children()[0])
    if name == "and" or name == "or":
        return sum([count_base_protocols(child) for child in formula.children()])
    if (name == '=' and formula.children()[0].decl().name() == 'mod'):  # mod
        if (formula.children()[0].children()[1].decl().name() != 'Int'):
            raise ArgumentError("The mode of a modulo expression has to be constant.")
        if (formula.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a modulo expression has to be constant.")
        return 1
    if (name in comparisions):
        if (formula.children()[1].decl().name() != 'Int'):
            raise ArgumentError("The right hand side of a comparision has to be constant.")
        return 1


def name(predicate):
    return "automatic log for {}".format(predicate)


def generate(predicate):
    formula = parse_predicate(predicate)
    m = count_base_protocols(formula)
    if m == 1:
        m = 0 # only 1 base protocol --> only base construction
    options = {"m": m, "id": 0}
    p = presburgerToProtocol(formula, options)
    res = p.to_2_way().to_PP()
    res.name=name(predicate)
    return res
