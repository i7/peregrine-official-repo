from protocol import Protocol
from transition import Transition
from multiset import Multiset
import itertools

# Dijkstra's mutex algorithm as population protocol as described in "Regular Model Checking by MARCUS NILSSON"

# pointer modeled as agent in states 'pointer_0 ... pointer_{n-1}'

# flag modeled implicitly:
#   flag=0: line 1 (and line 7 but line is not modeled)
#   flag=1: line 2 and line 2x and line 3 and line 4
#   flag=2: line 5 and line 6

# line 2x: 'implicit' line where the process is waiting to take the pointer

# critical section: line 6


def name(n):
    return "Mutex: Dijkstra({})".format(n)


def generate(n):
    assert n >= 1, "the number of processes must be larger than 0"

    Q = set()
    T = set()
    for i in range(n):
        Q |= {f'p{i}_l{j}' for j in range(1, 7)}
        Q |= {f'p{i}_l2x'}
        Q.add(f'pointer_{i}')

        # setting flag to 1
        T.add(Transition([f'p{i}_l{1}'], [f'p{i}_l{2}']))

        # already pointed to
        T.add(Transition([f'p{i}_l{2}', f'pointer_{i}'], [f'p{i}_l{4}', f'pointer_{i}']))

        # wait for pointed to process to have flag_0 (i.e. line 1)
        for j in range(n):
            if i != j:
                T.add(Transition([f'p{i}_l{2}', f'pointer_{j}'], [f'p{i}_l2x', f'pointer_{j}']))

        for j in range(n):
            T.add(Transition([f'p{i}_l2x', f'pointer_{j}', f'p{j}_l{1}'],
                             [f'p{i}_l3', f'pointer_{j}', f'p{j}_l{1}']))

        # set pointer on self
        for j in range(n):
            T.add(Transition([f'p{i}_l{3}', f'pointer_{j}'],
                             [f'p{i}_l{4}', f'pointer_{i}']))

        # set flag to 2
        T.add(Transition([f'p{i}_l{4}'], [f'p{i}_l{5}']))

        # check: only one with flag_2 (i.e. line 5 and line 6)
        # no:
        for j in range(n):
            T.add(Transition([f'p{i}_l{5}', f'p{j}_l{5}'],
                             [f'p{i}_l{2}', f'p{j}_l{5}']))
            T.add(Transition([f'p{i}_l{5}', f'p{j}_l{6}'],
                             [f'p{i}_l{2}', f'p{j}_l{6}']))
        # yes:
        for other in list(itertools.product([1, 2, 3, 4, "2x"], repeat=n - 1)):
            valid_flag_config = [f'p{j}_l{other[j if j < i else j - 1]}' for j in range(n) if j != i]
            T.add(Transition(
                [f'p{i}_l{5}'] + valid_flag_config,
                [f'p{i}_l{6}'] + valid_flag_config, tags={f'enter_{i}'}))

        T.add(Transition([f'p{i}_l{6}'], [f'p{i}_l{1}'], tags={f'leave_{i}'}))

    state_names = {q: q for q in Q}
    L = Multiset([f'p{i}_l{1}' for i in range(n)])
    L[f'pointer_0'] += 1

    protocol = Protocol(Q, T, state_names=state_names, leaders=L, name=name(n), family_name="Mutex: Dijkstra")

    return protocol
