from protocol import Protocol
from transition import Transition

def name():
    return "Moran Process"

# https://en.wikipedia.org/wiki/Moran_process
# random process with two types of cells 'A' and 'b'. In each step 1 cell dies and 1 cell replicates.
# show: drift (i.e. eventually one type wins) (i.e. stable termination)
# (can be seen as approximate majority protocol: if majority is large, the majority will win with high probability)

def generate():
    Q = {"A", "B"}
    T = {Transition(("A", "B"), ("A", "A")),
         Transition(("B", "A"), ("B", "B"))}
    I = {"A": "A", "B": "B"}
    O = {"A": 1, "B": 0}    # ignored -> only postcondition
    p = "(> A B)"           # this can't be shown... the protocol is not well-specified

    return Protocol(Q, T, alphabet_mapping=I, predicate=p, output_mapping=O, name=name())
