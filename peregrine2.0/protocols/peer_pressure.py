from protocol import Protocol
from transition import Transition


def name(k=3):
    return f'Peer Pressure {k}'

# a.k.a. "Coin Game" from "Fair Termination for Parameterized Probabilistic Concurrent System"

# agents have a coin with gold and solver side.
# In each step one agent 'a' is chosen together with a group of k other agents.
# Agent 'a' changes its coin to the side that has the majority among the k agents.
# Show: eventually all agents have same side (i.e. stable termination)
# (can be seen as approximate majority protocol: if majority is large, the majority will win with high probability)


def generate(k=3):
    assert k > 1 and k % 2 == 1, "k must be at least 2 and odd..."
    Q = {"G", "S"}
    T = set()
    for g in range(0, k+1):
        others = ["G"] * g + ["S"] * (k - g)
        q1 = "S" if 2 * g > k else "G"
        q2 = "G" if 2 * g > k else "S"
        T.add(Transition([q1] + others, [q2] + others))
    I = {"G": "G", "S": "S"}
    O = {"G": 1, "S": 0}
    p = "(> G S)"               # this can't be shown... the protocol is not well-specified
    pre = f'(> (+ G S) {k})'

    return Protocol(Q, T, alphabet_mapping=I, predicate=p, output_mapping=O, precondition=pre, name=name(k), family_name="Peer Pressure")
