from protocol import Protocol
from transition import Transition

# part of FireWire protocol: tossing coin until n switches (i.e HT or TH) occurred


def name(n):
    return f'Fire Wire ({n})'


def generate(n):
    assert n > 0, "n must be positive"

    Q = set()
    T = set()
    for i in range(n):
        Q.add(f'q{i}')  # last was H(ead)
        Q.add(f'q{i}T')     # last was T(ail)

        T.add(Transition([f'q{i}'], [f'q{i}'], tags={"head"}))
        T.add(Transition([f'q{i}'], [f'q{i+1}T'], tags={"tail", "different"}))
        T.add(Transition([f'q{i}T'], [f'q{i}T'], tags={"tail"}))
        T.add(Transition([f'q{i}T'], [f'q{i+1}'], tags={"head", "different"}))

    Q.add(f'q{n}')
    Q.add(f'q{n}T')

    initial_states = {"q0", "q0T"}

    state_names = {q:q for q in Q}
    pre = "(= (+ q0 q0T) 1)"
    post = f'(= (+ q{n} q{n}T) 1)'

    return Protocol(Q, T, state_names=state_names, initial_states=initial_states, precondition=pre, postcondition=post, name=name(n), family_name="Fire Wire")
