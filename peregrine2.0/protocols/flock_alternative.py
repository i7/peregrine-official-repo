from protocol import Protocol
from transition import Transition

def name(n):
    return "Flock-of-birds 2 ({})".format(n)

# From https://hal.archives-ouvertes.fr/hal-00565090/document
# Example 2, p. 5
def generate(n):
    Q = set(range(n + 1))
    T = set()
    I = {"x0": 0, "x1": 1}
    O = {i: (0 if i < n else 1) for i in Q}
    p = "(>= x1 {})".format(n)

    for i in range(1, n):
        T.add(Transition((i, i), (i, i + 1)))

    for i in range(0, n):
        T.add(Transition((n, i), (n, n)))

    return Protocol(Q, T, alphabet_mapping=I, output_mapping=O, predicate=p, name=name(n), family_name="Flock (tower)")
