from protocol import Protocol
from transition import Transition

# ref: T. Herman, “Probabilistic self-stabilization”, Information Processing Letters, vol. 35, no. 2, pp. 63–67, 1990

def name(n):
    return "Herman's algorithm with bits ({})".format(n)

def generate(n):
    assert(n >= 3 and n % 2 == 1)

    Q = {(i, t) for i in range(n) for t in range(2)}

    state_names = {q: q for q in Q}

    pre = "(and {})".format(' '.join("(= (+ x{}t0 x{}t1) 1)".format(i, i) for i in range(n)))
    post = "(= (+ {}) 1)".format(' '.join("(ite (= x{}t1 x{}t1) 1 0)".format(i, (i-1)%n) for i in range(n)))

    T = set()
    for j in range(n):
        i = (j-1)%n
        # process j reads the bit of i
        T.add(Transition(((i, 1), (j, 1)), ((i, 1), (j, 0))))
        T.add(Transition(((i, 0), (j, 0)), ((i, 0), (j, 1))))

    return Protocol(Q, T, state_names=state_names, initial_states=Q, precondition=pre, postcondition=post, name=name(n), family_name="Herman w. bits")

