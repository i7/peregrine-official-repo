let
  nixpkgsPinned = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/a7ceb2536ab11973c59750c4c48994e3064a75fa.tar.gz";
    sha256 = "0hka65f31njqpq7i07l22z5rs7lkdfcl4pbqlmlsvnysb74ynyg1";
  }) {};
in
nixpkgsPinned.mkShell {
    buildInputs = with nixpkgsPinned; [
      gmp
      zlib
      git
      nodejs-12_x
      python37
      ant
      openjdk
      stack
      haskell.compiler.ghc822Binary
    ];
}

